[33mcommit a4f543937fce0be2fd14934d53a45f2e87588110[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Author: ehughes_ <ehughes@wavenumber.net>
Date:   Thu Apr 28 16:43:38 2022 -0400

    modbus test write update

[33mcommit ca9ebaaa28dc2c33e64ff4082fa2e169792c45c3[m
Author: DESKTOP-UEGQNSQ\ehugh <ehughes@wavenumber.net>
Date:   Tue Apr 26 21:01:08 2022 -0400

    reworking modbus messages to remove Jack header

[33mcommit 8b11f734522cbe0d11e866f3c7c14d8a129f138c[m
Author: ehughes_ <ehughes@wavenumber.net>
Date:   Fri Apr 22 16:55:52 2022 -0400

    updated static pm config for spiny modbus

[33mcommit c95714afc593df3856e5e14193bbfd213411d8b2[m
Author: ehughes_ <ehughes@wavenumber.net>
Date:   Fri Apr 22 15:46:11 2022 -0400

    modbus read/write commands from the cloud and shell function again

[33mcommit a6ea10f729ef73a6ef07bb19961e54447c5e9c2f[m[33m ([m[1;31morigin/structural_rework[m[33m, [m[1;32mstructural_rework[m[33m)[m
Author: ehughes_ <ehughes@wavenumber.net>
Date:   Fri Apr 22 14:10:12 2022 -0400

    Spiny Modbus and Toad are working under the new Krusty re-orq.   The 4 channel and single channel code will be broken but should be easily fixed

[33mcommit 245aa15180af81a8b2841dca78b036e89a91034b[m[33m ([m[1;31morigin/toad[m[33m, [m[1;32mtoad[m[33m)[m
Author: ehughes_ <ehughes@wavenumber.net>
Date:   Thu Apr 21 09:13:55 2022 -0400

    dev update

[33mcommit 5c9f84297c2d59a55636d7cbe9525ccfdfe1a77e[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Mar 23 16:35:25 2022 -0400

    added RSRP to the cell command.   Also adding RSSI and RSRP to the bridge connect message

[33mcommit 17ebbafb396774597569315cb91f9380e97b17c6[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon Mar 7 15:24:27 2022 -0500

     baselining V1.0

[33mcommit 095219ab808591b6fbee2916b8554d878e8caaed[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Mar 3 16:11:50 2022 -0500

    added additional calibration parameters

[33mcommit 2fe8a4dab89283dd142adac6fab1efec933f41f3[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Mar 1 16:18:27 2022 -0500

    added voltage channel calibration

[33mcommit 9a1dade4bded58fea9c5552c6fbbb6af1abd6bab[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Mar 1 16:18:18 2022 -0500

    added voltage channel calibration

[33mcommit f914a3847073b2c9a53dd410e3c5cb61a1baa47d[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Feb 25 15:38:18 2022 -0500

    Fixed RMS calculation in frequency domain.   Made energy adjustment for hanning window

[33mcommit 70099c26b8f4bc1f88c10a5b352bfcde1d54095c[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Feb 24 17:47:46 2022 -0500

    working shell over RS485.    cal command.   read command

[33mcommit f33e4cba398d9664c0714a19ebafb7a10cb34bfe[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Feb 18 17:46:39 2022 -0500

    bring up of toad...

[33mcommit c072796ca418d90ad83438de78a4d853f0b357d1[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Feb 18 10:50:58 2022 -0500

    last commit before switching to actual toad board

[33mcommit 412009ac2847228d309d397ea12574c471b0dd13[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Feb 10 15:29:31 2022 -0500

    all toad packets will us the report header 0x7OAD0000

[33mcommit 0c216aa8d3f038ee86624f10de43643c41d1447e[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Feb 9 17:17:40 2022 -0500

    updating PSD report format

[33mcommit 9d4e0752272e7466b5abdb0a4b6e1189c679c6a3[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon Feb 7 17:31:03 2022 -0500

    fft play

[33mcommit 89eb23abc1a6be75a4a6fb250369054daa099229[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Feb 4 17:31:36 2022 -0500

    dev update

[33mcommit 973b8f8505960485efb966f3902028dd9c085acd[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Feb 4 16:11:11 2022 -0500

    adding FFT code

[33mcommit 88633169fe6ffdfc14645e81b8d10a02e2fbb621[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Feb 3 17:07:02 2022 -0500

    working in ADS1119 code and a starter thread for ADC Data

[33mcommit ed50134256fc3da326871a0c26739118c3cfc68f[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Feb 2 16:59:30 2022 -0500

    working in ADS1119

[33mcommit 8ee3c180e83def5b68501005cbe688ebd5a60b84[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Feb 1 17:10:41 2022 -0500

    worked in Frequency synthesizer.  I2C reads only seem to work with real controller, notbit bang.   Got started on a toad board template

[33mcommit e99dcea8ffb9a0334d3f47d9a3780abf7acc2d22[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Jan 27 16:27:41 2022 -0500

    prototyping a PSD report

[33mcommit 3126edc7754024154c94f2314df0aca2c68c9db9[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Jan 19 16:35:23 2022 -0500

    setting up the toad branch

[33mcommit 0542a8660be845260781bbea399d794fd9799e6b[m
Author: DESKTOP-UEGQNSQ\ehugh <ehughes@wavenumber.net>
Date:   Mon Dec 27 16:34:04 2021 -0500

    fixed cmake for modbus.   Also, it looks like you must you python 3.9 for build

[33mcommit 3ffbb1a964d6e227d6f7c13552b490bc89e1ec20[m[33m ([m[1;31morigin/bb_fota[m[33m)[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon Dec 27 15:51:15 2021 -0500

    proof of concept w/ digi modem offloading.  Need to load in cert with xctu and upgrade to latest firmware cell
    L0.0.00.00.05.12,A.02.19 and 11418

[33mcommit 22990a7fa6e115d68a14ae1bc6c90c4d79a0a658[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Dec 17 17:13:03 2021 -0500

    proof of concept w/ digi modem offloading.  Need to load in cert with xctu and upgrade to latest firmware cell
    L0.0.00.00.05.12,A.02.19 and 11418

[33mcommit 50bcbcfdfa6a1726b0c3e2b946480054f867f5ea[m
Author: DESKTOP-UEGQNSQ\ehugh <ehughes@wavenumber.net>
Date:   Sat Dec 11 20:08:09 2021 -0500

    added a simple filter to the thingy temperature readings

[33mcommit 85638846d88c0419285a04d92646f2757a6478cb[m
Author: DESKTOP-UEGQNSQ\ehugh <ehughes@wavenumber.net>
Date:   Sat Dec 11 13:04:54 2021 -0500

    updated to ncs 1.7.1.  fixed unsued variable warning

[33mcommit 73371429d7a06403883a090c48de9b78f3a7a0fc[m
Author: DESKTOP-UEGQNSQ\ehugh <ehughes@wavenumber.net>
Date:   Sat Dec 11 12:35:03 2021 -0500

    Proof of concept bootloader is working on brewbot.  The serial console needed to be turned off in MCUboot or else it would crash on reset.   I think it has something to do with our non standard serial setup.   Right now, you have to look at segger RTT to see MCUboot messages. Regular HTTP is working.   There still needs to be work on getting TLS working.   Also, it seems that downloads will fail sometimes.   It seems that sometimes the XBEE has a CRC error.  Not sure if this is on a status update packet or the HTTP in.      In these cases it seems like the process would ultimately fail at the end when this happens.   The XBEE would send a socket status update that the remote peep closed the connection.  I woudl return this in the offload close call which which cause the higher level stack to get in a weird unsynced state.     For now,  I will always return success to a close.     We need to do a bunch of update cycles to to really figure this out but you can get a good upgrade with a couple attempts.

[33mcommit 7c7199550137fccb84db36cd2c5001b9b47ac5f4[m
Author: DESKTOP-UEGQNSQ\ehugh <ehughes@wavenumber.net>
Date:   Fri Dec 10 18:33:52 2021 -0500

    Working on BBB bootloader.   regualr HTTP downlaods work but the issue of the MCUboot crashing on reset persists.    Prototype thingy project added to report temperature.    For somereason connections to the update site don't work.   If feels like the certificate store is not working correctly.  Need to recheck spiny.        Reorged cmake files to better share code

[33mcommit a9db0c20c6812d4be757bc18f02efdaf8307aa2a[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Dec 1 10:57:12 2021 -0500

    we now have a secondary key and a simple mechanism to switch via a new config setting ActiveKey

[33mcommit 07158a9d53ce9c30fd52a0e7eb29acb42276b83b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Nov 11 14:45:52 2021 -0500

    cleaning up LED code a bit

[33mcommit b098efa3a453d06b80168ddf6d07bb0c313f2cf0[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Nov 10 12:20:36 2021 -0500

    updated spiny version

[33mcommit 17589ce1009106900cdba4c83d2f9adaf026219d[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Nov 10 11:43:55 2021 -0500

    test12.   reworked implementation of a thread watchdog.  running tests on nrf9160

[33mcommit 44656b90869e424eff803b2e6c7367058229a78b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Nov 10 10:18:50 2021 -0500

    trying a different thread wdt approach

[33mcommit 01ec339a81e0ca321968436621a22f8b90871772[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Nov 9 16:18:29 2021 -0500

    rework wdt to use our own version

[33mcommit f32dea9283bed766559306b4636b10421c4a789f[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon Oct 25 16:17:56 2021 -0400

    fixed regression in SNS code where the GPIO for power enables was not correctly enabled

[33mcommit e834c5274510b056c0f8a145493fc5f542a1d07e[m[33m ([m[1;31morigin/xbee_test[m[33m)[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Oct 22 17:28:27 2021 -0400

    spiny and brewbot now share moste code

[33mcommit ac60dfcab49d63072729283d1085db7c5fcce162[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Oct 21 16:06:59 2021 -0400

    BrewBot now using LED driver subsystem

[33mcommit add45f55c768ee812c45e7c46920f172dd275ff8[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Oct 20 15:13:41 2021 -0400

    added LED condition for retransmit indication

[33mcommit 5ba5f2179f64dce7bf7c468a51a99da9b3453596[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Oct 15 17:18:04 2021 -0400

    running weekend test.  THere is a bug if optimizations are turned on.   After the 1st message is send to the ingest server, the thread seemed to block around recieve from

[33mcommit 034f57fb1e081ed115ec82f97bb020bb67208008[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Oct 15 10:49:31 2021 -0400

    fixed issue with reading SPI-UART data

[33mcommit adad1d8476d6b8f5dbff3c46f138fc5e61318beb[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Oct 14 17:16:05 2021 -0400

    all 4 channels working w/ LEDs

[33mcommit f016b8f0153546d76c8324baa4a0f88912d9272b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Oct 12 15:22:33 2021 -0400

    working on 4ch

[33mcommit 68663f244a6d4d6eb0ae0365745ba2e766a3deac[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Oct 12 15:22:25 2021 -0400

    working on 4ch

[33mcommit fc55344090556f167b1072ac80aa1ed98c01b33b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Oct 7 16:10:28 2021 -0400

    hardcode a bridge key but it appears that data is flowing in

[33mcommit 09e5adeee00fde7c072a60eda6aa1f185de47d21[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Oct 7 14:43:51 2021 -0400

    stashing working xbee config before working in other code

[33mcommit 06473444d63d835af40608084b25f353fa6800d0[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Oct 5 16:49:14 2021 -0400

    reworked XBEE  state machine to use threads and reimplemented behavior without compelx state machine

[33mcommit 5f46fe20a1240e42b18568d11d19beb546ce61d6[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Sep 30 17:00:13 2021 -0400

    fixed issue with running cell process from main loop

[33mcommit b7b76d3ec29da201592fef7308c624027d38fb12[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Sep 30 16:42:46 2021 -0400

    over night test

[33mcommit 13889203aa134a505bda8963e5e33334abdc0825[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Sep 30 16:24:07 2021 -0400

    its ugly, but there is enough socket offload for sntp_simple to work

[33mcommit 7d0a7876e9495c26e61cfea28c75759acdf4545b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Sep 29 16:52:14 2021 -0400

    crunching through XBEE offload driver.  Now working on send/recevie

[33mcommit 7f54540ada0cebaa38a474e4d5ac1f10f1c2bafc[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon Sep 27 17:07:29 2021 -0400

    have a skeleton DNS offload working with the XBEE

[33mcommit 940f2252036c509c0a0609ab3c1111715342356f[m[33m ([m[1;31morigin/nrf9160_baseline[m[33m)[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Sep 24 16:30:22 2021 -0400

    now using production salt

[33mcommit a5a90c040535bad0902548b14746fec9ff377065[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Sun Sep 19 15:11:33 2021 -0400

    experimenting w/ the BrewBot Bridge with a net socket offload driver.    The old cellular driver is building and reporting status

[33mcommit d9a1b1dd2282f2237e2b125d067ea729f2271bed[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Aug 11 11:36:33 2021 -0400

    dev update

[33mcommit 4f4d056e1f289cdf99d8ce3f3f9af50c0dd99ac9[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Aug 10 14:59:04 2021 -0400

    got the spi_uart prototyped

[33mcommit a8301171309b43871f79ea03f7f72cd54bac9edc[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Jun 25 08:54:56 2021 -0400

    brewbot updatE

[33mcommit e97af9e67bd575f39562d2615b05f817b17e6c37[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Jun 18 17:19:03 2021 -0400

    got the 1ch config prototype.  Leaving for a weekend test

[33mcommit 363da728d2562a19560db4a6519191046713af90[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Jun 17 17:20:55 2021 -0400

    working out new jack layer

[33mcommit fe553ef2778007d8754376c505ba05adcda1c232[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Jun 17 15:31:49 2021 -0400

    adding new files

[33mcommit febd688ba322f390f672be9ddf0b01035a9c6b07[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Jun 17 12:55:30 2021 -0400

    got the 4 channel logic working again.  CONFIG_LOG_IMMEDIATE was an issue as it block interrupts and context switches for the length of the log message.  This was causing lost bytes on the serial lines

[33mcommit 4f17088a59d35557b8645f1fc08f2c3707ac4bff[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Jun 9 15:05:31 2021 -0400

    adding test message to beep

[33mcommit e106b016bc66a6f3af58ddc2b330569560c5fd11[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed Jun 9 08:28:54 2021 -0400

    getting to work with new spiny tool

[33mcommit fa2ea7906dbe3e1623a4fc819e9e81921da864d4[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon Jun 7 17:10:38 2021 -0400

    fixed bug where bridge wouldn't report

[33mcommit 4952cc70fca66e0222fe6802410ae71f6a47a29c[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon Jun 7 17:07:05 2021 -0400

    connect is now reporting cell parameters.    updating blinks the lights white

[33mcommit 10ca0300bdaf82baa3af4d12a36e2d58fbb7ce7c[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Jun 4 17:13:08 2021 -0400

    reboot command

[33mcommit a5bec52ed21969b3f71579d56d7f86ac4a2009ed[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri Jun 4 13:48:37 2021 -0400

    worked in remote udpate trigger w/ status reports

[33mcommit f5029cb76c81fe3b2c7d124361c8baa187c91ad2[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu Jun 3 16:53:44 2021 -0400

    dev update

[33mcommit e9b009d1351979fac0d32108970cc7682bdb506b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue Jun 1 17:03:16 2021 -0400

    dev update

[33mcommit ec7bcd9df92ccecc9b70aedd53a320837873d7f5[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri May 28 17:09:36 2021 -0400

    working on cell command

[33mcommit ef590ca27f338893ad6720b2c05af748884612e4[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri May 28 09:54:17 2021 -0400

    using the bleeding edge nrf connect sdk to get the latest zephyr version with task watchdogs

[33mcommit 6df644b3d99c1f59f431d903dedf27f21a62cab3[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu May 27 12:36:01 2021 -0400

    using nrf master

[33mcommit 97f66597c42e975c7d21a8589467001b5a79b203[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon May 24 16:49:37 2021 -0400

    modbus now uses zephyr drivers for timer, uart and tx enables

[33mcommit f7f1c41ff080b5156a3bf0f45b20a749ab87d4f3[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon May 24 14:06:16 2021 -0400

    working on using device tree

[33mcommit 69952b3fb63e4a7926e72b9750940ef230c383bc[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri May 21 16:41:39 2021 -0400

    retry counter implemented.   doing a weekend test

[33mcommit 7b26bdb8836e47c3505d89ed02b85975a9fc6ac5[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed May 19 15:36:14 2021 -0400

    starting to parse commands

[33mcommit d719b1315564a259f4e99c792110b5b64a66fee8[m[33m ([m[1;31morigin/config[m[33m)[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Wed May 19 08:27:27 2021 -0400

    new rt_config is plumbed in.   Added additional retries/reconnects

[33mcommit 10bd2c7fa8dae9aa13cc62445d1681dcc58fd53b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon May 17 17:07:53 2021 -0400

    wroking on config system.  Need to test

[33mcommit 19fcdd62ac0d9bd368d94940b1d4e7a1cba5ff00[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Mon May 17 10:34:41 2021 -0400

    validating encrypted response

[33mcommit 27faf00a706d6ab5e6d00bc12413bcba3cd687fd[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri May 14 21:30:02 2021 -0400

    forgot to add crypto files

[33mcommit ae4ae669c8675ed866cc3d102df832b9eb1a2f48[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri May 14 15:57:45 2021 -0400

    working on new config system

[33mcommit 25b4c65d5b215536ef539c3d7b5ad5fbc2301f54[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Fri May 14 14:31:58 2021 -0400

    starting to test new encryption on UDP

[33mcommit 5ebcb7541519326f8732391ddb734148e95d9ee1[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu May 13 17:05:09 2021 -0400

    now using the work_queue for cloud, ui and bridge processes

[33mcommit a2bd9fc808630425bf05812decd8fbbe435879ee[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu May 13 13:16:30 2021 -0400

    updated readme

[33mcommit 0d0511ca3c41cf69fd07330771c79bc708aa5805[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu May 13 13:09:09 2021 -0400

    updated readme

[33mcommit 975dac560cd0ebf1ac2d4a1300f95dc421ef2ca3[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu May 13 13:08:04 2021 -0400

    removing spiny from manifest and moving code into machine_mailbox repo

[33mcommit 67e98567efce086905a8b6c2b9e6aca265296a30[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Thu May 13 12:46:55 2021 -0400

    dev updatE

[33mcommit fe595489ae941a745e88004664e035584cdd16e8[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue May 11 15:25:27 2021 -0400

    dev update

[33mcommit ad450b5772b7b9b7417cc3511bce5968865a8868[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue May 11 17:45:50 2021 +0000

    west.yml edited online with Bitbucket

[33mcommit 9a44ff188a3e10c47d97d1a8bac902b4b93995e9[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue May 11 17:42:23 2021 +0000

    readme.md created online with Bitbucket

[33mcommit 383b12d5dafb4683a82885aa35e82716520928af[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue May 11 17:40:59 2021 +0000

    west.yml created online with Bitbucket

[33mcommit 9866dc6c4b9b15ef975e15711ac02776a4c0ff8b[m
Author: Eli Hughes <eli.hughes@tzerolabs.com>
Date:   Tue May 11 17:34:28 2021 +0000

    Initial commit
