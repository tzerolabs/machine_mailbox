

target_include_directories(app PRIVATE
"../../LIB/XBEE"
"../../LIB/BRIDGE/CELL/XBEE"
)

target_sources(app PRIVATE ../../LIB/BRIDGE/CELL/XBEE/XBEE_CELL__Portable.c)
target_sources(app PRIVATE ../../LIB/BRIDGE/CELL/XBEE/XBEE_CELL_API_Mode.c)
target_sources(app PRIVATE ../../LIB/BRIDGE/CELL/XBEE/XBEE_CELL_CommandMode.c)
target_sources(app PRIVATE ../../LIB/BRIDGE/CELL/XBEE/XBEE_CELL_Common.c)
target_sources(app PRIVATE ../../LIB/BRIDGE/CELL/XBEE/XBEE_CELL.c)
target_sources(app PRIVATE ../../LIB/BRIDGE/CELL/XBEE/xbee_sockets_offload.c)
target_sources(app PRIVATE ../../LIB/BRIDGE/CELL/XBEE/XBEE_PacketBuffer.c)
