

#include <zephyr.h>
#include <sys/printk.h>
#include <shell/shell.h>
#include <logging/log.h>
#include <drivers/gpio.h>
#include <drivers/uart.h>
#include <drivers/spi.h>
#include <sys/ring_buffer.h>
#include <init.h>

#define SC16IS741A__READ_REG    0x80

#define SC16IS741A__CH0         (1<<1)
#define SC16IS741A__CH1         (1<<2)

#define SC16IS741A__RHR         0x00
#define SC16IS741A__THR         0x00
#define SC16IS741A__IER         0x01
#define SC16IS741A__FCR         0x02
#define SC16IS741A__IIR         0x02
#define SC16IS741A__LCR         0x03
#define SC16IS741A__MCR         0x04
#define SC16IS741A__LSR         0x05
#define SC16IS741A__MSR         0x06
#define SC16IS741A__SPR         0x07
#define SC16IS741A__TCR         0x06 // //only when MCR[2] == 1 and EFR[4] == 1
#define SC16IS741A__TLR         0x07
#define SC16IS741A__TXLVL       0x08
#define SC16IS741A__RXLVL       0x09
#define SC16IS741A__RSV         0x0D 
#define SC16IS741A__UART_RST    0x0E
#define SC16IS741A__EFCR        0x0F

#define SC16IS741A__SPECIAL_DLL     0x00
#define SC16IS741A__SPECIAL_DLH     0x01
#define SC16IS741A__SPECIAL_XON1    0x02
#define SC16IS741A__SPECIAL_XON2    0x04
#define SC16IS741A__SPECIAL_XOFF1   0x05
#define SC16IS741A__SPECIAL_XOFF2   0x06

#define SC16IS741A__LCR_BIT__DIVISOR_LATCH_ENABLE   (1<<7)

#define SC16IS741A__LCR_BITS__WORD_LENGTH_8BIT       (0x3)

#define SC16IS741A__FCR_BITS__FIFO_ENABLE            (1<<0)

void SC16IS741A_Init();

void SC16IS741A_WriteRegister(uint8_t Register, uint8_t Value);

void SC16IS741A_WriteTxFIFO(uint8_t *Value, uint32_t Length);

void SC16IS741A_ReadRxFIFO(uint8_t *Value, uint32_t Length);

uint8_t SC16IS741A_ReadRegister(uint8_t Register);

void spi_uart_shuffle();


LOG_MODULE_REGISTER(nxp_SC16IS741);

struct device * SC16IS741_SPI ;
struct spi_config	 SC16IS741_SPI_CFG;
struct spi_cs_control SC16IS741_CS_CTRL;


#define MACHINE_MAILBOX_NODE DT_NODELABEL(machine_mailbox)

#define MM_SHELL_SPI_LABEL    DT_PHANDLE(MACHINE_MAILBOX_NODE, mm_shell_spi)

#define MM_SHELL_SPI_CS_LABEL	DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, mm_shell_spi_gpios)
#define MM_SHELL_SPI_CS_PIN     DT_GPIO_PIN(MACHINE_MAILBOX_NODE, mm_shell_spi_gpios)
#define MM_SHELL_SPI_CS_FLAGS	DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, mm_shell_spi_gpios)


void SC16IS741A_Init()
{
	
  	SC16IS741_SPI = (struct device *)device_get_binding(DT_LABEL(MM_SHELL_SPI_LABEL));

	SC16IS741_SPI_CFG.operation = SPI_WORD_SET(8) | SPI_TRANSFER_MSB;
	SC16IS741_SPI_CFG.frequency = 4000000;
	SC16IS741_SPI_CFG.slave = 0;

	SC16IS741_CS_CTRL.gpio_dev = (struct device *)device_get_binding(MM_SHELL_SPI_CS_LABEL);
	SC16IS741_CS_CTRL.gpio_pin = MM_SHELL_SPI_CS_PIN;
	SC16IS741_CS_CTRL.gpio_dt_flags = GPIO_ACTIVE_LOW;
	SC16IS741_CS_CTRL.delay = 0U;

	SC16IS741_SPI_CFG.cs = &SC16IS741_CS_CTRL;

    //Set the DLB to be able to hit the baud registers
    SC16IS741A_WriteRegister(SC16IS741A__LCR, SC16IS741A__LCR_BIT__DIVISOR_LATCH_ENABLE);

    SC16IS741A_WriteRegister(SC16IS741A__SPECIAL_DLL, 2);
    SC16IS741A_WriteRegister(SC16IS741A__SPECIAL_DLH, 0);

    //Turn off the DLB and configure for no partiy 1 stop bit and 8 bit word
    SC16IS741A_WriteRegister(SC16IS741A__LCR, SC16IS741A__LCR_BITS__WORD_LENGTH_8BIT);

    SC16IS741A_WriteRegister(SC16IS741A__FCR, SC16IS741A__FCR_BITS__FIFO_ENABLE);
}


void SC16IS741A_WriteRegister(uint8_t Register, uint8_t Value)
{
	uint8_t BuffOut[2];


	BuffOut[0] = Register<<3;
	BuffOut[1] = Value;

	const struct spi_buf buf_tx = {
		.buf = BuffOut,
		.len = 2
	};
	const struct spi_buf_set tx = {
		.buffers = &buf_tx,
		.count = 1
	};


	if (spi_write(SC16IS741_SPI, &SC16IS741_SPI_CFG, &tx)) {

		LOG_ERR("spi_transceive fail");

	}
	
}

void SC16IS741A_WriteTxFIFO(uint8_t *Value, uint32_t Length)
{
	
	uint8_t BuffOut[Length+1];

	BuffOut[0] = 0;

	memcpy(&BuffOut[1],Value,Length);

	const struct spi_buf buf_tx = {
		.buf = BuffOut,
		.len = Length+1
	};
	const struct spi_buf_set tx = {
		.buffers = &buf_tx,
		.count = 1
	};

	if (spi_write(SC16IS741_SPI, &SC16IS741_SPI_CFG, &tx)) {
		LOG_ERR("spi_transceive fail");
	}

}


void SC16IS741A_ReadRxFIFO(uint8_t *Value, uint32_t Length)
{
	
	uint8_t BuffOut[Length+1];
	uint8_t BuffIn[Length+1];

	BuffOut[0] = SC16IS741A__READ_REG;

	memset(&BuffOut[1],0,Length);

	const struct spi_buf buf_tx = {
		.buf = BuffOut,
		.len = Length+1
	};
	const struct spi_buf_set tx = {
		.buffers = &buf_tx,
		.count = 1
	};
	const struct spi_buf buf_rx = {
		.buf = BuffIn,
		.len =Length+1
	};
	const struct spi_buf_set rx = {
		.buffers = &buf_rx,
		.count = 1
	};

	if (spi_transceive(SC16IS741_SPI, &SC16IS741_SPI_CFG, &tx, &rx)) {
		LOG_ERR("spi_transceive fail");
	}

	memcpy(Value,&BuffIn[1],Length);
	

}


uint8_t SC16IS741A_ReadRegister(uint8_t Register)
{
	
	uint8_t BuffOut[2];
	uint8_t BuffIn[2];

	BuffOut[0] = SC16IS741A__READ_REG | Register<<3;
	BuffOut[1] = 0;


	const struct spi_buf buf_tx = {
		.buf = BuffOut,
		.len = 2
	};
	const struct spi_buf_set tx = {
		.buffers = &buf_tx,
		.count = 1
	};
	const struct spi_buf buf_rx = {
		.buf = BuffIn,
		.len = 2
	};
	const struct spi_buf_set rx = {
		.buffers = &buf_rx,
		.count = 1
	};

	if (spi_transceive(SC16IS741_SPI, &SC16IS741_SPI_CFG, &tx, &rx)) {
		LOG_ERR("spi_transceive fail");
		return 0;
	}

    return BuffIn[1];
	
}


uint8_t TxOutBuffer[128];

extern struct ring_buf uart_mem_0__pipe_out;
extern struct ring_buf uart_mem_0__pipe_in;
uint8_t RxInBuffer[128];

void spi_uart_shuffle()
{
	uint32_t bytes_to_send;
	uint32_t bytes_read=0;
	uint32_t bytes_available=0;

	if((bytes_to_send = SC16IS741A_ReadRegister(SC16IS741A__TXLVL) ))
    {
	      bytes_read = ring_buf_get(&uart_mem_0__pipe_out,TxOutBuffer,bytes_to_send);
          if(bytes_read)
          {
             SC16IS741A_WriteTxFIFO(TxOutBuffer,bytes_read);
          }
    }
   
    if((bytes_available = SC16IS741A_ReadRegister(SC16IS741A__RXLVL)))
	  {
         SC16IS741A_ReadRxFIFO(RxInBuffer,bytes_available);
         ring_buf_put(&uart_mem_0__pipe_in,RxInBuffer,bytes_available);
       }
}


void spi_uart_work_handler(void *a,void *b,void *c);

K_THREAD_STACK_DEFINE(my_spi_uart_thread_stack_area, 8192);

struct k_thread my_spi_uart_thread_data;

int spi_uart_init(const struct device * d)
{
	SC16IS741A_Init();


    k_thread_create(&my_spi_uart_thread_data, my_spi_uart_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(my_spi_uart_thread_stack_area),
                    spi_uart_work_handler,
                    NULL, NULL, NULL,
                    K_HIGHEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);
 
	return 0;
}

void spi_uart_work_handler(void *a,void *b,void *c)
{
	while(1)
	{
		k_sleep(K_MSEC(10));
    	spi_uart_shuffle();
	}

}

SYS_INIT(spi_uart_init, APPLICATION , 99);
