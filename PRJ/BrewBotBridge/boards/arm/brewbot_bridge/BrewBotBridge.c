

#include <zephyr.h>
#include <sys/printk.h>
#include <drivers/gpio.h>
#include <init.h>

static int bbb_init(const struct device *dev)
{

    const struct device *ExpanderSPI_ResetGPIO;
    const struct device *LED_EN_GPIO;

   ExpanderSPI_ResetGPIO  = device_get_binding("GPIO_0");
   gpio_pin_configure(ExpanderSPI_ResetGPIO, 28, GPIO_OUTPUT_ACTIVE);
   gpio_pin_set(ExpanderSPI_ResetGPIO, 28, 1);

   LED_EN_GPIO  = device_get_binding("GPIO_0");
   gpio_pin_configure(LED_EN_GPIO, 12, GPIO_OUTPUT_ACTIVE);
   gpio_pin_set(LED_EN_GPIO, 12, 1);

    return 0;
}

SYS_INIT(bbb_init, POST_KERNEL , 99);