# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 22:12:26 2019

@author: Eli Hughes
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

#%matplotlib qt
SAMPLE_RATE = 8192
VIB_CAPTURE_BUFFER_SIZE  =  16384
VIB_ANALYSIS_WINDOW_SIZE =  2048
VIB_OVERLAP_STEP        =   int(512)
NUM_AVG_STEPS          =    int(((VIB_CAPTURE_BUFFER_SIZE-VIB_ANALYSIS_WINDOW_SIZE) / VIB_OVERLAP_STEP))
RESULT_SPECTRUM_SIZE = int(VIB_ANALYSIS_WINDOW_SIZE/2)

CaptureBuffer = np.zeros(VIB_CAPTURE_BUFFER_SIZE)

π = np.pi;


Hanning =  1
Quantize = 1


if Hanning ==1:
    Window = np.hanning(VIB_ANALYSIS_WINDOW_SIZE);
    WindowAdjust = 16;
else:
    Window = np.ones(VIB_ANALYSIS_WINDOW_SIZE);
    WindowAdjust = 4;


SPECGRAM_LEN = 96

Specgram = np.zeros((RESULT_SPECTRUM_SIZE,SPECGRAM_LEN))

SimG_Level = 1;

SimG_NoiseLevel = .00001;

FreqAxis = np.zeros(RESULT_SPECTRUM_SIZE);

for q in range(0,RESULT_SPECTRUM_SIZE):
    FreqAxis[q] = q*SAMPLE_RATE/VIB_ANALYSIS_WINDOW_SIZE;

SimSignal = [0,23169,32767,23169,0,-23170,-32768,-23170];
    
print("Running " + str(SPECGRAM_LEN) + " PSDs");
print('Overlap Percentage : ' + str(round((1-VIB_OVERLAP_STEP/VIB_ANALYSIS_WINDOW_SIZE),2)) )
    
for q in range(0,SPECGRAM_LEN):
        
    # Generate the capture buffer 
    
    
    for i in range (0,VIB_CAPTURE_BUFFER_SIZE):
        if Quantize == 1:
            CaptureBuffer[i] = np.random.normal(-SimG_NoiseLevel,SimG_NoiseLevel) + (SimSignal[int(i%8)]/32768.0) * SimG_Level
        else:
            CaptureBuffer[i] = np.random.normal(-SimG_NoiseLevel,SimG_NoiseLevel) + np.sin(2*π*i/8) * SimG_Level
  
    StartIndex =int(0)
      
    TmpSpec = np.zeros(RESULT_SPECTRUM_SIZE)
    FFT_MagSqOut = np.zeros(RESULT_SPECTRUM_SIZE)

    for i in range(0,NUM_AVG_STEPS):
           
   #     print("Overlap " + str(i+1) + ' of ' + str(NUM_AVG_STEPS))
   
        AnalysisWindow = CaptureBuffer[StartIndex:StartIndex+VIB_ANALYSIS_WINDOW_SIZE];
        AnalysisWindow = AnalysisWindow*Window;
     
        
        a   = np.fft.fft(AnalysisWindow)
    
        TmpSpec = abs(a)*abs(a);
        
        TmpSpec = WindowAdjust*TmpSpec/(VIB_ANALYSIS_WINDOW_SIZE * VIB_ANALYSIS_WINDOW_SIZE)
        
        TmpSpec = TmpSpec[0:int(RESULT_SPECTRUM_SIZE)]
        
       # plt.plot(TmpSpec)
        
        FFT_MagSqOut = FFT_MagSqOut + TmpSpec
        
        StartIndex = StartIndex + VIB_OVERLAP_STEP
    
    FFT_MagSqOut = FFT_MagSqOut / NUM_AVG_STEPS
    
    FFT_MagSqOut[0] =FFT_MagSqOut[2];
    FFT_MagSqOut[1] =FFT_MagSqOut[2];
    
    
    ToadMeasure = ((10*np.log10(FFT_MagSqOut))).astype(int)
   
    for z in range(0,RESULT_SPECTRUM_SIZE):
        if(ToadMeasure[z]<-128):
            ToadMeasure[z] = -128;
            
    Specgram[:,q] = ToadMeasure;
   


plt.figure(1);   
plt.clf();

plt.title("Daily PSD");
plt.imshow(Specgram,aspect='auto', extent=[0, 24, 4096,0], vmin=-128, vmax=35)

plt.colorbar()
plt.xlabel('Time (Hours)');    
plt.ylabel('Frequency (Hz)');    

plt.figure(2);   
plt.clf();
plt.title("PSD Snapshot");
plt.plot(FreqAxis,ToadMeasure);
plt.ylabel('10log(g^2)');    
plt.xlabel('Frequency (Hz)');    

plt.figure(3);   
plt.clf();
plt.title("FFT^2 Snapshot");
plt.stem(FreqAxis,FFT_MagSqOut);
plt.ylabel('FFT^2)');    
plt.xlabel('Frequency (Hz)'  );    

plt.figure(4);   
plt.clf();
plt.title("Capture Buffer");
plt.plot(CaptureBuffer);
plt.ylabel('Ampltitude)');    
plt.xlabel('Index');    


print("Done")




