#include "stdio.h"
#include "stdint.h"
#include "stdbool.h"
#include "bb_compute.h"


const int16_t SimSignal[] = { 0,23169,32767,23169,0,-23170,-32768,-23170 };
static uint32_t SimIdx = 0;

int16_t  CaptureBuffer1[VIB_CAPTURE_BUFFER_SIZE];
int16_t  CaptureBuffer2[VIB_CAPTURE_BUFFER_SIZE];


int8_t dB_G_Bins[PSD_LEN];

//#define NOISE ((rand()%4096) - 2048)
#define NOISE 0

void FillSimCaputureBuffer(int16_t * CaptureBuffer)
{
	for (int i = 0; i < VIB_CAPTURE_BUFFER_SIZE; i++)
	{
		CaptureBuffer[i] = (SimSignal[i & 0x7]/8) + NOISE;
	}
}

#define NUM_SAMPLES 512

uint16_t GMRS_Old[NUM_SAMPLES];
uint16_t GMRS_New[NUM_SAMPLES];



float Mean(uint16_t* D, uint32_t L)
{
	float Mean = 0;
	for (int i = 0; i < L; i++)
	{
		Mean += D[i];
	}

	return Mean / L;
}

float Variance(uint16_t* D, uint32_t L)
{
	float MeanV = Mean(D,L);
	float Var = 0;
	float Tmp;

	for (int i = 0; i < L; i++)
	{
		Tmp = ((float)(D[i]) - MeanV);
		Var += Tmp * Tmp;
	
	}
	return Var / (float)(L-1);
}


void bb_io_sample_ready(uint32_t SampleIndex)
{
	return true;

}
int main()
{
	
	bb_compute_init();


	uint32_t PeakDelta = 0;

	printf("go\r\n");



	for (int i = 0; i < NUM_SAMPLES; i++)
	{

		FillSimCaputureBuffer(&CaptureBuffer1);

		memcpy(CaptureBuffer2, CaptureBuffer1, sizeof(CaptureBuffer1));

		GMRS_Old[i] = bb_compute_grms_psd(CaptureBuffer1,
			VIB_CAPTURE_BUFFER_SIZE,
			NUM_AVG_STEPS,
			true,
			dB_G_Bins
		);

		//capture buffer gets trash by FFT

		
		GMRS_New[i] = bb_compute_grms_psd(CaptureBuffer2,
			8192,
			NUM_AVG_STEPS,
			true,
			dB_G_Bins
		 	);

		GMRS_New[i] = bb_compute_grms_td(CaptureBuffer2, 8192);

		printf("%i\t,%i\t,%i\r\n", GMRS_Old[i], GMRS_New[i], GMRS_New[i] - GMRS_Old[i]);

		uint32_t Delta = abs(GMRS_New - GMRS_Old);
		
		if (Delta > PeakDelta)
			PeakDelta = Delta;

	}
	
	float OldM = Mean(GMRS_Old, NUM_SAMPLES);

	float NewM = Mean(GMRS_New, NUM_SAMPLES);


	float OldV = Variance(GMRS_Old, NUM_SAMPLES);

	float NewV = Variance(GMRS_New, NUM_SAMPLES);


	printf("Peak Delta : %i\r\n\r\n\r\n", PeakDelta);

	printf("Mean %.3f  ,  %3.f\r\n", OldM, NewM);

	printf("Variance %.3f  ,  %3f\r\n", OldV, NewV);


	return 0;
}