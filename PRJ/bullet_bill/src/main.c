/*
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */
#include "bb_io.h"
#include <zephyr.h>
#include <stdio.h>
#include <modem/lte_lc.h>
#include <modem/at_cmd_parser.h>
#include <modem/at_params.h>
#include "NRF9160.h"
#include <shell/shell_rtt.h>
#include <shell/shell.h>
#include <cJSON.h>
#include <cJSON_os.h>
#include <epoch.h>
#include <logging/log.h>
#include "Cell.h"
#include "DeviceProvision.h"
#include "krusty.h"
#include "system.h"
#include "FIRMWARE_VERSION.h"
#include "bridge.h"
#include <string.h>
#include <sys/reboot.h>
#include "FOTA.h"
#include <dfu/mcuboot.h>

LOG_MODULE_REGISTER(main);


//#define CONFIG_NO_MODEM_SHUTDOWN  

#ifdef CONFIG_NO_MODEM_SHUTDOWN
	#warning   "CONFIG_NO_MODEM_SHUTDOWN enabled!   Don't use in production"
#endif

#define CONFIG_MAX_TX_ACTIVE_TIME 45000

#define CONFIG_MIN_TX_ACTIVE_TIME 10000

static uint32_t SensorCheckTimer;

static uint32_t TxReportTimer;

static uint32_t TxFailures;

static uint32_t HeartbeatTimer;

static struct k_work_delayable shell_work_disable;

static void shell_disable(struct k_work *work)
{
	shell_uninit(shell_backend_rtt_get_ptr(),0);
}

static bool WaitForReportsToSend = false;


void EnqueuePowerUpMessage()
{
    cJSON *Result = cJSON_CreateObject();
 
    cJSON_AddNumberToObject(Result, "Batt", Cell_GetBatt());

    cJSON_AddNumberToObject(Result, "RSSI", Cell_GetRSSI());
    
    cJSON_AddNumberToObject(Result, "RSRP", Cell_GetLastPostedRSRP());

    cJSON_AddNumberToObject(Result, "PSM_TAU", Cell_GetPSM_TAU());
    
    cJSON_AddNumberToObject(Result, "PSM_ActiveTime", Cell_GetPSM_ActiveTime());
 
    cJSON_AddNumberToObject(Result, "eDRX_Interval", Cell_Get_eDRX_IntervalTime());
    
    cJSON_AddNumberToObject(Result, "eDRX_PagingWindow", Cell_Get_eDRX_PagingWindow());
 
    cJSON_AddNumberToObject(Result, "CellID",Cell_GetCellID());

    cJSON_AddNumberToObject(Result, "LAC", Cell_GetTracking());
 
    cJSON_AddStringToObject(Result, "MCCMNO",Cell_COPS());
	
	cJSON_AddStringToObject(Result, "MCC",Cell_GetLastMCC_String());
	
	cJSON_AddStringToObject(Result, "MNC",Cell_GetLastMNC_String());

	cJSON_AddNumberToObject(Result, "Band", Cell_GetCurrentBand());

    cJSON_AddStringToObject(Result, "FirmwareVersion",FIRMWARE_VERSION_STRING);
	  
    cJSON_AddStringToObject(Result, "ObjID","PowerUp");

    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
  
    cJSON_Delete(Result);
}

void EnqueueModemUpdateMessage()
{
    cJSON *Result = cJSON_CreateObject();
 
    cJSON_AddNumberToObject(Result, "Batt", Cell_GetBatt());

    cJSON_AddNumberToObject(Result, "RSSI", Cell_GetRSSI());
    
    cJSON_AddNumberToObject(Result, "RSRP", Cell_GetLastPostedRSRP());

    cJSON_AddNumberToObject(Result, "CellID",Cell_GetCellID());

    cJSON_AddNumberToObject(Result, "LAC", Cell_GetTracking());
	
	cJSON_AddNumberToObject(Result, "Band", Cell_GetCurrentBand());
	
 	cJSON_AddStringToObject(Result, "MCCMNO",Cell_COPS());

	cJSON_AddStringToObject(Result, "MCC",Cell_GetLastMCC_String());
	
	cJSON_AddStringToObject(Result, "MNC",Cell_GetLastMNC_String());
	  
	cJSON_AddNumberToObject(Result, "LastActiveTime_mS",Cell_GetLastActiveTime_mS());

	cJSON_AddNumberToObject(Result, "TxFailures",TxFailures);

	cJSON_AddStringToObject(Result, "FirmwareVersion",FIRMWARE_VERSION_STRING);
	
    cJSON_AddStringToObject(Result, "ObjID","ModemUpdate");

    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
  
    cJSON_Delete(Result);
}

void EnqueueEpochUpdatedMessage()
{
    cJSON *Result = cJSON_CreateObject();
 
    cJSON_AddStringToObject(Result, "LastTimeServer",log_strdup(Epoch__GetLastTimeServer()));

    cJSON_AddStringToObject(Result, "ObjID","EpochUpdate");

    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
  
    cJSON_Delete(Result);
}

void EnqueueEpochFailedMessage()
{
    cJSON *Result = cJSON_CreateObject();
 
    cJSON_AddStringToObject(Result, "LastTimeServer",log_strdup(Epoch__GetLastTimeServer()));

    cJSON_AddStringToObject(Result, "ObjID","EpochFailed");

    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
  
    cJSON_Delete(Result);
}


#define REPORT_TIME   60

static bool UpdateMessageFlag = false;

static bool UpdateEpochFlag = false;
	
static bool FlagPowerDown = false;

static bool CheckBattFlag = false;

void main(void)
{
 	 /* This is needed so that MCUBoot won't revert the update */
	#if (CONFIG_BOOTLOADER_MCUBOOT)
        boot_write_img_confirmed();
    #endif

	FOTA_Init();


    LOG_INF("BulletBill Firmware : %s",log_strdup(FIRMWARE_VERSION_STRING));

    LOG_INF("Cellular Module Firmware :  %s", log_strdup(Cell_ModemFirmware()));

	k_work_init_delayable(&shell_work_disable, shell_disable);
	
	k_work_reschedule(&shell_work_disable, K_SECONDS(120));

    cJSON_Init();
    
	Provision__Init();
	
	bb_io_init();

	Cell_PowerDown();

	LOG_INF("Waiting for acceptable battery voltage");

	while(Cell_GetBatt()<3500)
	{
		k_msleep(10000);
	}

	#ifdef CONFIG_NO_MODEM_SHUTDOWN

		Cell_Init(false,false);

		LOG_WRN("NO MODEM SHUTDOWN MODE");

	#else

		Cell_Init(true,false);

	#endif

	k_msleep(100);

	LOG_INF("ICCID : %s",log_strdup(Cell_ICCID()));
	
	LOG_INF("IMEI : %s",log_strdup(Cell_IMEI()));
	
	uint32_t ConnectCnt = 0;

	while(Cell_IsConnected() == false)
	{
		k_msleep(1000);

		ConnectCnt++;

		if(ConnectCnt >= (60 * 10))
		{
			LOG_ERR("Could not connect after ten  minutes, rebooting");
			Cell_PowerDown();
			k_msleep(1000);
   		    sys_reboot(SYS_REBOOT_COLD);
		}
	}

	LOG_INF("RSSI : %i",Cell_GetRSSI());

	LOG_INF("RSRP : %i",Cell_GetLastPostedRSRP());

	LOG_INF("Operator : %s",log_strdup(Cell_COPS()));

	LOG_INF("MCC : %s",log_strdup(Cell_GetLastMCC_String()));

	LOG_INF("MNC : %s",log_strdup(Cell_GetLastMNC_String()));

	while(Epoch__Update(5000) == false)
	{
		k_msleep(500);
	}

	krusty__init();

	LOG_INF("Waiting for initial krusty connect");

	while(krusty__is_connected() == false)
	{
		k_msleep(250);
	}

	LOG_INF("Enqueue Power Up Message");

	EnqueuePowerUpMessage();

	EnqueueEpochUpdatedMessage();

	LOG_INF("Waiting for krusty queues to clear");
	
	while(krusty__tx_queue_empty() == false)
	{
		k_msleep(250);
	}

	#ifndef CONFIG_NO_MODEM_SHUTDOWN

		krusty__park();
	
	#endif

	k_msleep(2000);


	if(Cell_IsLowPowerGranted())
	{
		if(Cell_IsPSM_Granted())
		{
			LOG_INF("PSM granted");
		}

		if(Cell_IsEDRX_Granted())
		{
			LOG_INF("EDRX Granted");
		}
	}
	else
	{
		LOG_INF("No low power states granted.  Powering down modem");
		
		#ifndef CONFIG_NO_MODEM_SHUTDOWN
			Cell_PowerDown();
		#else
			LOG_WRN("NO MODEM SHUTDOWN MODE");
		#endif
	}
	
	#ifndef CONFIG_NO_MODEM_SHUTDOWN

	
	while(Cell_GetLastActiveTime_mS() == 0)
	{
		k_msleep(100);
	}

	#endif

	LOG_INF("Power up Connect time was %d mS",Cell_GetLastActiveTime_mS());

	//trigger the the sensor system to start collecting
	SensorCheckTimer = System_Get_mS_Ticker() - REPORT_TIME*1000;
	HeartbeatTimer = System_Get_mS_Ticker();
	while(1)
	{
			#ifdef CONFIG_NO_MODEM_SHUTDOWN

			//For testing incoming commands to avoid stateful firewall isses
			if(Check_mS_Timeout(&HeartbeatTimer,60000))
			{
				EnqueueModemUpdateMessage();
			}

			#endif 

			if(Check_mS_Timeout(&SensorCheckTimer,REPORT_TIME*1000))
			{
				if(bullet_bill__process(REPORT_TIME) == true)
				{
					TxReportTimer = System_Get_mS_Ticker();
					
					if(Cell_IsLowPowerGranted())
					{
						LOG_INF("We are in a low power state.  No need to power up modem manually");
					}
					else
					{
						#ifndef CONFIG_NO_MODEM_SHUTDOWN
							Cell_PowerUp();
						#else
							LOG_WRN("NO MODEM SHUTDOWN MODE");
						#endif
					}
					
					#ifndef CONFIG_NO_MODEM_SHUTDOWN

					krusty__go();
					
					#endif

					if(Epoch__NeedsRefreshed())
					{
						UpdateEpochFlag = true;
					}

					CheckBattFlag = true;

					WaitForReportsToSend = true;

					UpdateMessageFlag = true;
				
				}
			
			}

			

			if(CheckBattFlag == true)
			{
				CheckBattFlag = false;

				if(Cell_GetBatt() < 3200)
				{
					LOG_WRN("Battery low. Powering down cellular subsystem and rebooting");
					k_msleep(1000);
					Cell_PowerDown();
					k_msleep(2000);
					sys_reboot(SYS_REBOOT_COLD);	
				}

			}

			if((UpdateMessageFlag == true) && Cell_IsConnected())
			{
				LOG_INF("Sending Modem Update Message");
				UpdateMessageFlag = false;
				EnqueueModemUpdateMessage();
			}

			// only try to do this once per Tx Cycle if needed
			if(UpdateEpochFlag && Cell_IsConnected())
			{
	
				UpdateEpochFlag = false;

				if(Epoch__Update(5000) == true)
				{
					EnqueueEpochUpdatedMessage();
				}
				else
				{
					EnqueueEpochFailedMessage();
				}
			}

			if(WaitForReportsToSend == true)
			{
				if(System__ComputeElapsedTime(&TxReportTimer) >= CONFIG_MAX_TX_ACTIVE_TIME)
				{
					LOG_WRN("Could not get data transmitted in available window.  Bailing...");
					
					FlagPowerDown = true;
					TxFailures++;
				}
				else if((krusty__tx_queue_empty() == true) && (System__ComputeElapsedTime(&TxReportTimer) >= CONFIG_MIN_TX_ACTIVE_TIME))
				{
					FlagPowerDown = true;
				}
			}

			if(FOTA_IS_ACTIVE)
			{

			
				//If fota is started the just spin while it is active.
				// This will keep the modem active while we download
				while(FOTA_IS_ACTIVE)
				{
					k_msleep(2000);

				}	

				//if we are here then we got an error.  Wait a few seconds before 
				//exiting so error messages can get out

				k_sleep(K_MSEC(2500));

				System__FlagReboot();

			}

	
			if(FlagPowerDown == true)
				{
					FlagPowerDown = false;

					WaitForReportsToSend = false;
					
					#ifndef CONFIG_NO_MODEM_SHUTDOWN

						krusty__park();
		
					#endif

					if(Cell_IsLowPowerGranted())
					{
						LOG_INF("We were granted a low power state.   No need to power down.");
					}
					else
					{
					   #ifndef CONFIG_NO_MODEM_SHUTDOWN
						
							Cell_PowerDown();
							
							LOG_INF("Powering down modem");

						#else
							LOG_WRN("NO MODEM SHUTDOWN MODE");
						#endif
					}
				}

			//
		
		
		/*
			We need 1/2 second sleep to guarentee second accuracy on the timestamps
		*/
		k_msleep(400);

	}
}

int32_t bridge_test_handler(cJSON *Result, int argc, char **argv)
{

    cJSON_AddStringToObject(Result,"test","123");
    return 0;

}

KRUSTY_CMD(test,bridge_test_handler);