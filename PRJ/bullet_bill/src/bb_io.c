


#include <zephyr.h>


#include <logging/log.h>
#include "bb_io.h"
#include "nrf9160_bitfields.h"
#include "NRF9160.h"
#include "Hann.h"
#include "sensor_types.h"
#include "math.h"
#include "arm_math.h"
#include <nrfx_twim.h>
#include "data_buf.h"
#include "BridgeDataStructure.h"

#include <cJSON.h>
#include <cJSON_os.h>
#include "bridge_report_json.h"
#include "bridge_report_psd.h"
#include "bridge_report_tmp_accel_pairs.h"

#include "krusty.h"


#include <modem/lte_lc.h>
#include <modem/at_cmd_parser.h>
#include <modem/at_params.h>

#include "epoch.h"

#include "bb_compute.h"

LOG_MODULE_REGISTER(toad_io,LOG_LEVEL_INF);

#define SIM_1G_SIGNAL            (0)

#if SIM_1G_SIGNAL == 1

    #define ACCEL_SCALE(Channel) ((float)8.0f)/(32768.0f)

#endif



#define GRMS_ANALYSYS_LENGTH 8192

#define VIB_CAPTURE_BUFFER_SIZE    16384
#define VIB_ANALYSIS_WINDOW_SIZE   2048
#define VIB_OVERLAP_STEP           512
#define NUM_AVG_STEPS             ((VIB_CAPTURE_BUFFER_SIZE-VIB_ANALYSIS_WINDOW_SIZE) / VIB_OVERLAP_STEP) 
#define NUM_AVG_STEPS_GRMS        ((GRMS_ANALYSYS_LENGTH-VIB_ANALYSIS_WINDOW_SIZE) / VIB_OVERLAP_STEP) 

#define VIB_SAMPLE_RATE            4000
#define VIB_CLOCK_FACTOR           512      //We are going to use the 

static volatile int16_t  CaptureBuffer[VIB_CAPTURE_BUFFER_SIZE];


Sensor__PSD_1024 MyPSDs[CONFIG_MAX_PSD_STORAGE_CNT];
uint64_t PSD_TS[CONFIG_MAX_PSD_STORAGE_CNT];
uint16_t G_RMS_Scaled[CONFIG_MAX_TEMPERATURE_STORAGE_CNT];
int16_t Temperatures[CONFIG_MAX_TEMPERATURE_STORAGE_CNT];

uint32_t TempSampleCnt = 0;
uint32_t PSDSampleCnt = 0;
uint64_t StartingTimeStamp = 0;
uint64_t LastStartingTimeStamp = 0;

uint64_t LastPSD_TS = 0;


static volatile bool ADC_BufferReady = false;

volatile uint32_t AccelCaptureIdx = 0;
volatile uint32_t AccelCaptureCount= 0;

static int8_t Out[11];
static int8_t In[11];

#define GPIOTE_NODE DT_INST(0, nordic_nrf_gpiote)


void StartAccel_SPI(uint32_t SamplesToCapture)
{
    NRF_SPIM2_NS->RXD.PTR = (uint32_t)&In;
    NRF_SPIM2_NS->RXD.MAXCNT =10;
    NRF_SPIM2_NS->TXD.PTR = (uint32_t)&Out;
    NRF_SPIM2_NS->TXD.MAXCNT = 10;
    ACCEL_CS_LOW;

    NRF_SPIM2_NS->ENABLE = 7;

    ADC_BufferReady = false;
    AccelCaptureIdx = 0;
    AccelCaptureCount = SamplesToCapture;

    NRF_GPIOTE1_NS->EVENTS_IN[0]=0;
    NRF_GPIOTE1_NS->INTENSET= (1<<0);
    NRF_SPIM2_NS->INTENSET = 1<<4;
    irq_enable(UARTE2_SPIM2_SPIS2_TWIM2_TWIS2_IRQn);
    irq_enable(GPIOTE0_IRQn);
    
}

void StopADC_SPI()
{
    AccelCaptureIdx = 0;
    NRF_SPIM2_NS->INTENCLR = 1<<4;
    NRF_GPIOTE1_NS->INTENCLR = (1<<0);
    irq_disable(UARTE2_SPIM2_SPIS2_TWIM2_TWIS2_IRQn);
    irq_disable(GPIOTE0_IRQn);
    
    NRF_SPIM2_NS->ENABLE = 0;
    
    ACCEL_PWR_OFF; // SO we don't back power the sensors
}

bool ADC_BufferIsReady()
{
    return ADC_BufferReady;
}

const int16_t SimSignal[] = {0,23169,32767,23169,0,-23170,-32768,-23170};
static uint32_t SimIdx =0 ;

typedef union {

    uint8_t bytes[2];
    int16_t signed_word;

} signed_word_union;

#define ADXL33X_DEVID_AD                       0x00 
#define ADXL33X_DEVID_MST                      0x01 
#define ADXL33X_PARTID                         0x02 
#define ADXL33X_REVID                          0x03 
#define ADXL33X_STATUS                         0x04 
#define ADXL33X_FIFO_ENTRIES                   0x05 
#define ADXL33X_TEMP2 		                   0x06 
#define ADXL33X_TEMP1                          0x07 
#define ADXL33X_XDATA3                         0x08 
#define ADXL33X_XDATA2                         0x09 
#define ADXL33X_XDATA1                         0x0A 
#define ADXL33X_YDATA3                         0x0B 
#define ADXL33X_YDATA2                         0x0C 
#define ADXL33X_YDATA1                         0x0D 
#define ADXL33X_ZDATA3                         0x0E 
#define ADXL33X_ZDATA2                         0x0F 
#define ADXL33X_ZDATA1                         0x10 
#define ADXL33X_FIFO_DATA                      0x11 
#define ADXL33X_OFFSET_X_H                     0x1E 
#define ADXL33X_OFFSET_X_L                     0x1F 
#define ADXL33X_OFFSET_Y_H                     0x20 
#define ADXL33X_OFFSET_Y_L                     0x21 
#define ADXL33X_OFFSET_Z_H                     0x22 
#define ADXL33X_OFFSET_Z_L                     0x23 
#define ADXL33X_ACT_EN                         0x24 
#define ADXL33X_ACT_THRESH_H 			       0x25 
#define ADXL33X_ACT_THRESH_L 			       0x26 
#define ADXL33X_ACT_COUNT 		               0x27 
#define ADXL33X_FILTER                         0x28 
#define ADXL33X_FIFO_SAMPLES                   0x29 
#define ADXL33X_INT_MAP                        0x2A 
#define ADXL33X_SYNC                           0x2B 
#define ADXL33X_RANGE                          0x2C 
#define ADXL33X_POWER_CTL                      0x2D 
#define ADXL33X_SELF_TEST                      0x2E 
#define ADXL33X_RESET                          0x2F 

void ADXL35X_TxRx(uint32_t Length,uint8_t *DataOut,uint8_t *DataIn)
{
    NRF_SPIM2_NS->TXD.PTR = (uint32_t)DataOut;
    NRF_SPIM2_NS->TXD.MAXCNT = Length;
    NRF_SPIM2_NS->RXD.PTR = (uint32_t)DataIn;
    NRF_SPIM2_NS->RXD.MAXCNT =Length;
    NRF_SPIM2_NS->ENABLE = 7;

    ACCEL_CS_LOW;
    
    NRF_SPIM2_NS->EVENTS_ENDRX =0;
    NRF_SPIM2_NS->TASKS_START =1;

    while(NRF_SPIM2_NS->EVENTS_ENDRX== 0)
    {

    }

    ACCEL_CS_HIGH;
}

static uint8_t ADXL35X_Out[10];
static uint8_t ADXL35X_In[10];

uint8_t ADXL35X_ReadRegister(uint8_t Reg)
{
    ADXL35X_Out[0] = (Reg << 1) + 1;
    ADXL35X_Out[1] = 0;
    ADXL35X_TxRx(2,&ADXL35X_Out[0],&ADXL35X_In[0]);
    return ADXL35X_In[1];
}

void ADXL35X_ClearFIFO()
{
    ADXL35X_Out[0] = (0x11) + 1;
    ADXL35X_Out[1] = 0;

    for(int i=0;i<0x60;i++)
        ADXL35X_TxRx(10,&ADXL35X_Out[0],&ADXL35X_In[0]);
 
 
}

void ADXL35X_WriteRegister(uint8_t Reg, uint8_t Value)
{

    ADXL35X_Out[0] = (Reg << 1);
    ADXL35X_Out[1] = Value;
    ADXL35X_TxRx(2,&ADXL35X_Out[0],&ADXL35X_In[0]);
}

ISR_DIRECT_DECLARE(DRDY_Callback)
{
    NRF_GPIOTE1_NS->EVENTS_IN[0]=0;

    CaptureBuffer[AccelCaptureIdx] = SimSignal[SimIdx];

     signed_word_union tmpu;
    tmpu.bytes[0] = In[8];
    tmpu.bytes[1] = In[7];


    #if SIM_1G_SIGNAL == 1
        CaptureBuffer[AccelCaptureIdx] = SimSignal[SimIdx]/8;
    #else
        CaptureBuffer[AccelCaptureIdx] = tmpu.signed_word;
    #endif

    SimIdx++;
    if(SimIdx>= sizeof(SimSignal)/sizeof(int16_t))
        SimIdx = 0;

    AccelCaptureIdx++;

    if(AccelCaptureIdx>=AccelCaptureCount)
    {
        ADC_BufferReady = true;
        StopADC_SPI();
        return 0;
    }

    NRF_SPIM2_NS->TASKS_START =1;
    
    ACCEL_CS_LOW;
    
    return 0;
}

ISR_DIRECT_DECLARE(SPIM2_Callback)
{
    if(NRF_SPIM2_NS->EVENTS_ENDRX > 0)
    {
        NRF_SPIM2_NS->EVENTS_ENDRX =0;
    }

    ACCEL_CS_HIGH;

    return 0;
}

void SetupADC_SPI()
{
    NRF_SPIM2_NS->ENABLE = 0;
    NRF_SPIM2_NS->PSEL.MOSI = TOAD_ADC_MOSI_PIN;
    NRF_SPIM2_NS->PSEL.MISO = TOAD_ADC_MISO_PIN;
    NRF_SPIM2_NS->PSEL.SCK =  TOAD_ADC_SCK_PIN;
    NRF_SPIM2_NS->FREQUENCY = SPIM_FREQUENCY_FREQUENCY_M8;

    NRF_SPIM2_NS->RXD.PTR = (uint32_t)&In;
    NRF_SPIM2_NS->RXD.MAXCNT =10;
    NRF_SPIM2_NS->TXD.PTR = (uint32_t)&Out;
    NRF_SPIM2_NS->TXD.MAXCNT = 10;
   
    NRF_SPIM2_NS->INTENCLR = 1<<4;

    Out[0] = (ADXL33X_XDATA3<<1) + 1;
 
    //NRF_P0_NS->PIN_CNF[TOAD_DRDY_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos) | GPIO_PIN_CNF_INPUT_Msk;

    NRF_GPIOTE1_NS->CONFIG[0] = (GPIOTE_CONFIG_MODE_Event<<GPIOTE_CONFIG_MODE_Pos) |
                                (GPIOTE_CONFIG_POLARITY_LoToHi << GPIOTE_CONFIG_POLARITY_Pos) |
                                (TOAD_DRDY_PIN<<GPIOTE_CONFIG_PSEL_Pos);
  
   
      
       
    IRQ_DIRECT_CONNECT(DT_IRQN(GPIOTE_NODE), 0, DRDY_Callback, IRQ_ZERO_LATENCY);
    IRQ_DIRECT_CONNECT(UARTE2_SPIM2_SPIS2_TWIM2_TWIS2_IRQn, 0, SPIM2_Callback, IRQ_ZERO_LATENCY);
}

void  SPI_OFF()
{
    NRF_GPIOTE1_NS->CONFIG[0] = 0;
  
    NRF_GPIOTE1_NS->INTENCLR= (1<<0);
    ACCEL_PWR_OFF;
    ACCEL_CS_LOW;
}

void bb_io_init()
{
  	NRF_TWIM3_NS->PSEL.SCL =  BB_SCL_PIN;
	NRF_TWIM3_NS->PSEL.SDA =  BB_SDA_PIN;
	NRF_TWIM3_NS->FREQUENCY = NRF_TWIM_FREQ_400K;

    NRF_P0_NS->DIR |= (1<<TMP_PWR_PIN);
    NRF_P0_NS->DIR |= (1<<ACCEL_PWR_PIN);
    NRF_P0_NS->DIR |= (1<<ACCEL_CS_PIN);
  

    NRF_P0_NS->DIR &= ~(1<<BB_TEST_IO_0_PIN);
    NRF_P0_NS->DIR &= ~(1<<BB_TEST_IO_1_PIN);
    
    NRF_P0_NS->PIN_CNF[BB_TEST_IO_0_PIN] = (1<<2); //Pulldown on pin
    NRF_P0_NS->PIN_CNF[BB_TEST_IO_0_PIN] |= (1<<1); //Connect input buffer

    NRF_P0_NS->PIN_CNF[BB_TEST_IO_1_PIN] = (1<<2); //Pulldown on pin
    NRF_P0_NS->PIN_CNF[BB_TEST_IO_1_PIN] |= (1<<1); //Connect input buffer

  
    for(int i=0;i<CONFIG_NUM_PSDS_PER_TX;i++)
    {
        MyPSDs[i].SensorType = VAL_TOAD_PSD_1024;
        MyPSDs[i].JackID = 1;
        MyPSDs[i].SampleRateHz = VIB_SAMPLE_RATE;
        MyPSDs[i].BinSpaceHz = VIB_SAMPLE_RATE / VIB_ANALYSIS_WINDOW_SIZE;
        MyPSDs[i].StartBinHz = 0;

        for(int j=0;j<1024;j++)
        {
            MyPSDs[i].dB_G_Bins[j] = -128;
        }
    }
     
  ACCEL_PWR_OFF;
  TMP_PWR_OFF;
  ACCEL_CS_LOW;

  bb_compute_init();
}

#define ADT7420_ADDRESS (0x48)
#define ADT7420_REGISTER_TEMPERATURE		0x00
#define ADT7420_REGISTER_CONFIGURATION		0x03

uint8_t i2c_tx_buf[2];

uint8_t TemperatureRawData[4];

#define MAX_TEMPS_FOR_AVG   16

int16_t TempTemps[MAX_TEMPS_FOR_AVG];

int32_t TempIdx = 0;

void ADT7420_ReadData_Nonblock()
{
 	
	i2c_tx_buf[0] = ADT7420_REGISTER_TEMPERATURE;

 	NRF_TWIM3_NS->TXD.PTR = (uint32_t)(&i2c_tx_buf[0]);

	NRF_TWIM3_NS->TXD.MAXCNT = 1;

	NRF_TWIM3_NS->EVENTS_LASTTX = 0;
	NRF_TWIM3_NS->EVENTS_TXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_LASTRX = 0;
	NRF_TWIM3_NS->EVENTS_RXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_ERROR = 0;

	NRF_TWIM3_NS->ADDRESS = ADT7420_ADDRESS;

	//last Tx to start RX_EN0_ACTIVE
	//Stop to Last Rx

	NRF_TWIM3_NS->RXD.PTR = (uint32_t)(&TemperatureRawData[0]);

	NRF_TWIM3_NS->RXD.MAXCNT = 2;
	
    NRF_TWIM3_NS->SHORTS = TWIM_SHORTS_LASTTX_STARTRX_Msk | TWIM_SHORTS_LASTRX_STOP_Msk;

	NRF_TWIM3_NS->TASKS_STARTTX = 1;
}

void ADT7420_Setup_NonBlock()
{

 	NRF_TWIM3_NS->ENABLE = 6;

	NRF_TWIM3_NS->EVENTS_LASTTX = 0;
	NRF_TWIM3_NS->EVENTS_TXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_LASTRX = 0;
	NRF_TWIM3_NS->EVENTS_RXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_ERROR = 0;

	i2c_tx_buf[0] = ADT7420_REGISTER_CONFIGURATION;
	i2c_tx_buf[1] = 0x80;


	NRF_TWIM3_NS->TXD.PTR = (uint32_t)(&i2c_tx_buf[0]);
	NRF_TWIM3_NS->TXD.MAXCNT = 2;
	NRF_TWIM3_NS->EVENTS_ERROR = 0;
	NRF_TWIM3_NS->ADDRESS = ADT7420_ADDRESS;
	NRF_TWIM3_NS->TASKS_STARTTX = 1;
	
    NRF_TWIM3_NS->SHORTS = TWIM_SHORTS_LASTTX_STOP_Msk;

}

void ADT7420_Off()
{
    NRF_TWIM3_NS->ENABLE = 0;
	TMP_PWR_OFF;
}


bool ProcessPSD = false;

uint32_t CaptureBufferLength;

int32_t Sum =0;

bool bullet_bill__process(uint32_t SampleRateSeconds)
{

        bool ReportReady = false;

        LOG_INF("Collecting Temp/Vib Sample %i of %i",TempSampleCnt+1,CONFIG_NUM_TEMPS_PER_TX);


        if(TempSampleCnt == (CONFIG_NUM_TEMPS_PER_TX-1))
        {

            StartingTimeStamp = Epoch__Get64();
            LOG_INF("starting time stamp %lld",StartingTimeStamp);

            if(LastStartingTimeStamp!=0)
                LOG_INF("Delta: %lld",StartingTimeStamp - LastStartingTimeStamp);

            LastStartingTimeStamp = StartingTimeStamp;

            ProcessPSD = true;
            CaptureBufferLength = VIB_CAPTURE_BUFFER_SIZE;

         }
         else
         {
            ProcessPSD = false;
            CaptureBufferLength = GRMS_ANALYSYS_LENGTH;
         }

        PSD_TS[PSDSampleCnt] = Epoch__Get64();

        ACCEL_PWR_ON; 
        ACCEL_CS_HIGH;
        TMP_PWR_ON;

        k_sleep(K_MSEC(100));

        ADT7420_Setup_NonBlock();
        
        SetupADC_SPI();

        k_sleep(K_MSEC(250));

        ADXL35X_WriteRegister(ADXL33X_RESET,0x52);
    
      //  LOG_INF("DEVID_AD : 0x%02x",(int)ADXL35X_ReadRegister(ADXL33X_DEVID_AD));
      //  LOG_INF("DEVID_MST : 0x%02x",(int)ADXL35X_ReadRegister(ADXL33X_DEVID_MST));
      //  LOG_INF("ADXL33X_REVID : 0x%02x",(int)ADXL35X_ReadRegister(ADXL33X_REVID));
      //  LOG_INF("ADXL33X_POWER_CTL : 0x%02x",(int)ADXL35X_ReadRegister(ADXL33X_POWER_CTL));
    
        ADXL35X_WriteRegister(ADXL33X_POWER_CTL,0);
        
        ADXL35X_WriteRegister(ADXL33X_RANGE,0x80 | 3);   //8 G Range
   
    //    LOG_INF("ADXL33X_RANGE : 0x%02x",(int)ADXL35X_ReadRegister(ADXL33X_RANGE));
    
        StartAccel_SPI(CaptureBufferLength);
       
        TempIdx = 0;

        while(ADC_BufferIsReady() == false)
        {
           
            k_sleep(K_MSEC(250));
            ADT7420_ReadData_Nonblock();
            k_sleep(K_MSEC(50));
            if(TempIdx < MAX_TEMPS_FOR_AVG)
            {
               TempTemps[TempIdx] = ((int16_t)(TemperatureRawData[0])<<8) + (int16_t)(TemperatureRawData[1]);
               TempIdx++;
            }
             
        }
        
        Sum =0;

        for(int i=0;i<TempIdx;i++)
        {
            Sum+=(int32_t)TempTemps[i];
        }

        Sum = Sum / TempIdx;
        
        Temperatures[TempSampleCnt] = (int32_t)(Sum)*100/128;
       
        ADT7420_Off();

        SPI_OFF();

 
        G_RMS_Scaled[TempSampleCnt]  = bb_compute_grms_psd((int16_t *)CaptureBuffer,
                                                            GRMS_ANALYSYS_LENGTH,
                                                            NUM_AVG_STEPS_GRMS,  
                                                            ProcessPSD,  
                                                            &(MyPSDs[PSDSampleCnt].dB_G_Bins[0])
                                                           );
   




        if((TempSampleCnt%CONFIG_NUM_TEMPS_PER_PSD) == (CONFIG_NUM_TEMPS_PER_PSD-1))
        {

             bb_compute_grms_psd((int16_t *)CaptureBuffer,
                                 VIB_CAPTURE_BUFFER_SIZE,
                                 NUM_AVG_STEPS,  
                                 ProcessPSD,  
                                 &(MyPSDs[PSDSampleCnt].dB_G_Bins[0])
                                 );

            PSDSampleCnt++;

            LOG_INF("Collecting PSD %i of %i",PSDSampleCnt,CONFIG_NUM_PSDS_PER_TX);
        }

        TempSampleCnt++;
        
        if(TempSampleCnt>=CONFIG_NUM_TEMPS_PER_TX)
        {

            if(LastPSD_TS!=0)
                 LOG_INF("PSD Delta: %lld",PSD_TS[0] - LastPSD_TS);

            LastPSD_TS = PSD_TS[0];

            for(int i=0;i<CONFIG_NUM_PSDS_PER_TX;i++)
            {
                LOG_INF("Preparing PSD report %d of %d",i+1,CONFIG_NUM_PSDS_PER_TX);
                BridgeReportPSD(i,&MyPSDs[i],PSD_TS[i]);
            }

            LOG_INF("Preparing TempAccel report with %i samples",CONFIG_NUM_TEMPS_PER_TX);
          
            BridgeReportTempAccel(StartingTimeStamp,
                                  Temperatures,
                                  G_RMS_Scaled,
                                  CONFIG_NUM_TEMPS_PER_TX,
                                  SampleRateSeconds);

            krusty__connect_notify();

            ReportReady = true;

            TempSampleCnt = 0;
            
            PSDSampleCnt = 0;

        }

    return ReportReady;
}
