#include "stdint.h"
#include "stdbool.h"

#ifndef BB_COMPUTE_H
#define BB_COMPUTE_H


#define TD_RMS                     (0)

#define VIB_CAPTURE_BUFFER_SIZE    16384
#define VIB_ANALYSIS_WINDOW_SIZE   2048
#define VIB_OVERLAP_STEP           512
#define NUM_AVG_STEPS             ((VIB_CAPTURE_BUFFER_SIZE-VIB_ANALYSIS_WINDOW_SIZE) / VIB_OVERLAP_STEP) 
#define VIB_SAMPLE_RATE            4000
#define VIB_CLOCK_FACTOR           512      //We are going to use the 
#define PSD_LEN                   (VIB_ANALYSIS_WINDOW_SIZE/2)

extern void bb_compute_init();

extern uint16_t bb_compute_grms_td(int16_t* CaptureBuffer, uint32_t Len);

extern uint16_t bb_compute_grms_psd(int16_t* CaptureBuffer,
                                    uint32_t Len,
                                    uint32_t NumAverageSteps,
                                    bool ProcessPSD,
                                    int8_t* dB_G_Bins
                                 );


#endif
