
#include "stdint.h"
#include "Hann.h"
#include "bb_compute.h"
#include "math.h"
#include "arm_math.h"
#include "stdbool.h"


#define SIM_1G_SIGNAL            (0)

#ifdef SIM_1G_SIGNAL
    #if SIM_1G_SIGNAL == 1
        #warning "SIM 1G turned ON!"
    #endif
#endif

#define ACCEL_SCALE(Channel) ((float)8.0f)/(32768.0f)


float    AnalysisWindow[VIB_ANALYSIS_WINDOW_SIZE];
float    FFT_Out[VIB_ANALYSIS_WINDOW_SIZE];
float    FFT_MagSqOut[VIB_ANALYSIS_WINDOW_SIZE / 2];


arm_rfft_fast_instance_f32  MyFFT;

void bb_compute_init()
{
    arm_rfft_fast_init_f32(&MyFFT, 2048);
}


uint16_t bb_compute_grms_td(int16_t* CaptureBuffer, uint32_t Len)
{
    float Tmp;
    float GRMS_Temp = 0;

    for (int i = 0; i < Len; i++)
    {
        Tmp = (float)CaptureBuffer[i] * ACCEL_SCALE(CurrentVibChannel);
        GRMS_Temp += Tmp * Tmp;
    }

    return (uint16_t)(1000.0f * sqrtf(GRMS_Temp / Len));
}


uint32_t StartIndex = 0;
uint32_t NumAverages = 0;
float GRMS_Temp = 0;

uint16_t bb_compute_grms_psd(int16_t* CaptureBuffer,
                          uint32_t Len,
                          uint32_t NumAverageSteps,  
                          bool ProcessPSD,  
                          int8_t* dB_G_Bins
                         )
{

    StartIndex = 0;
    NumAverages = 0;
    GRMS_Temp = 0;

    memset(FFT_MagSqOut, 0, sizeof(FFT_MagSqOut));

    for (int j = 0; j < NumAverageSteps; j++)
    {
        for (int i = 0; i < VIB_ANALYSIS_WINDOW_SIZE; i++)
        {
            AnalysisWindow[i] = Hann2048_f32[i] * (float)CaptureBuffer[i + StartIndex] * ACCEL_SCALE(CurrentVibChannel);
        }

        arm_rfft_fast_f32(&MyFFT, &AnalysisWindow[0], &FFT_Out[0], 0);

        for (int i = 0; i < (VIB_ANALYSIS_WINDOW_SIZE / 2); i++)
        {
            FFT_MagSqOut[i] += (FFT_Out[i * 2] * FFT_Out[i * 2] + FFT_Out[(i * 2) + 1] * FFT_Out[(i * 2) + 1]) * (16.0f) / (2048.0f * 2048.0f);
        }

        StartIndex += VIB_OVERLAP_STEP;
        NumAverages++;

        //Handle the case if the settings up with a odd sized buffer at the end
        if (StartIndex > (Len - VIB_OVERLAP_STEP))
        {
            break;
        }
    }

    float LogTmp = 0;

    GRMS_Temp = 0;

    for (int i = 0; i < (VIB_ANALYSIS_WINDOW_SIZE / 2); i++)
    {
        FFT_MagSqOut[i] = FFT_MagSqOut[i] / NumAverages;

        //skip DC and super low frequency GRMS components.  DC bins and bin 1 re/im
        #if TD_RMS == 0
            
            if (i > 3)
            {
                GRMS_Temp += FFT_MagSqOut[i];
            }

        #endif

            if (ProcessPSD == true)
            {
                LogTmp = (10.0f * logf(FFT_MagSqOut[i])) + 0.5f;

                if (LogTmp < INT8_MIN)
                {
                    LogTmp = INT8_MIN;
                }
                else if (LogTmp > INT8_MAX)
                {
                    LogTmp = INT8_MAX;
                }
                else if (!isfinite(LogTmp))
                {
                    LogTmp = LogTmp + 0.5f;
                }

                dB_G_Bins[i] = LogTmp;
            }
    }

    #if TD_RMS == 0
         GRMS_Temp = sqrtf(GRMS_Temp) * 0.5775078090717444f;  //Adjust for the window gain in frequency domain mode
    #endif

    return (uint16_t)(GRMS_Temp * 1000.0f);
}
