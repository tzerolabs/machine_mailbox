#include "stdint.h"
#include "stdbool.h"
#include "sensor_types.h"

#ifndef _BB_IO_H
#define _BB_IO_H

void bb_io_init();



//returns true when the reports are ready and put in the krusty queue
bool bullet_bill__process(uint32_t SampleRateSeconds);

#define BB_TEST_IO_0_PIN    30
#define BB_TEST_IO_1_PIN    31



#define BB_SDA_PIN        6
#define BB_SCL_PIN         5

#define TOAD_DRDY_PIN      24
#define TOAD_ADC_MOSI_PIN  18
#define TOAD_ADC_MISO_PIN  17
#define TOAD_ADC_SCK_PIN   22


#define ACCEL_PWR_PIN   26
#define TMP_PWR_PIN    10

#define ACCEL_PWR_ON         NRF_P0_NS->OUTSET = (1<<ACCEL_PWR_PIN)
#define ACCEL_PWR_OFF        NRF_P0_NS->OUTCLR = (1<<ACCEL_PWR_PIN)

#define TMP_PWR_ON         NRF_P0_NS->OUTSET = (1<<TMP_PWR_PIN)
#define TMP_PWR_OFF         NRF_P0_NS->OUTCLR = (1<<TMP_PWR_PIN)

#define ACCEL_CS_PIN    23
#define ACCEL_CS_HIGH     NRF_P0_NS->OUTSET = (1<<ACCEL_CS_PIN)
#define ACCEL_CS_LOW      NRF_P0_NS->OUTCLR = (1<<ACCEL_CS_PIN)


#define TX_EN_PIN       21
#define TX_EN_HIGH       NRF_P0_NS->OUTSET = (1<<TX_EN_PIN)
#define TX_EN_LOW        NRF_P0_NS->OUTCLR = (1<<TX_EN_PIN)




#define FLASH_CS_PIN    28
#define RAM_CS_PIN      29

#define FLASH_CS_HIGH  NRF_P0_NS->OUTSET= (1<<FLASH_CS_PIN)
#define FLASH_CS_LOW   NRF_P0_NS->OUTCLR = (1<<FLASH_CS_PIN)

#define RAM_CS_HIGH  NRF_P0_NS->OUTSET= (1<<RAM_CS_PIN)
#define RAM_CS_LOW   NRF_P0_NS->OUTCLR = (1<<RAM_CS_PIN)



#define CONFIG_NUM_TEMPS_PER_TX            10
#define CONFIG_NUM_TEMPS_PER_PSD           10
#define CONFIG_NUM_PSDS_PER_TX             (CONFIG_NUM_TEMPS_PER_TX/CONFIG_NUM_TEMPS_PER_PSD)


#define CONFIG_MAX_PSD_STORAGE_CNT           6   
#define CONFIG_MAX_TEMPERATURE_STORAGE_CNT   (CONFIG_MAX_PSD_STORAGE_CNT * 10)


extern Sensor__PSD_1024 MyPSDs[CONFIG_MAX_PSD_STORAGE_CNT];
extern uint64_t PSD_TS[CONFIG_MAX_PSD_STORAGE_CNT];
extern float G_RMS[CONFIG_MAX_TEMPERATURE_STORAGE_CNT];
extern uint16_t G_RMS_Scaled[CONFIG_MAX_TEMPERATURE_STORAGE_CNT];
extern int16_t Temperatures[CONFIG_MAX_TEMPERATURE_STORAGE_CNT];



#endif