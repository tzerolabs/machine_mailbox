#include "stdint.h"
#include "stdbool.h"

#ifndef _ADT7420_H
#define _ADT7420_H
#define ADT7420_ADDRESS (0x48)
#define ADT7420_REGISTER_TEMPERATURE		0x00
#define ADT7420_REGISTER_CONFIGURATION		0x03
bool ADT7420_ReadData(int16_t *Temp);
void ADT7420_Init();

#endif