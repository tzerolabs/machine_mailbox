#include "stdint.h"
#include "System.h"
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <device.h>
#include <task_wdt/task_wdt.h>
#include <drivers/led.h>
#include <logging/log.h>

#include "Cell.h"
#include "BRIDGE/DataStructures/BridgeDataStructure.h"
#include "System.h"
#include "Cloud_UDP.h"
#include "Epoch.h"
#include "DeviceProvision.h"
#include "Modbus.h"
#include "FOTA.h"
#include "MachineMailboxIO.h"
#include "FOUR_CH.h"
#include "Jack.h"
#include "thread_wdt.h"

LOG_MODULE_REGISTER(toad_ui);

K_THREAD_STACK_DEFINE(my_ui_thread_stack_area, 1024);

#define JACK_LED_IDX_ARRAY DT_PROP(DT_NODELABEL(machine_mailbox), mm_jack_led_idx)

struct k_thread my_ui_thread_data;

#define NUM_MM_LED   (1)

typedef struct
{
    uint8_t Green;
    uint8_t Red;
    uint8_t Blue;
    uint8_t Brightness;    
  
} MM_LED_t;

MM_LED_t MM_LED[NUM_MM_LED];

uint8_t GlowIndex[4];

uint32_t FourChUI_Timer = 0;

uint32_t BridgeBlinkTimer = 0; 

uint32_t CellBlinkTimer = 0;

uint32_t SensorBlinkTimer = 0;

uint32_t ChannelBlinkTimer = 0;

uint32_t SensorLED_Toggle;

bool  HandleCellLED();

bool HandleCloudLED();

void HandleSensorLEDs();

void ui_work_handler(void *a, void *b, void *c);

const struct device *MM_LED_Ctrl;

int32_t ui_thread_wdt_channel = -1;

void toad_ui_init()
{
    ChannelBlinkTimer = System_Get_mS_Ticker();
    CellBlinkTimer = System_Get_mS_Ticker();
    BridgeBlinkTimer = System_Get_mS_Ticker();
    FourChUI_Timer = System_Get_mS_Ticker();

    MM_LED_Ctrl = device_get_binding(DT_LABEL(DT_PHANDLE(MACHINE_MAILBOX_NODE, mm_led_ctrl)));

    if(MM_LED_Ctrl == 0)
    {
        LOG_ERR("Could not get MachineMailbox LED controller");
    }

    k_thread_create(&my_ui_thread_data, my_ui_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(my_ui_thread_stack_area),
                    ui_work_handler,
                    NULL, NULL, NULL,
                    K_HIGHEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);
}



void ui_work_handler(void *a, void *b, void *c)
{

    ui_thread_wdt_channel = thread_wdt_add_channel();

    while(1)
    {

    thread_wdt_channel_feed(ui_thread_wdt_channel);

    int FOTA_Status = 0;

    if((FOTA_Status = FOTA_GetStatus()))
    {
            if(Check_mS_Timeout(&CellBlinkTimer,100 + (1000 - (FOTA_Status * 10))))
            {
                for(int i=0;i<NUM_MM_LED;i++)
                {
                    if(FOTA_Status>=100)
                    {
                         MM_LED[0].Brightness = 0xFF;
                    }
                    else
                    {
                        if(MM_LED[0].Brightness!=0)
                            MM_LED[0].Brightness = 0;
                        else
                            MM_LED[0].Brightness = 0xFF;
                    }

                    MM_LED[0].Blue = 0xFF;
                    MM_LED[0].Green = 0xFF;
                    MM_LED[0].Red = 0xFF;
                }
            }
    }
    else
    {
            HandleCloudLED();
 
    }

            if(MM_LED_Ctrl)
            {
                for(int i=0;i<NUM_MM_LED;i++)
                {
                    led_set_brightness(MM_LED_Ctrl, i,MM_LED[i].Brightness);
                    led_set_color(MM_LED_Ctrl, i,3,(const uint8_t *) &MM_LED[i]);
                }
            }

            GlowIndex[0]+= 8;
            GlowIndex[1] = GlowIndex[0] -64;
            GlowIndex[2] = GlowIndex[1] -64;
            GlowIndex[3] = GlowIndex[2] -64;

            k_sleep(K_MSEC(50));
    }

}

bool HandleCellLED()
{
    Cell_ConnectionState_t CellConnectionState;

    if(Cell_GetConnectionState(&CellConnectionState))
    {
        MM_LED[0].Brightness = 0xFF;
        MM_LED[0].Blue = 0;
        MM_LED[0].Green = 0xFF;
        MM_LED[0].Red = 0;
        return false;
    }
    else
    {
          {
                if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_NOT_REGISTERED)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,150))
                    {
                        if(MM_LED[0].Brightness!=0)
                            MM_LED[0].Brightness = 0;
                        else
                          MM_LED[0].Brightness = 0xFF;
                    }

                    MM_LED[0].Blue = 0;
                    MM_LED[0].Green = 0x80;
                    MM_LED[0].Red = 0xFF;
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_HOME || CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_ROAMING)
                {

                        if(CellConnectionState.ID == -1)
                        {
                            if(Check_mS_Timeout(&CellBlinkTimer,150))
                            {
                                if(MM_LED[0].Brightness!=0)
                                    MM_LED[0].Brightness = 0;
                                else
                                MM_LED[0].Brightness = 0xFF;
                            }

                            MM_LED[0].Blue = 0;
                            MM_LED[0].Green = 0x80;
                            MM_LED[0].Red = 0xFF;
                        }
                        else
                        {
                            MM_LED[0].Brightness = 0xFF;
                                MM_LED[0].Blue = 0;
                                MM_LED[0].Green = 0xFF;
                                MM_LED[0].Red = 0;
                        }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_SEARCHING)
                {
                   if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                         MM_LED[0].Brightness = 0xFF;

                         if(MM_LED[0].Green == 0xFF)
                         {
                            MM_LED[0].Blue = 0;
                            MM_LED[0].Green = 0x80;
                            MM_LED[0].Red = 0xFF;
                         }
                         else
                         {
                            MM_LED[0].Blue = 0;
                            MM_LED[0].Green = 0xFF;
                            MM_LED[0].Red = 0;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTRATION_DENIED)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,100))
                    {
                        if(MM_LED[0].Brightness!=0)
                            MM_LED[0].Brightness = 0;
                        else
                        MM_LED[0].Brightness = 0xFF;
                    }

                    MM_LED[0].Blue = 0;
                    MM_LED[0].Green = 0;
                    MM_LED[0].Red = 0xFF;
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_UNKNOWN)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                         MM_LED[0].Brightness = 0xFF;

                         if(MM_LED[0].Blue != 0x80)
                         {
                            MM_LED[0].Blue = 0x80;
                            MM_LED[0].Green = 0;
                            MM_LED[0].Red = 0xFF;
                         }
                         else
                         {
                            MM_LED[0].Blue = 0;
                            MM_LED[0].Green = 0;
                            MM_LED[0].Red = 0xFF;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_EMERGENCY)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                        MM_LED[0].Brightness = 0xFF;

                         if(MM_LED[0].Green != 0x80)
                         {
                            MM_LED[0].Blue = 0;
                            MM_LED[0].Green = 0x80;
                            MM_LED[0].Red = 0xFF;
                         }
                         else
                         {
                            MM_LED[0].Blue = 0;
                            MM_LED[0].Green = 0;
                            MM_LED[0].Red = 0xFF;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_UICC_FAIL)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                        MM_LED[0].Brightness = 0xFF;

                        if(MM_LED[0].Green !=0)
                        {
                            MM_LED[0].Blue = 0x00;
                            MM_LED[0].Green = 0x00;
                            MM_LED[0].Red = 0xFF;
                        }
                        else
                        {
                            MM_LED[0].Blue = 0xFF;
                            MM_LED[0].Green = 0xFF;
                             MM_LED[0].Red = 0xFF;
                        }
                    }
                }
                else
                {
                    MM_LED[0].Blue = 0;
                    MM_LED[0].Green = 0;
                    MM_LED[0].Red = 0xFF;
                    MM_LED[0].Brightness = 0xFF;
                }
        }
    }

    return true;
}

uint32_t  LED_TX_Flag = 0;
uint32_t  LED_TX_Count = 0;

bool HandleCloudLED()
{
    if(!Provision__IsGood())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,500))
             {
                if(MM_LED[0].Brightness<255)
                    MM_LED[0].Brightness = 255;
                else
                    MM_LED[0].Brightness = 0;
                }

                MM_LED[0].Green = 0;
                MM_LED[0].Red = 255;
                MM_LED[0].Blue = 0;
    }
    else if(Cell_IsConnected() == 0)
    {
        HandleCellLED();
    }
    else if(Epoch__Get32() == 0)
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,100))
             {
                if(MM_LED[0].Brightness<255)
                    MM_LED[0].Brightness = 255;
                else
                    MM_LED[0].Brightness = 0;
            }

                MM_LED[0].Green = 0;
                MM_LED[0].Red = 255;
                MM_LED[0].Blue = 255;
    }
    else if(LED_TX_Flag != Cloud_MessagesTransmitted())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,50))
             {
                 if(LED_TX_Count < 8)
                 {
                    LED_TX_Count++;
                 }
                 else
                 {
                     LED_TX_Flag++;
                     LED_TX_Count = 0;
                 }

                if(MM_LED[0].Brightness<255)
                    MM_LED[0].Brightness = 255;
                else
                    MM_LED[0].Brightness = 0;
              }

              MM_LED[0].Green = 0;
              MM_LED[0].Red = 0;
              MM_LED[0].Blue = 255;
    }
    else if(Cloud_IsInRetry())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,50))
             {
                if(MM_LED[0].Brightness<255)
                    MM_LED[0].Brightness = 255;
                else
                    MM_LED[0].Brightness = 0;
            }

                MM_LED[0].Green = 255;
                MM_LED[0].Red = 255;
                MM_LED[0].Blue = 0;
    }
    else if(Cloud_IsConnected())
    {
         MM_LED[0].Brightness = 255;
         MM_LED[0].Green = 0;
         MM_LED[0].Red = 0;
         MM_LED[0].Blue = 255;
    }
    else
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,200))
             {
                if(MM_LED[0].Brightness<255)
                    MM_LED[0].Brightness = 255;
                else
                    MM_LED[0].Brightness = 0;
            }

                MM_LED[0].Green = 127;
                MM_LED[0].Red = 255;
                MM_LED[0].Blue = 0;
    }

    return true;
}

void HandleModbusLEDs()
{

}

void HandleSensorLEDs()
{
  

}

