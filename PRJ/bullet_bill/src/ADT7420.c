
#include <zephyr.h>
#include <device.h>
#include <drivers/i2c.h>
#include <logging/log.h>
#include <drivers/i2c.h>
#include "MachineMailboxIO.h"
#include "ADT7420.h"
#include "NRF9160.h"
#include <nrfx_twim.h>
#include "NRF9160_Bitfields.h"

LOG_MODULE_REGISTER(ADT7420);

const struct device *ADT7420_I2C_Dev;



  int16_t TemperatureRaw;


uint8_t RawData[4];

uint8_t i2c_tx_buf[2];
  
bool ADT7420_ReadData(int16_t *Temp)
{
     
        uint8_t rdata = ADT7420_REGISTER_TEMPERATURE;


 	
	i2c_tx_buf[0] = ADT7420_REGISTER_TEMPERATURE;

 	NRF_TWIM3_NS->TXD.PTR = (uint32_t)(&i2c_tx_buf[0]);

	NRF_TWIM3_NS->TXD.MAXCNT = 1;



	NRF_TWIM3_NS->EVENTS_LASTTX = 0;
	NRF_TWIM3_NS->EVENTS_TXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_LASTRX = 0;
	NRF_TWIM3_NS->EVENTS_RXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_ERROR = 0;

	NRF_TWIM3_NS->ADDRESS = ADT7420_ADDRESS;

	//last Tx to start RX_EN0_ACTIVE
	//Stop to Last Rx

	NRF_TWIM3_NS->RXD.PTR = (uint32_t)(&RawData[0]);

	NRF_TWIM3_NS->RXD.MAXCNT = 2;
	
    NRF_TWIM3_NS->SHORTS = TWIM_SHORTS_LASTTX_STARTRX_Msk | TWIM_SHORTS_LASTRX_STOP_Msk;


	NRF_TWIM3_NS->TASKS_STARTTX = 1;
	


	while(NRF_TWIM3_NS->EVENTS_STOPPED == 0 && NRF_TWIM3_NS->EVENTS_ERROR == 0)
	{

	}
	
	k_msleep(1);
	
	*Temp = ((int16_t)(RawData[0])<<8) + (int16_t)(RawData[1]);


        return false;
}

void ADT7420_Init()
{

 	NRF_TWIM3_NS->ENABLE = 6;

	NRF_TWIM3_NS->EVENTS_LASTTX = 0;
	NRF_TWIM3_NS->EVENTS_TXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_LASTRX = 0;
	NRF_TWIM3_NS->EVENTS_RXSTARTED = 0;
	NRF_TWIM3_NS->EVENTS_ERROR = 0;

	i2c_tx_buf[0] = ADT7420_REGISTER_CONFIGURATION;
	i2c_tx_buf[1] = 0x80;


	NRF_TWIM3_NS->TXD.PTR = (uint32_t)(&i2c_tx_buf[0]);
	NRF_TWIM3_NS->TXD.MAXCNT = 2;
	NRF_TWIM3_NS->EVENTS_ERROR = 0;
	NRF_TWIM3_NS->ADDRESS = ADT7420_ADDRESS;
	NRF_TWIM3_NS->TASKS_STARTTX = 1;
	

    NRF_TWIM3_NS->SHORTS = TWIM_SHORTS_LASTTX_STOP_Msk;


	while(NRF_TWIM3_NS->EVENTS_LASTTX == 0 && NRF_TWIM3_NS->EVENTS_ERROR == 0)
	{

	}

}

