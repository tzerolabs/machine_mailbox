#include "MasterConfiguration.h"
#include "stdint.h"
#include "BinShell.h"
#include "Queue/eQueue.h"
#include "CRC/CRC.h"
#include "fsl_lpuart.h"
#include "System.h"
#include "stdbool.h"
#include "BinShell_MsgTypes.h"
#include "string.h"
#include "BinShell_MsgHandler.h"
#include "tusb.h"

#define BIN_SHELL_MAX_INCOMING_MSG_SIZE 256

#define DMX_LPUART			    LPUART1
#define DMX_LPUART_CLK_FREQ 		GetUartFreq()
#define DMC_LPUART_IRQn 			LPUART1_IRQn
#define DMX_LPUART_IRQHandler 	LPUART1_IRQHandler

#define BIN_SHELL_HEADER1	0xFF
#define BIN_SHELL_HEADER2	0xFF
#define BIN_SHELL_HEADER3   0xFF
#define BIN_SHELL_HEADER4   0xFF

#define BIN_SHELL_HEADER_32 (BIN_SHELL_HEADER1 | BIN_SHELL_HEADER2<<8 | BIN_SHELL_HEADER3<<16 | BIN_SHELL_HEADER4<<24)

//ByteQueue BinShellOutputQueue;

//uint8_t  BinShellOutputQueueStorage[BIN_SHELL_OUTGOING_QUEUE_SIZE];

uint32_t BinShellUART_TxActive = 0 ;

uint8_t BinShellIncomingMessage[BIN_SHELL_MAX_INCOMING_MSG_SIZE];

uint32_t BinShellHasBeenInitialized = 0;

void BinShell_IO_Init();

void BinShell_Init()
{
	BinShellHasBeenInitialized = 0;

	BinShell_IO_Init();

    //InitByteQueue(&BinShellOutputQueue, BIN_SHELL_OUTGOING_QUEUE_SIZE, BinShellOutputQueueStorage);
}

void BinShell_IO_Init()
{
	//BinShellUART_TxActive = 0 ;

   // lpuart_config_t config;


      /*
       * config.baudRate_Bps = 115200U;
       * config.parityMode = kLPUART_ParityDisabled;
       * config.stopBitCount = kLPUART_OneStopBit;
       * config.txFifoWatermark = 0;
       * config.rxFifoWatermark = 0;
       * config.enableTx = false;
       * config.enableRx = false;
       */
     // LPUART_GetDefaultConfig(&config);

    //  config.baudRate_Bps = 230400;
    //  config.enableTx = true;
   //   config.enableRx = true;
//
    //  LPUART_Init(BIN_SHELL_LPUART, &config, BIN_SHELL_LPUART_CLK_FREQ);

      /* Enable RX interrupt. */
   //   LPUART_EnableInterrupts(BIN_SHELL_LPUART, kLPUART_RxDataRegFullInterruptEnable);

   //   NVIC_SetPriority(BIN_SHELL_LPUART_IRQn,BIN_SHELL_IRQ_PRIORITY);

   //   EnableIRQ(BIN_SHELL_LPUART_IRQn);

      BinShellHasBeenInitialized = true;

}

void BinShell_MoveQueues()
{
  // if(BinShellHasBeenInitialized == 0)
  //     return;

  // if(BytesInQueue(&BinShellOutputQueue) > 0)
  //  {
	//   if((LPUART_GetEnabledInterrupts(BIN_SHELL_LPUART)&kLPUART_TxDataRegEmptyInterruptEnable) == 0)
    //   {
//		   LPUART_EnableInterrupts(BIN_SHELL_LPUART, kLPUART_TxDataRegEmptyInterruptEnable);
//		   BinShellUART_TxActive = 1;
//	   }
 //   }
}


typedef enum
{

    SCAN_FOR_HEADER1 = 0,
    SCAN_FOR_HEADER2,
    SCAN_FOR_HEADER3,
    SCAN_FOR_HEADER4,

    GRAB_LENGTH1,
    GRAB_LENGTH2,

    GRAB_MSG_TYPE1,
    GRAB_MSG_TYPE2,

    GRAB_PAYLOAD,
    GRAB_CRC1,
    GRAB_CRC2,
    GRAB_CRC3,
    GRAB_CRC4

} PacketDetectState;

uint8_t DetectState = SCAN_FOR_HEADER1;
uint32_t IncomingCRC = 0;
uint16_t DataCnt = 0;
uint16_t IncomingPacketLength = 0;
uint16_t IncomingPacketMsgType = 0;
uint8_t IncomingPacketPayload[BIN_SHELL_MAX_INCOMING_MSG_SIZE];
uint32_t IncomingPacketCRC = 0;
uint32_t PacketStartTicker = 0;

void BinShell_Feed(uint8_t DataIn)
{
	if(System__Check_mS_Timeout(&PacketStartTicker, 2000) && DetectState!=SCAN_FOR_HEADER1)
	{
		DetectState = SCAN_FOR_HEADER1;
	}

	  switch (DetectState)
	        {
	            case SCAN_FOR_HEADER1:
	                if (DataIn == BIN_SHELL_HEADER1)
	                {
	                	IncomingCRC = CRC32_Start();
	                    IncomingCRC = CRC32_Step(IncomingCRC,BIN_SHELL_HEADER1);
	                    DetectState = SCAN_FOR_HEADER2;
	                    PacketStartTicker = System_Get_mS_Ticker();
	                    DataCnt = 0;
	                }
	                else
	                {
	                    DetectState = SCAN_FOR_HEADER1;
	                }
	                break;

	            case SCAN_FOR_HEADER2:
	                if (DataIn == BIN_SHELL_HEADER2)
	                {
	                    IncomingCRC = CRC32_Step(IncomingCRC,BIN_SHELL_HEADER2);
	                    DetectState = SCAN_FOR_HEADER3;
	                }
	                else
	                {
	                    DetectState = SCAN_FOR_HEADER1;
	                }
	                break;

	            case SCAN_FOR_HEADER3:
	                if (DataIn == BIN_SHELL_HEADER3)
	                {
	                    IncomingCRC = CRC32_Step(IncomingCRC,BIN_SHELL_HEADER3);
	                    DetectState = SCAN_FOR_HEADER4;
	                }
	                else
	                {
	                    DetectState = SCAN_FOR_HEADER1;
	                }
	                break;


	            case SCAN_FOR_HEADER4:
	                if (DataIn == BIN_SHELL_HEADER4)
	                {
	                    IncomingCRC = CRC32_Step(IncomingCRC,BIN_SHELL_HEADER4);
	                    DetectState = GRAB_LENGTH1;
	                }
	                else
	                {
	                    DetectState = SCAN_FOR_HEADER1;
	                }
	                break;

	            case GRAB_LENGTH1:

	            	IncomingPacketLength = DataIn;
	                IncomingCRC = CRC32_Step(IncomingCRC,DataIn);
	                DetectState = GRAB_LENGTH2;

	                break;

	            case GRAB_LENGTH2:

	            	IncomingPacketLength = (uint16_t)(IncomingPacketLength| ((uint16_t)(DataIn)<<8));
	                IncomingCRC = CRC32_Step(IncomingCRC,DataIn);
	                DetectState = GRAB_MSG_TYPE1;

	                break;

	            case GRAB_MSG_TYPE1:

	            	IncomingPacketMsgType= DataIn;
	                IncomingCRC = CRC32_Step(IncomingCRC,DataIn);
	                DataCnt = 0;

	                DetectState = GRAB_MSG_TYPE2;
	                break;


	            case GRAB_MSG_TYPE2:

	            	IncomingPacketMsgType = (uint16_t)(IncomingPacketMsgType | (uint16_t)((uint16_t)(DataIn) << 8)); ;
	                IncomingCRC = CRC32_Step(IncomingCRC,DataIn);
	                DataCnt = 0;

	                if(IncomingPacketLength ==2)
	                {
	                	DetectState = GRAB_CRC1;
	                }
	                else
	                {
	                	DetectState = GRAB_PAYLOAD;
	                }
	                break;


	            case GRAB_PAYLOAD:

	            	IncomingPacketPayload[DataCnt] = DataIn;
	                IncomingCRC = CRC32_Step(IncomingCRC,DataIn);
	                DataCnt++;

	                if (DataCnt == (IncomingPacketLength - 2))
	                {
	                    DetectState = GRAB_CRC1;
	                }
	                else if (DataCnt == BIN_SHELL_MAX_INCOMING_MSG_SIZE)
	                {
	                    DetectState = SCAN_FOR_HEADER1;
	                }
	                break;

	            case GRAB_CRC1:

	            	IncomingPacketCRC = DataIn;
	                    DetectState = GRAB_CRC2;

	                break;

	            case GRAB_CRC2:

	            	IncomingPacketCRC |= (uint32_t)(DataIn)<<8;
	                DetectState = GRAB_CRC3;

	                break;

	            case GRAB_CRC3:

	            	IncomingPacketCRC |= (uint32_t)(DataIn)<<16;
	                DetectState = GRAB_CRC4;

	                break;

	            case GRAB_CRC4:

	            	IncomingPacketCRC |= (uint32_t)(DataIn)<<24;
	                DetectState = SCAN_FOR_HEADER1;

	                IncomingCRC = CRC32_Stop(IncomingCRC);

	                if ((IncomingCRC == IncomingPacketCRC))
	                {
	                    BinShell_MsgHandler(IncomingPacketMsgType, IncomingPacketPayload, DataCnt);
	                }
	                else
	                {

	                }

	                break;

	            default:
	                DetectState = SCAN_FOR_HEADER1;
	                break;
	        }
}

/*
void BIN_SHELL_LPUART_IRQHandler(void)
{
    uint8_t Data;

    if ((kLPUART_RxDataRegFullFlag)&LPUART_GetStatusFlags(BIN_SHELL_LPUART))
    {

    	BinShell_Feed(LPUART_ReadByte(BIN_SHELL_LPUART));

    }
    if ((kLPUART_TxDataRegEmptyFlag)&LPUART_GetStatusFlags(BIN_SHELL_LPUART))
    {
    	   if(BytesInQueue(&BinShellOutputQueue))
    	   {
    		   ByteDequeue(&BinShellOutputQueue,&Data);

    		   BIN_SHELL_LPUART->DATA = Data;
    	   }
    	   else
    	   {
    		   BinShellUART_TxActive = 0;
    		   LPUART_DisableInterrupts(BIN_SHELL_LPUART, kLPUART_TxDataRegEmptyInterruptEnable);
    	   }
    }



    // Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
    //  exception return operation might vector to incorrect interrupt
#if defined __CORTEX_M && (__CORTEX_M == 4U)
    __DSB();
#endif
}
*/
void BinShellTxOut(uint8_t *Msg, uint16_t MsgLength);

void BinShell_Process()
{
	if(CommsV2__CaptureInProcess())
	{
		if(CommsV2__CaptureComplete())
		{
			CaptureBuffer_t *CB = CommsV2_GetActiveCaptureBuffer();


			uint32_t Header = BIN_SHELL_HEADER_32;
			uint32_t CRC = CRC32_Start();
			uint16_t PacketMessageLength = 2 + sizeof(LineCaptureHeader) + (CB->max_length*CB->BytesPerSample); //two bytes for the message type
			uint16_t MsgType = BIN_SHELL_TX_CAPTURE_BUFFER;

			LineCaptureHeader LCH;

			LCH.SampleRate = SYSTEM_SAMPLERATE;
			LCH.DataPoints = CB->max_length;

			/*
				Send the 4 byte header
			*/
			BinShellTxOut((uint8_t *)(&Header), sizeof(Header));
			CRC = CRC32_StepArray(CRC, (uint8_t *)(&Header),sizeof(Header));
			/*
				Send the Length of the packet (payload only.  Doesn't count the CRC or header)
			*/
			BinShellTxOut((uint8_t *)(&PacketMessageLength), sizeof(PacketMessageLength));
			CRC = CRC32_StepArray(CRC, (uint8_t *)(&PacketMessageLength),sizeof(PacketMessageLength));

			/*
				All packets start with a 16 bit message type
			*/
			BinShellTxOut( (uint8_t *)(&MsgType), sizeof(MsgType));
			CRC = CRC32_StepArray(CRC, (uint8_t *)(&MsgType),sizeof(MsgType));

			USB_Process();
			/*
			Send out the capture header
			*/

			BinShellTxOut( (uint8_t *)(&LCH), sizeof(LCH));
			CRC = CRC32_StepArray(CRC, (uint8_t *)(&LCH),sizeof(LCH));
			
			USB_Process();
			
			/*
				Send out the data
			*/
			uint32_t SamplesToSend;
			while(CB->ReadPtr<CB->WritePtr)
			{
				if((CB->WritePtr - CB->ReadPtr) > 512)
				{
					SamplesToSend = 512;

				}
				else
				{
					SamplesToSend = CB->WritePtr - CB->ReadPtr;
				}

				BinShellTxOut( (uint8_t *)(&CB->SampleBuffer[CB->ReadPtr]), SamplesToSend * CB->BytesPerSample);
				CRC = CRC32_StepArray(CRC,(uint8_t *)(&CB->SampleBuffer[CB->ReadPtr]), SamplesToSend * CB->BytesPerSample);

				USB_Process();
				System__Delay_mS(1);
				USB_Process();
				System__Delay_mS(1);
				USB_Process();
				System__Delay_mS(1);
				USB_Process();

				CB->ReadPtr+=SamplesToSend;

			}

			CRC = CRC32_Stop(CRC);
			BinShellTxOut((uint8_t *)(&CRC), sizeof(CRC));
			
			CommsV2__ResetCaptureInProcess();

		}
	}


}

void BinShellTxOut(uint8_t *Msg, uint16_t MsgLength)
{
#if ENABLE_USB == 1

	tud_cdc_n_write( 0 , Msg,  MsgLength);
	tud_cdc_n_write_flush(0);
#endif

}

void BinShell_TxMsg(uint16_t MsgType, uint8_t *Msg, uint16_t MsgLength)
{

#if ENABLE_USB == 1
	uint32_t Header = BIN_SHELL_HEADER_32;
	uint32_t CRC = CRC32_Start();
	uint16_t PacketMessageLength = MsgLength + 2;

	BinShellTxOut((uint8_t *)(&Header), sizeof(Header));
	CRC = CRC32_StepArray(CRC, (uint8_t *)(&Header),sizeof(Header));

	BinShellTxOut((uint8_t *)(&PacketMessageLength), sizeof(PacketMessageLength));
	CRC = CRC32_StepArray(CRC, (uint8_t *)(&PacketMessageLength),sizeof(PacketMessageLength));

	BinShellTxOut( (uint8_t *)(&MsgType), sizeof(MsgType));
	CRC = CRC32_StepArray(CRC, (uint8_t *)(&MsgType),sizeof(MsgType));

	BinShellTxOut( Msg, MsgLength);
	CRC = CRC32_StepArray(CRC,Msg,MsgLength);

	CRC = CRC32_Stop(CRC);
	BinShellTxOut((uint8_t *)(&CRC), sizeof(CRC));
#endif

}

void BinShell_SendTxt(char *Msg)
{
	BinShell_TxMsg(BIN_SHELL_MSG__TXT,(uint8_t *)Msg,strlen(Msg));
}


static char BinMsgStringBuffer[256];

void BinShell_printf(const char *FormatString,...)
{
    va_list argptr;
    va_start(argptr,FormatString);
    vsprintf((char *)BinMsgStringBuffer,FormatString,argptr);
    va_end(argptr);

	BinShell_TxMsg(BIN_SHELL_MSG__TXT,(uint8_t *)BinMsgStringBuffer,strlen(BinMsgStringBuffer));
}
