#include "MachineMailboxIO.h"
#include "ADS1119.h"
#include <zephyr.h>
#include <sys/printk.h>
#include <device.h>
#include <drivers/i2c.h>
#include <drivers/gpio.h>
#include <logging/log.h>
#include <drivers/i2c.h>
#include "MachineMailboxIO.h"

LOG_MODULE_REGISTER(ADS1119);

const struct device *ADS1119_I2C_Dev;

#define ADS1119_CMD__RESET          0x06
#define ADS1119_CMD__START_SYNC     0x08
#define ADS1119_CMD__POWERDOWN      0x02
#define ADS1119_CMD__RDATA          0x10
#define ADS1119_CMD__RREG           0x20
#define ADS1119_CMD__WREG           0x40

#define ADS1119_REGISTER__CONFIG            0x00
#define ADS1119_REGISTER__STATIS            0x01


uint8_t ADS1119_Reset(uint8_t I2C_Address)
{
	if(ADS1119_I2C_Dev == NULL)
	{
		LOG_ERR("No I2C device for the ADS1119");
		return 0;
	}
     
	uint8_t tx_buf[1] = {ADS1119_CMD__RESET};

	if(0 != i2c_write(ADS1119_I2C_Dev, tx_buf, 1, I2C_Address))
	{
		LOG_ERR("Error writing ADS1119");
	}

	return 0;
}

uint8_t ADS1119_StartSync(uint8_t I2C_Address)
{
	if(ADS1119_I2C_Dev == NULL)
	{
		LOG_ERR("No I2C device for the ADS1119");
		return 0;
	}
     
	uint8_t tx_buf[1] = {ADS1119_CMD__START_SYNC};

	if(0 != i2c_write(ADS1119_I2C_Dev, tx_buf, 1, I2C_Address))
	{
		LOG_ERR("Error writing ADS1119");
	}

	return 0;
}

uint8_t ADS1119_WriteConfigReg(uint8_t I2C_Address,uint8_t Data)
{
	if(ADS1119_I2C_Dev == NULL)
	{
		LOG_ERR("No I2C device for the ADS1119");
		return 0;
	}
     
	uint8_t tx_buf[2] = {ADS1119_CMD__WREG ,Data};

	if(0 != i2c_write(ADS1119_I2C_Dev, tx_buf, 2, I2C_Address))
	{
		LOG_ERR("Error writing ADS1119");
	}

	return 0;
}

int16_t ADS1119_ReadData(uint8_t I2C_Address)
{
        uint8_t value[2];
        uint8_t rdata = ADS1119_CMD__RDATA;

    	if( 0 == i2c_write_read(ADS1119_I2C_Dev, I2C_Address,
			      &rdata, sizeof(rdata),
			      &value[0], sizeof(value)))
        {

            uint16_t flip = 0;

            flip = (uint16_t)(value[1]);
            flip |= ((uint16_t)(value[0]))<<8;
            
            return (int16_t)(flip);
            
        }
        else
        {
            LOG_ERR("Error reading data from ADS1119");
        }

        return 0;
}


void ADS1119_Init()
{
	ADS1119_I2C_Dev = device_get_binding(DT_LABEL(DT_PHANDLE(MACHINE_MAILBOX_NODE, mm_i2c)));

	if(ADS1119_I2C_Dev == NULL)
	{
		LOG_ERR("Could not get I2C device for the ADS1119");
		return;
	}

}

