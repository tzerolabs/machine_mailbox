# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os

#http://www.itsirl.com/images/references/datasheets/1399021951CalVan.pdf

alpha = 0.003850
beta = 0.10863
delta = 1.4999

R0 = 100.0;

A = alpha + ((alpha * delta) / 100);
B =  -1   * (alpha * delta) / (100*100);
C =  -1   * (alpha *  beta) / (100*100*100*100);

BaseFileNameString = 'RTD_Table'



RTD_OutputFile = open(BaseFileNameString + '.c','w+')
RTD_H_OutputFile = open(BaseFileNameString + '.h','w+')

RTD_H_OutputFile.write("#include \"lut.h\"\n\n");
RTD_H_OutputFile.write("#ifndef __RTD_TABLE_H_\n");
RTD_H_OutputFile.write("#define __RTD_TABLE_H_\n\n");
RTD_H_OutputFile.write("extern const LUT_F RTD_Table;\n\n");
RTD_H_OutputFile.write("#endif\n\n");


RTD_OutputFile.write("#include \"lut.h\"\n\n");

TemperatureVector = []
ResistanceVector = []


for Temperature in range(-100,200,1):
    TemperatureVector.append(Temperature)

    Correction = 0;    
    if(Temperature < 0):
        Correction = C*((Temperature-100)**3)
    
    R = R0*(1 + A*Temperature + B*(Temperature**2) + Correction);
    
    ResistanceVector.append(R)
    
    
RTD_OutputFile.write("const float TemperatureVector[" +  str(len(TemperatureVector)) + "] = {\n");

i =0;
for T in TemperatureVector:
    
    RTD_OutputFile.write(str(T));
    
    if i%32 == 31:
        RTD_OutputFile.write("\n")
    
    if (i != (len(TemperatureVector)-1)):
         RTD_OutputFile.write(",")   
    
    i = i + 1
    
RTD_OutputFile.write("};\n\n");        
        
RTD_OutputFile.write("const float ResistanceVector[" +  str(len(TemperatureVector)) + "] = {\n");

i =0;
for R in ResistanceVector:
    
    RTD_OutputFile.write(str(R));
    
    if i%32 == 31:
        RTD_OutputFile.write("\n")
    
    if (i != (len(TemperatureVector)-1)):
         RTD_OutputFile.write(",")   
    
    i = i + 1
    
RTD_OutputFile.write("};\n\n");        
    

RTD_OutputFile.write("const LUT_F RTD_Table = {\n");
RTD_OutputFile.write("sizeof(TemperatureVector)/sizeof(float),\n");
RTD_OutputFile.write("(float *)(ResistanceVector),\n");
RTD_OutputFile.write("(float *)(TemperatureVector),\n");
RTD_OutputFile.write("};\n");



RTD_OutputFile.close();
RTD_H_OutputFile.close();