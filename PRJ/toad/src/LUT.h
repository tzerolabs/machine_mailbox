#ifndef LUT_H
#define	LUT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <string.h>
#include <stdint.h>


typedef struct
{

    uint32_t TableLength;
    float *X;
    float *Y;

} LUT_F;


float Interplotate_LUT_F(const LUT_F *LUT, float X);


#ifdef	__cplusplus
}
#endif

#endif	/* LUT_H */