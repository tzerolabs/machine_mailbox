# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 22:12:26 2019

@author: Eli Hughes
"""
import numpy as np
import matplotlib.pyplot as plt

TableLength = 2048

f = open('hann.txt','w')

f.write('{\r\n')

HannTable = np.hanning(TableLength);

for i in range (0,TableLength):
    
    f.write('{:f}'.format(HannTable[i]))

    if(i%32 == 31):
        f.write(',\n');
    else:
        f.write(',')   
    
    if(i==(TableLength-1)):
        f.write('\n};')
    
f.close();
plt.plot(HannTable)
