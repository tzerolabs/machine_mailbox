/*
 * Copyright (c) 2018 Makaio GmbH
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <sys/ring_buffer.h>
#include <shell_rs485.h>
#include <init.h>
#include <logging/log.h>
#include <drivers/uart.h>

#include "toad_io.h"

LOG_MODULE_REGISTER(rs485_shell);

RING_BUF_DECLARE(rs485_pipe_in, 256);

const struct device      *RS485_UART;

#define MACHINE_MAILBOX_NODE DT_NODELABEL(machine_mailbox)

#define RS485_UART_LABEL    DT_PHANDLE(MACHINE_MAILBOX_NODE, mm_uart)

static void mm_ch_uart_cb_handler(const struct device *dev, void *app_data);

static volatile uint8_t IncomingChar;

volatile uint8_t * MM_TxOut;

volatile uint8_t MM_TxOutLen;

bool TxComplete;

static void mm_ch_uart_cb_handler(const struct device *dev, void *app_data)
{
   
        uint32_t Count = 0 ;
        int n;

        while (uart_irq_update(dev) && uart_irq_is_pending(dev))
        {
            if (uart_irq_rx_ready(dev)) 
            {
                 if((Count =uart_fifo_read(dev,(uint8_t *)&IncomingChar, 1)))
                {
                    ring_buf_put(&rs485_pipe_in,(const uint8_t *)&IncomingChar,1);
                }
            }

            if (uart_irq_tx_ready(dev))
            {
                if (MM_TxOutLen> 0)
                {
                    
                    n = uart_fifo_fill(dev, 
                                      (const uint8_t *)MM_TxOut,
                                       MM_TxOutLen);
                  
                    MM_TxOutLen -= n;
                    MM_TxOut += n;
                }

                if(MM_TxOutLen == 0 && uart_irq_tx_complete(RS485_UART))
                {
                    uart_irq_tx_disable(RS485_UART);
                    TxComplete = true;

                }
            }

            
        }
    }

SHELL_RS485_DEFINE(shell_transport_rs485);

SHELL_DEFINE(shell_rs485, "", &shell_transport_rs485,
	     0,
	     0,
	     SHELL_FLAG_OLF_CRLF);

static bool rs485_blocking;

static void timer_handler(struct k_timer *timer)
{
	const struct shell_rs485 *sh_rs485 = k_timer_user_data_get(timer);

    if(ring_buf_is_empty(&rs485_pipe_in) == 0)    
	{
    	sh_rs485->handler(SHELL_TRANSPORT_EVT_RX_RDY, sh_rs485->context);
    }

}

static int init(const struct shell_transport *transport,
		const void *config,
		shell_transport_handler_t evt_handler,
		void *context)
{
	struct shell_rs485 *sh_rs485 = (struct shell_rs485 *)transport->ctx;

	sh_rs485->handler = evt_handler;
	sh_rs485->context = context;

	k_timer_init(&sh_rs485->timer, timer_handler, NULL);
	k_timer_user_data_set(&sh_rs485->timer, (void *)sh_rs485);
	k_timer_start(&sh_rs485->timer, K_MSEC(10),K_MSEC(10));

	return 0;
}

static int uninit(const struct shell_transport *transport)
{
	struct shell_rs485 *sh_rs485 = (struct shell_rs485 *)transport->ctx;

	k_timer_stop(&sh_rs485->timer);

	return 0;
}

static int enable(const struct shell_transport *transport, bool blocking)
{
	struct shell_rs485 *sh_rs485 = (struct shell_rs485 *)transport->ctx;

	if (blocking) {
		rs485_blocking = true;
		k_timer_stop(&sh_rs485->timer);
	}

	return 0;
}

static int write(const struct shell_transport *transport,
		 const void *data, size_t length, size_t *cnt)
{
	struct shell_rs485 *sh_rs485 = (struct shell_rs485 *)transport->ctx;
	const uint8_t *data8 = (const uint8_t *)data;
   
    TX_EN_HIGH;
  
    TxComplete = false;
    
    
    MM_TxOutLen = length;
    
    MM_TxOut = (volatile uint8_t *)data8;
    
    uart_irq_tx_enable(RS485_UART);
  
     while(TxComplete == false)
     {
         k_sleep(K_USEC(100));
     }

    TX_EN_LOW;

    *cnt = length;
  
	sh_rs485->handler(SHELL_TRANSPORT_EVT_TX_RDY, sh_rs485->context);

	return 0;
}

static int read(const struct shell_transport *transport,
		void *data, size_t length, size_t *cnt)
{
    *cnt =ring_buf_get(&rs485_pipe_in,data,length);

	return 0;
}

const struct shell_transport_api shell_rs485_transport_api = {
	.init = init,
	.uninit = uninit,
	.enable = enable,
	.write = write,
	.read = read
};

static int enable_shell_rs485(const struct device *arg)
{
	ARG_UNUSED(arg);

    RS485_UART = device_get_binding(DT_LABEL(RS485_UART_LABEL));

    struct uart_config uart_cfg;
 
    uart_cfg.baudrate = 115200;

    uart_cfg.flow_ctrl = UART_CFG_FLOW_CTRL_NONE;

    uart_cfg.data_bits = UART_CFG_DATA_BITS_8;

	uart_cfg.parity = UART_CFG_PARITY_NONE;
    
	uart_cfg.stop_bits = UART_CFG_STOP_BITS_1;

    if (uart_configure(RS485_UART, &uart_cfg) != 0) 
    {
	//	LOG_ERR("Failed to configure UART");
	}

    uart_irq_callback_user_data_set(RS485_UART, mm_ch_uart_cb_handler, NULL);

    uart_irq_rx_enable(RS485_UART);

	//bool log_backend = CONFIG_SHELL_RS485_INIT_LOG_LEVEL > 0;
//	uint32_t level = (CONFIG_SHELL_RS485_INIT_LOG_LEVEL > LOG_LEVEL_DBG) ?
	//	      CONFIG_LOG_MAX_LEVEL : CONFIG_SHELL_RS485_INIT_LOG_LEVEL;

    struct shell_backend_config_flags cfg_flags;
    shell_init(&shell_rs485, 0,cfg_flags,false,0);

	return 0;
}

/* Function is used for testing purposes */
const struct shell *shell_backend_rs485_get_ptr(void)
{
	return &shell_rs485;
}

SYS_INIT(enable_shell_rs485, POST_KERNEL, 0);
