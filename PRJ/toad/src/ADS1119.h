#ifndef _ADS1119_H
#define _ADS1119_H

int16_t ADS1119_ReadData(uint8_t I2C_Address);
uint8_t ADS1119_WriteConfigReg(uint8_t I2C_Address, uint8_t Data);
uint8_t ADS1119_Reset(uint8_t I2C_Address);
uint8_t ADS1119_StartSync(uint8_t I2C_Address);
void ADS1119_Init();

#define ADS1119_CONFIG_BIT_CM   0x02

#define ADS1119_CONFIG_MUX_SINGLE_ENDED(x)  (((x+3)&0x7)<<5)



#endif