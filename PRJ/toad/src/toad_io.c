#include <zephyr.h>

#include <logging/log.h>
#include "toad_io.h"
#include "nrf9160_bitfields.h"
#include "NRF9160.h"
#include "Si5351.h"
#include "Hann.h"
#include "sensor_types.h"
#include "math.h"
#include "ADS1119.h"
#include "arm_math.h"
#include "rt_config.h"
#include <cJSON.h>
#include <shell/shell.h>
#include "stdio.h"


LOG_MODULE_REGISTER(toad_io,LOG_LEVEL_INF);

#define SIM_1G_SIGNAL            (0)

#if SIM_1G_SIGNAL == 1
    #define ACCEL_SCALE(Channel) (1.0/32768.0)
#else
    #define ACCEL_SCALE(Channel) ((2.5 *1000.0)/ (32768.0 * (float)(g_a[Channel])))
#endif

#define TD_RMS                  (0)


#define VIB_CAPTURE_BUFFER_SIZE   16384
#define VIB_ANALYSIS_WINDOW_SIZE   2048
#define VIB_OVERLAP_STEP           512
#define NUM_AVG_STEPS             ((VIB_CAPTURE_BUFFER_SIZE-VIB_ANALYSIS_WINDOW_SIZE) / VIB_OVERLAP_STEP) 


#define VIB_SAMPLE_RATE            8192
#define VIB_CLOCK_FACTOR           512      //We are going to use the 

static volatile int16_t  CaptureBuffer[VIB_CAPTURE_BUFFER_SIZE];

static float    AnalysisWindow[VIB_ANALYSIS_WINDOW_SIZE];
static float    FFT_Out[VIB_ANALYSIS_WINDOW_SIZE];
static volatile float    FFT_MagSqOut[VIB_ANALYSIS_WINDOW_SIZE/2];

volatile Sensor__PSD_1024 MyPSDs[4];
static volatile bool ADC_BufferReady = false;

struct k_mutex PSD_Mutex[4];

volatile uint32_t Count = 0;

static int8_t Out[4];
static int8_t In[4];

static arm_rfft_fast_instance_f32  MyFFT;
static uint32_t StartIndex = 0;
static uint32_t NumAverages = 0;
static uint32_t CurrentVibChannel = 0;
    
float G_RMS[4];

K_THREAD_STACK_DEFINE(adc_capture_stack_area, 512);

struct k_thread adc_capture_thread_data;

static void adc_capture_thread(void *a, void *b, void *c);

#define GPIOTE_NODE DT_INST(0, nordic_nrf_gpiote)

#define JACK12_ADC_I2C  0x40
#define JACK34_ADC_I2C  0x41

int16_t RawVoltage[4];
int16_t ScaledVoltage[4];
int16_t CalVoltage[4];

int16_t Temperatures[4];
uint16_t FourToTwenty[4];

struct k_mutex TempCalMutex;

int32_t v_a[4];
int32_t v_b[4];

int32_t t_a[4];
int32_t t_b[4];

int32_t g_a[4];


RT_CONFIG_ITEM(v1_a,"v1_a",&v_a[0],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");
RT_CONFIG_ITEM(v2_a,"v2_a",&v_a[1],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");
RT_CONFIG_ITEM(v3_a,"v3_a",&v_a[2],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");
RT_CONFIG_ITEM(v4_a,"v4_a",&v_a[3],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");

RT_CONFIG_ITEM(v1_b,"v1_b",&v_b[0],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-20000000","20000000","0");
RT_CONFIG_ITEM(v2_b,"v2_b",&v_b[1],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-20000000","20000000","0");
RT_CONFIG_ITEM(v3_b,"v3_b",&v_b[2],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-20000000","20000000","0");
RT_CONFIG_ITEM(v4_b,"v4_b",&v_b[3],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-20000000","20000000","0");

RT_CONFIG_ITEM(t1_a,"t1_a",&t_a[0],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");
RT_CONFIG_ITEM(t2_a,"t2_a",&t_a[1],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");
RT_CONFIG_ITEM(t3_a,"t3_a",&t_a[2],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");
RT_CONFIG_ITEM(t4_a,"t4_a",&t_a[3],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100000");

RT_CONFIG_ITEM(t1_b,"t1_b",&t_b[0],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-200000000","200000000","0");
RT_CONFIG_ITEM(t2_b,"t2_b",&t_b[1],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-200000000","200000000","0");
RT_CONFIG_ITEM(t3_b,"t3_b",&t_b[2],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-200000000","200000000","0");
RT_CONFIG_ITEM(t4_b,"t4_b",&t_b[3],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"-200000000","200000000","0");


RT_CONFIG_ITEM(g1_a,"g1_a",&g_a[0],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100");
RT_CONFIG_ITEM(g2_a,"g2_a",&g_a[1],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100");
RT_CONFIG_ITEM(g3_a,"g3_a",&g_a[2],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100");
RT_CONFIG_ITEM(g4_a,"g4_a",&g_a[3],RT_CONFIG_DATA_TYPE_INT32,sizeof(int32_t),"0","200000","100");


void Toad_GrabSlowADC();

int16_t Toad_GetTempChannel(uint8_t Channel)
{
    if(Channel>4)
        Channel = 4;

    return Temperatures[Channel];
}

uint16_t Toad_GetFourToTwentyChannel(uint8_t Channel)
{
    if(Channel>4)
        Channel = 4;

    return FourToTwenty[Channel];
}

uint16_t Toad_GetGRMS_Channel(uint8_t Channel)
{
    if(Channel>4)
        Channel = 4;

    uint32_t GRMS = G_RMS[Channel] * 1000;

    if (GRMS>65355)
    {
        GRMS = 65535;
    }

    return (uint16_t)GRMS;
}

void ADC_SelectChannel(uint8_t Channel)
{
  
    switch(Channel)
    {
        default:
        case 0: ICP_SEL1_LOW;ICP_SEL0_LOW; break;
        case 1: ICP_SEL1_LOW;ICP_SEL0_HIGH; break;
        case 2: ICP_SEL1_HIGH;ICP_SEL0_LOW; break;
        case 3: ICP_SEL1_HIGH;ICP_SEL0_HIGH; break;
    }

}

void StartADC_SPI(uint8_t Channel)
{
    ADC_SelectChannel(Channel);
    k_sleep(K_MSEC(50));
    Count = 0;
    ADC_BufferReady = false;
    NRF_GPIOTE1_NS->EVENTS_IN[0]=0;
    irq_enable(GPIOTE0_IRQn);
}

void StopADC_SPI()
{
    Count = 0;
    irq_disable(GPIOTE0_IRQn);
}

bool ADC_BufferIsReady()
{
    return ADC_BufferReady;
}

const int16_t SimSignal[] = {0,23169,32767,23169,0,-23170,-32768,-23170};
static uint32_t SimIdx =0 ;

typedef union {

    uint8_t bytes[2];
    int16_t signed_word;

} signed_word_union;

ISR_DIRECT_DECLARE(DRDY_Callback)
{
    NRF_GPIOTE1_NS->EVENTS_IN[0]=0;
    
  //  CaptureBuffer[Count] = SimSignal[SimIdx];

    signed_word_union tmpu;

    tmpu.bytes[0] = In[1];
    tmpu.bytes[1] = In[0];


    #if SIM_1G_SIGNAL == 1
        CaptureBuffer[Count] = SimSignal[SimIdx];
    #else
        CaptureBuffer[Count] = tmpu.signed_word;
    #endif

    SimIdx++;
    if(SimIdx>= sizeof(SimSignal)/sizeof(int16_t))
        SimIdx = 0;

    Count++;

    if(Count>=VIB_CAPTURE_BUFFER_SIZE)
    {
        ADC_BufferReady = true;
        StopADC_SPI();
        return 0;
    }

    NRF_SPIM2_NS->TASKS_START =1;

    return 0;
}


void SetupADC_SPI()
{
    NRF_SPIM2_NS->ENABLE = 0;
    NRF_SPIM2_NS->PSEL.MOSI = TOAD_ADC_MOSI_PIN;
    NRF_SPIM2_NS->PSEL.MISO = TOAD_ADC_MISO_PIN;
    NRF_SPIM2_NS->PSEL.SCK =  TOAD_ADC_SCK_PIN;
    NRF_SPIM2_NS->FREQUENCY = SPIM_FREQUENCY_FREQUENCY_M8;

    NRF_SPIM2_NS->TXD.PTR = (uint32_t)&Out;
    NRF_SPIM2_NS->TXD.MAXCNT = 3;
    
    NRF_SPIM2_NS->RXD.PTR = (uint32_t)&In;
        
    NRF_SPIM2_NS->RXD.MAXCNT =3;
    NRF_SPIM2_NS->ENABLE = 7;

    NRF_P0_NS->PIN_CNF[TOAD_DRDY_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos) | GPIO_PIN_CNF_INPUT_Msk;

    NRF_GPIOTE1_NS->CONFIG[0] = (GPIOTE_CONFIG_MODE_Event<<GPIOTE_CONFIG_MODE_Pos) |
                                (GPIOTE_CONFIG_POLARITY_HiToLo << GPIOTE_CONFIG_POLARITY_Pos) |
                                (TOAD_DRDY_PIN<<GPIOTE_CONFIG_PSEL_Pos);
  
    NRF_GPIOTE1_NS->INTENSET = (1<<0);
      
    IRQ_DIRECT_CONNECT(DT_IRQN(GPIOTE_NODE), 0, DRDY_Callback, IRQ_ZERO_LATENCY);

    k_thread_create(&adc_capture_thread_data, adc_capture_stack_area,
                    K_THREAD_STACK_SIZEOF(adc_capture_stack_area),
                    adc_capture_thread,
                    NULL, NULL, NULL,
                    K_LOWEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);


}

void toad_io_init()
{
    /*
        We are going to manual initialize a few of the IO
        to make PCB bringup simpler
    */    
    NRF_P0_NS->PIN_CNF[V0_PIN] = (GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos) |
                                  GPIO_PIN_CNF_INPUT_Msk;

    NRF_P0_NS->PIN_CNF[V1_PIN] = (GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos) |
                                  GPIO_PIN_CNF_INPUT_Msk;

    NRF_P0_NS->PIN_CNF[V2_PIN] = (GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos) |
                                  GPIO_PIN_CNF_INPUT_Msk;


    NRF_P0_NS->PIN_CNF[nCS0_TEMP_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);
    NRF_P0_NS->PIN_CNF[nCS1_TEMP_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);
    NRF_P0_NS->PIN_CNF[nCS2_TEMP_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);
    NRF_P0_NS->PIN_CNF[nCS3_TEMP_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);

    NRF_P0_NS->DIR |= (1<<nCS0_TEMP_PIN);
    NRF_P0_NS->DIR |= (1<<nCS1_TEMP_PIN);
    NRF_P0_NS->DIR |= (1<<nCS2_TEMP_PIN);
    NRF_P0_NS->DIR |= (1<<nCS3_TEMP_PIN);

    nCS0_TEMP_HIGH;
    nCS1_TEMP_HIGH;
    nCS2_TEMP_HIGH;
    nCS3_TEMP_HIGH;

    NRF_P0_NS->PIN_CNF[ICP_SEL0_PIN] = (GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos);
    NRF_P0_NS->PIN_CNF[ICP_SEL1_PIN] = (GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos);

    NRF_P0_NS->DIR |= (1<<ICP_SEL0_PIN);
    NRF_P0_NS->DIR |= (1<<ICP_SEL1_PIN);    

    ICP_SEL0_LOW;
    ICP_SEL1_LOW;

    NRF_P0_NS->PIN_CNF[ADC_MODE_PIN] = 0;// Float the mode pin for high resolution

    NRF_P0_NS->PIN_CNF[TX_EN_PIN] = 0;// extern pulldown to TX_EN
    NRF_P0_NS->DIR |= (1<<TX_EN_PIN);   
    TX_EN_LOW;

    NRF_P0_NS->PIN_CNF[FLASH_CS_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);
    NRF_P0_NS->DIR |= (1<<FLASH_CS_PIN);
    NRF_P0_NS->PIN_CNF[RAM_CS_PIN] = (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos);
    NRF_P0_NS->DIR |= (1<<RAM_CS_PIN);
    
    RAM_CS_HIGH;
    FLASH_CS_HIGH;

    NRF_P0_NS->DIR |= (1<<ADC_SYNC_PIN);

    ADC_SYNC_HIGH;

    Si5351__init(SI5351_CRYSTAL_LOAD_10PF, 25000000, 0);

    Si5351__set_freq((uint64_t )VIB_SAMPLE_RATE*(uint64_t )VIB_CLOCK_FACTOR*(uint64_t )100, SI5351_CLK0);
  
    Si5351__set_freq((uint64_t )VIB_SAMPLE_RATE*(uint64_t )VIB_CLOCK_FACTOR*(uint64_t )100/4, SI5351_CLK1);

    SetupADC_SPI();

    k_mutex_init(&TempCalMutex);

    for(int i=0;i<4;i++)
    {
        
        k_mutex_init(&PSD_Mutex[i]);

        MyPSDs[i].SensorType = VAL_TOAD_PSD_1024;
        MyPSDs[i].JackID = i+1;
        MyPSDs[i].SampleRateHz = VIB_SAMPLE_RATE;
        MyPSDs[i].BinSpaceHz = VIB_SAMPLE_RATE / VIB_ANALYSIS_WINDOW_SIZE;
        MyPSDs[i].StartBinHz = 0;

        for(int j=0;j<1024;j++)
        {
            MyPSDs[i].dB_G_Bins[j] = -128;
        }
    }

    ADS1119_Init();
    ADS1119_Reset(JACK12_ADC_I2C);
    ADS1119_StartSync(JACK12_ADC_I2C);
    ADS1119_Reset(JACK34_ADC_I2C);
    ADS1119_StartSync(JACK34_ADC_I2C);
    ADS1119_WriteConfigReg(JACK12_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(0) ); //contiuous conversion;
    ADS1119_WriteConfigReg(JACK34_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(0) ); //contiuous conversion;
   
}



static void adc_capture_thread(void *a, void *b, void *c)
{

    arm_rfft_fast_init_f32(&MyFFT,2048);	
      
      
    while(1)
    {
        LOG_INF("Starting channel %d",(CurrentVibChannel+1));
        
        StartADC_SPI(CurrentVibChannel);

        while(ADC_BufferIsReady() == false)
        {
            k_sleep(K_MSEC(500));      
        }
        
        for(int i=0;i<(VIB_ANALYSIS_WINDOW_SIZE/2);i++)
        {
            FFT_MagSqOut[i] = 0;
        }
        
        NumAverages = 0;
        StartIndex = 0;   
    
        float GRMS_Temp = 0;

        #if TD_RMS == 1

            float Tmp2;

            for(int i=0; i<VIB_CAPTURE_BUFFER_SIZE ; i++)
            {
                Tmp2 = (float)CaptureBuffer[i]  * ACCEL_SCALE(CurrentVibChannel);
                GRMS_Temp += Tmp2 * Tmp2;
            }
            
            G_RMS[CurrentVibChannel] =  sqrtf(GRMS_Temp/VIB_CAPTURE_BUFFER_SIZE);

        #endif 

        for(int j=0; j< NUM_AVG_STEPS;j++)
        {
            for(int i=0; i<VIB_ANALYSIS_WINDOW_SIZE ; i++)
            {
                 AnalysisWindow[i] = Hann2048_f32[i] * (float)CaptureBuffer[i+StartIndex] * ACCEL_SCALE(CurrentVibChannel);
            }
                       
            arm_rfft_fast_f32(&MyFFT,&AnalysisWindow[0],&FFT_Out[0],0);    

            for(int i=0;i<(VIB_ANALYSIS_WINDOW_SIZE/2);i++)
            {
                FFT_MagSqOut[i] += (FFT_Out[i*2]*FFT_Out[i*2] + FFT_Out[(i*2)+1]*FFT_Out[(i*2)+1])*(16.0)/(2048.0*2048.0);
            }

            k_sleep(K_MSEC(50));

            StartIndex += VIB_OVERLAP_STEP;
            NumAverages++;

            //Handle the case if the settings up with a odd sized buffer at the end
            if(StartIndex > (VIB_CAPTURE_BUFFER_SIZE - VIB_OVERLAP_STEP))
            {
                break;
            }
        }

        float LogTmp = 0;

        k_mutex_lock(&PSD_Mutex[CurrentVibChannel], K_FOREVER);
    
        GRMS_Temp = 0;

        for(int i=0;i<(VIB_ANALYSIS_WINDOW_SIZE/2);i++)
        {
            FFT_MagSqOut[i] = FFT_MagSqOut[i]/NumAverages;

            #if TD_RMS == 0

                if(i>3)
                {
                  GRMS_Temp += FFT_MagSqOut[i];
                }

            #endif

            LogTmp = (10 * logf(FFT_MagSqOut[i])) + 0.5;

            if(LogTmp < INT8_MIN)
            {
                LogTmp = INT8_MIN;
            }
            else if(LogTmp > INT8_MAX)
            {
                LogTmp = INT8_MAX;
            }
            else if (!isfinite(LogTmp))
            {
                LogTmp = LogTmp + 0.5;
            }

            MyPSDs[CurrentVibChannel].dB_G_Bins[i] = LogTmp;

        }
        
        k_mutex_unlock(&PSD_Mutex[CurrentVibChannel]);
        
        #if TD_RMS == 0
            G_RMS[CurrentVibChannel] = sqrtf(GRMS_Temp) * 0.5775078090717444;  //Adjust for the window gain
        #endif

        CurrentVibChannel++;
        if(CurrentVibChannel >= 4)
        {
            CurrentVibChannel = 0;
        }

    }
}

int16_t ScaleADC_Voltage(int16_t ADC_In)
{
    int16_t ADC_Scaled = (int16_t)((int32_t)ADC_In * (int32_t)5 / (int32_t)8);
   
    return (int16_t)(ADC_Scaled);
}

int16_t CalADC_Voltage(uint8_t Channel, int16_t ScaledVoltageIn)
{
    if(Channel > 3)
    {
        Channel = 0;
    }
  
    int64_t Scale =   (int64_t)v_a[Channel] * (int64_t)ScaledVoltageIn + (int64_t)v_b[Channel];

    Scale = (Scale +(100000/2))/100000;   

     return (int16_t)(Scale);
}

int16_t CalTemperature(uint8_t Channel, int16_t ScaledTempIn)
{
    if(Channel > 3)
    {
        Channel = 0;
    }
  
    int64_t Scale =   (int64_t)t_a[Channel] * (int64_t)ScaledTempIn + (int64_t)t_b[Channel];

    Scale = (Scale +(100000/2))/100000;   

     return (int16_t)(Scale);
}


int16_t ScaleADC_FourTwenty(int16_t ADC_In)
{
    int32_t Tmp;

    Tmp = (((int32_t)(ADC_In)  * (int32_t)5000)/(8*499));
    
    if(Tmp<0){Tmp=0;}
    
     return   (int16_t)(Tmp);
}


void Toad_GrabSlowADC()
{
    k_mutex_lock(&TempCalMutex, K_FOREVER);

    ADS1119_WriteConfigReg(JACK12_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(0)); 
    ADS1119_WriteConfigReg(JACK34_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(0)); 
    k_sleep(K_MSEC(100));        
     
    RawVoltage[0] = ADS1119_ReadData(JACK12_ADC_I2C);
    RawVoltage[2] = ADS1119_ReadData(JACK34_ADC_I2C);
    
    ScaledVoltage[0] = ScaleADC_Voltage(RawVoltage[0]);
    ScaledVoltage[2] = ScaleADC_Voltage(RawVoltage[2]);
    
    CalVoltage[0] = CalADC_Voltage(0,ScaledVoltage[0]);
    CalVoltage[2] = CalADC_Voltage(2,ScaledVoltage[2]);
    
    Temperatures[0] =   CalTemperature(0,CalVoltage[0]);           
    Temperatures[2] =   CalTemperature(2,CalVoltage[2]);         

    ADS1119_WriteConfigReg(JACK12_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(2)); 
    ADS1119_WriteConfigReg(JACK34_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(2)); 
    k_sleep(K_MSEC(100));        
     

    RawVoltage[1] = ADS1119_ReadData(JACK12_ADC_I2C);
    RawVoltage[3] = ADS1119_ReadData(JACK34_ADC_I2C);

    ScaledVoltage[1] = ScaleADC_Voltage(RawVoltage[1]);
    ScaledVoltage[3] = ScaleADC_Voltage(RawVoltage[3]);
    
    CalVoltage[1] = CalADC_Voltage(1,ScaledVoltage[1]);
    CalVoltage[3] = CalADC_Voltage(3,ScaledVoltage[3]);
    
    Temperatures[1] =   CalTemperature(1,CalVoltage[1]);          
    Temperatures[3] =   CalTemperature(3,CalVoltage[3]);       



    ADS1119_WriteConfigReg(JACK12_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(1)); 
    ADS1119_WriteConfigReg(JACK34_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(1)); 
    k_sleep(K_MSEC(100));    
     
    FourToTwenty[0] = ScaleADC_FourTwenty(ADS1119_ReadData(JACK12_ADC_I2C));
    FourToTwenty[2] = ScaleADC_FourTwenty(ADS1119_ReadData(JACK34_ADC_I2C));          

    ADS1119_WriteConfigReg(JACK12_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(3)); 
    ADS1119_WriteConfigReg(JACK34_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(3)); 
    k_sleep(K_MSEC(100));        
    
    FourToTwenty[1] = ScaleADC_FourTwenty(ADS1119_ReadData(JACK12_ADC_I2C));
    FourToTwenty[3] = ScaleADC_FourTwenty(ADS1119_ReadData(JACK34_ADC_I2C));          
         
    k_mutex_unlock(&TempCalMutex);
    
}


void Toad_CalTemperatures(int16_t TargetTemp, int16_t *AdjArray)
{
  /*  k_mutex_lock(&TempCalMutex, K_FOREVER);

    ADS1119_WriteConfigReg(JACK12_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(0)); 
    ADS1119_WriteConfigReg(JACK34_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(0)); 
    k_sleep(K_MSEC(50));        
    
    AdjArray[0] = TargetTemp - ScaleADC_Temperature(0,ADS1119_ReadData(JACK12_ADC_I2C));            
    AdjArray[2] = TargetTemp - ScaleADC_Temperature(2,ADS1119_ReadData(JACK34_ADC_I2C));            

    ADS1119_WriteConfigReg(JACK12_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(2)) ; 
    ADS1119_WriteConfigReg(JACK34_ADC_I2C, ADS1119_CONFIG_BIT_CM | ADS1119_CONFIG_MUX_SINGLE_ENDED(2)); 
    k_sleep(K_MSEC(50));        

    AdjArray[1] = TargetTemp - ScaleADC_Temperature(1,ADS1119_ReadData(JACK12_ADC_I2C));            
    AdjArray[3] = TargetTemp - ScaleADC_Temperature(2,ADS1119_ReadData(JACK34_ADC_I2C));              

    k_mutex_unlock(&TempCalMutex);
    */

}




cJSON * Toad_GetReadJSON()
{
    int Temps[4];
    int FourTwenties[4];
    int Gs[4];
    int ScaledVoltages[4];
    int RawVoltages[4];
    int CalVoltages[4];
    cJSON *Result = cJSON_CreateObject();
    
    cJSON_AddStringToObject(Result, "ObjID", "Read");
    cJSON_AddStringToObject(Result, "Result", "OK");

    for(int i=0;i<4;i++)
    {
        Temps[i] = Toad_GetTempChannel(i);
        Gs[i] = Toad_GetGRMS_Channel(i);
        FourTwenties[i] = Toad_GetFourToTwentyChannel(i);
        ScaledVoltages[i] = ScaledVoltage[i];
        CalVoltages[i] = CalVoltage[i];
        RawVoltages[i]= RawVoltage[i];
    }

    cJSON_AddItemToObject(Result, "RawVoltages", cJSON_CreateIntArray(RawVoltages, 4));
    cJSON_AddItemToObject(Result, "ScaledVoltages", cJSON_CreateIntArray(ScaledVoltages, 4));
    cJSON_AddItemToObject(Result, "CalVoltages", cJSON_CreateIntArray(CalVoltages, 4));
 
    cJSON_AddItemToObject(Result, "Temperatures", cJSON_CreateIntArray(Temps, 4));
    cJSON_AddItemToObject(Result, "Accels", cJSON_CreateIntArray(Gs, 4));
    cJSON_AddItemToObject(Result, "FourTwenties", cJSON_CreateIntArray(FourTwenties, 4));


    return Result;
}

static int read_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

        char *string = NULL;
        
        cJSON *Result = Toad_GetReadJSON();

        string = cJSON_Print(Result);
        shell_fprintf(shell, SHELL_NORMAL ,"\x02");
        shell_fprintf(shell, SHELL_NORMAL ,"%s",string);
        shell_fprintf(shell, SHELL_NORMAL ,"\x03\r\n");
        cJSON_free(string);
        cJSON_Delete(Result);
        
        return 0;
}


SHELL_CMD_REGISTER(read, NULL, "", read_handler);


static int clear_tcal_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);
        
        
        for(int i=0;i<4;i++)
        {
            t_a[0] = 100000;
            t_b[0] = 0;
            
        }

        shell_print(shell,"Clearing voltage calibration");

        Toad_GrabSlowADC();

        rt_config_export();

        read_handler(shell,0,NULL);

        return 0;
}



static int clear_vcal_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);
        
        
        for(int i=0;i<4;i++)
        {
            v_a[i] = 100000;
            v_b[i] = 0;
            
        }

        shell_print(shell,"Clearing voltage calibration");

        Toad_GrabSlowADC();

        rt_config_export();

        read_handler(shell,0,NULL);

        return 0;
}


SHELL_CMD_REGISTER(clear_tcal, NULL, "Clear temperature calibration", clear_tcal_handler);
SHELL_CMD_REGISTER(clear_vcal, NULL, "Clear voltage input calibration", clear_vcal_handler);
