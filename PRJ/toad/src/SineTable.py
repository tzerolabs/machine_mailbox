# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 22:12:26 2019

@author: Eli Hughes
"""
import numpy as np
import matplotlib as plt

TableLength = 8

SineTable = np.zeros(TableLength)

f = open('sinetable.txt','w')

f.write('{\r\n')


for i in range (0,TableLength):
    
    SineTable[i] = np.sin(i*np.pi*2/TableLength)
    
    f.write('{:d}'.format(int(SineTable[i]*32767.5 - 0.5)))
    if(i%32 == 31):
        f.write(',\n');
    else:
        f.write(',')   
    
    if(i==(TableLength-1)):
        f.write('\n};')
    
f.close();

plt.plot(SineTable)