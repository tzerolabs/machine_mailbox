#include "LUT.h"
#include "stdlib.h"


float Interplotate_LUT_F(const LUT_F *LUT, float X)
{
    uint32_t i;
    float Slope,dX,dY,Result;

    //See if the table is of useful length

    if( LUT->TableLength == 0)
        return 0.0;

    if( LUT->TableLength == 1)
        return LUT->Y[0];

    /* First Check the boundaries*/

    if(X <= LUT->X[0])  // if the input is below our lowest table value,  use the 1st 2 points
        i = 0;
    else if(X >= LUT->X[LUT->TableLength-1])  //if the input is higher than the maximum X value in the the table, then we will use the last 2 points to estimate
        i = LUT->TableLength-2;
    else //Scan through the table to see what pair we need
        {
            for(i=0; i<LUT->TableLength - 1; i++)
                {
                    if(X >= LUT->X[i] && X <= LUT->X[i+1])
                        {
                            break;
                        }
                }
        }

    //The variable i contains the starting index of the pair
    //Calculate the slope between the 2 points
    Slope = (LUT->Y[i + 1] - LUT->Y[i]) / (LUT->X[i + 1] - LUT->X[i]);
    dX = X - LUT->X[i];
    dY = dX * Slope;
    Result = LUT->Y[i] + dY;
    return Result;
}

