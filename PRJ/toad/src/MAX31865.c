#include "LUT.h"
#include "RTD_Table.h"
#include "MAX31865.h"

void MAX31865_Configure();


/***
 *      _   _    _    _
 *     | | | |  / \  | |
 *     | |_| | / _ \ | |
 *     |  _  |/ ___ \| |___
 *     |_| |_/_/   \_\_____|
 *
 */

#ifndef MAX31865_CS_ACTIVE
    #define MAX31865_CS_ACTIVE    
#endif

#ifndef MAX31865_CS_INACTIVE
    #define MAX31865_CS_INACTIVE    
#endif

#define MAX31865_nDRDY__IS_ACTIVE 1

void MAX31865_Init()
{
    MAX31865_CS_INACTIVE;

    MAX31865_PowerOn();

    MAX31865_Configure();
}

uint8_t MAX31865_DataIsReady()
{
   return MAX31865_nDRDY__IS_ACTIVE;
}

uint8_t MAX31865_SPI_ReadWrite(uint8_t DataOut)
{


	//	while ( (LPC_SPI0->STAT & SPI_STAT_TXRDY) == 0 );
	//	LPC_SPI0->TXDATCTL = SPI_CTL_LEN(8) | SPI_CTL_EOT | DataOut;
	//	while ( (LPC_SPI0->STAT & SPI_STAT_RXRDY) == 0 );
		return 0;

}

void MAX31865_PowerOn()
{
}

void MAX31865_PowerOff()
{
}



/***
 *      ____  _                 _     _
 *     |  _ \| |_   _ _ __ ___ | |__ (_)_ __   __ _
 *     | |_) | | | | | '_ ` _ \| '_ \| | '_ \ / _` |
 *     |  __/| | |_| | | | | | | |_) | | | | | (_| |
 *     |_|   |_|\__,_|_| |_| |_|_.__/|_|_| |_|\__, |
 *                                            |___/
 */


static uint8_t RTD_Type = RTD_TYPE_100_AMERICAN_ALPHA_003920;
static float RTD_ReferenceResistor = 402.0;
const LUT_F *MyRTDTable = &RTD_Table;



uint8_t MAX31865_ReadRegister(uint8_t Address)
{
    uint8_t Data;
    MAX31865_CS_ACTIVE;
    MAX31865_SPI_ReadWrite(Address); //Send out the Address
    Data = MAX31865_SPI_ReadWrite(0); //Get the Data
    MAX31865_CS_INACTIVE;
    return Data;
}


void MAX31865_WriteRegister(uint8_t Address, uint8_t Data)
{
    MAX31865_CS_ACTIVE;
    MAX31865_SPI_ReadWrite(Address); //Send out the Address
    MAX31865_SPI_ReadWrite(Data); //Send the Data
    MAX31865_CS_INACTIVE;
}


/*
   Function: MAX31865_GetIntegerTemperature
 Gets the temperature from the MAX31865.   It assumes a 400ohm reference resistor.
 This function uses the ADC CODE/32-256  approximation described on page 19 of the MAX3185 datasheet 19-6478; Rev 0; 10/12.
   Parameters:
 None
   Returns:
    A signed integer representing termperature in degrees C.  Scaling is 1C per bit.
*/

int16_t MAX31865_GetIntegerTemperature()
{
    uint16_t T;
    T = (uint16_t)(MAX31865_ReadRegister(MAX31865_REG_RTD_MSB_READ))<<8;
    T |= (uint16_t)(MAX31865_ReadRegister(MAX31865_REG_RTD_LSB_READ));
    //Shift by one to get the 15 value.  Then do the divide by 32 substract by 256
    return (((int16_t)T)>>6)-256;
}

void MAX31865_Configure()
{
 
    //Set VBIAS On, Conversion Mode Auto, 3 wire mode 60Hz filter.
    MAX31865_WriteRegister(MAX31865_REG_CONFIGURATION_WRITE , 0xE0);

    MAX31865_SetupRTD(402.0f,RTD_TYPE_100_EUROPEAN_ALPHA_003850);
}



void MAX31865_SetupRTD(float ReferenceResistor,uint8_t Type)
{
    RTD_ReferenceResistor = ReferenceResistor;
    RTD_Type = Type;

}

uint16_t T_Temp;
float R_Temp;

float MAX31865_GetResistance()
{
    T_Temp = (uint16_t)(MAX31865_ReadRegister(MAX31865_REG_RTD_MSB_READ))<<8;
    
    T_Temp |= (uint16_t)(MAX31865_ReadRegister(MAX31865_REG_RTD_LSB_READ));
   
    T_Temp = T_Temp>>1;

    R_Temp = ((float)(T_Temp)/32768.0) * RTD_ReferenceResistor;
  

    return R_Temp;
}

float MAX31865_GetTemperature()
{
    float R;
    R = MAX31865_GetResistance();
    
    return Interplotate_LUT_F(MyRTDTable , R);
}

