#include "stdint.h"
#include "sensor_types.h"

#ifndef _toad_IO_H
#define _toad_IO_H

void toad_io_init();

void Toad_GrabSlowADC();

#define TOAD_DRDY_PIN      18
#define TOAD_ADC_MOSI_PIN  20
#define TOAD_ADC_MISO_PIN  16
#define TOAD_ADC_SCK_PIN   19

/*
    Move to Device tree....
*/

#define V0_PIN  01
#define V1_PIN  02
#define V2_PIN  12

#define nCS0_TEMP_PIN   5
#define nCS1_TEMP_PIN   6
#define nCS2_TEMP_PIN   7
#define nCS3_TEMP_PIN   9

#define nCS0_TEMP_HIGH   NRF_P0_NS->OUTSET = (1<<nCS0_TEMP_PIN)
#define nCS1_TEMP_HIGH   NRF_P0_NS->OUTSET = (1<<nCS1_TEMP_PIN)
#define nCS2_TEMP_HIGH   NRF_P0_NS->OUTSET = (1<<nCS2_TEMP_PIN)
#define nCS3_TEMP_HIGH   NRF_P0_NS->OUTSET = (1<<nCS3_TEMP_PIN)

#define nCS0_TEMP_LOW   NRF_P0_NS->OUTCLR = (1<<nCS0_TEMP_PIN)
#define nCS1_TEMP_LOW   NRF_P0_NS->OUTCLR = (1<<nCS1_TEMP_PIN)
#define nCS2_TEMP_LOW   NRF_P0_NS->OUTCLR = (1<<nCS2_TEMP_PIN)
#define nCS3_TEMP_LOW   NRF_P0_NS->OUTCLR = (1<<nCS3_TEMP_PIN)

#define ICP_SEL0_PIN    14
#define ICP_SEL1_PIN    13

#define ICP_SEL0_HIGH    NRF_P0_NS->OUTSET = (1<<ICP_SEL0_PIN)
#define ICP_SEL0_LOW     NRF_P0_NS->OUTCLR = (1<<ICP_SEL0_PIN)

#define ICP_SEL1_HIGH    NRF_P0_NS->OUTSET = (1<<ICP_SEL1_PIN)
#define ICP_SEL1_LOW     NRF_P0_NS->OUTCLR = (1<<ICP_SEL1_PIN)

#define ADC_MODE_PIN    15

#define ADC_MODE_HIGH     NRF_P0_NS->OUTSET = (1<<ADC_MODE_PIN)
#define ADC_MODE_LOW      NRF_P0_NS->OUTCLR = (1<<ADC_MODE_PIN)

#define ADC_SYNC_PIN    17

#define ADC_SYNC_HIGH     NRF_P0_NS->OUTSET = (1<<ADC_SYNC_PIN)
#define ADC_SYNC_LOW      NRF_P0_NS->OUTCLR = (1<<ADC_SYNC_PIN)


#define TX_EN_PIN       21
#define TX_EN_HIGH       NRF_P0_NS->OUTSET = (1<<TX_EN_PIN)
#define TX_EN_LOW        NRF_P0_NS->OUTCLR = (1<<TX_EN_PIN)


#define FLASH_CS_PIN    28
#define RAM_CS_PIN      29

#define FLASH_CS_HIGH  NRF_P0_NS->OUTSET= (1<<FLASH_CS_PIN)
#define FLASH_CS_LOW   NRF_P0_NS->OUTCLR = (1<<FLASH_CS_PIN)

#define RAM_CS_HIGH  NRF_P0_NS->OUTSET= (1<<RAM_CS_PIN)
#define RAM_CS_LOW   NRF_P0_NS->OUTCLR = (1<<RAM_CS_PIN)


int16_t Toad_GetTempChannel(uint8_t Channel);
uint16_t Toad_GetFourToTwentyChannel(uint8_t Channel);
uint16_t Toad_GetGRMS_Channel(uint8_t Channel);

extern struct k_mutex PSD_Mutex[4];
extern volatile Sensor__PSD_1024 MyPSDs[4];

#endif