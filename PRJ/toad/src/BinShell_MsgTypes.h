/*
 * BinShell_MsgTypes.h
 *
 *  Created on: Jul 6, 2019
 *      Author: Eli Hughes
 */
#include "stdint.h"

#ifndef SYSTEM_COMPONENTS_BIN_SHELL_BINSHELL_MSGTYPES_H_
#define SYSTEM_COMPONENTS_BIN_SHELL_BINSHELL_MSGTYPES_H_

typedef enum
{
	BIN_SHELL_MSG__TXT = 0,
	BIN_SHELL_MSG__TEST,
	BIN_SHELL_MSG__CAPTURE_IDLE_LINE,
	BIN_SHELL_MSG__TX_SYMBOL,
	BIN_SHELL_MSG__REQUEST_OFDM_PARAMS,
	BIN_SHELL_MSG__OFDM_PARAMS,
	BIN_SHELL_MSG__TX_NORMALIZED_SYMBOL,
	BIN_SHELL_MSG__SELF_FEED_START,
	BIN_SHELL_MSG__SELF_FEED_STOP,
	BIN_SHELL_MSG__TX_DATA,
	BIN_SHELL_MSG__SET_TX_LEVEL,
	BIN_SHELL_MSG__RX_DATA,
	BIN_SHELL_MSG__RAW,
	BIN_SHELL_TX_CAPTURE_BUFFER,
	BIN_SHELL_REQUEST_MODULE_INFO,
	BIN_SHELL_RESET_MODULE_STATS,
	BIN_SHELL_MSG__CAPTURE_IDLE_W_NOISE,


	NUM_BIN_SHELL_MSGS

}BinShell_MsgTypes;


#define SYMBOL_IDLE_LINE_CAPTURE 0x01

typedef struct
{
	uint16_t Number;
	uint16_t Flags;
	uint16_t  OFDM_FFT_Size;
	int16_t Data[OFDM_FFT_SIZE];
} __attribute__((packed)) SymbolMessage ;

typedef struct
{
	uint32_t SampleRate;
	uint16_t OFDM_FFT_Size;
	uint16_t PrefixLength;
	uint16_t PostfixLength;
	uint16_t NumBins;
	uint8_t Bins[NUM_OFDM_BINS];

} __attribute__((packed)) OFDM_ParametersMessage ;

typedef struct
{
	uint16_t Flags;
	uint16_t Number;
	uint16_t NumBins;
	int32_t  Data[NUM_OFDM_BINS*2]; // paked as bins with re then im
} __attribute__((packed)) NormalizedSymbolMessage;

typedef struct
{
	uint8_t  Data[BYTES_PER_OFDM_FRAME];
} __attribute__((packed)) TxPacket;

typedef struct
{
	uint8_t  Data[BYTES_PER_OFDM_FRAME];
} __attribute__((packed)) RxPacket;


typedef struct
{
	uint32_t SampleRate;
	uint32_t DataPoints;
} __attribute__((packed)) LineCaptureHeader ;


#endif /* SYSTEM_COMPONENTS_BIN_SHELL_BINSHELL_MSGTYPES_H_ */
