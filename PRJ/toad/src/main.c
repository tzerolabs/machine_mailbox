#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>
#include <stdlib.h>
#include <shell/shell.h>
#include <dfu/mcuboot.h>
#include <drivers/uart.h>
#include <sys/ring_buffer.h>
#include <logging/log.h>

#include "FIRMWARE_VERSION.h"
#include "epoch.h"
#include "bridge_toad.h"
#include "System.h"
#include "DeviceProvision.h"
#include "FOTA.h"
#include "rt_config.h"
#include "FIRMWARE_VERSION.h"
#include "Cell.h"
#include "thread_wdt.h"
#include <drivers/gpio.h>
#include "toad_ui.h"
#include "toad_io.h"
#include "krusty.h"

LOG_MODULE_REGISTER(main);



void main(void)
{
  	 /* This is needed so that MCUBoot won't revert the update */
	#if (CONFIG_BOOTLOADER_MCUBOOT)
        boot_write_img_confirmed();
    #endif


    thread_wdt_init(10000);

    LOG_INF("TOAD : %s",log_strdup(FIRMWARE_VERSION_STRING));

    cJSON_Init();

    Provision__Init();

	FOTA_Init();
   
    Cell_Init();
	
	Epoch__Init();

    toad_ui_init();

    toad_io_init();

	toad_bridge_init();

	krusty__init();
  
    while (1)
    { 

     thread_wdt_process_channels();

     Toad_GrabSlowADC();

     k_sleep(K_MSEC(500));
     
    }
      
}
