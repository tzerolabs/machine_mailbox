

#ifndef SYSTEM_COMPONENTS_BIN_SHELL_BINSHELL_MSG_HANDLER_H_
#define SYSTEM_COMPONENTS_BIN_SHELL_BINSHELL_MSG_HANDLER_H_



void BinShell_Init();

void BinShell_SendTxt(char *Msg);

void BinShell_TxMsg(uint16_t MsgType, uint8_t *Msg, uint16_t MsgLength);

void BinShell_printf(const char *FormatString,...);

void BinShell_Feed(uint8_t DataIn);

void BinShell_Process();

#endif


