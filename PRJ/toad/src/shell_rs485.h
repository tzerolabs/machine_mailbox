/*
 * Copyright (c) 2018 Makaio GmbH
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef SHELL_RTT_H__
#define SHELL_RTT_H__

#include <shell/shell.h>

#ifdef __cplusplus
extern "C" {
#endif

extern const struct shell_transport_api shell_rs485_transport_api;

struct shell_rs485 {
	shell_transport_handler_t handler;
	struct k_timer timer;
	void *context;
};

#define SHELL_RS485_DEFINE(_name)					\
	static struct shell_rs485 _name##_shell_rs485;			\
	struct shell_transport _name = {				\
		.api = &shell_rs485_transport_api,			\
		.ctx = (struct shell_rs485 *)&_name##_shell_rs485		\
	}

/**
 * @brief Function provides pointer to shell rs485 backend instance.
 *
 * Function returns pointer to the shell rs485 instance. This instance can be
 * next used with shell_execute_cmd function in order to test commands behavior.
 *
 * @returns Pointer to the shell instance.
 */
const struct shell *shell_backend_rs485_get_ptr(void);
#ifdef __cplusplus
}
#endif

#endif /* SHELL_RTT_H__ */
