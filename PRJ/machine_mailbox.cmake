
	include("../liblightmodbus.cmake")
	
	target_include_directories(app PUBLIC
     
    "../../LIB/liblightmodbus/include/"
	"../../LIB/BRIDGE"
    "../../LIB/BRIDGE/CELL/"
    "../../LIB/BRIDGE/TIME"
	"../../LIB/BRIDGE/DataStructures"
	"../../LIB/BRIDGE"
    
    "../../LIB/BRIDGE/CELL/"
    "../../LIB/BRIDGE/TIME"
    "../../LIB/BRIDGE/DataStructures"
    "../../LIB/MACHINE_MAILBOX/MODBUS"
    "../../LIB/MACHINE_MAILBOX/SENSOR"
    "../../LIB/MACHINE_MAILBOX/"
    "../../LIB/MACHINE_MAILBOX/UI"
	"../../LIB/Provision"
    "../../LIB/rt_config"
    "../../LIB/thread_wdt"
	"../../LIB/"
	)
	
target_sources(app PRIVATE "../../LIB/BRIDGE/TIME/Epoch.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/DataStructures/eQueue.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/DataStructures/BridgeDataStructure.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/DataStructures/DataBuffer.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/bridge.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/bridge_report_4ch.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/bridge_report_json.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/bridge_report_modbus.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/bridge_report_psd.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/bridge_report_toad_jack.c")


target_sources(app PRIVATE "../../LIB/BRIDGE/Bridge_CMD.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/hkdf.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/bridge_crypto.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/Cloud_UDP.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/FOTA.c")
target_sources(app PRIVATE "../../LIB/BRIDGE/Jack.c")

	
target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/MachineMailBoxIO.c")
target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/SENSOR/FOUR_CH.c")
target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/UI/beep.c")
#target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/UI/MachineMailboxUI.c")
target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/UI/LP5018.c")
target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/MODBUS/MB_IO.c")
target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/MODBUS/MB_Handle.c")

target_sources(app PRIVATE "../../LIB/MACHINE_MAILBOX/MachineMailBoxIO.c")

target_sources(app PRIVATE "../../LIB/rt_config/rt_config.c")

target_sources(app PRIVATE "../../LIB/thread_wdt/thread_wdt.c")

target_sources(app PRIVATE "../../LIB/Provision/DeviceProvision.c")

target_sources(app PRIVATE "../../LIB/System.c")

zephyr_linker_sources(SECTIONS "../custom-sections.ld")