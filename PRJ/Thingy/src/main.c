#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>
#include <stdlib.h>
#include <shell/shell.h>
#include <dfu/mcuboot.h>
#include <logging/log.h>
#include "Epoch.h"
#include "bridge.h"
#include "System.h"
#include "DeviceProvision.h"
#include "Cloud_UDP.h"
#include "FOTA.h"
#include "rt_config.h"
#include "FIRMWARE_VERSION.h"
#include "Cell.h"
#include "FOUR_CH.h"
#include "thread_wdt.h"
#include "Jack.h"
#include "ThingyUI.h"
#include "ThingySensor.h"



LOG_MODULE_REGISTER(main);


static int cow_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"     /)  (\\\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,".-._((,~~.))_.-,\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW," `-.   @@   ,-'\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"   / ,o--o. \\\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ( ( .__. ) )\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"   ) `----' (\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  /          \\\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW," /            \\\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"/              \\\r\n\r\n");
        shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"      MOO!\r\n");
        return 0;
}



SHELL_CMD_REGISTER(cow, NULL, "It Moos", cow_handler);


void main(void)
{
  	 /* This is needed so that MCUBoot won't revert the update */
	#if (CONFIG_BOOTLOADER_MCUBOOT)
        boot_write_img_confirmed();
    #endif
      
    thread_wdt_init(10000);

    LOG_INF("MBOX : %s",log_strdup(FIRMWARE_VERSION_STRING));

    cJSON_Init();

    Provision__Init();

	FOTA_Init();

    Cell_Init();
	
	Epoch__Init();

    Thingy_UI_Init();

    ThingySensor__Init();

/*
    if(MachineMailBoxIO_Mode == MACHINE_MAILBOX_MODE__MODBUS)
    {
        LOG_INF("MachineMailBox Mode : MODBUS");

        Modbus__IO_Init();
    }
    else
    {
    
        LOG_INF("MachineMailBox Mode : 4 CHANNEL");
    
        FourChannel__Init();
    }
*/
	Bridge_Init();

	Cloud_Init();

    while (1)
    { 
         

      thread_wdt_process_channels();

      k_sleep(K_MSEC(100));
    }
      
}
