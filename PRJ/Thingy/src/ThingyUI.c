#include "stdint.h"
#include "System.h"
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <device.h>
#include <task_wdt/task_wdt.h>
#include <drivers/led.h>
#include <logging/log.h>

#include "Cell.h"
#include "BRIDGE/DataStructures/BridgeDataStructure.h"
#include "System.h"
#include "Cloud_UDP.h"
#include "Epoch.h"
#include "DeviceProvision.h"
#include "Modbus.h"
#include "FOTA.h"
#include "MachineMailboxIO.h"
#include "FOUR_CH.h"
#include "Jack.h"
#include "thread_wdt.h"



#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN0	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS0	DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN0	0
#define FLAGS0	0
#endif

#define LED1_NODE DT_ALIAS(led1)

#if DT_NODE_HAS_STATUS(LED1_NODE, okay)
#define LED1	DT_GPIO_LABEL(LED1_NODE, gpios)
#define PIN1	DT_GPIO_PIN(LED1_NODE, gpios)
#define FLAGS1	DT_GPIO_FLAGS(LED1_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED1	""
#define PIN1	0
#define FLAGS1	0
#endif

#define LED2_NODE DT_ALIAS(led2)

#if DT_NODE_HAS_STATUS(LED2_NODE, okay)
#define LED2	DT_GPIO_LABEL(LED2_NODE, gpios)
#define PIN2	DT_GPIO_PIN(LED2_NODE, gpios)
#define FLAGS2	DT_GPIO_FLAGS(LED2_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED2	""
#define PIN2	0
#define FLAGS2	0
#endif

LOG_MODULE_REGISTER(thingy_ui);

K_THREAD_STACK_DEFINE(my_thingy_ui_thread_stack_area, 1024);

struct k_thread my_thingy_ui_thread_data;

#define NUM_THINGY_LED   (1)

const struct device *RedLED;
const struct device *GreenLED;
const struct device *BlueLED;

typedef struct
{
    uint8_t Green;
    uint8_t Red;
    uint8_t Blue;
    uint8_t Brightness;    
  
} THINGY_LED_t;

THINGY_LED_t THINGY_LED[NUM_THINGY_LED];


static uint8_t GlowIndex[NUM_THINGY_LED];

static uint32_t BridgeBlinkTimer = 0; 

static uint32_t CellBlinkTimer = 0;

static void thingy_ui_work_handler(void *a, void *b, void *c);

static int32_t ui_thread_wdt_channel = -1;

static Cell_ConnectionState_t CellConnectionState;

void Thingy_UI_Init()
{

    CellBlinkTimer = System_Get_mS_Ticker();
    BridgeBlinkTimer = System_Get_mS_Ticker();

   
    RedLED = device_get_binding(LED0);
    GreenLED = device_get_binding(LED1);
    BlueLED = device_get_binding(LED2);

	gpio_pin_configure(RedLED, PIN0, GPIO_OUTPUT_ACTIVE | FLAGS0);
	gpio_pin_configure(GreenLED, PIN1, GPIO_OUTPUT_ACTIVE | FLAGS1);
	gpio_pin_configure(BlueLED, PIN2, GPIO_OUTPUT_ACTIVE | FLAGS2);
	
    

    k_thread_create(&my_thingy_ui_thread_data, my_thingy_ui_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(my_thingy_ui_thread_stack_area),
                    thingy_ui_work_handler,
                    NULL, NULL, NULL,
                    K_HIGHEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);
}

void HandleThingyLED();

void thingy_ui_work_handler(void *a, void *b, void *c)
{

    ui_thread_wdt_channel = thread_wdt_add_channel();

    while(1)
    {

    thread_wdt_channel_feed(ui_thread_wdt_channel);

    int FOTA_Status = 0;

    if((FOTA_Status = FOTA_GetStatus()))
    {
            if(Check_mS_Timeout(&CellBlinkTimer,100 + (1000 - (FOTA_Status * 10))))
            {
                for(int i=0;i<NUM_THINGY_LED;i++)
                {
                    if(FOTA_Status>=100)
                    {
                         THINGY_LED[0].Brightness = 0xFF;
                    }
                    else
                    {
                        if(THINGY_LED[0].Brightness!=0)
                            THINGY_LED[0].Brightness = 0;
                        else
                            THINGY_LED[0].Brightness = 0xFF;
                    }

                    THINGY_LED[0].Blue = 0xFF;
                    THINGY_LED[0].Green = 0xFF;
                    THINGY_LED[0].Red = 0xFF;
                }
            }
    }
    else
    {
            HandleThingyLED();
    }

            if(THINGY_LED[0].Brightness)
            {
                if(THINGY_LED[0].Red){gpio_pin_set(RedLED, PIN0, 1);}else{gpio_pin_set(RedLED, PIN0, 0);}
                if(THINGY_LED[0].Green){gpio_pin_set(GreenLED, PIN1, 1);}else{gpio_pin_set(GreenLED, PIN1, 0);}
                if(THINGY_LED[0].Blue){gpio_pin_set(BlueLED, PIN2, 1);}else{gpio_pin_set(BlueLED, PIN2, 0);}
            }
            else
            {
                gpio_pin_set(RedLED, PIN0, 0);
                gpio_pin_set(GreenLED, PIN1, 0);
                gpio_pin_set(BlueLED, PIN2, 0);
            }

            GlowIndex[0]+= 8;
       

            k_sleep(K_MSEC(50));
    }

}

static uint32_t  LED_TX_Flag = 0;
static uint32_t  LED_TX_Count = 0;

void HandleThingyLED()
{
    if(!Provision__IsGood())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,500))
             {
                if(THINGY_LED[0].Brightness<255)
                    THINGY_LED[0].Brightness = 255;
                else
                    THINGY_LED[0].Brightness = 0;
                }

                THINGY_LED[0].Green = 0;
                THINGY_LED[0].Red = 255;
                THINGY_LED[0].Blue = 0;
    }
    else if(Cell_GetConnectionState(&CellConnectionState))
    {
           if(Epoch__Get32() == 0)
            {
                    if( Check_mS_Timeout(&BridgeBlinkTimer,100))
                    {
                        if(THINGY_LED[0].Brightness<255)
                            THINGY_LED[0].Brightness = 255;
                        else
                            THINGY_LED[0].Brightness = 0;
                    }

                        THINGY_LED[0].Green = 0;
                        THINGY_LED[0].Red = 255;
                        THINGY_LED[0].Blue = 255;
            }
            else if(LED_TX_Flag != Cloud_MessagesTransmitted())
            {
                    if( Check_mS_Timeout(&BridgeBlinkTimer,50))
                    {
                        if(LED_TX_Count < 8)
                        {
                            LED_TX_Count++;
                        }
                        else
                        {
                            LED_TX_Flag++;
                            LED_TX_Count = 0;
                        }

                        if(THINGY_LED[0].Brightness<255)
                            THINGY_LED[0].Brightness = 255;
                        else
                            THINGY_LED[0].Brightness = 0;
                    }

                    THINGY_LED[0].Green = 0;
                    THINGY_LED[0].Red = 0;
                    THINGY_LED[0].Blue = 255;
            }
            else if(Cloud_IsInRetry())
            {
                    if( Check_mS_Timeout(&BridgeBlinkTimer,50))
                    {
                        if(THINGY_LED[0].Brightness<255)
                            THINGY_LED[0].Brightness = 255;
                        else
                            THINGY_LED[0].Brightness = 0;
                    }

                        THINGY_LED[0].Green = 255;
                        THINGY_LED[0].Red = 255;
                        THINGY_LED[0].Blue = 0;
            }
            else
            {
                THINGY_LED[0].Brightness = 0xFF;
                THINGY_LED[0].Blue = 0;
                THINGY_LED[0].Green = 0xFF;
                THINGY_LED[0].Red = 0;
            }
    }
    else
    {
         if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_NOT_REGISTERED)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,150))
                    {
                        if(THINGY_LED[0].Brightness!=0)
                            THINGY_LED[0].Brightness = 0;
                        else
                          THINGY_LED[0].Brightness = 0xFF;
                    }

                    THINGY_LED[0].Blue = 0;
                    THINGY_LED[0].Green = 0x80;
                    THINGY_LED[0].Red = 0xFF;
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_HOME || CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_ROAMING)
                {

                        if(CellConnectionState.ID == -1)
                        {
                            if(Check_mS_Timeout(&CellBlinkTimer,150))
                            {
                                if(THINGY_LED[0].Brightness!=0)
                                    THINGY_LED[0].Brightness = 0;
                                else
                                THINGY_LED[0].Brightness = 0xFF;
                            }

                            THINGY_LED[0].Blue = 0;
                            THINGY_LED[0].Green = 0x80;
                            THINGY_LED[0].Red = 0xFF;
                        }
                        else
                        {
                            THINGY_LED[0].Brightness = 0xFF;
                                THINGY_LED[0].Blue = 0;
                                THINGY_LED[0].Green = 0xFF;
                                THINGY_LED[0].Red = 0;
                        }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_SEARCHING)
                {
                   if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                         THINGY_LED[0].Brightness = 0xFF;

                         if(THINGY_LED[0].Green == 0xFF)
                         {
                            THINGY_LED[0].Blue = 0;
                            THINGY_LED[0].Green = 0x80;
                            THINGY_LED[0].Red = 0xFF;
                         }
                         else
                         {
                            THINGY_LED[0].Blue = 0;
                            THINGY_LED[0].Green = 0xFF;
                            THINGY_LED[0].Red = 0;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTRATION_DENIED)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,100))
                    {
                        if(THINGY_LED[0].Brightness!=0)
                            THINGY_LED[0].Brightness = 0;
                        else
                        THINGY_LED[0].Brightness = 0xFF;
                    }

                    THINGY_LED[0].Blue = 0;
                    THINGY_LED[0].Green = 0;
                    THINGY_LED[0].Red = 0xFF;
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_UNKNOWN)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                         THINGY_LED[0].Brightness = 0xFF;

                         if(THINGY_LED[0].Blue != 0x80)
                         {
                            THINGY_LED[0].Blue = 0x80;
                            THINGY_LED[0].Green = 0;
                            THINGY_LED[0].Red = 0xFF;
                         }
                         else
                         {
                            THINGY_LED[0].Blue = 0;
                            THINGY_LED[0].Green = 0;
                            THINGY_LED[0].Red = 0xFF;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_EMERGENCY)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                        THINGY_LED[0].Brightness = 0xFF;

                         if(THINGY_LED[0].Green != 0x80)
                         {
                            THINGY_LED[0].Blue = 0;
                            THINGY_LED[0].Green = 0x80;
                            THINGY_LED[0].Red = 0xFF;
                         }
                         else
                         {
                            THINGY_LED[0].Blue = 0;
                            THINGY_LED[0].Green = 0;
                            THINGY_LED[0].Red = 0xFF;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_UICC_FAIL)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                        THINGY_LED[0].Brightness = 0xFF;

                        if(THINGY_LED[0].Green !=0)
                        {
                            THINGY_LED[0].Blue = 0x00;
                            THINGY_LED[0].Green = 0x00;
                            THINGY_LED[0].Red = 0xFF;
                        }
                        else
                        {
                            THINGY_LED[0].Blue = 0xFF;
                            THINGY_LED[0].Green = 0xFF;
                             THINGY_LED[0].Red = 0xFF;
                        }
                    }
                }
                else
                {
                    THINGY_LED[0].Blue = 0;
                    THINGY_LED[0].Green = 0;
                    THINGY_LED[0].Red = 0xFF;
                    THINGY_LED[0].Brightness = 0xFF;
                }
    }
    


}


/*



void HandleCloudLED()
{
    
    else if(Cell_IsConnected() == 0)
    {
        THINGY_LED[CLOUD_LED].Brightness = 0xFF;
        THINGY_LED[CLOUD_LED].Blue = 0;
        THINGY_LED[CLOUD_LED].Green = 0;
        THINGY_LED[CLOUD_LED].Red = 0;
    }
    else if(Epoch__Get32() == 0)
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,100))
             {
                if(THINGY_LED[CLOUD_LED].Brightness<255)
                    THINGY_LED[CLOUD_LED].Brightness = 255;
                else
                    THINGY_LED[CLOUD_LED].Brightness = 0;
            }

                THINGY_LED[CLOUD_LED].Green = 0;
                THINGY_LED[CLOUD_LED].Red = 255;
                THINGY_LED[CLOUD_LED].Blue = 255;
    }
    else if(LED_TX_Flag != Cloud_MessagesTransmitted())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,50))
             {
                 if(LED_TX_Count < 8)
                 {
                    LED_TX_Count++;
                 }
                 else
                 {
                     LED_TX_Flag++;
                     LED_TX_Count = 0;
                 }

                if(THINGY_LED[CLOUD_LED].Brightness<255)
                    THINGY_LED[CLOUD_LED].Brightness = 255;
                else
                    THINGY_LED[CLOUD_LED].Brightness = 0;
              }

              THINGY_LED[CLOUD_LED].Green = 0;
              THINGY_LED[CLOUD_LED].Red = 0;
              THINGY_LED[CLOUD_LED].Blue = 255;
    }
    else if(Cloud_IsInRetry())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,50))
             {
                if(THINGY_LED[CLOUD_LED].Brightness<255)
                    THINGY_LED[CLOUD_LED].Brightness = 255;
                else
                    THINGY_LED[CLOUD_LED].Brightness = 0;
            }

                THINGY_LED[CLOUD_LED].Green = 255;
                THINGY_LED[CLOUD_LED].Red = 255;
                THINGY_LED[CLOUD_LED].Blue = 0;
    }
    else if(Cloud_IsConnected())
    {
         THINGY_LED[CLOUD_LED].Brightness = 255;
         THINGY_LED[CLOUD_LED].Green = 0;
         THINGY_LED[CLOUD_LED].Red = 0;
         THINGY_LED[CLOUD_LED].Blue = 255;
    }
    else
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,200))
             {
                if(THINGY_LED[CLOUD_LED].Brightness<255)
                    THINGY_LED[CLOUD_LED].Brightness = 255;
                else
                    THINGY_LED[CLOUD_LED].Brightness = 0;
            }

                THINGY_LED[CLOUD_LED].Green = 127;
                THINGY_LED[CLOUD_LED].Red = 255;
                THINGY_LED[CLOUD_LED].Blue = 0;
    }
}

static uint32_t LastMailBoxActivity = 0;


void HandleModbusLEDs()
{

}
*/