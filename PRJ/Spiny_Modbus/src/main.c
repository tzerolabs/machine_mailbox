#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>
#include <stdlib.h>
#include <shell/shell.h>
#include <dfu/mcuboot.h>
#include <logging/log.h>
#include "Epoch.h"
#include "bridge_modbus.h"
#include "System.h"
#include "DeviceProvision.h"
#include "krusty.h"
#include "FOTA.h"
#include "rt_config.h"
#include "FIRMWARE_VERSION.h"
#include "Cell.h"
#include "MachineMailboxIO.h"
#include "MachineMailboxUI.h"

#include "thread_wdt.h"
#include "Modbus.h"

LOG_MODULE_REGISTER(main);


void main(void)
{
  	 /* This is needed so that MCUBoot won't revert the update */
	#if (CONFIG_BOOTLOADER_MCUBOOT)
        boot_write_img_confirmed();
    #endif
      
    thread_wdt_init(10000);

    LOG_INF("MBOX : %s",log_strdup(FIRMWARE_VERSION_STRING));

    cJSON_Init();

    Provision__Init();

	FOTA_Init();

    Cell_Init();
	
	Epoch__Init();

    Modbus__IO_Init();

	modbus_bridge_init();

	krusty__init();

    MM_UI_Init();


    while (1)
    { 
      thread_wdt_process_channels();

      k_sleep(K_MSEC(100));
    }
      
}
