# MachineMailBox #

This project is setup as Workflow #4 described here:

https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/dm_adding_code.html#user-workflows


Step 1.)

Make sure to get setup to with prerequisties:

https://docs.zephyrproject.org/latest/getting_started/index.html

You need west, python prerequisites and a toolchain installed.

Step 2:)

Create a workspace folder (machine_mailbox),  cd to the folder then run:

a.) "west init -m https://bitbucket.org/tzerolabs/machine_mailbox --mr master"

b.) "west update"    <-----this may take a few minutes 

Step 3.)

The "BulletBill"  project is in PRJ/bullet_bill

cd to the that directory and run "west build".


# Notes:

On the 1st build you may see a warning: 
WARNING: This looks like a fresh build and BOARD is unknown; so it probably won't work. To fix, use --board=<your-board>.

This is OK as the board gets defined in the CmakeList.txt

