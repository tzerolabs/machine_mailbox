#include "stdint.h"
#include <stddef.h>

#ifndef _DATA_BUFFER_H
#define _DATA_BUFFER_H

typedef void (*DataBufferCallBack_t)(void *) ; 

typedef struct
{
    void * fifo_reserved; //zephyr needs this for the linked list

    uint16_t current_length;
    uint16_t max_length;
    uint8_t *data;

    DataBufferCallBack_t Complete;

} data_buf_t;


void DataBufferDestroy(data_buf_t * DB);

data_buf_t * DataBufferCreate(uint32_t length,DataBufferCallBack_t CB);


#endif