
#include <zephyr.h>
#include "data_buf.h"

K_HEAP_DEFINE(data_buffer_heap, CONFIG_DATA_BUF_POOL_BYTES);

data_buf_t * DataBufferCreate(uint32_t length,DataBufferCallBack_t CB)
{
    data_buf_t * DB = 0;
    
    DB = k_heap_alloc(&data_buffer_heap, length + sizeof(data_buf_t),K_NO_WAIT);

    if(DB)
    {
        DB->data = (uint8_t *)DB + sizeof(data_buf_t);
        DB->max_length = length;
        DB->current_length = 0;
        DB->Complete = CB;
    }

    return DB;

}

void DataBufferDestroy(data_buf_t * DB)
{
    if(DB->Complete != NULL)
    {
        DB->Complete((void *)DB);
    }

    k_heap_free(&data_buffer_heap,DB);
    
}
