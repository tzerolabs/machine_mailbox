#include "thread_wdt.h"
#include <zephyr.h>
#include <sys/printk.h>
#include <logging/log.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "string.h"
#include <drivers/watchdog.h>

LOG_MODULE_REGISTER(thread_wdt);

#define CONFIG_MAX_THREAD_WDT_CHANNELS 16

/*
 * To use this sample, either the devicetree's /aliases must have a
 * 'watchdog0' property, or one of the following watchdog compatibles
 * must have an enabled node.
 */
#if DT_NODE_HAS_STATUS(DT_ALIAS(watchdog0), okay)
#define WDT_NODE DT_ALIAS(watchdog0)
#elif DT_HAS_COMPAT_STATUS_OKAY(st_stm32_window_watchdog)
#define WDT_NODE DT_INST(0, st_stm32_window_watchdog)
#define WDT_MAX_WINDOW  100U
#elif DT_HAS_COMPAT_STATUS_OKAY(st_stm32_watchdog)
#define WDT_NODE DT_INST(0, st_stm32_watchdog)
#elif DT_HAS_COMPAT_STATUS_OKAY(nordic_nrf_watchdog)
/* Nordic supports a callback, but it has 61.2 us to complete before
 * the reset occurs, which is too short for this sample to do anything
 * useful.  Explicitly disallow use of the callback.
 */
#define WDT_ALLOW_CALLBACK 0
#define WDT_NODE DT_INST(0, nordic_nrf_watchdog)
#elif DT_HAS_COMPAT_STATUS_OKAY(espressif_esp32_watchdog)
#define WDT_NODE DT_INST(0, espressif_esp32_watchdog)
#elif DT_HAS_COMPAT_STATUS_OKAY(silabs_gecko_wdog)
#define WDT_NODE DT_INST(0, silabs_gecko_wdog)
#elif DT_HAS_COMPAT_STATUS_OKAY(nxp_kinetis_wdog32)
#define WDT_NODE DT_INST(0, nxp_kinetis_wdog32)
#elif DT_HAS_COMPAT_STATUS_OKAY(microchip_xec_watchdog)
#define WDT_NODE DT_INST(0, microchip_xec_watchdog)
#elif DT_HAS_COMPAT_STATUS_OKAY(ti_cc32xx_watchdog)
#define WDT_NODE DT_INST(0, ti_cc32xx_watchdog)
#endif

/*
 * If the devicetree has a watchdog node, get its label property.
 */
#ifdef WDT_NODE
#define WDT_DEV_NAME DT_LABEL(WDT_NODE)
#else
#define WDT_DEV_NAME ""
#error "Unsupported SoC and no watchdog0 alias in zephyr.dts"
#endif

 const struct device *hw_wdt_dev;
 static int wdt_channel_id = -1;  

typedef struct {
    bool Initialized;
    bool HasBeenFeed;
} thread_wdt_channel_t;

thread_wdt_channel_t thread_wdt_channels[CONFIG_MAX_THREAD_WDT_CHANNELS];

int32_t thread_wdt_init(int32_t timeout)
{
    hw_wdt_dev = device_get_binding(WDT_DEV_NAME);
    int err;

    if(hw_wdt_dev == NULL)
    {
        LOG_ERR("Could not get watchdog device");
        return -ENOTSUP;
    }

	if (!device_is_ready(hw_wdt_dev)) {
		LOG_ERR("Hardware watchdog %s is not ready; ignoring it.\n",
		       hw_wdt_dev->name);
		return -ENOTSUP;
	}

    struct wdt_timeout_cfg wdt_config = {
		/* Reset SoC when watchdog timer expires. */
		.flags = WDT_FLAG_RESET_SOC,

		/* Expire watchdog after max window */
		.window.min = 0U,
		.window.max = timeout,
	};

    wdt_channel_id = wdt_install_timeout(hw_wdt_dev, &wdt_config);

    if (wdt_channel_id < 0) {
		LOG_ERR("Watchdog install error\n");
        return -ENOTSUP;
	}

	err = wdt_setup(hw_wdt_dev, WDT_OPT_PAUSE_HALTED_BY_DBG);
	if (err < 0) {
		LOG_ERR("Watchdog setup error\n");
        return -ENOTSUP;
	}

    memset(thread_wdt_channels,0,sizeof(thread_wdt_channels));

    return 0;
}

int32_t thread_wdt_add_channel()
{
    int32_t channel = -1;

    for(int i=0;i<CONFIG_MAX_THREAD_WDT_CHANNELS;i++)
    {
        if(thread_wdt_channels[i].Initialized == false)
        {
            thread_wdt_channels[i].Initialized = true;
            thread_wdt_channels[i].HasBeenFeed = true;
            channel = i;
            break;
        }
    }

    return channel;
}

int32_t thread_wdt_channel_feed(int32_t channel)
{
    if(channel>=0 && channel<CONFIG_MAX_THREAD_WDT_CHANNELS)
    {
        if(thread_wdt_channels[channel].Initialized)
        {
             thread_wdt_channels[channel].HasBeenFeed = true;
             return channel;
        }
    }

    return -1;
}

static     int32_t TotalChannels = 0;
static     int32_t ChannelSum = 0;
    
int32_t thread_wdt_process_channels()
{
    TotalChannels = 0;
    ChannelSum = 0;
    
    for(int i=0;i<CONFIG_MAX_THREAD_WDT_CHANNELS;i++)
    {
        if(thread_wdt_channels[i].Initialized == true)
        {
            TotalChannels++;
            if(thread_wdt_channels[i].HasBeenFeed)
            {
                ChannelSum++;
            }
        }
    }

    if(TotalChannels == ChannelSum)
    {
        wdt_feed(hw_wdt_dev, wdt_channel_id);

        for(int i = 0; i<CONFIG_MAX_THREAD_WDT_CHANNELS;i++)
        {
            thread_wdt_channels[i].HasBeenFeed = false;
        }
   }

    return (TotalChannels == ChannelSum);
}