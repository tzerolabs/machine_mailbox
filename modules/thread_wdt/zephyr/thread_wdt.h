#include <stdint.h>

#ifndef _THREAD_WDT
#define _THREAD_WDT

int32_t thread_wdt_init(int32_t timeout);

int32_t thread_wdt_add_channel();

int32_t thread_wdt_channel_feed(int32_t channel);

int32_t thread_wdt_process_channels();

#endif