#include "stdint.h"

#ifndef _SYSTEM_H
#define _SYSTEM_H

uint32_t System_Get_mS_Ticker();

uint32_t Check_mS_Timeout(uint32_t *TimeIn, uint32_t Delta);

void System__FlagReboot();

void System__Delay_mS(uint32_t Ticks);

int32_t System__ComputeElapsedTime(uint32_t *TimeIn);

#endif 