#include "System.h"
#include <zephyr.h>
#include <sys/reboot.h>
#include <logging/log.h>
#include "Cell.h"

LOG_MODULE_REGISTER(system,LOG_LEVEL_DBG);

static struct k_work_delayable reboot_work;

void reboot_work_handler(struct k_work *work)
{
       LOG_INF("Powering down modem and rebooting...");
       Cell_PowerDown();
       k_sleep(K_MSEC(2000));
       sys_reboot(SYS_REBOOT_COLD);
}

extern void Bridge_RebootNotify();

 void System__FlagReboot()
 {

    Bridge_RebootNotify();

    LOG_INF("Rebooting in 10 seconds");
    
    k_work_init_delayable(&reboot_work,reboot_work_handler);

	k_work_reschedule(&reboot_work, K_MSEC(10000));
 }

uint32_t System_Get_mS_Ticker()
{
   return k_uptime_get_32();
}

int32_t System__ComputeElapsedTime(uint32_t *TimeIn)
{
    if(TimeIn == NULL)
        return 0;
       
    uint32_t Elapsed;

    Elapsed = k_uptime_get_32();

    if(Elapsed >= *TimeIn)
    {
        Elapsed -= *TimeIn;
    }
    else
    {
        Elapsed += (0xFFFFFFFF - *TimeIn)  ;
    }

    return Elapsed;
}

uint32_t Check_mS_Timeout(uint32_t *TimeIn, uint32_t Delta)
{

    if(TimeIn == NULL)
        return 0;
       
    uint32_t Elapsed;


    Elapsed = System__ComputeElapsedTime(TimeIn);

    if(Elapsed >= Delta)
    {
        *TimeIn = k_uptime_get_32() - ( 1 + (Elapsed - Delta));
        return 1 + (Elapsed - Delta);
    }
   
    return 0;
    
}

void System__Delay_mS(uint32_t Ticks)
{
    k_sleep(K_MSEC(Ticks));
}


