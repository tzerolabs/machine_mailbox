#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>
#include <stdlib.h>
#include <shell/shell.h>
#include <dfu/mcuboot.h>
#include <dfu/dfu_target_mcuboot.h>
#include <net/fota_download.h>
#include <net/download_client.h>
#include <net/socket.h>
#include <cJSON.h>
#include "FOTA.h"
#include "rt_config.h"
#include "System.h"
#include "krusty.h"
#include "bridge_report_json.h"
#include "data_buf.h"

LOG_MODULE_REGISTER(fota);


#ifndef CONFIG_MM_FOTA_UPDATE_PATH
	#error "CONFIG_MM_FOTA_UPDATE_PATH not defined in prj.conf"
#endif

#ifndef CONFIG_MM_FOTA_UPDATE_HOST
	#error "CONFIG_MM_FOTA_UPDATE_HOST not defined in prj.conf"
#endif

static char UpdateServerHostStorage[96];
static char UpdateServerPathStorage[96];
static int UpdateSecturityTagStorage;


RT_CONFIG_ITEM(UpdateServer,"Hostname for updates",UpdateServerHostStorage,RT_CONFIG_DATA_TYPE_STRING,sizeof(UpdateServerHostStorage),"","",CONFIG_MM_FOTA_UPDATE_HOST);
RT_CONFIG_ITEM(UpdatePath,"Path for update",UpdateServerPathStorage,RT_CONFIG_DATA_TYPE_STRING,sizeof(UpdateServerPathStorage),"","",CONFIG_MM_FOTA_UPDATE_PATH);
RT_CONFIG_ITEM(UpdateSecturityTag,"Update Security Tag",&UpdateSecturityTagStorage,RT_CONFIG_DATA_TYPE_INT32,sizeof(UpdateSecturityTagStorage),"-1","65535","100");

static int FOTA_LastProgress = 0;
int FOTA_LastProgressFine = 0;



static int shell_update_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
	UpdateFirmware(UpdateServerHostStorage,
				   UpdateServerPathStorage,
				   UpdateSecturityTagStorage);

	return 0;
}


void update_complete_notify()
{

    cJSON *Result = cJSON_CreateObject();

    cJSON_AddStringToObject(Result, "ObjID","Update");
 
    cJSON_AddStringToObject(Result, "Result","OK");

    cJSON_AddStringToObject(Result, "Message","Download complete.  Rebooting to flash new FW");
 
    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);

}

void update_error_notify(int32_t ErrorCode)
{
    cJSON *Result = cJSON_CreateObject();

    char Error[64];

    sprintf(Error,"Error Code %i",ErrorCode);

    cJSON_AddStringToObject(Result, "ObjID","Update");

    cJSON_AddStringToObject(Result, "Result","FOTA Error");
 
    cJSON_AddStringToObject(Result, "Message",Error);
 
    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);

}


void update_progress_notify(int32_t Progress)
{
    cJSON *Result = cJSON_CreateObject();

    char ProgressString[64];

    sprintf(ProgressString,"Progress : %i%%",Progress);

    cJSON_AddStringToObject(Result, "ObjID","Update");

    cJSON_AddStringToObject(Result, "Result","OK");
 
    cJSON_AddStringToObject(Result, "Message",ProgressString);
 
    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);

}


void UpdateFromDefault()
{
		UpdateFirmware(UpdateServerHostStorage,
				   UpdateServerPathStorage,
				   UpdateSecturityTagStorage);
}


void UpdateFromPath(char * Path)
{
		UpdateFirmware(UpdateServerHostStorage,
				   Path,
				   UpdateSecturityTagStorage);
}


SHELL_CMD_REGISTER(update, NULL, "Fetch Update", shell_update_handler);


void UpdateFirmware(char * Host,char *File,int SecurityTag)
{
	#if  CONFIG_BOOTLOADER_MCUBOOT
		int err;
		
		FOTA_LastProgress = 0;
		FOTA_LastProgressFine = 0;

		err = fota_download_start(
									Host, 
									File,
									SecurityTag,
									0, 0);

		if (err != 0) 
		{
			
			LOG_INF("fota_download_start() failed, err %d\n", err);
		}
	#endif
	
}


/*
	returns -1 if FOTA had an error.
	returns 0 if FOTA is inactive.
	Retusrn 1-100 to indicate progress.
*/
int32_t FOTA_GetStatus()
{
	return FOTA_LastProgressFine;
}

#if CONFIG_BOOTLOADER_MCUBOOT

static uint8_t  fota_buf[512] __attribute__ ((aligned (4)));

static void fota_dl_handler(const struct fota_download_evt *evt)
{

	switch (evt->id)
	 {

		case FOTA_DOWNLOAD_EVT_ERASE_PENDING:
			LOG_INF("FOTA Erase Pending");
		break;

		case FOTA_DOWNLOAD_EVT_ERASE_DONE:
			LOG_INF("FOTA Erase done");
		break;

		case FOTA_DOWNLOAD_EVT_CANCELLED:
			LOG_INF("FOTA canceled");
			FOTA_LastProgressFine = 0;
		break;
		
		case FOTA_DOWNLOAD_EVT_PROGRESS:

		FOTA_LastProgressFine = evt->progress;

		if(FOTA_LastProgressFine == 0)
			FOTA_LastProgressFine = 1;

		if((evt->progress / 10)*10 > FOTA_LastProgress)
		{
			FOTA_LastProgress = (evt->progress / 10)*10;
			update_progress_notify(FOTA_LastProgress);
			
		}
	
		break;

		case FOTA_DOWNLOAD_EVT_ERROR:
			FOTA_LastProgressFine = -1;
			LOG_INF("Received error from fota_download\n");
			update_error_notify(evt->cause);
		break;
			
		case FOTA_DOWNLOAD_EVT_FINISHED:

			LOG_INF("Update Finished");

			update_complete_notify();

			System__FlagReboot();

			FOTA_LastProgressFine = 100;
			
			break;

		default:
			break;
	}


}
#endif



void FOTA_Init()
{
	#if CONFIG_BOOTLOADER_MCUBOOT

    int err = 0;

	FOTA_LastProgressFine = 0;
	FOTA_LastProgress = 0;

    err = dfu_target_mcuboot_set_buf(fota_buf, sizeof(fota_buf));
	if (err != 0) {
		LOG_ERR("dfu_target_mcuboot_set_buf() failed, err %d\n", err);
		return;
	}

	err = fota_download_init(fota_dl_handler);
	if (err != 0) {
		LOG_ERR("fota_download_init() failed, err %d\n", err);
		return;
	}
	
	#endif
}