#include "stdint.h"

#ifndef XBEE_CELL_COMMAND_MODE
#define XBEE_CELL_COMMAND_MODE


uint8_t XBEE_GetLastCommandReponseState();


void HandleCommandResponse(uint8_t *Reponse,uint32_t Length);

void DecodeCommand_Response(uint8_t DataIn);

void XBEE_EnterCommandMode();

void XBEE_AT_Send_BD_115200();

void XBEE_AT_Send_WR();

void XBEE_AT_Send_AC();

void XBEE_AT_Request_AP_API_Mode();

void XBEE_AT_Set_AP_API_Mode();

char * GetLastAT_CommandResponse();

void XBEE_AT_Request_APN();

void XBEE_AT_Set_APN(char * APN);

void XBEE_AT_ExitCommandMode();

void XBEE_AT_GetPreferredNetwork();

void XBEE_AT_SetPreferredNetworkTechnology();

void XBEE_AT_GetPreferredNetworkTechnology();

#endif
