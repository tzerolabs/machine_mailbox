#include "stdint.h"

#ifndef XBEE_CELL__PORTABLE_H_
#define XBEE_CELL__PORTABLE_H_

void Cell_IO_Init();
void Cell_SetBaud(uint32_t Baud);
uint32_t Cell_GetBaud();

void Cell_ResetHigh();
void Cell_ResetLow();
void Cell_PowerEnableHigh();
void Cell_PowerEnableLow();

void Cell_WriteByteToTxQueue(uint8_t DataOut);
void Cell_WriteBytesToTxQueue(uint8_t *DataOut, uint32_t Len);
void Cell_WriteStringToTxQueue(char *DataOut);

uint32_t Cell_ByteRead(uint8_t *Buf, uint32_t BufSize);
void Cell_MoveQueues();

#endif
