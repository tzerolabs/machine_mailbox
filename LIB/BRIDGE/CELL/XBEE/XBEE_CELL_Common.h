#include "stdint.h"
#include <zephyr.h>
#include <sys/printk.h>
#include <shell/shell.h>
#include <logging/log.h>
#include <drivers/gpio.h>
#include <drivers/uart.h>
#include "XBEE_CELL_Config.h"



#ifndef XBEE_COMMON_H
#define XBEE_COMMON_H

extern volatile uint32_t System__SecondsSinceBoot ;


typedef enum {

        PROTOCOL_UDP = 0,
        PROTOCOL_TCP = 1

}Cell_Protocol;


typedef enum
{
    CELL_RESET_HARD = 0,
    CELL_RESET_AIRPLANE_SOFT
}CELL_ResetType;


typedef enum {

        HOST_LOOKUP_IDLE = 0,
        HOST_LOOKUP_IN_PROGRESS,
        HOST_LOOKUP_COMPLETE,
        HOST_LOOKUP_ERROR

}HostLookupState;


typedef enum
{
    XBEE_COMMAND_MODE = 0,
    XBEE_API_MODE  = 1

} XBEE_MODES;


typedef enum
{
        WAIT_FOR_START = 0,

        GRAB_LENGTH_HIGH,

        GRAB_LENGTH_LOW,

        GRAB_FRAME,

        GRAB_CHECKSUM

} API_FrameDecodeState;


typedef enum {

        FRAME_TYPE__AT = 0x08,
        FRAME_TYPE__AT_RESPONSE = 0x88,

        FRAME_TYPE__TX_SMS = 0x1F,
        FRAME_TYPE__RX_SMS = 0x9F,

        FRAME_TYPE__IPV4_TX             = 0x20,
        FRAME_TYPE__IPV4_TX_TLS = 0x23,

        FRAME_TYPE__IPV4_RX = 0xB0,

        FRAME_TYPE__MODEM_STATUS = 0x8A,

        FRAME_TYPE__IPV4_TX_STATUS = 0x89,

        FRAME_TYPE__SOCKET_CREATE = 0x40,
        FRAME_TYPE__SOCKET_CREATE_RESPONSE = 0xC0,
        

        FRAME_TYPE__SOCKET_OPTION_REQUEST = 0x41,
        FRAME_TYPE__SOCKET_OPTION_REQUEST_RESPONSE = 0xC1,

        FRAME_TYPE__SOCKET_CLOSE = 0x43,
        FRAME_TYPE__SOCKET_CLOSE_RESPONSE = 0xC3,
        

        FRAME_TYPE__SOCKET_STATUS = 0xCF,

        FRAME_TYPE__SOCKET_CONNECT = 0x42,
        FRAME_TYPE__SOCKET_CONNECT_RESPONSE = 0xC2,

   
        FRAME_TYPE__SOCKET_SEND = 0x44,

        FRAME_TYPE__SOCKET_RECEIVE = 0xCD,
        FRAME_TYPE__SOCKET_RECEIVE_IPV4 = 0xCE,
        
        

} API_FrameType;



typedef enum
{

        AT_RESPONSE_OK = 0,
        AT_RESPONSE_ERROR = 1,
        AT_RESPONSE_INVALID_COMMAND = 2,
        AT_RESPONSE_INVALID_PARAMETER = 3

} API_AT_CMD_ResponseStatus;


typedef enum
{
        COMMAND_RESPONSE_OK = 0,

        COMMAND_RESPONSE_ERROR,

        COMMAND_RESPONSE_WAITING = 255,

} AT_COMMAND_RESPONSE;



typedef enum
{
        CELL_TIME_FETCH_STATE_IDLE = 0,
        CELL_TIME_FETCH_STATE_WAITING = 1,
        CELL_TIME_FETCH_STATE_COMPLETE = 2
       
} CellTimeFetchState_t;


typedef enum {

    XBEE_SOCKET_UDP = 0,
    XBEE_SOCKET_TCP = 1,
    XBEE_SOCKET_SSL = 4
} XBEE_SocketType_t;


#define API_FRAME_ID_HOST_LOOKUP        0x10
#define API_FRAME_TCP_TX                0x11
#define API_FRAME_NTP_TX                0x12
#define API_FRAME_REQUEST_RSSI          0x13
#define API_FRAME_REQUEST_IMEI          0x14
#define API_FRAME_REQUEST_OPERATOR      0x15
#define API_FRAME_REQUEST_ICCID         0x16
#define API_FRAME_REQUEST_PHONE_NUMBER  0x17
#define API_FRAME_REQUEST_CONNECTION_STATE 0x18
#define API_FRAME_REQUEST_FIRMWARE      0x19

#define API_FRAME_REQUEST_FORCE_RESET    0x20
#define API_FRAME_REQUEST_NETWORK_RESET  0x21
#define API_FRAME_REQUEST_AIRPLANE       0x22
#define API_FRAME_REQUEST_TIME           0x23

#define API_FRAME_REQUEST_SOCKET_CREATE  0x24
#define API_FRAME_REQUEST_SOCKET_CLOSE   0x25

#define API_FRAME_REQUEST_SOCKET_CONNECT   0x26

#define API_FRAME_REQUEST_SOCKET_SEND     0x27


#define API_FRAME_REQUEST_SOCKET_OPTION   0x28


#define TX_FRAME_STATUS_WAITING         0xFF

#define TX_FLAG_CLOSE_AFTER_TX  0x02
#define TX_FLAG_LEAVE_OPEN      0x00


#define CELL_NETWORK_STATE__CONNECTED       0x00        
#define CELL_NETWORK_STATE__REGISTERING     0x22        
#define CELL_NETWORK_STATE__CONNECTING      0x23        
#define CELL_NETWORK_STATE__CORRUPT         0x24        
#define CELL_NETWORK_STATE__DENIED          0x25        
#define CELL_NETWORK_STATE__AIRPLANE        0x2A        
#define CELL_NETWORK_STATE__DIRECT          0x2B      
#define CELL_NETWORK_STATE__PSM             0x2C 
#define CELL_NETWORK_STATE__MODEM_SHUT_DOWN 0x2D 
#define CELL_NETWORK_STATE__LOW_VOLTAGE_SHUT_DOWN 0x2E 
#define CELL_NETWORK_STATE__BYPASS               0x2F
#define CELL_NETWORK_STATE__UPDATE_IN_PROGRESS  0x30        
#define CELL_NETWORK_STATE__REG_TEST          0x31

#define CELL_NETWORK_STATE__INITIALIZING    0xFF        


#define AT_CMD__LA      0x414C
#define AT_CMD__DB      0x4244
#define AT_CMD__IM      0x4d49
#define AT_CMD__S_POUND  0x2353
#define AT_CMD__AI      0x4941
#define AT_CMD__NR      0x524e
#define AT_CMD__FR      0x5246
#define AT_CMD__MN      0x4E4D
#define AT_CMD__AN      0x4E41
#define AT_CMD__VR      0x5256
#define AT_CMD__MV      0x564D
#define AT_CMD__VL      0x4C56
#define AT_CMD__DT      0x5444
#define AT_CMD__AS      0x5341

#define XBEE_MODEM_MAX_SOCKET_COUNT 6

extern char Cell_IMEI_String[MAX_IMEI_SIZE+1];

extern char Cell_ICCID_String[MAX_ICCID_SIZE+1];

extern char Cell_Operator_String[MAX_OPERATOR_SIZE+1];

extern char Cell_APN[MAX_APN_SIZE+1];

extern volatile uint8_t Cell_IMEI_Rx;

extern volatile uint8_t Cell_ICCID_Rx;

extern volatile uint8_t Cell_Network_Rx;

extern volatile uint8_t Cell_APN_Rx;

extern volatile uint8_t CellInfoRequested;

extern volatile uint8_t CellFirmwareVersionRecieved;

extern volatile uint32_t CellFirmwareVersion;

extern volatile uint8_t CellModuleFirmwareVersionRecieved;

extern volatile char CellModuleFirmwareVersion[64];

extern volatile uint8_t CellLastConnectionState;

extern volatile int8_t CellLastRSSI;

extern volatile uint8_t __CellLastConnectionState;

extern volatile int8_t __CellLastRSSI;

extern volatile uint32_t LastLookupIP;

extern volatile HostLookupState CurrentHostLookupState;

extern API_FrameDecodeState CurrentAPI_FrameDecodeState;

extern uint32_t API_FrameCRC;

extern uint16_t API_FrameInCounter;

extern uint8_t API_FrameInBuffer[MAX_API_FRAME_IN_BUFFER_SIZE];

extern uint8_t API_FrameBuffer[API_FRAME_BUFFER_SIZE];

extern uint32_t API_FrameBufferLength;

extern uint32_t API_FrameLength;

extern volatile uint8_t FrameID_LastStatus[256];

extern uint8_t XBEE_Mode;

extern  volatile CellTimeFetchState_t CellTimeFetchState;

extern  volatile uint32_t CellIncomingUnixTime;

extern uint32_t CellSecondsSinceLastTimeRefresh;

#endif


