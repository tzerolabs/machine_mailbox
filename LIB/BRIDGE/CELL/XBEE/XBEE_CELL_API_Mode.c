#include "XBEE_CELL.h"
#include "XBEE_CELL_Config.h"
#include "XBEE_CELL_Common.h"
#include "XBEE_CELL_API_Mode.h"
#include "stdint.h"
#include <string.h>

LOG_MODULE_DECLARE(cell,CONFIG_CELL_LOG_LEVEL);


static K_MUTEX_DEFINE(xbee_api_frame_tx_lock);

volatile uint8_t XBEE_SocketReponse__SocketID;
volatile uint8_t XBEE_SocketReponse__CloseSocketID;
volatile uint8_t XBEE_SocketReponse__CloseStatus;

volatile bool XBEE_SocketReponse__ConnectStatus_Received;

volatile bool XBEE_SocketReponse__CreateStatus_Received;
volatile uint8_t XBEE_SocketResponse_CreateStatus;

volatile bool XBEE_SocketReponse__CloseStatus_Received;

volatile uint8_t XBEE_SocketReponse__ConnectSocketID;
volatile uint8_t XBEE_SocketReponse__ConnectStatus;

volatile uint8_t XBEE_SocketOptionReponse__SocketID;
volatile uint8_t XBEE_SocketOptionReponse__OptionID;
volatile uint8_t XBEE_SocketOptionReponse__Status;
volatile uint8_t XBEE_SocketOptionReponse__OptionData;
volatile bool XBEE_SocketOptionReponse__Received;

size_t strnlen(const char *s, size_t maxlen)
{
	size_t n = 0;

	while (*s != '\0' && n < maxlen) {
		s++;
		n++;
	}

		return n;
}


void ProcessAT_Response(uint8_t FrameID, char *AT_Cmd, uint8_t * ParameterValue, uint32_t ParameterValueLength)
{
        uint16_t AT_CMD_Int = 0;

        uint32_t Len;
           
        (void)(FrameID);

        AT_CMD_Int = *(uint16_t *)AT_Cmd;

        switch (AT_CMD_Int)
        {

                default:

                        LOG_DBG("AT response of %c%c or 0x%04x", AT_Cmd[0], AT_Cmd[1], AT_CMD_Int);
                

                        if(ParameterValueLength == 0)
                        {
                            LOG_DBG("No parameter");
                        }
                        else
                        {
                            LOG_DBG("Parameter Value : ");
                            for(uint32_t i=0;i<ParameterValueLength;i++)
                            {
                                LOG_DBG("0x%02x ", ParameterValue[i]);          
                            }

                        }
                 
              
                        break;

                case AT_CMD__VR:

                    CellFirmwareVersionRecieved = TRUE;
                    CellFirmwareVersion = (uint32_t)(ParameterValue[0]<<24) +
                                          (uint32_t)(ParameterValue[1]<<16) +
                                          (uint32_t)(ParameterValue[2]<<8) +
                                          (uint32_t)ParameterValue[3];


                    LOG_DBG("Cell Firmware version 0x%08x.",CellFirmwareVersion);

                break;



                case AT_CMD__MV:

        
                    if(ParameterValueLength !=0)
                    {
                          ParameterValue[ParameterValueLength] = 0;

                          strncpy((char *)CellModuleFirmwareVersion,(char *)ParameterValue,sizeof(CellModuleFirmwareVersion));

                          LOG_DBG("Cell module firmware : %s",log_strdup((const char *)CellModuleFirmwareVersion));

                          CellModuleFirmwareVersionRecieved = true;

                    }

                break;



                case AT_CMD__VL:

                    
                    ParameterValue[ParameterValueLength] = 0;

                    LOG_DBG("%s",log_strdup(ParameterValue));

                break;

                case AT_CMD__AS:

                  
                    ParameterValue[ParameterValueLength] = 0;

                    LOG_DBG("%s",log_strdup(ParameterValue));

                break;

                case AT_CMD__NR:
                    
                    LOG_DBG("Network Reset Acknowledged.");
               
                break;

                case AT_CMD__FR:

                    LOG_DBG("Forced Reset Acknowledged.");
               
                break;


                case AT_CMD__AI:

                    if(ParameterValueLength == 0)
                    {
                            LOG_DBG("No parameters sent back with AI!");
                    }
                    else
                    {
                        CellLastConnectionState = ParameterValue[0];

                        #if CELL_DEBUG__AI_RESPONSE == 1
                        if(CellLastConnectionState != __CellLastConnectionState)
                        {
                            __CellLastConnectionState = CellLastConnectionState;
                            LOG_DBG("AI : [0x%02x] %s",CellLastConnectionState,log_strdup(Cell_GetConnectionStateString(CellLastConnectionState)));
                        }
                        #endif
                    }

                break;


                case AT_CMD__DT:


                    if(ParameterValueLength == 0)
                    {
                            LOG_DBG("DT Response : Network time not Available");
                    }
                    else if(ParameterValueLength >4)
                    {
                             LOG_DBG("DT Response : %d bytes received, expected 4",ParameterValueLength);
                    }
                    else
                    {
                         CellIncomingUnixTime = (((uint32_t)ParameterValue[0])<<24) | 
                                            (((uint32_t)ParameterValue[1])<<16) |
                                            (((uint32_t)ParameterValue[2])<<8) |
                                            (((uint32_t)ParameterValue[3]));

                            //Time from tower is relative to Jan 1, 200 at 00:00:00.  Adjust for
                            //unix expoch
                            CellIncomingUnixTime += 946684800;
                                
                            CellTimeFetchState = CELL_TIME_FETCH_STATE_COMPLETE;        

                    }

                break;


                case AT_CMD__IM:


                    if(ParameterValueLength == 0)
                    {
                            LOG_DBG("No IMEI Available");
                    }
                    else
                    {
                            Cell_IMEI_Rx = 1;

                            if(ParameterValueLength>MAX_IMEI_SIZE)
                            {
                                Len = MAX_IMEI_SIZE;
                            }
                            else
                            {
                                Len = ParameterValueLength;
                            }

                            memcpy(Cell_IMEI_String,ParameterValue,Len);
                            Cell_IMEI_String[Len] = 0;

                            LOG_DBG("IMEI %s",log_strdup(Cell_IMEI_String));          
                    }

                break;


                case AT_CMD__AN:


                    if(ParameterValueLength == 0)
                    {
                            LOG_DBG("No APN Available");
                    }
                    else
                    {
                            Cell_APN_Rx = 1;

                            LOG_DBG("APN : ");

                            if(ParameterValueLength>MAX_APN_SIZE)
                            {
                                Len = MAX_APN_SIZE;
                            }
                            else
                            {
                                Len = ParameterValueLength;
                            }

                            memcpy(Cell_APN,ParameterValue,Len);
                            Cell_APN[Len] = 0;

                            LOG_DBG("APN : %s",log_strdup(Cell_APN));          
                    }

                break;

               case AT_CMD__S_POUND:

                    if(ParameterValueLength == 0)
                    {
                            CellLastRSSI = 0;       LOG_DBG("No ICCID Available");
                    }
                    else
                    {
                            Cell_ICCID_Rx = 1;

                        
                            if(ParameterValueLength>MAX_ICCID_SIZE)
                            {
                                Len = MAX_ICCID_SIZE;
                            }
                            else
                            {
                                Len = ParameterValueLength;
                            }

                            memcpy(Cell_ICCID_String,ParameterValue,Len);
                            Cell_ICCID_String[Len] = 0;

                            LOG_DBG("ICCID : %s",log_strdup(Cell_ICCID_String));  
                    }
               
               break;


               case AT_CMD__MN:

                    if(ParameterValueLength == 0)
                    {
                            CellLastRSSI = 0;
                           // LOG_DBG("No Carrier Available");
                    }
                    else
                    {

                            Cell_Network_Rx = 1;

                            if(ParameterValueLength>MAX_OPERATOR_SIZE)
                            {
                                Len = MAX_OPERATOR_SIZE;
                            }
                            else
                            {
                                Len = ParameterValueLength;
                            }

                            memcpy(Cell_Operator_String,ParameterValue,Len);
                            Cell_Operator_String[Len] = 0;

                            LOG_DBG("Operator : %s",log_strdup(Cell_Operator_String)); 
                    }


               break;

                case AT_CMD__DB:

                    if(ParameterValueLength == 0)
                    {
                            CellLastRSSI = 0;
                    }
                    else
                    {
                           CellLastRSSI = (int8_t)ParameterValue[0];
                    }

                        #if CELL_DEBUG__RSSI == 1

                        if(__CellLastRSSI != CellLastRSSI)
                        {
                             __CellLastRSSI = CellLastRSSI;
                            if(CellLastRSSI == 0)
                                 LOG_DBG("RSSI : Not Available");
                            else
                                LOG_DBG("RSSI : -%ddbm",CellLastRSSI);
                        }
                        #endif
                       

                break;

                case AT_CMD__LA:

                        if (ParameterValueLength != 4)
                        {       
                                 CurrentHostLookupState = HOST_LOOKUP_ERROR;
                        }
                        else
                        {
                                LastLookupIP = (uint32_t)ParameterValue[3] << 24 |
                                                                (uint32_t)ParameterValue[2] << 16 |
                                                                (uint32_t)ParameterValue[1] << 8 |
                                                                (uint32_t)ParameterValue[0];

                                CurrentHostLookupState = HOST_LOOKUP_COMPLETE;
                        }

                        break;
        }

}

void Cell_SendTxt(char * PhoneNumber, char * Msg)
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);

        uint32_t Idx = 0;
        API_FrameBuffer[Idx++] = FRAME_TYPE__TX_SMS;
        API_FrameBuffer[Idx++] = 0x01;
        API_FrameBuffer[Idx++] = 0x00;

        memset(&API_FrameBuffer[Idx], 0, 20);

        strncpy((char *)&API_FrameBuffer[Idx], PhoneNumber, 19); //Make sure 20'th character is always null

        Idx += 19;
        API_FrameBuffer[Idx++] = 0x00;
        
        strncpy((char *)&API_FrameBuffer[Idx], Msg, 160);

        Idx+=strnlen(Msg, 160);

        TxAPI_Frame(&API_FrameBuffer[0], Idx);
    k_mutex_unlock(&xbee_api_frame_tx_lock);
}

void Cell_SendAT_Cmd(char * AT_Cmd,char * AT_Parameter,uint8_t FrameID)
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);
    
        uint32_t Idx = 0;
        API_FrameBuffer[Idx++] = FRAME_TYPE__AT;
        API_FrameBuffer[Idx++] = FrameID;
        API_FrameBuffer[Idx++] = AT_Cmd[0];
        API_FrameBuffer[Idx++] = AT_Cmd[1];
                
        strncpy((char *)&API_FrameBuffer[Idx], AT_Parameter, MAX_AT_CMD_PARAM_LENGTH);

        Idx += strnlen(AT_Parameter, MAX_AT_CMD_PARAM_LENGTH);

        TxAPI_Frame(&API_FrameBuffer[0], Idx);
    k_mutex_unlock(&xbee_api_frame_tx_lock);
}

void Cell_SendAT_Cmd_No_Param(char * AT_Cmd,uint8_t FrameID)
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);
    
        uint32_t Idx = 0;
   
        API_FrameBuffer[Idx++] = FRAME_TYPE__AT;
        API_FrameBuffer[Idx++] = FrameID;
        API_FrameBuffer[Idx++] = AT_Cmd[0];
        API_FrameBuffer[Idx++] = AT_Cmd[1];

        TxAPI_Frame(&API_FrameBuffer[0], Idx);
    k_mutex_unlock(&xbee_api_frame_tx_lock);
}

void Cell_SendAT_Cmd_w_Param(char * AT_Cmd,uint8_t AT_Parameter,uint8_t FrameID)
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);
    
        uint32_t Idx = 0;
 

        API_FrameBuffer[Idx++] = FRAME_TYPE__AT;
        API_FrameBuffer[Idx++] = FrameID;
        API_FrameBuffer[Idx++] = AT_Cmd[0];
        API_FrameBuffer[Idx++] = AT_Cmd[1];
        API_FrameBuffer[Idx++] = AT_Parameter; 

        TxAPI_Frame(&API_FrameBuffer[0], Idx);
    k_mutex_unlock(&xbee_api_frame_tx_lock);
}

void Cell_FetchIPFromHostName(char * HostName)
{

        Cell_SendAT_Cmd("LA", HostName, API_FRAME_ID_HOST_LOOKUP);
        CurrentHostLookupState = HOST_LOOKUP_IN_PROGRESS;
}

void Cell_FetchTime()
{
        CellTimeFetchState = CELL_TIME_FETCH_STATE_WAITING;
        Cell_SendAT_Cmd_No_Param("DT", API_FRAME_REQUEST_TIME);
}

void Cell_FetchRSSI()
{
       Cell_SendAT_Cmd_No_Param("DB", API_FRAME_REQUEST_RSSI);
}

void Cell_FetchIMEI()
{
    Cell_SendAT_Cmd_No_Param("IM", API_FRAME_REQUEST_IMEI);
}

void Cell_FetchOperator()
{
    Cell_SendAT_Cmd_No_Param("MN", API_FRAME_REQUEST_OPERATOR);
}

void Cell_FetchICCID()
{
    Cell_SendAT_Cmd_No_Param("S#", API_FRAME_REQUEST_ICCID);
}

void Cell_FetchPhoneNumber()
{
    Cell_SendAT_Cmd_No_Param("PH", API_FRAME_REQUEST_PHONE_NUMBER);
}

void Cell_FetchConnectionState()
{
    Cell_SendAT_Cmd_No_Param("AI", API_FRAME_REQUEST_CONNECTION_STATE);
}

void Cell_FetchVersion()
{
    CellFirmwareVersionRecieved = 0;
    Cell_SendAT_Cmd_No_Param("VR", API_FRAME_REQUEST_FIRMWARE);
}

void Cell_ModuleFetchVersion()
{
    CellModuleFirmwareVersionRecieved = 0;
    Cell_SendAT_Cmd_No_Param("MV", API_FRAME_REQUEST_FIRMWARE);
}

void Cell_EnterAirplane()
{
    Cell_SendAT_Cmd_w_Param("AM",1, API_FRAME_REQUEST_AIRPLANE);
}

void Cell_ExitAirplane()
{
    Cell_SendAT_Cmd_w_Param("AM",0, API_FRAME_REQUEST_AIRPLANE);
}

char * Cell_GetConnectionStateString(uint8_t ConnectionState)
{
   switch(ConnectionState)
    {
        case CELL_NETWORK_STATE__CONNECTED       :  return  "Connected to the Internet.";  break;
        case CELL_NETWORK_STATE__REGISTERING     :  return  "Registering to cellular network."; break;
        case CELL_NETWORK_STATE__CONNECTING      :  return  "Connecting to the Internet.";  break;
        case CELL_NETWORK_STATE__CORRUPT         :  return  "The cellular component is missing, corrupt, or otherwise in error. The cellular component requires a new firmware image.";break;
        case CELL_NETWORK_STATE__DENIED          :  return  "Cellular network registration denied."; break;
        case CELL_NETWORK_STATE__AIRPLANE        :  return  "Airplane mode."; break;
        case CELL_NETWORK_STATE__DIRECT          :  return  "Direct Mode.";  break; 
        case CELL_NETWORK_STATE__BYPASS          :  return  "Bypass Mode Active.";   break;
        case CELL_NETWORK_STATE__INITIALIZING    :  return  "Initializing"; break;
        default: return "???";
    }
}

void Cell_RequestVersion()
{
    CellFirmwareVersion = 0;
    CellFirmwareVersionRecieved = FALSE;
    Cell_SendAT_Cmd_No_Param("VR", API_FRAME_REQUEST_CONNECTION_STATE);
}

void Cell_ForceReset()
{
       Cell_SendAT_Cmd_No_Param("FR", API_FRAME_REQUEST_CONNECTION_STATE);
}

void Cell_NetworkReset()
{
       CellLastConnectionState = CELL_NETWORK_STATE__INITIALIZING;  

       Cell_SendAT_Cmd_No_Param("NR", API_FRAME_REQUEST_CONNECTION_STATE);
}

void Cell_ResetHostLookup()
{
        CurrentHostLookupState = HOST_LOOKUP_IDLE;
}

HostLookupState Cell_CheckForHostResolution(uint32_t *IP_Address)
{
        HostLookupState R = HOST_LOOKUP_IDLE;

        if (CurrentHostLookupState == HOST_LOOKUP_COMPLETE)
        {
                if (IP_Address != NULL)
                {
                        *IP_Address = LastLookupIP;
                }

                R =  HOST_LOOKUP_COMPLETE;

                CurrentHostLookupState = HOST_LOOKUP_IDLE;
        }
        else
        {
                R = CurrentHostLookupState;
        }

        return R;
}

void Cell_TxRequest_TLS(uint8_t FrameID,
                    uint32_t IP_Address, //BIG ENDIAN
                    uint16_t DestPort, 
                    uint16_t SrcPort, 
                                
                    Cell_Protocol CP,
                    uint8_t *Data,
                    uint32_t Length
                    )
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);
    
        (void)(CP);

        if (Length > 1500)
        {
                LOG_DBG("Max Tx Size is 1500 bytes.");
                        return;
        }

        uint32_t Idx = 0;
        API_FrameBuffer[Idx++] = FRAME_TYPE__IPV4_TX_TLS;
       
        API_FrameBuffer[Idx++] = FrameID;

        API_FrameBuffer[Idx++] = (IP_Address>> 24)&0xFF;
        API_FrameBuffer[Idx++] = (IP_Address>> 16)&0xFF;
        API_FrameBuffer[Idx++] = (IP_Address>> 8)&0xFF;
        API_FrameBuffer[Idx++] = IP_Address & 0XFF;


        API_FrameBuffer[Idx++] = (DestPort >> 8) & 0xFF;
        API_FrameBuffer[Idx++] = (DestPort & 0xFF);

        API_FrameBuffer[Idx++] = (SrcPort >> 8) & 0xFF;
        API_FrameBuffer[Idx++] = (SrcPort & 0xFF);

        API_FrameBuffer[Idx++] = (0);
        
        API_FrameBuffer[Idx++] = 0x00; //leave the socket open

        if (Length)
        {
                memcpy(&API_FrameBuffer[Idx], Data, Length);
                Idx += Length;
        }
        
        FrameID_LastStatus[FrameID] = TX_FRAME_STATUS_WAITING;
        
        TxAPI_Frame(&API_FrameBuffer[0], Idx);

k_mutex_unlock(&xbee_api_frame_tx_lock);

}


void Cell_TxRequest(uint8_t FrameID,
                    uint32_t IP_Address, //BIG ENDIAN
                    uint16_t DestPort, 
                    uint16_t SrcPort, 
                    Cell_Protocol CP,
                    uint8_t Flags,
                    uint8_t *Data,
                    uint32_t Length
                    )
{
     k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);
       
        if (Length > 1500)
        {
            LOG_DBG("Max Tx Size is 1500 bytes.");
            return;
        }

        uint32_t Idx = 0;
       API_FrameBuffer[Idx++] = FRAME_TYPE__IPV4_TX;
       
        API_FrameBuffer[Idx++] = FrameID;

        API_FrameBuffer[Idx++] = (IP_Address>> 24)&0xFF;
        API_FrameBuffer[Idx++] = (IP_Address>> 16)&0xFF;
        API_FrameBuffer[Idx++] = (IP_Address>> 8)&0xFF;
        API_FrameBuffer[Idx++] = IP_Address & 0XFF;


        API_FrameBuffer[Idx++] = (DestPort >> 8) & 0xFF;
        API_FrameBuffer[Idx++] = (DestPort & 0xFF);

        API_FrameBuffer[Idx++] = (SrcPort >> 8) & 0xFF;
        API_FrameBuffer[Idx++] = (SrcPort & 0xFF);

        API_FrameBuffer[Idx++] = (CP);
        
        API_FrameBuffer[Idx++] = Flags;

        if (Length)
        {
                memcpy(&API_FrameBuffer[Idx], Data, Length);
                Idx += Length;
        }
        
        FrameID_LastStatus[FrameID] = TX_FRAME_STATUS_WAITING;
        
        TxAPI_Frame(&API_FrameBuffer[0], Idx);

k_mutex_unlock(&xbee_api_frame_tx_lock);
}




int32_t Cell_SocketCreate(XBEE_SocketType_t XBEE_SocketType,uint8_t *SocketID)
{

    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);
    
    XBEE_SocketReponse__CreateStatus_Received = false;
    
    uint32_t Idx = 0;
    int32_t retval;

    API_FrameBuffer[Idx++] = FRAME_TYPE__SOCKET_CREATE;
       
    API_FrameBuffer[Idx++] = API_FRAME_REQUEST_SOCKET_CREATE;

    API_FrameBuffer[Idx++] = XBEE_SocketType;
        
    TxAPI_Frame(&API_FrameBuffer[0], Idx);
    
    uint32_t Timeout =0;

	do
	{
		k_sleep(K_MSEC(10));
		Timeout+=10;
			
		if(Timeout>5000)
		{
			LOG_ERR("Timeout creating UDP Socket");
			errno = ETIMEDOUT;
			retval = -ETIMEDOUT;
			goto error;
		}

	} while ( XBEE_SocketReponse__CreateStatus_Received == false);

    switch(XBEE_SocketResponse_CreateStatus)
    {
        case 0:
            LOG_DBG("Socket Create Success");
            *SocketID = XBEE_SocketReponse__SocketID;
            retval = 0;
        break;

        case 0x22:
            LOG_ERR("Socket Create: Not registered to cell network");
            errno = ENETDOWN;
            retval = -ENETDOWN;
        break;
        
        case 0x31:
            LOG_ERR("Socket Create: Socket Create: Internal Error");
            errno = EINVAL;
            retval = -EINVAL;
        break;
        
        case 0x32:
            LOG_ERR("Socket Create: Socket Create: Resource Error");
            errno = ENOMEM;
            retval = -ENOMEM;
        break;
        
        case 0x7B:
            LOG_ERR("Socket Create: Invalid Protocol");
            errno = EINVAL;
            retval = -EINVAL;
        break;
        
        case 0x7E:
            LOG_ERR("Socket Create: Modem update in process");
            errno = EBUSY;
            retval = -EBUSY;
        break;
        
        case 0x85:
            LOG_ERR("Socket Create: Unknown Error");
             errno = EINVAL;
            retval = -EINVAL;
        break;
        
        default:
            LOG_ERR("Socket Create: Unknown Response of %d",XBEE_SocketResponse_CreateStatus);
            errno = EINVAL;
            retval = -EINVAL;
        break;
    }

    error:

    k_mutex_unlock(&xbee_api_frame_tx_lock);
    return retval;
}


int32_t Cell_SocketOptionRequest(uint8_t SocketID)
{

    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);

    int retval = 0;

    uint32_t Idx = 0;
 
    API_FrameBuffer[Idx++] = FRAME_TYPE__SOCKET_OPTION_REQUEST;
       
    API_FrameBuffer[Idx++] = API_FRAME_REQUEST_SOCKET_OPTION; 

    API_FrameBuffer[Idx++] = SocketID;

    API_FrameBuffer[Idx++] = 0; //TLS Profile select

    API_FrameBuffer[Idx++] = 0; //Select TLS profile 1

    XBEE_SocketOptionReponse__Received = false;

    TxAPI_Frame(&API_FrameBuffer[0], Idx);
    
    uint32_t Timeout = 0;

    do
    {
        k_sleep(K_MSEC(10));
        Timeout+= 10;
        if(Timeout > 1000)
        {
            LOG_ERR("No response to XBEE socket option request");
            errno = ETIMEDOUT;
            retval = -ETIMEDOUT;
            goto error;
        }

    } while (XBEE_SocketOptionReponse__Received == false);

    if(XBEE_SocketOptionReponse__Status)
    {
         LOG_ERR("Error 0x%02x while setting socket option on XBEE Socket ID %d",XBEE_SocketOptionReponse__Status,SocketID);
         errno = EINVAL;
         retval = -EINVAL;          
    }
    else
    {
        LOG_DBG("Socket Option Success on XBEE Socket ID %d",SocketID);
    }

error:
    k_mutex_unlock(&xbee_api_frame_tx_lock);

    return retval;
}

int32_t Cell_SocketClose(uint8_t SocketID)
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);

    int retval;

    uint32_t Idx = 0;
 
    API_FrameBuffer[Idx++] = FRAME_TYPE__SOCKET_CLOSE;
       
    API_FrameBuffer[Idx++] = API_FRAME_REQUEST_SOCKET_CLOSE; //zero means don't send a response

    API_FrameBuffer[Idx++] = SocketID;
        
    TxAPI_Frame(&API_FrameBuffer[0], Idx);

    XBEE_SocketReponse__CloseStatus_Received = false;

    uint32_t Timeout = 0;
    uint32_t Attempts = 0;
    do
    {
        k_sleep(K_MSEC(10));
        Timeout+= 10;
        if(Timeout > 1000)
        {
            Attempts++;
            if(Attempts == 3)
            {
                LOG_ERR("Could not close socket after %d attempts",(Attempts));
                retval = -ETIMEDOUT;
                goto error;
            }
            else
            {
                LOG_WRN("Timeout waiting for socket close response... Attempt %d",(Attempts+1) );
                Timeout = 0;
                TxAPI_Frame(&API_FrameBuffer[0], Idx);
            }
           
        }

    } while (XBEE_SocketReponse__CloseStatus_Received == false);


    if(XBEE_SocketReponse__CloseStatus == 0)
    {
        LOG_DBG("Socket %d has been closed",XBEE_SocketReponse__CloseSocketID);
        retval = 0;
    }
    else
    {
        LOG_ERR("Socket %d was reported as invalid",XBEE_SocketReponse__CloseSocketID);
         retval = -EINVAL;
        
    }

error:
    k_mutex_unlock(&xbee_api_frame_tx_lock);

    return retval;
}

int32_t Cell_SocketConnect(uint8_t SocketID,uint32_t IP_NBO, uint16_t Port_NBO)
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);

    uint32_t Idx = 0;
    int32_t retval;

    XBEE_SocketReponse__ConnectStatus_Received = false;;

    API_FrameBuffer[Idx++] = FRAME_TYPE__SOCKET_CONNECT;
       
    API_FrameBuffer[Idx++] = API_FRAME_REQUEST_SOCKET_CONNECT; //zero means don't send a response

    API_FrameBuffer[Idx++] = SocketID;

    memcpy(&API_FrameBuffer[Idx],&Port_NBO,sizeof(Port_NBO));
    Idx+=sizeof(Port_NBO);

    API_FrameBuffer[Idx++] = 0; //Binary address in network byte order.

    memcpy(&API_FrameBuffer[Idx],&IP_NBO,sizeof(IP_NBO));
    Idx+=sizeof(IP_NBO);
        
    TxAPI_Frame(&API_FrameBuffer[0], Idx);

    uint32_t Timeout = 0;

    do
    {
        k_sleep(K_MSEC(10));
        Timeout+= 10;

        if(Timeout > 5000)
        {
            LOG_ERR("Timeout waiting for socket connect response");
            Timeout = 0;
            retval = -ETIMEDOUT;
            errno = ETIMEDOUT;
            goto error;
        }

    } while (XBEE_SocketReponse__ConnectStatus_Received == false);

    switch(XBEE_SocketReponse__ConnectStatus)
    {
        case 0x00:
            LOG_DBG("Socket %d connected",SocketID);
             retval = 0;
        break;
        case 0x01:
            LOG_ERR("Socket Connect : Invalid Destination Address Type");
            errno = EINVAL;
            retval = -EINVAL;
        break;
        case 0x02:
            LOG_ERR("Socket Connect : Invalid Address or Port");
            errno = EINVAL;
            retval = -EINVAL;
        break;
        case 0x03:
            LOG_ERR("Socket Connect : Socket Invalid Address or Port");
            errno = EINVAL;
            retval = -EINVAL;
        break;
        case 0x04:
            LOG_ERR("Socket already connected");
            errno = EALREADY;
            retval = -EALREADY;
        break;

        case 0x05:
            LOG_ERR("Socket connect not allowed on %i",SocketID);
            errno = EALREADY;
            retval = -EALREADY;
        break;
    
         case 0x20:
            LOG_ERR("Invalid Socket ID of %i",SocketID);
            errno = EINVAL;
            retval = -EINVAL;
        break;

        default:

            LOG_ERR("Unknown Socket connect response of %d",SocketID);
            errno = EINVAL;
            retval = -EINVAL;
       
        break;
    }
    
   
error:

    k_mutex_unlock(&xbee_api_frame_tx_lock);
    return retval;
}


int32_t Cell_SocketSend(uint8_t SocketID,uint8_t *Data,uint32_t Len)
{
    k_mutex_lock(&xbee_api_frame_tx_lock, K_FOREVER);

    uint32_t Idx = 0;
    int32_t retval = 0;

    API_FrameBuffer[Idx++] = FRAME_TYPE__SOCKET_SEND;
       
    API_FrameBuffer[Idx++] = API_FRAME_REQUEST_SOCKET_SEND; //zero means don't send a response

    API_FrameBuffer[Idx++] = SocketID;

    API_FrameBuffer[Idx++] = 0; //Transmit Options
    
    memcpy(&API_FrameBuffer[Idx],Data,Len);

    Idx+=Len;

     k_mutex_unlock(&xbee_api_frame_tx_lock);

   TxAPI_Frame(&API_FrameBuffer[0], Idx);

   return retval;
}

void ProcessAPI_Frame(uint8_t * FrameIn,uint16_t FrameLength);

static uint8_t CRC_Check;

void DecodeAPI_Frame(uint8_t DataIn)
{
        switch(CurrentAPI_FrameDecodeState)
        {
                        default:
                        case WAIT_FOR_START:
                        
                                if(DataIn == 0x7E)
                                {

                                        CurrentAPI_FrameDecodeState = GRAB_LENGTH_HIGH;

                                        API_FrameLength = 0;

                                        API_FrameCRC = 0;

                                        API_FrameInCounter = 0;

                                }

                        break;

                        case GRAB_LENGTH_HIGH:

                                API_FrameLength = ((uint16_t)(DataIn))<<8;

                                CurrentAPI_FrameDecodeState = GRAB_LENGTH_LOW;
                
                                break;          

                        case GRAB_LENGTH_LOW:

                                API_FrameLength |= DataIn;

                                if(API_FrameLength <MAX_API_FRAME_IN_BUFFER_SIZE)
                                {
                                        CurrentAPI_FrameDecodeState = GRAB_FRAME;
                                }
                                else
                                {
                                        CurrentAPI_FrameDecodeState = WAIT_FOR_START;
                                }

                        break;

                        case GRAB_FRAME:

                                API_FrameInBuffer[API_FrameInCounter] = DataIn;

                                API_FrameInCounter++;

                                API_FrameCRC+= DataIn;

                                if(API_FrameInCounter == API_FrameLength)
                                {
                                        CurrentAPI_FrameDecodeState = GRAB_CHECKSUM;
                                }

                        break;

                        

                        case GRAB_CHECKSUM:

                                CRC_Check = API_FrameCRC & 0xFF;

                                CRC_Check = 0xFF - CRC_Check;

                                if(CRC_Check == DataIn)
                                {
                                     ProcessAPI_Frame(&API_FrameInBuffer[0],API_FrameInCounter);
                                }
                                else
                                {
                                    #if CELL_DEBUG__BAD_API_FRAMING == 1
                                        LOG_ERR("Bad CRC!");
                                    #endif
                                }

                                CurrentAPI_FrameDecodeState = WAIT_FOR_START;

                        break;

        }

        

}

extern void update_socket_status(int32_t XBEE_SocketID,uint8_t Status);

void ProcessAPI_Frame(uint8_t * FrameIn,uint16_t FrameLength)
{
  
        #define AT_FRAME_ID             FrameIn[1]
        #define AT_CMD_0                FrameIn[2]
        #define AT_CMD_1                FrameIn[3]
        #define AT_STATUS               FrameIn[4]
        #define AT_PARAM_START          FrameIn[5]
        #define AT_PARAM_LENGTH         (FrameLength-5)

        if (FrameLength == 0)
        {
                #if CELL_DEBUG__BAD_API_FRAMING == 1
                    LOG_DBG("Cannot process zero length frame.");
                #endif
        }
        else
        {
            
            switch (FrameIn[0])
            {
                    default:
                            #if CELL_DEBUG__UNHANDLED_API_FRAME == 1
                                    LOG_DBG("Unhandled Frame type : 0x%02X", FrameIn[0]);
                            #endif
                    break;

                    case FRAME_TYPE__SOCKET_STATUS:
                        LOG_DBG("Updating socket %d to status 0x%02x", FrameIn[1],FrameIn[2]);
                        update_socket_status(FrameIn[1],FrameIn[2]);
                    break;

                    case FRAME_TYPE__MODEM_STATUS:

                        #if CELL_DEBUG__ASYNC_MODEM_STATUS == 1
                            LOG_DBG("Modem Status :%d ", FrameIn[0]);

                            switch(FrameIn[1])
                                {

                                    default:
                                         LOG_DBG("Unknown status code 0x%02x",FrameIn[1]);
                                    break;

                                    case 0:
                                         LOG_DBG("Hardware Reset");
            
                                    break;

                                    case 1:
                                        LOG_DBG("Watchdog timer reset");
                                    break;
        
                                    case 2:
                                        LOG_DBG("Registered wth cellular network");
                                    break;

                                    case 3:
                                        LOG_DBG("Unregistered with cellular network");
                   
                                    break;

                                    case 0x0E:
                                        LOG_DBG("Remote manager Connected");
                                    break;

                                    case 0x0F:
                                         LOG_DBG("Remote manager disconnected");
                                    break;

                                }
                           #endif



                    break;

                    case FRAME_TYPE__SOCKET_CREATE_RESPONSE:

                        XBEE_SocketResponse_CreateStatus = (int32_t)FrameIn[3];

                        if(FrameIn[2] != 0xFF)
                        {
                            XBEE_SocketReponse__SocketID = FrameIn[2];
                        }

                          XBEE_SocketReponse__CreateStatus_Received = true;

                    break;

                    case FRAME_TYPE__SOCKET_CLOSE_RESPONSE:

                        XBEE_SocketReponse__CloseSocketID = (int32_t)FrameIn[2];
                        XBEE_SocketReponse__CloseStatus = (int32_t)FrameIn[3];
                        
                        XBEE_SocketReponse__CloseStatus_Received = true;

                    break;

                    case FRAME_TYPE__SOCKET_OPTION_REQUEST_RESPONSE:

                        XBEE_SocketOptionReponse__SocketID = FrameIn[2];
                        XBEE_SocketOptionReponse__OptionID= FrameIn[3];
                        XBEE_SocketOptionReponse__Status=   FrameIn[4];
                        XBEE_SocketOptionReponse__OptionData= FrameIn[5];
                        
                        XBEE_SocketOptionReponse__Received = true;

                    break;

                    case FRAME_TYPE__SOCKET_CONNECT_RESPONSE:

                        XBEE_SocketReponse__ConnectSocketID = (int32_t)FrameIn[2];
                        XBEE_SocketReponse__ConnectStatus = (int32_t)FrameIn[3];
                        XBEE_SocketReponse__ConnectStatus_Received = true;

                    break;

                    case FRAME_TYPE__SOCKET_RECEIVE:

                        xbee_push_into_socket(FrameIn[2], &FrameIn[4], FrameLength - 4);
                    
                    break;

                    case FRAME_TYPE__SOCKET_RECEIVE_IPV4:

                    LOG_WRN("Socket IPv4_RX");

                    break;


                    case FRAME_TYPE__IPV4_RX:

                    LOG_WRN("IPv4_RX");

                    break;

                    case FRAME_TYPE__IPV4_TX_STATUS:

                            #if CELL_DEBUG__TX_ACK == 1
                            LOG_DBG("TX status : 0x%02X", FrameIn[2]);
                            #endif
                            FrameID_LastStatus[FrameIn[1]] = FrameIn[2];


                    break;

                    case FRAME_TYPE__RX_SMS:

                            LOG_DBG("Recieved Text Message");
                            LOG_DBG("    PH: %s",&FrameIn[1]);
                            LOG_DBG("   MSG: %s", &FrameIn[21]);

                    break;

                    case FRAME_TYPE__AT_RESPONSE:

                            if (FrameLength < 4)
                            {
                                    #if CELL_DEBUG__BAD_AT_FRAMING == 1
                                      LOG_DBG("AT Command response was too short. %d bytes recieved", FrameLength);
                                    #endif
                            }
                            else
                            {
                                    switch (AT_STATUS)//Status byte
                                    {
                                    default:
                                        
                                            #if CELL_DEBUG__BAD_AT_FRAMING == 1
                                                LOG_DBG("Invalid AT status of 0x%02x", AT_STATUS);
                                            #endif 
                                            break;

                                    case    AT_RESPONSE_OK:

                                            ProcessAT_Response(AT_FRAME_ID, (char *)(&AT_CMD_0), &AT_PARAM_START, AT_PARAM_LENGTH);

                                            break;

                                    case    AT_RESPONSE_ERROR:
                                        
                                            LOG_DBG("AT Frame ID of 0x%02x reported an error", AT_FRAME_ID);

                                            break;
                                    case    AT_RESPONSE_INVALID_COMMAND:

                                            LOG_DBG("AT Frame ID of 0x%02x reported an invalid command of %c%c", AT_FRAME_ID, AT_CMD_0,AT_CMD_1);

                                            break;
                                    case            AT_RESPONSE_INVALID_PARAMETER:
                                        
                                            LOG_DBG("AT Frame ID of 0x%02x with command %c%c reported and invalid parameter", AT_FRAME_ID, AT_CMD_0, AT_CMD_1);

                                            break;

                                    }
                            }
                        

                            break;

            }
        }


        #undef AT_FRAME_ID 
        #undef AT_CMD_0 
        #undef AT_CMD_1 
        #undef AT_STATUS   
        #undef AT_PARAM_START   
        #undef AT_PARAM_LENGTH  
}


