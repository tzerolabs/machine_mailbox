#include "XBEE_CELL__Portable.h"
#include "XBEE_CELL_Common.h"
#include "XBEE_CELL_CommandMode.h"
#include <drivers/uart.h>
#include <sys/ring_buffer.h>

bool CellUART_TxActive = false;

/***
 *      _   _   _    ____ _____                     
 *     | | | | / \  |  _ \_   _|                    
 *     | | | |/ _ \ | |_) || |                      
 *     | |_| / ___ \|  _ < | |                      
 *      \___/_/   \_\_| \_\|_|  _     _             
 *     |  _ \| |_   _ _ __ ___ | |__ (_)_ __   __ _ 
 *     | |_) | | | | | '_ ` _ \| '_ \| | '_ \ / _` |
 *     |  __/| | |_| | | | | | | |_) | | | | | (_| |
 *     |_|   |_|\__,_|_| |_| |_|_.__/|_|_| |_|\__, |
 *                                            |___/ 
 */



#define CELL_SERIAL_QUEUE_SIZE          1600

RING_BUF_DECLARE(cell_rb_tx_out, CELL_SERIAL_QUEUE_SIZE);
RING_BUF_DECLARE(cell_rb_rx_in, CELL_SERIAL_QUEUE_SIZE);

struct device *CELL_GPIO_dev;

#define CELL_RST_PIN  1
#define CELL_PWR_PIN  9

struct uart_config cell_uart_cfg;
const struct device *CELL_UART;

  void cell_uart_cb_handler(const struct device *dev, void *app_data)
    {
        uint8_t IncomingChar;
        uint32_t Count = 0 ;

        while (uart_irq_update(dev) && uart_irq_is_pending(dev))
        {
            if (uart_irq_rx_ready(dev)) 
            {
                if((Count =uart_fifo_read(dev, &IncomingChar, 1)))
                {
                   ring_buf_put(&cell_rb_rx_in,&IncomingChar,1);
                }
            }

           // if (uart_irq_tx_ready(dev))
          //  {
               /* if (FourCH_TxOutLen> 0)
                {
                    
                    n = uart_fifo_fill(dev, 
                                      (const uint8_t *)FourCH_TxOut,
                                       FourCH_TxOutLen);
                  
                    FourCH_TxOutLen -= n;
                    FourCH_TxOut += n;
                }

                if(FourCH_TxOutLen == 0 && uart_irq_tx_complete(CELL_UART))
                {
                    TEST_PIN_4_CLEAR;    
                    uart_irq_tx_disable(CELL_UART);
                }*/
          //  }

            
        }
    }

void Cell_IO_Init()
{
 
   CELL_GPIO_dev = (struct device *)device_get_binding("GPIO_1");
   
    gpio_pin_configure(CELL_GPIO_dev,CELL_RST_PIN,GPIO_OUTPUT);
    gpio_pin_configure(CELL_GPIO_dev,CELL_PWR_PIN,GPIO_OUTPUT);
    
    Cell_PowerEnableLow();
    Cell_ResetHigh();

                 
    CELL_UART = device_get_binding("UART_1");

    cell_uart_cfg.baudrate = 115200;

    cell_uart_cfg.flow_ctrl = UART_CFG_FLOW_CTRL_NONE;

    cell_uart_cfg.data_bits = UART_CFG_DATA_BITS_8;

    cell_uart_cfg.parity = UART_CFG_PARITY_NONE;
      
    cell_uart_cfg.stop_bits = UART_CFG_STOP_BITS_1;

    uart_irq_callback_user_data_set(CELL_UART, cell_uart_cb_handler, NULL);

    uart_irq_rx_enable(CELL_UART);

    Cell_SetBaud(115200);


   
}

/*
void CELL_UART_HANDLER(void)
{
    uint8_t DataOut = 0;

    while (CELL_UART->EVENTS_RXDRDY)
    {
        CELL_UART->EVENTS_RXDRDY = 0;
        (void) CELL_UART->EVENTS_RXDRDY;

        ByteEnqueue(&CellInputQueue,CELL_UART->RXD);

    }

  
    if (CELL_UART->EVENTS_TXDRDY)
    {
        CELL_UART->EVENTS_TXDRDY = 0;
        (void) CELL_UART->EVENTS_TXDRDY;

        if(BytesInQueue(&CellOutputQueue) == 0)
        {
            CELL_UART->TASKS_STOPTX = 1;
            CellUART_TxActive = 0;
      
        }
        else
        { 
        
             ByteDequeue(&CellOutputQueue,&DataOut);
     
             CELL_UART->TXD = DataOut;
        }

    }
}
    */


void Cell_MoveQueues()
{
    // uint8_t DataOut;
     
     if (CellUART_TxActive == 0)
       {
         /*
           if(BytesInQueue(&CellOutputQueue))
           {
                 ByteDequeue(&CellOutputQueue,&DataOut);
               
                 CELL_UART->EVENTS_TXDRDY = 0;
    
                 CELL_UART->TASKS_STARTTX = 1;
    
                 CELL_UART->TXD = DataOut;
    
                 CellUART_TxActive = 1;
            }
            */
      }
}



void Cell_SetBaud(uint32_t Baud)
{
    cell_uart_cfg.baudrate = Baud;
   
    uart_configure(CELL_UART, &cell_uart_cfg); 
}

uint32_t Cell_GetBaud()
{
    return cell_uart_cfg.baudrate;
}

void Cell_ResetHigh()
{
    gpio_pin_set(CELL_GPIO_dev,CELL_RST_PIN,1);
}

void Cell_ResetLow()
{
     gpio_pin_set(CELL_GPIO_dev,CELL_RST_PIN,0);
}

void Cell_PowerEnableHigh()
{
  gpio_pin_set(CELL_GPIO_dev,CELL_PWR_PIN,1);
}

void Cell_PowerEnableLow()
{
  gpio_pin_set(CELL_GPIO_dev,CELL_PWR_PIN,0);
}


void Cell_WriteByteToTxQueue(uint8_t DataOut)
{
   uart_poll_out(CELL_UART,DataOut);
}

void Cell_WriteBytesToTxQueue(uint8_t *DataOut, uint32_t Len)
{
       for(int i=0;i<Len;i++)
        {
            uart_poll_out(CELL_UART,DataOut[i]);
        }
}

void Cell_WriteStringToTxQueue(char *DataOut)
{
    for(int i=0;i<strlen(DataOut);i++)
    {
      uart_poll_out(CELL_UART,DataOut[i]);
    }

}

uint32_t Cell_ByteRead(uint8_t *Buf, uint32_t BufSize)
{
    return ring_buf_get(&cell_rb_rx_in,Buf,BufSize);
}

