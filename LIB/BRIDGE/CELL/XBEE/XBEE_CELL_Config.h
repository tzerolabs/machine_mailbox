#ifndef XBEE_CONFIG_H
#define XBEE_CONFIG_H

#define CONFIG_CELL_LOG_LEVEL 4


#define MAX_API_FRAME_IN_BUFFER_SIZE    (1500 + 16)
#define API_FRAME_BUFFER_SIZE           MAX_API_FRAME_IN_BUFFER_SIZE
#define MAX_AT_CMD_PARAM_LENGTH         64
#define RX_BUFFER_SIZE                  MAX_API_FRAME_IN_BUFFER_SIZE
#define NUM_PACKET_BUFFERS              8



#define CELL_DEBUG__TX_ACK              (0)
#define CELL_DEBUG__RX_IP_IN            (0)
#define CELL_DEBUG__RX_IP_OVERFLOW      (1)

#define CELL_DEBUG__BAD_AT_FRAMING      (1)
#define CELL_DEBUG__BAD_API_FRAMING     (1)
#define CELL_DEBUG__UNHANDLED_API_FRAME (1)

#define CELL_DEBUG__ASYNC_MODEM_STATUS  (1)
#define CELL_DEBUG__AI_RESPONSE         (1)
#define CELL_DEBUG__RSSI                (1)


#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


#define MAX_IMEI_SIZE       16
#define MAX_ICCID_SIZE      20
#define MAX_OPERATOR_SIZE   32
#define MAX_APN_SIZE        100



#endif