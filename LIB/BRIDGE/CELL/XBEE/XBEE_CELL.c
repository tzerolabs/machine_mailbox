#include "stdint.h"
#include "stdio.h"
#include "System.h"
#include "string.h"
#include <stdlib.h>     

#include "XBEE_CELL.h"
#include "XBEE_CELL__Portable.h"
#include "XBEE_CELL_Config.h"
#include "XBEE_CELL_Common.h"
#include "XBEE_CELL_CommandMode.h"
#include "XBEE_CELL_API_Mode.h"
#include "rt_config.h"
#include "thread_wdt.h"
#include <modem/lte_lc.h>
#include "FIRMWARE_VERSION.h"
#include <shell/shell.h>
#include <cJSON.h>
#include "Cell.h"

LOG_MODULE_REGISTER(cell,CONFIG_CELL_LOG_LEVEL);

K_THREAD_STACK_DEFINE(my_cell_thread_stack_area, 2048);
K_THREAD_STACK_DEFINE(my_cell_state_thread_stack_area, 2048);

struct k_thread my_cell_thread_data;
struct k_thread my_cell_state_thread_data;

char APN_String[MAX_APN_SIZE+1];

RT_CONFIG_ITEM(APN,"Cellular APN",APN_String,RT_CONFIG_DATA_TYPE_STRING,sizeof(APN_String),"","","tzerolabs01.com.attz");

uint32_t CellRequestTimer = 0;

int32_t LastTxStatus = -1;

uint32_t CellProcessTimer;

uint32_t CellInactivityTimeOut = 0;

uint32_t CellBootParametersCheck = 0;

uint32_t CellReconnects =0;

int32_t LastAP_ParameterInt=0;

uint32_t XBEE_SettingsAreDirty = 0;

uint32_t CellNetworkTimeCheck = 0;

uint32_t CellInactivityResets = 0;

bool CellModemCheck = false;

char NetStatusString[64];
volatile enum lte_lc_nw_reg_status NetworkStatus;
volatile int32_t Cell_ID = -1;  // Modem reports this when disconnected!
volatile int32_t Cell_TrackingArea = 65554; //reports 65554 when disconnected


void DecodeCommand_Response(uint8_t DataIn);

uint32_t CellInactivityTimer;

void Cell_ResetConnectionState();

void Cell_ResetStateVariables()
{
    CellLastRSSI = 0;

    CellLastConnectionState = CELL_NETWORK_STATE__INITIALIZING;

    CurrentHostLookupState = HOST_LOOKUP_IDLE; 

    memset(Cell_IMEI_String,0,sizeof(Cell_IMEI_String));
    memset(Cell_ICCID_String,0,sizeof(Cell_ICCID_String));
    memset(Cell_Operator_String,0,sizeof(Cell_Operator_String));
    memset(Cell_APN,0,sizeof(Cell_APN));

    Cell_IMEI_Rx = 0;

    Cell_ICCID_Rx = 0;

    Cell_Network_Rx = 0;

    Cell_APN_Rx = 0;

    CellInfoRequested = 0;

    CellModuleFirmwareVersionRecieved = 0;

    XBEE_SettingsAreDirty = 0;

    CellInactivityResets = 0;

    CellInactivityTimer = System_Get_mS_Ticker(); 
}

void Cell_StateManagement(void *a,void *b,void *c);

int32_t cell_state_thread_wdt_channel = -1;
int32_t cell_process_thread_wdt_channel = -1;

void Cell_Init()
{
    Cell_IO_Init();

    Cell_ResetHigh();

    k_thread_create(&my_cell_thread_data, my_cell_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(my_cell_thread_stack_area),
                    Cell_Process,
                    NULL, NULL, NULL,
                    K_HIGHEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);
        
    k_thread_create(&my_cell_state_thread_data, my_cell_state_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(my_cell_state_thread_stack_area),
                    Cell_StateManagement,
                    NULL, NULL, NULL,
                    K_HIGHEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);
}

void Cell_Reset(CELL_ResetType ResetType)
{
        Cell_ResetConnectionState();

        switch(ResetType)
        {
            default:
                LOG_DBG("Unknown cell reset type of 0x%02x.  Using Hard reset...",ResetType);
                LOG_DBG("Reseting cellular");

            case CELL_RESET_HARD:
            
                LOG_DBG("Reseting cellular via reset pin");
               
                Cell_ResetHigh();
                k_sleep(K_MSEC(50));
                Cell_ResetLow();
                
                k_sleep(K_MSEC(500));
                Cell_ResetHigh();

            break;

            case CELL_RESET_AIRPLANE_SOFT:
                LOG_DBG("Reseting cellular using airplane soft method..");
                LOG_DBG("Entering Airplane Mode and waiting 5 seconds");
                Cell_EnterAirplane();
                k_sleep(K_MSEC(5000));
                LOG_DBG("Exiting Airplane Mode");
                Cell_ExitAirplane();
            break;
        }

        CellInactivityTimer = System_Get_mS_Ticker();

     
}

bool Cell_IsConnected()
{
    return (Cell_GetXBEEConnectionState() == CELL_NETWORK_STATE__CONNECTED);
}

void Cell_PowerCycle()
{
    LOG_DBG("Power cycling cell");
 
    Cell_PowerEnableLow();
    k_sleep(K_MSEC(3000));
    Cell_PowerEnableHigh();
      
    xbee_notify_all_closed();
       
    Cell_ResetStateVariables();

}

uint8_t  Cell_GetXBEEConnectionState()
{
    return CellLastConnectionState;
}

void Cell_ResetConnectionState()
{
    CellLastConnectionState = CELL_NETWORK_STATE__INITIALIZING;
}

int  Cell_GetRSSI()
{
    return CellLastRSSI;
}

int Cell_GetLastPostedRSSI()
{
    return CellLastRSSI;
}

char *  Cell_IMEI()
{
    return &Cell_IMEI_String[0];
}

char *  Cell_ICCID()
{
    return &Cell_ICCID_String[0];
}

char *  Cell_GetOperator()
{
    return &Cell_Operator_String[0];
}

char *  Cell_ModemFirmware()
{
  if(CellModuleFirmwareVersionRecieved)
  {
    return (char *)&CellModuleFirmwareVersion[0];
  }
  else 
  {
    return  "";
  }
}

uint8_t EvenOdd = 0;

void RequestCellModemData()
{
    /*
         Request IMEI data, etc
    */

    if(Check_mS_Timeout(&CellRequestTimer,1000))
     {
            EvenOdd++;
            if(EvenOdd &0x01)
            {
                Cell_FetchRSSI();
            }
            else
            {
                Cell_FetchConnectionState();
            }
     
            switch(CellInfoRequested)
                  {


                        case 0:
                        if(CellFirmwareVersionRecieved == 0)
                        {
                            Cell_FetchVersion();
                        }
                        else
                        {
                            CellInfoRequested++;
                        }
                        break;


                        case 1:
                        if(CellModuleFirmwareVersionRecieved == 0)
                        {
                            Cell_ModuleFetchVersion();
                        }
                        else
                        {
                            CellInfoRequested++;
                        }
                        break;

                     case 2:
                        if(Cell_IMEI_Rx == 0)
                        {
                            Cell_FetchIMEI();
                        }
                        else
                        {
                            CellInfoRequested++;
                        }
                        break;

                     case 3:

                        if(Cell_ICCID_Rx == 0)
                        {
                            Cell_FetchICCID();
                        }
                        else
                        {
                            CellInfoRequested++;
                        }
                    
                    break;

                        case 4:

                        if(Cell_Network_Rx == 0)
                        {
                           Cell_FetchOperator();
                        }
                        else
                        {
                            CellInfoRequested++;
                        }

                        break;

                    default:

                    break;
             
                 }
      }
}

uint8_t PollCommandResponse(uint32_t TimeoutMSec)
{
    uint32_t TimeoutCnt = 0;
    
    do
    {
        if(XBEE_GetLastCommandReponseState() != COMMAND_RESPONSE_WAITING)
        {
                break;
        }
        
        thread_wdt_channel_feed(cell_state_thread_wdt_channel);

        k_sleep(K_MSEC(50));
       
        TimeoutCnt+=50;

    } while (TimeoutCnt <= TimeoutMSec);

    return XBEE_GetLastCommandReponseState();
}

void Cell_StateManagement(void *a,void *b,void *c)
{ 
 Init:  

    cell_state_thread_wdt_channel = thread_wdt_add_channel();

    Cell_ResetStateVariables();

    Cell_SetBaud(115200);
    
    k_sleep(K_MSEC(100));

    LOG_DBG("Process Init");
    
  Start:   

    CellProcessTimer = System_Get_mS_Ticker();
             
    CellRequestTimer = System_Get_mS_Ticker();
       
    Cell_PowerCycle();      

    k_sleep(K_MSEC(1000));

    LOG_DBG("Sending +++ after 1 second guard");
                       
    XBEE_EnterCommandMode();

    LOG_DBG("Waiting for command mode OK at %d",Cell_GetBaud());
  
    if(PollCommandResponse(2000) != COMMAND_RESPONSE_OK)
    {
        if(Cell_GetBaud() == 9600)
        {
            while(1)
            {
                CellLastConnectionState = CELL_NETWORK_STATE__CORRUPT;
                k_sleep(K_MSEC(1000));
                LOG_ERR("Critical Cell Error");
            }

        }
        else
        {
            LOG_DBG("No reponse to entering command mode at 115200");
            LOG_DBG("Trying 9600 Baud");
            Cell_SetBaud(9600);
        }

         goto Start;
    }

    if(Cell_GetBaud() == 9600)
    {
        LOG_DBG("We are in command mode at 9600 Baud. Changing to 115200");
        XBEE_AT_Send_BD_115200(); 
        
        if(PollCommandResponse(2000)!=COMMAND_RESPONSE_OK)
        {
              LOG_ERR("Error sending 115200 baud change");
              goto Init;
        }
     
        XBEE_AT_Send_WR();
        k_sleep(K_MSEC(500));
        XBEE_AT_Send_AC();
    
        if(PollCommandResponse(2000)!=COMMAND_RESPONSE_OK)
        {
              LOG_ERR("Error saving change");
              goto Init;
        }
     
        goto Init;
    }

    XBEE_SettingsAreDirty = 0;

    LOG_DBG("Checking current API MODE");

    XBEE_AT_Request_AP_API_Mode();
  
    if(PollCommandResponse(2000) == COMMAND_RESPONSE_OK)
    {
         LastAP_ParameterInt = atoi(GetLastAT_CommandResponse());
                            
        if(LastAP_ParameterInt != 1)
        {
            LOG_WRN("XBEE Mode is currently %i,  setting to 1",LastAP_ParameterInt);
                         
             XBEE_AT_Set_AP_API_Mode();

             if(PollCommandResponse(2000) != COMMAND_RESPONSE_OK)
             {
                LOG_ERR("Error changing API mode.  Reseting...");
                goto Init;
             }
             else
             {
                LOG_DBG("API mode has been set");
                XBEE_SettingsAreDirty++;
            }
        }
    }
    else
    {
         LOG_DBG("No response at to requesting API mode, hard reset");
         goto Init;
    }

    LOG_DBG("Checking APN");

    XBEE_AT_Request_APN();

    if(PollCommandResponse(5000) == COMMAND_RESPONSE_OK)
    {
        if(strncmp(GetLastAT_CommandResponse(),APN_String,MAX_APN_SIZE) == 0)
        {
            LOG_DBG("APN in MODEM matches config  : %s",log_strdup(APN_String));
        }
        else
        {
            LOG_WRN("APN in modem is %s.  Changing to %s",log_strdup(GetLastAT_CommandResponse()),log_strdup(APN_String));

            XBEE_AT_Set_APN(APN_String);

            if(PollCommandResponse(5000) == COMMAND_RESPONSE_OK)
            {
                LOG_DBG("APN mode has been set");
                         
                XBEE_SettingsAreDirty++;
            }
            else
            {
                LOG_ERR("Could not change APN");
                goto Init;
            }
        }
    }
    else
    {
         LOG_ERR("No response at to checking APN, hard reset");
         goto Init;
    }


    LOG_DBG("Checking Preferred Network Tech");

    XBEE_AT_GetPreferredNetworkTechnology();

    if(PollCommandResponse(5000) == COMMAND_RESPONSE_OK)
    {
        LastAP_ParameterInt = atoi(GetLastAT_CommandResponse());
                            
        if(LastAP_ParameterInt != 2)
        {
          LOG_DBG("XBEE preferred network mode is currently %i,  setting to 2 (LTE-M)",LastAP_ParameterInt);
                       
          XBEE_AT_SetPreferredNetworkTechnology();
          
          if(PollCommandResponse(5000) == COMMAND_RESPONSE_OK)
          {
             LOG_DBG("Preferred network mode has been set to LTE-M");
                         
             XBEE_SettingsAreDirty++;
          }
          else
          {
              LOG_ERR("No response changing network mode");
              goto Init;
          }
                                
        }
        else
        {
            LOG_DBG("Preferred network technology is 2 (LTE-M)");
        }
    }
    else
    {
         LOG_ERR("No response at to checking Network Tech, hard reset");
         goto Init;
    }


    if(XBEE_SettingsAreDirty)
    {
        LOG_DBG("%d settings have changed.  Writing....",XBEE_SettingsAreDirty);
        XBEE_AT_Send_WR();

        if(PollCommandResponse(5000) == COMMAND_RESPONSE_OK)
        {
           LOG_DBG("%d settings have saved.  Restarting",XBEE_SettingsAreDirty);
           goto Init;
        }
        else
        {
              LOG_ERR("No response to settings save");
              goto Init;
        }
    }
    else
    {
       LOG_DBG("Exiting command mode ");
       XBEE_AT_ExitCommandMode();
    }

    while(1)
    {

        thread_wdt_channel_feed(cell_state_thread_wdt_channel);

        k_sleep(K_MSEC(500));
        RequestCellModemData();
             
        switch(Cell_GetXBEEConnectionState())
        {

                case CELL_NETWORK_STATE__CONNECTED:  
                     CellInactivityTimer = System_Get_mS_Ticker(); 
                break;

                case CELL_NETWORK_STATE__REGISTERING     :        
                break;

                case CELL_NETWORK_STATE__CONNECTING      :        
                break;

                case CELL_NETWORK_STATE__CORRUPT         : 

                    LOG_ERR("CELL Corrupt. Waiting 5 seconds and then resetting");
                    k_sleep(K_MSEC(5000));
                 //    Cell_PowerCycle();
                 
                    Cell_Reset(CELL_RESET_AIRPLANE_SOFT);

                break;

                case CELL_NETWORK_STATE__DENIED          : 

                    LOG_ERR("CELL ACCESS DENIED. Waiting 5 seconds and then resetting");
                    k_sleep(K_MSEC(5000));
                   // Cell_PowerCycle();
                    Cell_Reset(CELL_RESET_AIRPLANE_SOFT);

                break;

                case CELL_NETWORK_STATE__AIRPLANE        :        
                break;

                case CELL_NETWORK_STATE__DIRECT          :      
                break;

                case CELL_NETWORK_STATE__PSM             : 
                        /*
                            When we 1st wrote the 1st version of the firmware,
                            state 0x2C did not exist in the manual
                            It seems that it low signal strength,
                             sometimes the modem ends up in state 0x2C. 
                             We are just going to reset in this case
                        */

                       Cell_Reset(CELL_RESET_AIRPLANE_SOFT); 
                        Cell_PowerCycle();
                break;

                case CELL_NETWORK_STATE__MODEM_SHUT_DOWN : 
                break;

                case CELL_NETWORK_STATE__LOW_VOLTAGE_SHUT_DOWN : 
                break;

                case CELL_NETWORK_STATE__BYPASS               :
                break;

                case CELL_NETWORK_STATE__UPDATE_IN_PROGRESS  :        
                break;

                case CELL_NETWORK_STATE__REG_TEST          :
                break;

                case CELL_NETWORK_STATE__INITIALIZING    :        
                break;

                default:
                    LOG_ERR("Unknown XBEE state : 0x%02X",Cell_GetXBEEConnectionState());
                break;
        }

        if(Cell_GetXBEEConnectionState() != CELL_NETWORK_STATE__CONNECTED)
        {
                if(Check_mS_Timeout(&CellInactivityTimer, 60*5*1000))
                {
                    LOG_ERR("No activity on cell for 5 minutes,  reseting modem.");
                
                    CellInactivityResets++;
                
                    if(CellInactivityResets < 3)
                    {
                      Cell_Reset(CELL_RESET_AIRPLANE_SOFT);
                    }
                    else
                    {
                        CellInactivityResets = 0;
                        Cell_PowerCycle();
                    }
                }
        }
    }

}

void Cell_Process(void *a, void *b, void *c)
{
    uint8_t ReadBuf[256];

    cell_process_thread_wdt_channel = thread_wdt_add_channel();

    while(1)
    {
        k_sleep(K_MSEC(1));
        
        thread_wdt_channel_feed(cell_process_thread_wdt_channel);

        Cell_MoveQueues();

        uint32_t Available = Cell_ByteRead(&ReadBuf[0], sizeof(ReadBuf));

        for(uint32_t i=0; i< Available ;i++)
        {
            
            if(XBEE_Mode == XBEE_COMMAND_MODE)
            {
                    
                DecodeCommand_Response(ReadBuf[i]);
            }
            else
            {
                DecodeAPI_Frame(ReadBuf[i]);
            }
        }
    }
}

char * Cell_GetTxStatusErrorCodeString(uint8_t StatusCode)
{
        char *r = "Unknown Status Code";

        switch (StatusCode)
        {
                default: break;
                case 0x0:       r = "Successful transmit";  break;
                case 0x21:      r = "Failure to transmit to cell network";  break;
                case 0x22:      r = "Not registered to cell network";  break;
                case 0x2c:      r = "Invalid frame values (check the phone number)";  break;
                case 0x31:      r = "Internal error";  break;
                case 0x32:      r = "Resource error (retry operation later).";  break;
                case 0x74:      r = "Message too long";  break;
                case 0x78:      r = "Invalid UDP port";  break;
                case 0x79:      r = "Invalid TCP port";  break;
                case 0x7A:      r = "Invalid host address";  break;
                case 0x7B:      r = "Invalid data mode";  break;
                case 0x7C:      r = "Invalid interface. See User Data Relay - 0x2D.";  break;

                case 0x7D:      r = "Interface not accepting frames. See User Data Relay - 0x2D.";  break;

                case 0x80:      r = "Connection refused";  break;
                case 0x81:      r = "Socket connection lost";  break;
                case 0x82:      r = "No server"; break; 
                case 0x83:      r = "Socket closed"; break;
                case 0x84:      r = "Unknown server"; break;
                case 0x85:      r = "Unknown error"; break;
                case 0x86:      r = "Invalid TLS configuration (missing file, and so forth)"; break;
        }

        return r;

}

uint8_t ComputeAPI_CheckSum(uint8_t *DataIn, uint32_t LengthIn)
{
        uint32_t Sum = 0;

        for (uint32_t i = 0; i < LengthIn; i++)
        {
                Sum += DataIn[i];
        }

        uint8_t CheckSum = 0xFF - (Sum & 0xFF);
        
        return CheckSum;

}

void TxAPI_Frame(uint8_t *DataIn, uint32_t LengthIn)
{
        uint8_t Data = 0;

        if (LengthIn)
        {
                
                Cell_WriteByteToTxQueue(0x7E);
                Cell_WriteByteToTxQueue((LengthIn ) >> 8);
                Cell_WriteByteToTxQueue((LengthIn & 0xFF));
                Cell_WriteBytesToTxQueue(DataIn,LengthIn);

                Data=ComputeAPI_CheckSum(DataIn, LengthIn);
                
                Cell_WriteByteToTxQueue(Data);
                
                Cell_MoveQueues();
        }
        
}

char * Cell_COPS()
{
    return "";
}

int32_t Cell_GetCellID()
{
	return Cell_ID;
}

int32_t Cell_GetTracking()
{
	return Cell_TrackingArea;
}

char * GetNetStatusString(enum lte_lc_nw_reg_status  S)
{
	char * result = "";

		switch(S)
		{
			default:								result = "Unhandled";break;
			case LTE_LC_NW_REG_NOT_REGISTERED:		result = "Not Registered";break;
			case LTE_LC_NW_REG_REGISTERED_HOME:		result = "Connected - Home";break;
			case LTE_LC_NW_REG_SEARCHING:			result = "Searching";break;
			case LTE_LC_NW_REG_REGISTRATION_DENIED:	result = "Regisration Denied";break;
			case LTE_LC_NW_REG_UNKNOWN:			    result = "Unknown";break;
			case LTE_LC_NW_REG_REGISTERED_ROAMING:  result = "Connected - Roaming";break;
			case LTE_LC_NW_REG_REGISTERED_EMERGENCY:result = "Connected - Emergency";break;
			case LTE_LC_NW_REG_UICC_FAIL:			result = "UICC Fail";break;

		}

	snprintf(NetStatusString,sizeof(NetStatusString),"[0x%02x] %s",NetworkStatus,result);

	return &NetStatusString[0];

}

bool Cell_GetConnectionState(Cell_ConnectionState_t * C)
{
    switch (Cell_GetXBEEConnectionState())
    {
        default: C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN; break;
        case CELL_NETWORK_STATE__CONNECTED:  C->NetworkStatus = LTE_LC_NW_REG_REGISTERED_HOME;   break;
        case CELL_NETWORK_STATE__REGISTERING:C->NetworkStatus = LTE_LC_NW_REG_SEARCHING;   break;   
        case CELL_NETWORK_STATE__CONNECTING:C->NetworkStatus = LTE_LC_NW_REG_SEARCHING; break;       
        case CELL_NETWORK_STATE__CORRUPT:C->NetworkStatus = LTE_LC_NW_REG_UICC_FAIL; break;    
        case CELL_NETWORK_STATE__DENIED: C->NetworkStatus = LTE_LC_NW_REG_REGISTRATION_DENIED;break;     
        case CELL_NETWORK_STATE__AIRPLANE:C->NetworkStatus = LTE_LC_NW_REG_NOT_REGISTERED; break;       
        case CELL_NETWORK_STATE__DIRECT: C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN;break;      
        case CELL_NETWORK_STATE__PSM: C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN;break; 
        case CELL_NETWORK_STATE__MODEM_SHUT_DOWN:C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN; break;
        case CELL_NETWORK_STATE__LOW_VOLTAGE_SHUT_DOWN:C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN; break;
        case CELL_NETWORK_STATE__BYPASS:C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN;  break;
        case CELL_NETWORK_STATE__UPDATE_IN_PROGRESS :  C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN; break;      
        case CELL_NETWORK_STATE__REG_TEST: C->NetworkStatus = LTE_LC_NW_REG_UNKNOWN; break;
        case CELL_NETWORK_STATE__INITIALIZING:C->NetworkStatus = LTE_LC_NW_REG_SEARCHING; break;  

    }

    return false;
}

char * GetCellInfo()
{
        cJSON *Result = cJSON_CreateObject();

		char RSSI_Temp[32];

		sprintf(RSSI_Temp,"%idbm",Cell_GetLastPostedRSSI());
 
                        
        cJSON_AddStringToObject(Result, "ObjID", "Cell");
        cJSON_AddStringToObject(Result, "State", GetNetStatusString(NetworkStatus));
        cJSON_AddStringToObject(Result, "RSSI", RSSI_Temp);
        cJSON_AddStringToObject(Result, "IMEI", Cell_IMEI());
        cJSON_AddStringToObject(Result, "ICCID", Cell_ICCID());
		cJSON_AddStringToObject(Result, "Operator", Cell_COPS());
			
        cJSON_AddStringToObject(Result, "ModuleFirmware", Cell_ModemFirmware());
        cJSON_AddStringToObject(Result, "Firmware",FIRMWARE_VERSION_STRING);
          
        char *string = NULL;
        string = cJSON_Print(Result);
   
        cJSON_Delete(Result);


		return string;
}

static int cell_cmd_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

		char * result = GetCellInfo();
        
		shell_print(shell,"\x02%s\x03",result);
		
		cJSON_free(result);

        return 0;
}



SHELL_CMD_REGISTER(cell, NULL, "Get cellular modem info", cell_cmd_handler);







