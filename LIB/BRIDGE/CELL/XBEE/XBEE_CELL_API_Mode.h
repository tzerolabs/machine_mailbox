
#ifndef  XBEE_CELL_API_MODE_H
#define  XBEE_CELL_API_MODE_H



void DecodeAPI_Frame(uint8_t DataIn);

void ProcessAT_Response(uint8_t FrameID, char *AT_Cmd, uint8_t * ParameterValue, uint32_t ParameterValueLength);

char * Cell_GetConnectionStateString(uint8_t ConnectionState);

void Cell_FetchTime();

#endif
