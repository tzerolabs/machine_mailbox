
#include <zephyr.h>
#include "XBEE_PacketBuffer.h"


K_HEAP_DEFINE(PacketBufferHeap, 32768);

PacketBuffer * PacketBufferCreate(uint32_t Length)
{
    PacketBuffer * PB = 0;
    
    PB = k_heap_alloc(&PacketBufferHeap, Length + sizeof(PacketBuffer),K_NO_WAIT);

    if(PB)
    {
        PB->Data = (uint8_t *)PB + sizeof(PacketBuffer);
        PB->MaxLength = Length;
        PB->CurrentLength = 0;
    }

    return PB;
}

void PacketBufferDestroy(PacketBuffer * PB)
{
    k_heap_free(&PacketBufferHeap,PB);
}
