#include "XBEE_CELL_Common.h"
#include "stdint.h"


char Cell_IMEI_String[MAX_IMEI_SIZE+1];

char Cell_ICCID_String[MAX_ICCID_SIZE+1];

char Cell_Operator_String[MAX_OPERATOR_SIZE+1];

char Cell_APN[MAX_APN_SIZE+1];

volatile uint8_t Cell_IMEI_Rx = 0;

volatile uint8_t Cell_ICCID_Rx = 0;

volatile uint8_t Cell_Network_Rx = 0;

volatile uint8_t Cell_APN_Rx = 0;

volatile uint8_t CellInfoRequested = 0;

volatile uint8_t CellFirmwareVersionRecieved = 0;

volatile uint8_t CellModuleFirmwareVersionRecieved = 0;

volatile char CellModuleFirmwareVersion[64];

volatile uint32_t CellFirmwareVersion = 0;

volatile uint8_t CellLastConnectionState = CELL_NETWORK_STATE__INITIALIZING;

volatile uint8_t __CellLastConnectionState = 0xFE;

volatile int8_t CellLastRSSI = 0;

volatile int8_t __CellLastRSSI = -1;

volatile uint32_t LastLookupIP = 0;

volatile HostLookupState CurrentHostLookupState = HOST_LOOKUP_IDLE;

volatile uint32_t PacketBufferRead;

volatile uint32_t PacketBufferWrite;

API_FrameDecodeState CurrentAPI_FrameDecodeState;

uint32_t API_FrameCRC = 0;

uint16_t API_FrameInCounter = 0;

uint8_t API_FrameInBuffer[MAX_API_FRAME_IN_BUFFER_SIZE];

uint8_t API_FrameBuffer[API_FRAME_BUFFER_SIZE];

uint32_t API_FrameBufferLength = 0;

uint32_t API_FrameLength = 0;

volatile uint8_t FrameID_LastStatus[256];

uint8_t XBEE_Mode = XBEE_COMMAND_MODE;

volatile  CellTimeFetchState_t CellTimeFetchState = 0;

volatile  uint32_t CellIncomingUnixTime = 0;

uint32_t CellSecondsSinceLastTimeRefresh = 0;