#include "XBEE_CELL_Config.h"
#include "XBEE_CELL_Common.h"
#include "XBEE_CELL_CommandMode.h"
#include "stdint.h"
#include "string.h"
#include "XBEE_CELL__Portable.h"
#include "stdio.h"


LOG_MODULE_DECLARE(cell,CONFIG_CELL_LOG_LEVEL);


/***
 *         _  _____    ____ ___  __  __ __  __    _    _   _ ____  
 *        / \|_   _|  / ___/ _ \|  \/  |  \/  |  / \  | \ | |  _ \ 
 *       / _ \ | |   | |  | | | | |\/| | |\/| | / _ \ |  \| | | | |
 *      / ___ \| |   | |__| |_| | |  | | |  | |/ ___ \| |\  | |_| |
 *     /_/_  \_\_|__  \____\___/|_|  |_|_|  |_/_/   \_\_| \_|____/ 
 *      |  \/  |/ _ \|  _ \| ____|                                 
 *      | |\/| | | | | | | |  _|                                   
 *      | |  | | |_| | |_| | |___                                  
 *      |_|  |_|\___/|____/|_____|                                 
 *                                                                 
 */

 #define MAX_COMMAND_RESPONSE    128

uint8_t CommandResponse[MAX_COMMAND_RESPONSE+1];
uint32_t CommandResponseIdx = 0;

volatile  uint8_t LastCommandResponse  = COMMAND_RESPONSE_WAITING;

void HandleCommandResponse(uint8_t *Reponse,uint32_t Length)
{
    (void)(Length);

    if(strncmp((const char *)Reponse,"OK",MAX_COMMAND_RESPONSE) == 0)
    {
        LOG_DBG("Recieved an OK to the last command");
        LastCommandResponse  = COMMAND_RESPONSE_OK;
    }
    else if(strncmp((const char *)Reponse,"ERROR",MAX_COMMAND_RESPONSE) == 0)
    {
        LOG_DBG("Recieved an ERROR to the last command");
        LastCommandResponse  = COMMAND_RESPONSE_ERROR;
    }
    else
    {
        LOG_DBG("AT Command Response of %s",log_strdup(Reponse));
        LastCommandResponse = COMMAND_RESPONSE_OK;
    }

}

uint8_t XBEE_GetLastCommandReponseState()
{
    return LastCommandResponse;
}

void DecodeCommand_Response(uint8_t DataIn)
{
    if(DataIn == '\r')
    {
         if(CommandResponseIdx == 0)
         {
                LOG_DBG("Command response is zero length");
         }
         else
         {
            CommandResponse[CommandResponseIdx] = 0;
            CommandResponseIdx++;
            HandleCommandResponse(CommandResponse,CommandResponseIdx);
            CommandResponseIdx = 0;
         }
    }
    else
    {
        CommandResponse[CommandResponseIdx] = DataIn;
        CommandResponseIdx++;

        if (CommandResponseIdx >= MAX_COMMAND_RESPONSE)
        {
            LOG_DBG("Max command response of %d reached. ",MAX_COMMAND_RESPONSE);
            CommandResponseIdx = 0;
        }

    }

}


void XBEE_EnterCommandMode()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;

     Cell_WriteByteToTxQueue('+');
     Cell_WriteByteToTxQueue('+');
     Cell_WriteByteToTxQueue('+');

     XBEE_Mode = XBEE_COMMAND_MODE;
}

void XBEE_AT_Send_BD_115200()
{
     LastCommandResponse = COMMAND_RESPONSE_WAITING;
     Cell_WriteStringToTxQueue("ATBD 7\r");
}


void XBEE_AT_Send_WR()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATWR\r");
}

void XBEE_AT_Send_AC()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATAC\r");
}


void XBEE_AT_Request_AP_API_Mode()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATAP\r");
}

void XBEE_AT_Set_AP_API_Mode()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATAP 1\r");
}

char * GetLastAT_CommandResponse()
{
    return (char *)(&CommandResponse[0]);
}

void XBEE_AT_Request_APN()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATAN\r");
}


void XBEE_AT_Set_APN(char * APN)
{
    char APN_String[128];
     
    LastCommandResponse = COMMAND_RESPONSE_WAITING;

    snprintf(APN_String,sizeof(APN_String),"ATAN %s\r",APN);

    Cell_WriteStringToTxQueue(APN_String);
}


void XBEE_AT_ExitCommandMode()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATCN\r");
    XBEE_Mode = XBEE_API_MODE;
}

void XBEE_AT_GetPreferredNetworkTechnology()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATN#\r");
}

void XBEE_AT_SetPreferredNetworkTechnology()
{
    LastCommandResponse = COMMAND_RESPONSE_WAITING;
    Cell_WriteStringToTxQueue("ATN# 2\r");
}