#include "stdint.h"

#ifndef XBEE_CELL
#define XBEE_CELL

#include "XBEE_CELL_Common.h"

void Cell_Init();

void Cell_Process();

HostLookupState Cell_CheckForHostResolution(uint32_t *IP_Address);

void Cell_FetchIPFromHostName(char * HostName);

void Cell_ResetHostLookup();

void DecodeAPI_Frame(uint8_t DataIn);

void Cell_SendTxt(char * PhoneNumber, char * Msg);

void Cell_Init();

void Cell_Reset(CELL_ResetType ResetType);

void Cell_PowerCycle();

void TxAPI_Frame(uint8_t *DataIn, uint32_t LengthIn);


void Cell_TxRequest(uint8_t FrameID,
                                        uint32_t IP_Address, //BIG ENDIAN
                                        uint16_t DestPort, 
                                        uint16_t SrcPort,

                                        Cell_Protocol CP,
                                        uint8_t Flags,
                                        uint8_t *Data,
                                        uint32_t Length
                                );




extern volatile uint8_t FrameID_LastStatus[256];

char * Cell_GetTxStatusErrorCodeString(uint8_t StatusCode);

void Cell_FetchRSSI();

void Cell_FetchIMEI();

void Cell_FetchOperator();

void Cell_FetchICCID();

void Cell_FetchPhoneNumber();

void Cell_FetchOperator();

void Cell_FetchVersion();

void Cell_ModuleFetchVersion();

void  Cell_FetchConnectionState();

void Cell_EnterAirplane();

void Cell_ExitAirplane();

void Cell_ForceReset();

void Cell_NetworkReset();

void Cell_PowerCycle();

void Cell_ResetStateVariables();

void Cell_RequestVersion();

uint8_t  Cell_GetXBEEConnectionState();

char * Cell_GetConnectionStateString(uint8_t ConnectionState);

void Cell_SendAT_Cmd(char * AT_Cmd,char * AT_Parameter,uint8_t FrameID);

void Cell_SendAT_Cmd_w_Param(char * AT_Cmd,uint8_t AT_Parameter,uint8_t FrameID);

void Cell_SendAT_Cmd_No_Param(char * AT_Cmd,uint8_t FrameID);

char *  Cell_GetICCID();

char *  Cell_GetIMEI();

char *  Cell_GetOperator();

char *  Cell_GetModuleFirmwareversion();

uint32_t Cell_IsReadyForAction();

int32_t Cell_SocketCreate(XBEE_SocketType_t XBEE_SocketType,uint8_t *SocketID);

int32_t Cell_SocketClose(uint8_t SocketID);

int32_t Cell_SocketOptionRequest(uint8_t SocketID);

int32_t Cell_SocketConnect(uint8_t SocketID,uint32_t IP_NBO, uint16_t Port_NBO);

int32_t Cell_SocketSend(uint8_t SocketID,uint8_t *Data,uint32_t Len);

void xbee_push_into_socket(int32_t XBEE_SocketID, uint8_t *Data, uint32_t Len);

//tell the socket offload driver that all modem sockets were closed (because of reset, etc)
void xbee_notify_all_closed();

#endif
