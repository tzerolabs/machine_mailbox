#include "stdint.h"
#include <stddef.h>

#ifndef _XBEE_PACKET_BUFFER_H
#define _XBEE_PACKET_BUFFER_H

typedef struct
{
    void * fifo_reserved; //zephyr needs this for the linked list

    uint16_t CurrentLength;
    uint16_t MaxLength;
    uint8_t *Data;

    uint32_t IP; //Network byte order
    uint16_t Port; //Network byte order

} PacketBuffer;


void PacketBufferDestroy(PacketBuffer * PB);
PacketBuffer * PacketBufferCreate(uint32_t Length);


#endif