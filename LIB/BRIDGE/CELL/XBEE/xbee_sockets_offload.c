/*
 * Copyright (c) 2018 Tzero
 *
 * SPDX-License-Identifier: bsd-like
 *
 */

/**
 * @file
 * @brief xbee socket offload provider
 */

#include <errno.h>
#include <fcntl.h>
#include <init.h>
#include <net/socket_offload.h>
#include <net/socket.h>
#include <sockets_internal.h>
#include <sys/fdtable.h>
#include <zephyr.h>
#include <logging/log.h>
#include "XBEE_CELL.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "XBEE_PacketBuffer.h"

LOG_MODULE_REGISTER(xbee_offload,LOG_LEVEL_DBG);
#if defined(CONFIG_POSIX_API)
#include <posix/poll.h>
#include <posix/sys/time.h>
#include <posix/sys/socket.h>
#endif

#define CONFIG_NET_SOCKETS_OFFLOAD

#if defined(CONFIG_NET_SOCKETS_OFFLOAD)

#define XBEE_MAX_SOCKET_COUNT 8

#define PROTO_WILDCARD 0

#define OBJ_TO_SOCKET_ID(obj) (((struct xbee_sock_ctx *)obj)->XBEE_SocketID)

#define OBJ_TO_CTX(obj) ((struct xbee_sock_ctx *)obj)

static K_MUTEX_DEFINE(ctx_lock);

/* Offloading context related to nRF socket. */
static struct xbee_sock_ctx 
{
	int XBEE_SocketID;
	
	int fd; //zephyr file descriptor

	struct k_mutex *lock; /* Mutex associated with the socket. */
	
	uint8_t XBEE_SocketStatus;

	struct k_fifo PacketBufferQueue;

} offload_ctx[XBEE_MODEM_MAX_SOCKET_COUNT];


struct xbee_sock_ctx * XBEE_SocketID_to_ctx(int32_t XBEE_SocketID)
{
	for(int i=0;i<XBEE_MODEM_MAX_SOCKET_COUNT;i++)
	{
		if(XBEE_SocketID == offload_ctx[i].XBEE_SocketID)
		{
			return &offload_ctx[i];
		}
	}

	return NULL;
}

void update_socket_status(int32_t XBEE_SocketID,uint8_t Status)
{
	struct xbee_sock_ctx * ctx = NULL;

	ctx = XBEE_SocketID_to_ctx(XBEE_SocketID);

	if(ctx!=NULL)
	{
		ctx->XBEE_SocketStatus = Status;
	}
}

void xbee_notify_all_closed()
{
	LOG_WRN("All sockets being flagged as closed");

	for(int i=0;i<XBEE_MODEM_MAX_SOCKET_COUNT;i++)
	{
		offload_ctx[i].XBEE_SocketStatus = 0x07;
	}
}

static struct xbee_sock_ctx *allocate_ctx(int XBEE_SocketID)
{
	struct xbee_sock_ctx *ctx = NULL;

	k_mutex_lock(&ctx_lock, K_FOREVER);

	for (int i = 0; i < ARRAY_SIZE(offload_ctx); i++) {
		if (offload_ctx[i].XBEE_SocketID == -1) {
			ctx = &offload_ctx[i];
			ctx->XBEE_SocketID = XBEE_SocketID;
			break;
		}
	}

	k_mutex_unlock(&ctx_lock);

	return ctx;
}

static void release_ctx(struct xbee_sock_ctx *ctx)
{
	k_mutex_lock(&ctx_lock, K_FOREVER);

	while(!k_fifo_is_empty(&(ctx->PacketBufferQueue)))
	{
		PacketBufferDestroy(((PacketBuffer *)(&ctx->PacketBufferQueue)));
	}

	ctx->XBEE_SocketID = -1;
	ctx->lock = NULL;

	k_mutex_unlock(&ctx_lock);
}


void xbee_push_into_socket(int32_t XBEE_SocketID, uint8_t *Data, uint32_t Len)
{
	struct xbee_sock_ctx * ctx;

	ctx = XBEE_SocketID_to_ctx(XBEE_SocketID);

	if(ctx!=NULL)
	{
	
		PacketBuffer * PB = PacketBufferCreate(Len);

		if(PB == NULL)
		{
			LOG_ERR("Could not allocate a packet buffer for incoming data on SocketID %d",XBEE_SocketID);
			
				k_mutex_unlock(ctx->lock);

			return;
		}

		memcpy(PB->Data,Data,Len);
		PB->MaxLength = Len;
		PB->CurrentLength = Len;

		k_fifo_put(&(ctx->PacketBufferQueue),PB);
		LOG_DBG("Incoming packet of length %d on socket %d",Len,XBEE_SocketID);
		
	}
	else
	{
		LOG_ERR("No valid context for SocketID %d.  Dropping incoming data",XBEE_SocketID);
	}
}


static void release_ctx(struct xbee_sock_ctx *ctx);

static const struct socket_op_vtable xbee_socket_fd_op_vtable;
/*
static int xbee_socket_offload_socket(int family, int type, int proto)
{

	ssize_t retval;

		errno = EAFNOSUPPORT;
		retval = -EAFNOSUPPORT;
		return retval;
}
*/

static int xbee_socket_offload_accept(void *obj, struct sockaddr *addr,
				       socklen_t *addrlen)
{

	LOG_ERR("offload accept not implemented");
	
//	ssize_t retval;

	/*
	int fd = z_reserve_fd();
	int sd = OBJ_TO_SD(obj);
	int new_sd = -1;
	struct nrf_sock_ctx *ctx = NULL;
	struct nrf_sockaddr *nrf_addr_ptr = NULL;
	nrf_socklen_t *nrf_addrlen_ptr = NULL;
	struct nrf_sockaddr_in6 nrf_addr;
	nrf_socklen_t nrf_addrlen;

	if (fd < 0) {
		return -1;
	}

	if ((addr != NULL) && (addrlen != NULL)) {
		nrf_addr_ptr = (struct nrf_sockaddr *)&nrf_addr;
		nrf_addrlen_ptr = &nrf_addrlen;

	if (*addrlen == sizeof(struct sockaddr_in)) {
			nrf_addrlen = sizeof(struct nrf_sockaddr_in);
		} else {
			nrf_addrlen = sizeof(struct nrf_sockaddr_in6);
		}
	}

	new_sd = nrf_accept(sd, nrf_addr_ptr, nrf_addrlen_ptr);
	if (new_sd < 0) {
		return -1;
	}

	ctx = allocate_ctx(new_sd);
	if (ctx == NULL) {
		errno = ENOMEM;
		goto error;
	}

	if ((addr != NULL) && (addrlen != NULL)) {
		if (nrf_addr_ptr->sa_family == NRF_AF_INET) {
			*addrlen = sizeof(struct sockaddr_in);
			nrf_to_z_ipv4(
			    addr, (const struct nrf_sockaddr_in *)&nrf_addr);
		} else if (nrf_addr_ptr->sa_family == NRF_AF_INET6) {
			*addrlen = sizeof(struct sockaddr_in6);
			nrf_to_z_ipv6(
			    addr, (const struct nrf_sockaddr_in6 *)&nrf_addr);
		} else {
			goto error;
		}
	}

	z_finalize_fd(fd, ctx,
		      (const struct fd_op_vtable *)&xbee_socket_fd_op_vtable);

	return fd;

error:
	if (new_sd != -1) {
		nrf_close(new_sd);
	}

	if (ctx != NULL) {
		release_ctx(ctx);
	}

	z_free_fd(fd);
	return -1;
	*/
		errno = EAFNOSUPPORT;
		return -EAFNOSUPPORT;

}

static int xbee_socket_offload_bind(void *obj, const struct sockaddr *addr,
				     socklen_t addrlen)
{
	LOG_ERR("offload bind not implemented");
	
	ssize_t retval;
	retval = -1;
	errno = ENOTSUP;
	return retval;
}

static int xbee_socket_offload_listen(void *obj, int backlog)
{

	LOG_ERR("offload listen not implemented");

	ssize_t retval;
	retval = -1;
	errno = ENOTSUP;
	return retval;
}

static int xbee_socket_offload_connect(void *obj, const struct sockaddr *addr,
					socklen_t addrlen)
{
	int SocketID = OBJ_TO_SOCKET_ID(obj);

	int retval;


	if (addr->sa_family == AF_INET)
	{
		struct sockaddr_in *sockaddr = (struct sockaddr_in *)addr;

		char ipv4_addr[NET_IPV4_ADDR_LEN];
									
		inet_ntop(AF_INET,
					&sockaddr->sin_addr.s_addr,
					ipv4_addr,
					sizeof(ipv4_addr)
					);

		LOG_DBG("Connect request to %s @ %d",log_strdup(ipv4_addr),ntohs(sockaddr->sin_port));
		
		retval = Cell_SocketConnect(SocketID,sockaddr->sin_addr.s_addr,sockaddr->sin_port);
	
		if(retval == 0)
		{
			((struct xbee_sock_ctx *)obj)->XBEE_SocketStatus = 0;
		}

	} 
	else
	{
		LOG_ERR("Unsupported Family %d on connect",addr->sa_family);
		retval = -ENOTSUP;
		errno = ENOTSUP;
	}

	return retval;
}

static int xbee_socket_offload_setsockopt(void *obj, int level, int optname,
					   const void *optval, socklen_t optlen)
{
	LOG_ERR("Returning OK to setsockopt");

	
	//LOG_ERR("offload set socketopt not implemented");
	
	//ssize_t retval;
	//retval = -1;
	//errno = ENOPROTOOPT;
	//return retval;

	return 0;
}

static int xbee_socket_offload_getsockopt(void *obj, int level, int optname,
					   void *optval, socklen_t *optlen)
{
	//ssize_t retval;

	
	//LOG_ERR("offload get socketopt not implemented");

	//retval = -1;
	//errno = ENOPROTOOPT;
	//return retval;

	return 0;
}

static ssize_t xbee_socket_offload_recvfrom(void *obj, void *buf, size_t len,
					     int flags, struct sockaddr *from,
					     socklen_t *fromlen)
{
	ssize_t retval = -1;
	
	if(obj == NULL)
	{
		LOG_ERR("Socket object is null. Did you connect?");
		errno = EINVAL;
		return -EINVAL;
	}

	struct xbee_sock_ctx *ctx;

	ctx = OBJ_TO_CTX(obj);

	if(ctx->XBEE_SocketStatus != 0)
	{
		
		LOG_ERR("Socket recvfrom... Socket not connected");
		retval = -EINVAL;
		errno = EINVAL;
		return retval;
	}

	k_mutex_lock(ctx->lock, K_FOREVER);


	int CopyLen = 0;

	k_timeout_t timeout = K_NO_WAIT;

	if(flags & MSG_DONTWAIT)
	{
		 timeout = K_NO_WAIT;
	}
	else
	{
		timeout = K_MSEC(10000);
	}

	PacketBuffer *PB = k_fifo_get(&(ctx->PacketBufferQueue),timeout);
		
	if(PB == NULL)
	{
		retval = -1;
		errno = EAGAIN;
		goto error;
	}

    CopyLen = PB->CurrentLength;


	if(PB->CurrentLength > len)
	{
		CopyLen = len;
	}
	
	memcpy(buf,PB->Data,CopyLen);

	PacketBufferDestroy(PB);

	retval = CopyLen;

error:
	k_mutex_unlock(ctx->lock);

	return CopyLen;
}

static ssize_t xbee_socket_offload_sendto(void *obj, const void *buf,
					   size_t len, int flags,
					   const struct sockaddr *to,
					   socklen_t tolen)
{
	int32_t retval;

	if(obj == NULL)
	{
			LOG_ERR("Socket Send... Null Socket");
		retval = -EINVAL;
		errno = EINVAL;
		return retval;
	}

	struct xbee_sock_ctx *ctx = OBJ_TO_CTX(obj);

	if(ctx->XBEE_SocketStatus != 0)
	{
		
		LOG_ERR("Socket Send... Socket not connected");
		retval = -EINVAL;
		errno = EINVAL;
		return retval;
	}

	if(len > 1500)
	{
		LOG_ERR("Socket Send... Too Many bytes");
		retval = -EINVAL;
		errno = EINVAL;
		return retval;
	}

	LOG_DBG("Sending %d bytes on data on SocketID %d",len,ctx->XBEE_SocketID);

	Cell_SocketSend(ctx->XBEE_SocketID,(uint8_t *)buf,len);
	
	return 0;
}

static ssize_t xbee_socket_offload_sendmsg(void *obj, const struct msghdr *msg,
					    int flags)
{
	ssize_t retval;

	LOG_ERR("offload sendmsg not implemented");
	retval = -1;
	errno = EINVAL;
	return retval;
}

static inline int xbee_socket_offload_poll(struct pollfd *fds, int nfds,
					    int timeout)
{
	int retval = 0;
	
		void *obj;
		
	if(nfds>1)
	{
		LOG_ERR("xbee poll only supports 1 fd at a time");
	}

	for (int i = 0; i < nfds; i++) 
	{
		
		fds[i].revents = 0;

		if (fds[i].fd < 0) {
			/* Per POSIX, negative fd's are just ignored */
			continue;
		} else {
			obj = z_get_fd_obj(fds[i].fd,
					   (const struct fd_op_vtable *)
						     &xbee_socket_fd_op_vtable,
					   ENOTSUP);
			if (obj != NULL) {
				/* Offloaded socket found. */
				
				struct xbee_sock_ctx * ctx= OBJ_TO_CTX(obj);

  				uint32_t timeout_cnt = 0;

				if(ctx->XBEE_SocketStatus != 0)
				{
					return -1;
					
					fds[i].revents = POLLHUP | POLLERR;
				}
				
				//sntp simple sends really small timeouts.... we need to widen this to deal with the slow network
				if(timeout < 3000)
					timeout = 3000;


				while((timeout_cnt<timeout))
				{
					if(k_fifo_is_empty(&(ctx->PacketBufferQueue)))
					{
						k_sleep(K_MSEC(50));
						timeout_cnt+=50;
					}
					else
					{
						fds[i].revents |= POLLIN;
						retval++;
						break;
					}
				}

			} else {
				/* Non-offloaded socket, return an error. */
				fds[i].revents = POLLNVAL;
				retval = -1;
			}
		}

	}
	return retval;
}

static void xbee_socket_offload_freeaddrinfo(struct zsock_addrinfo *root)
{
	struct zsock_addrinfo *next = root;

	while (next != NULL) {
		struct zsock_addrinfo *this = next;

		next = next->ai_next;
		if(this->ai_addr)
		{
			k_free(this->ai_addr);
		}
		k_free(this);
	}
}

static int xbee_socket_offload_getaddrinfo(const char *node,
					    const char *service,
					    const struct zsock_addrinfo *hints,
					    struct zsock_addrinfo **res)
{
int retval = DNS_EAI_FAIL;

static K_MUTEX_DEFINE(getaddrinfo_lock);

LOG_DBG("Looking up %s",log_strdup(node));

k_mutex_lock(&getaddrinfo_lock, K_FOREVER);


Cell_FetchIPFromHostName((char *)node);

uint32_t IP_Address;
uint32_t WaitCnt =0 ;
do
{
	k_sleep(K_MSEC(500));
	if((WaitCnt+=500)>10000)
	{
	 	Cell_ResetHostLookup();
		 LOG_ERR("Timeout waiting for xbee response resolving DNS");
		goto error;
	}

} while (Cell_CheckForHostResolution(&IP_Address) != HOST_LOOKUP_COMPLETE);


struct zsock_addrinfo *next_z_res =k_malloc(sizeof(struct zsock_addrinfo));

if (next_z_res == NULL)
{
		retval = DNS_EAI_MEMORY;
		 LOG_ERR("Could not alloc addrinfo");
		goto error;
}

struct sockaddr_in *sockaddr =k_malloc(sizeof(struct sockaddr_in) );

if (sockaddr == NULL)
{
	if(next_z_res)
		k_free(next_z_res);

 	LOG_ERR("Could not alloc sockaddr_in");

	retval = DNS_EAI_MEMORY;
	goto error;
}

next_z_res->ai_next = NULL;
next_z_res->ai_flags = AI_ADDRCONFIG;
next_z_res->ai_family = AF_INET;
next_z_res->ai_addrlen = sizeof(struct sockaddr_in);
next_z_res->ai_addr = (struct sockaddr *)sockaddr;

sockaddr->sin_addr.s_addr = IP_Address;
sockaddr->sin_family = AF_INET;

uint32_t Port;

sscanf(service, "%u",&Port);

sockaddr->sin_port = htons(((uint16_t)Port));
  
  
  char ipv4_addr[NET_IPV4_ADDR_LEN];
                                         
  inet_ntop(AF_INET,
            &sockaddr->sin_addr.s_addr,
            ipv4_addr,
            sizeof(ipv4_addr)
                               );
LOG_DBG("Resolved to  %s",log_strdup(ipv4_addr));

*res = next_z_res;

retval = 0;

error:
	k_mutex_unlock(&getaddrinfo_lock);

	return retval;
}
/*
static int xbee_socket_offload_fcntl(int fd, int cmd, va_list args)
{
	ssize_t retval;

	LOG_ERR("offload fcntl not implemented");

	retval = -1;
	errno = EINVAL;
	return retval;
}
*/
static int xbee_socket_offload_ioctl(void *obj, unsigned int request,
				      va_list args)
{
	ssize_t retval = 0;

	switch(request)
	{
		default:
			LOG_WRN("%d not supported but returning as success",request);
			retval = 0;
		break;
		case ZFD_IOCTL_FSYNC:
			LOG_DBG("ZFD_IOCTL_FSYNC not supported");
			retval = -ENOTSUP;
			errno = ENOTSUP;
		break;

		case ZFD_IOCTL_LSEEK:
			LOG_DBG("ZFD_IOCTL_LSEEK not supported");
			retval = -ENOTSUP;
			errno = ENOTSUP;
		break;
		

		case ZFD_IOCTL_POLL_PREPARE:
			LOG_DBG("ZFD_IOCTL_POLL_PREPARE not supported");
			return -EXDEV;

		case ZFD_IOCTL_POLL_UPDATE:
			LOG_DBG("ZFD_IOCTL_POLL_UPDATE not supported");
			return -EOPNOTSUPP;

		case ZFD_IOCTL_POLL_OFFLOAD:
				{
					struct zsock_pollfd *fds;
					int nfds;
					int timeout;

					fds = va_arg(args, struct zsock_pollfd *);
					nfds = va_arg(args, int);
					timeout = va_arg(args, int);

					return xbee_socket_offload_poll(fds, nfds, timeout);
				}	
		break;
		case ZFD_IOCTL_SET_LOCK:
		    {
				struct xbee_sock_ctx *ctx = OBJ_TO_CTX(obj);
				ctx->lock = va_arg(args, struct k_mutex *);
		 	}	
		break;
		
	}

	return retval;
}

static ssize_t xbee_socket_offload_read(void *obj, void *buffer, size_t count)
{
	return xbee_socket_offload_recvfrom(obj, buffer, count, 0, NULL, 0);
}

static ssize_t xbee_socket_offload_write(void *obj, const void *buffer,
					  size_t count)
{
	return xbee_socket_offload_sendto(obj, buffer, count, 0, NULL, 0);

}

static int xbee_socket_offload_close(void *obj)
{
	struct xbee_sock_ctx *ctx = OBJ_TO_CTX(obj);

	
	LOG_WRN("Closing socket %d",ctx->XBEE_SocketID);

	Cell_SocketClose(ctx->XBEE_SocketID);

	release_ctx(ctx);
	
	/*We are always going to to report a sucess on close*/

	return 0;
}

static const struct socket_op_vtable xbee_socket_fd_op_vtable = {
	.fd_vtable = {
		.read = xbee_socket_offload_read,
		.write = xbee_socket_offload_write,
		.close = xbee_socket_offload_close,
		.ioctl = xbee_socket_offload_ioctl,
	},
	.bind = xbee_socket_offload_bind,
	.connect = xbee_socket_offload_connect,
	.listen = xbee_socket_offload_listen,
	.accept = xbee_socket_offload_accept,
	.sendto = xbee_socket_offload_sendto,
	.sendmsg = xbee_socket_offload_sendmsg,
	.recvfrom = xbee_socket_offload_recvfrom,
	.getsockopt = xbee_socket_offload_getsockopt,
	.setsockopt = xbee_socket_offload_setsockopt,
};

static bool xbee_socket_is_supported(int family, int type, int proto)
{
	

    switch(proto)
	{
		case IPPROTO_TLS_1_0:
		case IPPROTO_TLS_1_1:
			LOG_DBG("TLS 1.0 and 1.1 not supported");
			return false;
		break;

		case IPPROTO_DTLS_1_0:
		case IPPROTO_DTLS_1_2:
			LOG_DBG("DTLS not supported");
			return false;
		break;
	}


	if(family == AF_INET && type == SOCK_DGRAM && proto == IPPROTO_UDP)
	{
		LOG_DBG("AF_INET, SOCK_DGRAM,  IPPROTO_RAW socket supported");
		return true;
	}

	if(family == AF_INET && type == SOCK_STREAM && proto == IPPROTO_TCP)
	{
		LOG_DBG("AF_INET, SOCK_STREAM,  IPPROTO_TCP socket supported");
		return true;
	}

	if(family == AF_INET && type == SOCK_STREAM && proto == IPPROTO_TLS_1_2)
	{
		LOG_DBG("AF_INET, SOCK_STREAM,  IPPROTO_TLS_1_2 socket supported");
		return true;
	}
	if(family == AF_INET && type == SOCK_STREAM && proto == IPPROTO_TLS_1_1)
	{
		LOG_DBG("AF_INET, SOCK_STREAM,  IPPROTO_TLS_1_1 socket supported");
		return true;
	}
	if(family == AF_INET && type == SOCK_STREAM && proto == IPPROTO_TLS_1_0)
	{
		LOG_DBG("AF_INET, SOCK_STREAM,  IPPROTO_TLS_1_0 socket supported");
		return true;
	}
	
	if (IS_ENABLED(CONFIG_NET_SOCKETS_OFFLOAD_TLS)) {
		return false;
	}

	return false;
}

static int xbee_socket_create(int family, int type, int proto)
{
	ssize_t retval;

	struct xbee_sock_ctx *ctx;
	int fd = z_reserve_fd();
	
	if (fd < 0) {
		LOG_ERR("Could not reserce fd during socket create");
		errno = ENOMEM;
		retval = -ENOMEM;
		goto error;
	}

    //XBEE_SOCKET_UDP = 0,
    //XBEE_SOCKET_TCP = 1,
    //XBEE_SOCKET_SSL = 4


	XBEE_SocketType_t SocketType = XBEE_SOCKET_UDP;

	if(type == SOCK_DGRAM && proto == IPPROTO_UDP)
	{
		SocketType = XBEE_SOCKET_UDP;
		LOG_DBG("Attempting socket create SOCK_DGRAM, IPPROTO_UDP");
	}
	else if (type == SOCK_STREAM)
	{

		if(proto == IPPROTO_TCP){SocketType = XBEE_SOCKET_TCP; LOG_DBG("Attempting socket create SOCK_STREAM, IPPROTO_TCP");}
		else if (proto == IPPROTO_TLS_1_0){SocketType = XBEE_SOCKET_SSL; LOG_DBG("Attempting socket create SOCK_STREAM, IPPROTO_TLS_1_0");}
		else if (proto == IPPROTO_TLS_1_1){SocketType = XBEE_SOCKET_SSL; LOG_DBG("Attempting socket create SOCK_STREAM, IPPROTO_TLS_1_1");}
		else if (proto == IPPROTO_TLS_1_2){SocketType = XBEE_SOCKET_SSL; LOG_DBG("Attempting socket create SOCK_STREAM, IPPROTO_TLS_1_2");}
		else {

		errno = EINVAL;
		retval = -EINVAL;
		LOG_ERR("Socket create of proto %d is not supported on SOCK_STREAM",proto);
		goto error;
		}
	}
	else
	{
		errno = EINVAL;
		retval = -EINVAL;
		LOG_ERR("Socket create of type %d proto %d is not supported",type,proto);
		goto error;
	}
	
		uint8_t SocketID = 0;
		
		retval = Cell_SocketCreate(SocketType,&SocketID);
		
		if(retval == 0)
		{
			LOG_DBG("Created socket %d",SocketID);
		
			if(SocketType == XBEE_SOCKET_SSL)
			{
				LOG_DBG("Socket is SSL,  attempting to set TLS mode on XBEE");
				if(Cell_SocketOptionRequest(SocketID)!=0)
				{
					LOG_ERR("Could not set TLS option on XBEE socket.");
					errno = EINVAL;
					retval = -EINVAL;
					Cell_SocketClose(SocketID);
					z_free_fd(fd);
					LOG_ERR("Could not allocate socket context");
					goto error;
				}

			}
	
			ctx = allocate_ctx(SocketID);

			if (ctx == NULL) 
			{
				errno = ENOMEM;
				retval = -ENOMEM;
				Cell_SocketClose(SocketID);
				z_free_fd(fd);
				LOG_ERR("Could not allocate socket context");
				goto error;
			}

			ctx->XBEE_SocketID = SocketID;
			ctx->XBEE_SocketStatus = 0xFF;
			ctx->fd = fd;
		
		   //Just in case there was something left aorund
			
			while(!k_fifo_is_empty(&(ctx->PacketBufferQueue)))
			{
				PacketBufferDestroy((PacketBuffer *)(&(ctx->PacketBufferQueue)));
			}

			z_finalize_fd(fd, ctx,(const struct fd_op_vtable *)&xbee_socket_fd_op_vtable);

			errno = 0;
			return fd;

		}
		else
		{
			LOG_DBG("error %d from xbee while creating socket",retval);
			z_free_fd(fd);
			errno = EINVAL;
			retval = -EINVAL;
		}




	error:
		return retval;
}

NET_SOCKET_REGISTER(xbee_socket, AF_UNSPEC, xbee_socket_is_supported,
		    xbee_socket_create);

/* Create a network interface for nRF91 */

static int xbee_modem_lib_socket_offload_init(const struct device *arg)
{
	ARG_UNUSED(arg);

	for (int i = 0; i < ARRAY_SIZE(offload_ctx); i++) {

		offload_ctx[i].XBEE_SocketID= -1;

	    k_fifo_init(&offload_ctx[i].PacketBufferQueue);


	}

	return 0;
}

static const struct socket_dns_offload xbee_socket_dns_offload_ops = {
	.getaddrinfo = xbee_socket_offload_getaddrinfo,
	.freeaddrinfo = xbee_socket_offload_freeaddrinfo,
};

static struct xbee_socket_iface_data {
	struct net_if *iface;
} xbee_socket_iface_data;

static void xbee_socket_iface_init(struct net_if *iface)
{
	xbee_socket_iface_data.iface = iface;

	iface->if_dev->offloaded = true;

	socket_offload_dns_register(&xbee_socket_dns_offload_ops);
}

static struct net_if_api xbee_if_api = {
	.init = xbee_socket_iface_init,
};

NET_DEVICE_OFFLOAD_INIT(xbee_socket, "xbee_socket",
			xbee_modem_lib_socket_offload_init,
			NULL,
			&xbee_socket_iface_data, NULL,
			0, &xbee_if_api, 1280);

#endif
