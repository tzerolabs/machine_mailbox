#include "stdint.h"
#include <modem/lte_lc.h>

#ifndef _CELL_H
#define _CELL_H

void Cell_Init(bool RequestPSM,  bool RequestEDRX);

bool Cell_IsConnected();

int32_t Cell_GetRSSI();

int32_t Cell_GetState();

int32_t Cell_GetCellID();

char * Cell_COPS();

int32_t Cell_GetTracking();

char * Cell_ICCID();

char * Cell_IMEI();

int32_t Cell_GetLastPostedRSSI();

int32_t Cell_GetLastPostedRSRP();

int32_t Cell_GetLastPostedRSRQ();

int32_t Cell_GetPSM_TAU();

int32_t Cell_GetPSM_ActiveTime();

int32_t Cell_Get_eDRX_IntervalTime();

int32_t Cell_Get_eDRX_PagingWindow();

int32_t Cell_GetBatt();

uint32_t Cell_GetLastActiveTime_mS();

int32_t Cell_GetLastBatt();

int32_t  Cell_GetCurrentBand();
//Call only after calling Cell_COPS
char * Cell_GetLastMCC_String();

//Call only after calling Cell_COPS
char * Cell_GetLastMNC_String();

char * Cell_ModemFirmware();

bool Cell_IsPSM_Granted();

bool Cell_IsEDRX_Granted();

bool Cell_IsLowPowerGranted();

void Cell_PowerUp();

void Cell_PowerDown();

typedef struct 
{
    enum lte_lc_nw_reg_status NetworkStatus;
    int32_t ID;  // Modem reports -1 when disconnected
    int32_t TrackingArea; //reports 65554 when disconnected
} Cell_ConnectionState_t;

bool Cell_GetConnectionState(Cell_ConnectionState_t * C);

#endif