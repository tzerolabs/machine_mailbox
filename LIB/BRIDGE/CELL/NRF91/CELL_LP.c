#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <modem/lte_lc.h>
#include "FIRMWARE_VERSION.h"
#include <nrf_modem_at.h>
#include <modem/at_cmd_parser.h>
#include <modem/at_params.h>
#include "Cell.h"
#include "rt_config.h"
#include <shell/shell.h>
#include <cJSON.h>
#include "System.h"

#include <modem/at_cmd_parser.h>
#include <modem/at_params.h>
#include <modem/at_monitor.h>

LOG_MODULE_REGISTER(lte,LOG_LEVEL_INF);

static void lte_handler(const struct lte_lc_evt *const evt);

#define MAX_APN_SIZE 100

static char APN_String[MAX_APN_SIZE+1];

RT_CONFIG_ITEM(APN,"Cellular APN",APN_String,RT_CONFIG_DATA_TYPE_STRING,sizeof(APN_String),"","","super");

static char 		NetStatusString[64];

volatile enum lte_lc_nw_reg_status NetworkStatus;

volatile int32_t 	Cell_ID = -1;  // Modem reports this when disconnected!

volatile int32_t	Cell_TrackingArea; //reports 65554

volatile int32_t	Cell_PSM_TAU = -1;

volatile int32_t	Cell_PSM_ActiveTime = -1;

volatile int32_t	Cell_eDRX_IntervalTime = -1;

volatile int32_t	Cell_eDRX_PagingWindow = -1;

static int32_t LastRSSI;

static int32_t LastRSRP;

static int32_t LastRSRQ;

static uint32_t	CellActiveStartTimeStamp = 0;

static uint32_t	LastCellActiveTime_mS= 0;

static bool	FirstShutdownFlag;

static char COPS_OpString[64];

static char ICCID_String[32];

static char IMEI_String[32];

static char ModemFirmware_String[32];

static int32_t BatteryVoltage = 0;

void Cell_Init(bool RequestPSM,  bool RequestEDRX)
{
	int err;
	if (IS_ENABLED(CONFIG_LTE_AUTO_INIT_AND_CONNECT))
	{
		/* Do nothing, modem is already configured and LTE connected. */
	}
	else
    {

		LOG_INF("Initializing Modem...");

		err = lte_lc_init();

		if (err) {
			LOG_ERR("Modem initialization failed, error: %d\n", err);
			return;
	
		}
	}

		char cgdcont[144];

	    char cmd_rcv_buf[50];

		sprintf(cgdcont,"AT+CGDCONT=0,\"IP\",\"%s\"",APN_String);

		LOG_INF("Setting PDP content to %s",log_strdup(cgdcont));
	
		if (nrf_modem_at_cmd( cmd_rcv_buf, sizeof(cmd_rcv_buf), "%s",cgdcont) != 0) 
		{
				LOG_ERR("Error Setting PDP Context");
		}
		else
		{
    		LOG_INF("Yeah! PDP Context: %s", log_strdup(cgdcont));
		}

		LOG_INF("Connecting to LTE network");
		
		if(CellActiveStartTimeStamp == 0)
		{
			CellActiveStartTimeStamp = System_Get_mS_Ticker();
		}

        err = lte_lc_init_and_connect_async(lte_handler);

		if (err)
		{
			LOG_ERR("Modem could not be configured, error: %d",	err);
			return;

		}


		if(RequestPSM == true)
		{
			/** Power Saving Mode */
			err = lte_lc_psm_req(true);
			if (err) {
				LOG_ERR("lte_lc_psm_req, error: %d\n", err);
			}
		}
		else
		{
			err = lte_lc_psm_req(false);
			if (err) {
				LOG_ERR("lte_lc_psm_req, error: %d\n", err);
			}
		}

		if(RequestEDRX == true)
		{
			/** enhanced Discontinuous Reception */
			err = lte_lc_edrx_req(true);
			if (err) {
				LOG_ERR("lte_lc_edrx_req, error: %d\n", err);
			}

		}
		else
		{
			err = lte_lc_edrx_req(false);
			if (err) {
				LOG_ERR("lte_lc_edrx_req, error: %d\n", err);
			}
		}

	
		if(lte_lc_modem_events_enable() == 0)
		{
			LOG_INF("Modem domain notifications enabled");
		}
		else
		{
			LOG_INF("Request for  modem domain notifications failed");
		}

		#if defined(CONFIG_UDP_RAI_ENABLE)
			/** Release Assistance Indication  */
			err = lte_lc_rai_req(true);
			if (err) {
				LOG_ERR("lte_lc_rai_req, error: %d\n", err);
			}
		#endif

		LOG_INF("Requesting sleep notifications");
																 
		if (nrf_modem_at_cmd( cmd_rcv_buf, sizeof(cmd_rcv_buf), "AT%%XMODEMSLEEP=1,1000,60000") != 0) 
		{
				LOG_ERR("Error requesting sleep notifications");
		}

		/*
		LOG_INF("Requesting low battery notifications");
																 
		if (nrf_modem_at_cmd( cmd_rcv_buf, sizeof(cmd_rcv_buf), "AT%%XVBATLVL=1") != 0) 
		{
				LOG_ERR("Error requesting low level battery notifications");
		}
		*/

}

/*

static void at_handler_lowbatt(const char *response)
{

	LOG_DBG("Low batt %s",log_strdup(response));

}

AT_MONITOR(ltelc_atmon_lowbatt, "%XVBATLOWLVL", at_handler_lowbatt);
*/


int32_t Cell_GetCurrentBand()
{
	int32_t CurrentBand = -1;

	struct at_param_list MyList;

	struct at_param * MyParam;

    at_params_list_init(&MyList,2);

    char cmd_rcv_buf[50];

    if ( nrf_modem_at_cmd(cmd_rcv_buf, 50, "AT%%XCBAND") == 0) 
	{
         
            at_parser_max_params_from_str(cmd_rcv_buf,NULL,&MyList,10);
              
            MyParam = &MyList.params[1];

			CurrentBand = MyParam->value.int_val;
	}

	at_params_list_free(&MyList);

	return CurrentBand;
}

int32_t Cell_Get_eDRX_IntervalTime()
{
	return Cell_eDRX_IntervalTime;
}

int32_t Cell_Get_eDRX_PagingWindow()
{
	return Cell_eDRX_PagingWindow;
}

int32_t Cell_GetPSM_TAU()
{
	return Cell_PSM_TAU;
}

int32_t Cell_GetPSM_ActiveTime()
{
	return Cell_PSM_ActiveTime;
}

int32_t Cell_GetCellID()
{
	return Cell_ID;
}

int32_t Cell_GetTracking()
{
	return Cell_TrackingArea;
}

bool Cell_IsPSM_Granted()
{
	if(
	    (Cell_GetPSM_ActiveTime()>=0) && 
	    (Cell_GetPSM_TAU()>=0)
	   )
	{
		return true;
	}

	return false;
}

bool Cell_IsEDRX_Granted()
{
	if(
		(Cell_Get_eDRX_PagingWindow()>=0)  && 
	    (Cell_Get_eDRX_IntervalTime()>=0)
	   )
	   {
			return true;
	   }

	return false;   
}

bool Cell_IsLowPowerGranted()
{
	return (
		    	Cell_IsPSM_Granted() ||
	        	Cell_IsEDRX_Granted()
			);
}

void  Cell_PowerUp()
{
	lte_lc_normal();
}

void Cell_PowerDown()
{
	lte_lc_offline();
}

uint32_t Cell_GetLastActiveTime_mS()
{
	if(FirstShutdownFlag)
		return LastCellActiveTime_mS;
	else
		return 0;
}

char * GetNetStatusString(enum lte_lc_nw_reg_status  S)
{
	char * result = "";

		switch(S)
		{
			default:								result = "Unhandled";break;
			case LTE_LC_NW_REG_NOT_REGISTERED:		result = "Not Registered";break;
			case LTE_LC_NW_REG_REGISTERED_HOME:		result = "Connected - Home";break;
			case LTE_LC_NW_REG_SEARCHING:			result = "Searching";break;
			case LTE_LC_NW_REG_REGISTRATION_DENIED:	result = "Regisration Denied";break;
			case LTE_LC_NW_REG_UNKNOWN:			    result = "Unknown";break;
			case LTE_LC_NW_REG_REGISTERED_ROAMING:  result = "Connected - Roaming";break;
			case LTE_LC_NW_REG_REGISTERED_EMERGENCY:result = "Connected - Emergency";break;
			case LTE_LC_NW_REG_UICC_FAIL:			result = "UICC Fail";break;

		}

	snprintf(NetStatusString,sizeof(NetStatusString),"[0x%02x] %s",NetworkStatus,result);

	return &NetStatusString[0];

}

bool Cell_IsConnected()
{
   	if(Cell_ID == -1)
	{
		return false;
	}
	
	if(NetworkStatus==LTE_LC_NW_REG_REGISTERED_ROAMING)
	{
		return true;
	}

	if(NetworkStatus==LTE_LC_NW_REG_REGISTERED_HOME)
	{
		return true;
	}


	return false;

}

bool Cell_GetConnectionState(Cell_ConnectionState_t * C)
{
	C->ID = Cell_ID;
	C->TrackingArea = Cell_TrackingArea;
	C->NetworkStatus = NetworkStatus;
	return Cell_IsConnected();
}

char * Cell_MakeNetRegStatusString(int32_t S)
{
	char * R;

	switch(S)
	{
			case LTE_LC_NW_REG_NOT_REGISTERED:  R = "NOT_REGISTERED"; break;
			case LTE_LC_NW_REG_REGISTERED_HOME: R = "REGISTERED_HOME"; break;
			case LTE_LC_NW_REG_SEARCHING:	    R = "SEARCHING"; break;
			case LTE_LC_NW_REG_REGISTRATION_DENIED: R = "REGISTRATION_DENIED"; break;
			case LTE_LC_NW_REG_UNKNOWN: R = "UNKNOWN"; break;
			case LTE_LC_NW_REG_REGISTERED_ROAMING: R = "REGISTERED_ROAMINGG"; break;
			case LTE_LC_NW_REG_REGISTERED_EMERGENCY: R = "REGISTERED_EMERGENCY"; break;
			case LTE_LC_NW_REG_UICC_FAIL: R = "UICC_FAIL"; break;

			default :  R = "???"; break;

	}

	return R;

} 

static void lte_handler(const struct lte_lc_evt *const evt)
{

	switch (evt->type)
	{

	case LTE_LC_EVT_MODEM_EVENT:
		if(evt->modem_evt == LTE_LC_MODEM_EVT_BATTERY_LOW)
		{
			LOG_WRN("LOW BATTERY");
		}
	break;

	case LTE_LC_EVT_TAU_PRE_WARNING:

		
		LOG_INF("TAU_PRE_WARNING");

		break;

	case LTE_LC_EVT_MODEM_SLEEP_EXIT_PRE_WARNING:

		LOG_INF("SLEEP_EXIT_PRE_WARNING");
		
		break;

	case LTE_LC_EVT_MODEM_SLEEP_EXIT:

		
			LOG_INF("SLEEP_EXIT");

			CellActiveStartTimeStamp = System_Get_mS_Ticker();

		break;

	case LTE_LC_EVT_MODEM_SLEEP_ENTER:

		
			LOG_INF("SLEEP_ENTER");

			LastCellActiveTime_mS = System__ComputeElapsedTime(&CellActiveStartTimeStamp);

			FirstShutdownFlag = true;

		break;

	case LTE_LC_EVT_NW_REG_STATUS:
		

		NetworkStatus = evt->nw_reg_status;
		
		LOG_INF("Network status : %i , %s ",NetworkStatus,log_strdup(Cell_MakeNetRegStatusString(NetworkStatus)));

		break;

	case LTE_LC_EVT_PSM_UPDATE:
		LOG_WRN("PSM parameter update: TAU: %d, Active time: %d",
		       evt->psm_cfg.tau, evt->psm_cfg.active_time);

		Cell_PSM_TAU = evt->psm_cfg.tau;
		Cell_PSM_ActiveTime = evt->psm_cfg.active_time;
		

		break;
	case LTE_LC_EVT_EDRX_UPDATE:
	{
		char log_buf[60];
		ssize_t len;

		len = snprintf(log_buf, sizeof(log_buf),
			       "eDRX parameter update: eDRX: %f, PTW: %f",
			       evt->edrx_cfg.edrx, evt->edrx_cfg.ptw);

		Cell_eDRX_IntervalTime = (int32_t)((evt->edrx_cfg.edrx + 0.00005f) * 1000);
		
		Cell_eDRX_PagingWindow = (int32_t)((evt->edrx_cfg.ptw + 0.0005f) * 1000);

		if (len > 0) {
			LOG_INF("%s", log_buf);
		}
		break;
	}
	case LTE_LC_EVT_RRC_UPDATE:
		LOG_DBG("	: %s",
		       evt->rrc_mode == LTE_LC_RRC_MODE_CONNECTED ?
		       "Connected" : "Idle");
		break;
	case LTE_LC_EVT_CELL_UPDATE:

	Cell_ID = evt->cell.id;
	Cell_TrackingArea = evt->cell.tac;

		LOG_WRN("LTE cell changed: Cell ID: %d, Tracking area: %d", 
		       Cell_ID, Cell_TrackingArea);
		break;
	default:
		break;
	}
}

int32_t Cell_GetLastPostedRSSI()
{
	return LastRSSI;
}

int32_t Cell_GetLastPostedRSRP()
{
	return LastRSRP;
}

int32_t Cell_GetLastPostedRSRQ()
{
	return LastRSRQ;
}

//Do not call from IRQ context
int32_t Cell_GetRSSI()
{
	int RSSI = 0;
	int RSRQ = 0;
	int RSRP = 0;

	struct at_param_list MyList;

	struct at_param * MyParam;

    at_params_list_init(&MyList,10);

    char cmd_rcv_buf[50];

    if ( nrf_modem_at_cmd(cmd_rcv_buf, 50, "AT+CESQ") == 0) 
	{
         
            at_parser_max_params_from_str(cmd_rcv_buf,NULL,&MyList,10);
              
            MyParam = &MyList.params[6];

			RSRP = -140+MyParam->value.int_val;

            RSSI = RSRP + 19; //http://www.techplayon.com/rssi/      CAT m1 N=6

            MyParam = &MyList.params[5];

			RSRQ = -19.5 + (MyParam->value.int_val)*0.5;

			LastRSSI = RSSI;
		
			LastRSRP = RSRP;
			
			LastRSRQ = RSRQ;
	}

	at_params_list_free(&MyList);

	return RSSI;
}

char * Cell_ICCID()
{
	struct at_param_list MyList;

    at_params_list_init(&MyList,10);

    char cmd_rcv_buf[64];
   	
    if (nrf_modem_at_cmd(cmd_rcv_buf,sizeof(cmd_rcv_buf), "AT+CRSM=176,12258,0,0,10") == 0) 
	{
        at_parser_max_params_from_str(cmd_rcv_buf,NULL,&MyList,10);
        

		// https://devzone.nordicsemi.com/f/nordic-q-a/48992/at-command-to-get-iccid-from-nrf9160-modem
		ICCID_String[0] = 0;
		int len = sizeof(ICCID_String);
		at_params_string_get(&MyList,3,ICCID_String,&len);
    
		int Len = strlen(ICCID_String);

		if(Len&0x01)
		{
			//if off length,  subtract one one we don't swap the last character
		}

		uint8_t Swap = 0;

		for(int i=0;i<(Len/2);i++)
		{
			Swap = ICCID_String[i*2];
			ICCID_String[i*2] = ICCID_String[i*2 + 1];
			ICCID_String[i*2 + 1] = Swap;
		}
	
	}	

	at_params_list_free(&MyList);


	return ICCID_String;
}

//Do not call from IRQ context
char MCC_String[8];

char MNC_String[8];

char * Cell_GetLastMCC_String()
{
	return MCC_String;
}

char * Cell_GetLastMNC_String()
{
	return MNC_String;
}

char * Cell_COPS()
{

	struct at_param_list MyList;


    at_params_list_init(&MyList,10);

    char cmd_rcv_buf[64];
   
    if (nrf_modem_at_cmd(cmd_rcv_buf, sizeof(cmd_rcv_buf), "AT+COPS?") == 0) 
	{
        at_parser_max_params_from_str(cmd_rcv_buf,NULL,&MyList,10);
        
		COPS_OpString[0] = 0;
		int len = sizeof(COPS_OpString);

		at_params_string_get(&MyList,3,COPS_OpString,&len);


		memset(MCC_String,0,sizeof(MCC_String));
		memset(MNC_String,0,sizeof(MNC_String));
		
		//MCC is always 3 characters
		memcpy(MCC_String,COPS_OpString,3);

		len = strlen(&COPS_OpString[3]);

		if(len == 2 || len == 3)
		{
			strcpy(MNC_String,&COPS_OpString[3]);
		}
		else
		{
			strcpy(MNC_String,"");
		}
	
    }	

	at_params_list_free(&MyList);


	return COPS_OpString;
}



//Do not call from IRQ context
char * Cell_IMEI()
{
	struct at_param_list MyList;

	at_params_list_init(&MyList,10);

    char cmd_rcv_buf[64];
    
    if (nrf_modem_at_cmd(cmd_rcv_buf, sizeof(cmd_rcv_buf), "AT+CGSN=1") == 0)
	{
        at_parser_max_params_from_str(cmd_rcv_buf,NULL,&MyList,3);
        
		IMEI_String[0] = 0;
		int len = sizeof(IMEI_String);
		at_params_string_get(&MyList,1,IMEI_String,&len);

       }	

	at_params_list_free(&MyList);


	return IMEI_String;
}

//Do not call from IRQ context
char * Cell_ModemFirmware()
{
	struct at_param_list MyList;
	
    at_params_list_init(&MyList,10);

    char cmd_rcv_buf[64];

    if (nrf_modem_at_cmd(cmd_rcv_buf, sizeof(cmd_rcv_buf), "AT+CGMR") == 0)
	{
        at_parser_max_params_from_str(cmd_rcv_buf,NULL,&MyList,3);
        
		ModemFirmware_String[0] = 0;
		int len = sizeof(ModemFirmware_String);
		at_params_string_get(&MyList,0,ModemFirmware_String,&len);
       }	

	at_params_list_free(&MyList);


	return ModemFirmware_String;
}

char * GetCellInfo()
{
        cJSON *Result = cJSON_CreateObject();
 
        cJSON_AddStringToObject(Result, "ObjID", "Cell");
        cJSON_AddStringToObject(Result, "State", GetNetStatusString(NetworkStatus));

        cJSON_AddNumberToObject(Result, "RSSI", Cell_GetLastPostedRSSI());

		cJSON_AddNumberToObject(Result, "RSRP", Cell_GetLastPostedRSRP());

        cJSON_AddStringToObject(Result, "IMEI", Cell_IMEI());
        cJSON_AddStringToObject(Result, "ICCID", Cell_ICCID());
		cJSON_AddStringToObject(Result, "Operator", Cell_COPS());

		cJSON_AddStringToObject(Result, "MCC", Cell_GetLastMCC_String());
		cJSON_AddStringToObject(Result, "MNO", Cell_GetLastMNC_String());

		cJSON_AddNumberToObject(Result, "Band", Cell_GetCurrentBand());
		
					
        cJSON_AddStringToObject(Result, "ModuleFirmware", Cell_ModemFirmware());
        cJSON_AddStringToObject(Result, "Firmware",FIRMWARE_VERSION_STRING);
          
        char *string = NULL;
        string = cJSON_Print(Result);
   
        cJSON_Delete(Result);


		return string;
}


static int cell_cmd_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

		char * result = GetCellInfo();
        
		shell_print(shell,"\x02%s\x03",result);
		
		cJSON_free(result);

        return 0;
}


int32_t Cell_GetBatt()
{

	struct at_param_list MyList;

    at_params_list_init(&MyList,10);

    char cmd_rcv_buf[64];
   	
    if (nrf_modem_at_cmd(cmd_rcv_buf,sizeof(cmd_rcv_buf), "AT%%XVBAT") == 0) 
	{
        at_parser_max_params_from_str(cmd_rcv_buf,NULL,&MyList,10);

		at_params_int_get(&MyList,1,&BatteryVoltage);
    
	}	

	at_params_list_free(&MyList);

	return BatteryVoltage;
}

/*
	Get the return the value of the battery reading
*/

int32_t Cell_GetLastBatt()
{
	return BatteryVoltage;
}

SHELL_CMD_REGISTER(cell, NULL, "Get cellular modem info", cell_cmd_handler);
