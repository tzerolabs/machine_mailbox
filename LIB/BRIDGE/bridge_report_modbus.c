#include <zephyr.h>
#include "stdbool.h"
#include "stdio.h"
#include "System.h"
#include <logging/log.h>
#include "bridge.h"
#include "BridgeDataStructure.h"
#include "data_buf.h"
#include "Modbus.h"
#include "DeviceProvision.h"
#include "rt_config.h"
#include "Epoch.h"
#include "Cell.h"
#include "FIRMWARE_VERSION.h"
#include "krusty.h"

LOG_MODULE_DECLARE(bridge);

#define MODBUS_MAX_TX_BUFFER_SIZE   1024 

static uint8_t ModbusRptCnt = 0;

void InitModbusAddressDataPairReport(data_buf_t * DB)
{
  
    if(DB != NULL)
    {
        memset(DB->data,0,DB->max_length);

        BridgeReportHeader *BRH = (BridgeReportHeader *)DB->data;

        BRH->ReportType = BRIDGE_MODBUS_REPORT;
        
        BRH->Flags = 0;

        BRH->RSSI = (int8_t)Cell_GetRSSI();

        BRH->RetryCounter = 0;

        BRH->ReportCounter = ModbusRptCnt++;
        
        BRH->TimeStamp = Epoch__Get64();

        BRH->SerialNumber= Provision__GetDeviceSerialNumber();
        
        BRH->SerialNumberExtention = Provision__GetDeviceSerialNumberExtention();

        BRH->ReportSize = 2;//The  data will start as just the modbus function code length;

        DB->current_length = sizeof(BridgeReportHeader);
        DB->data[DB->current_length++] = 0xF0;
        DB->data[DB->current_length++] = 0x00;

        //this puts us after the header, modbus function code and into the data. 
        //Start by filling in the register address/data pairs
  
    }
}


void InitModbusBlockReport(data_buf_t * DB)
{
    if(DB != NULL)
    {
        memset(DB->data,0,DB->max_length);

        BridgeReportHeader *BRH = (BridgeReportHeader *)DB->data;

        BRH->ReportType = BRIDGE_MODBUS_REPORT;
        
        BRH->Flags = 0;

        BRH->RSSI = (int8_t)Cell_GetRSSI();

        BRH->RetryCounter = 0;

        BRH->ReportCounter = ModbusRptCnt++;
        
        BRH->TimeStamp = Epoch__Get64();
        
        BRH->SerialNumber= Provision__GetDeviceSerialNumber();
        
        BRH->SerialNumberExtention = Provision__GetDeviceSerialNumberExtention();

     
        BRH->ReportSize = 2;//The  data will start as just the modbus function code length;

        //this puts us after the header and into the actual modbus data
     
        DB->current_length = sizeof(BridgeReportHeader);
        DB->data[DB->current_length++] = 0xF1;
        DB->data[DB->current_length++] = 0x00;

    }
}



bool BridgeReportModbus()
{
    static uint16_t HeartBeatCnt;
    uint32_t NumDirty;
    
    int RegIndex = -1;

    data_buf_t * DB = NULL;

    uint32_t RegistersWritten = 0;

    LOG_INF("Collecting Bridge Report...");

    Modbus__WriteRegister(MB_NUM_APP_REGISTERS-1,HeartBeatCnt++);

    if(ModbusTestWrite_Storage)
    {
        for(int i=0;i<10;i++)
        {
          Modbus__WriteRegister(i+20,HeartBeatCnt);
          Modbus__WriteRegister(i+200,HeartBeatCnt);
        }     
    }

 //Figure out the continuous regions
    if((NumDirty = Modbus__GetNumberDirtyRegisters()))
    {
            LOG_DBG("Collecting contiguous dirty regions");

            uint16_t RegBlockStart = 0;
            uint16_t RegBlockLength= 0;
            
            while(Modbus__GetNextDirtyRange(&RegBlockStart, &RegBlockLength) >= 0)
            {

                #define MAX_BLOCK_LENGTH   ((MODBUS_MAX_TX_BUFFER_SIZE - (sizeof(BridgeReportHeader) + 4))/2)  //need to take of 4 bytes for the function code an base address
             
                if(RegBlockLength <=4 )
                {
                    RegBlockStart+=RegBlockLength; 
                    //Skip this block and move on
                }
                else
                {
                  uint16_t UsedRegBlockLength = RegBlockLength;
                  if(RegBlockLength > MAX_BLOCK_LENGTH)
                  {
                      UsedRegBlockLength = MAX_BLOCK_LENGTH;
                  }

                   LOG_DBG("Block Found from %d to %d",RegBlockStart,RegBlockStart+UsedRegBlockLength-1);
                 
                    DB = DataBufferCreate(MODBUS_MAX_TX_BUFFER_SIZE,ReportCompleteCallBack);
                    
                    if(DB == NULL)
                    {
                        LOG_DBG("Cannot get a free 1024 byte block.  Bail.");
                        break;
                    }

                    InitModbusBlockReport(DB);

                    DB->data[DB->current_length++] = ((uint16_t)RegBlockStart)>>8;
                    DB->data[DB->current_length++] = ((uint16_t)RegBlockStart)&0xFF;


                    BridgeReportHeader *BRH = (BridgeReportHeader *)DB->data;

                    //Increment length fields
                    BRH->ReportSize+=2;
                                  

                    for(int i=0;i<UsedRegBlockLength;i++)
                    {
                        DB->data[DB->current_length++] = ((uint16_t)Modbus__ReadRegister(i))>>8;
                        DB->data[DB->current_length++] = ((uint16_t)Modbus__ReadRegister(i))&0xFF; 
                            
                        //Increment length fields
                        BRH->ReportSize+=2;
                  
                    }

                    LOG_DBG("Enqueuing modbus block report");
                   
                 
                    krusty__enqueue_tx_data(DB);

                    Modbus__MarkRegisterRangeClean(RegBlockStart, UsedRegBlockLength);

                    RegBlockStart+=UsedRegBlockLength;
                
                }
            }

    }

    DB = NULL; //Reset our pointer


    #define SPARSE_BLOCK_SIZE   256

    //Handle the sparse areas
    if((NumDirty = Modbus__GetNumberDirtyRegisters()))
    {
        
      
        LOG_DBG("%d registers dirty to assembly into address-value pairs",NumDirty);

        uint32_t BytesAvailablePerBlock = SPARSE_BLOCK_SIZE - sizeof(BridgeReportHeader) - 2; //the "2" is for the modbus function code
        uint32_t ModbusDataSize = NumDirty * 4 ;
        uint32_t NumDataBlocks = (ModbusDataSize / BytesAvailablePerBlock) + 1;
        LOG_DBG("%d bytes overhead",sizeof(BridgeReportHeader));
        LOG_DBG("%d blocks of 256 bytes will be required",NumDataBlocks);


        do 
        {
            RegIndex++;
            RegIndex = Modbus__GetNextDirty(RegIndex);

            if(RegIndex>=0)
            {
              
              //If we dont have an active buffer, start a new one
              if(DB == NULL)
              {
                    DB = DataBufferCreate(SPARSE_BLOCK_SIZE,ReportCompleteCallBack);

                    if(DB == NULL)
                    {
                         LOG_DBG("Cannot get a free block.  Bail.");
                         break;
                    }
                   
                    LOG_DBG("Starting new data block");

                    InitModbusAddressDataPairReport(DB);
              }

             DB->data[DB->current_length++] = ((uint16_t)RegIndex)>>8;
             DB->data[DB->current_length++] = ((uint16_t)RegIndex)&0xFF;
             DB->data[DB->current_length++] = ((uint16_t)Modbus__ReadRegister(RegIndex))>>8;
             DB->data[DB->current_length++] = ((uint16_t)Modbus__ReadRegister(RegIndex))&0xFF;
            
            Modbus__MarkRegisterClean(RegIndex);
            RegistersWritten++;

            BridgeReportHeader *BRH = (BridgeReportHeader *)DB->data;

            //Increment length fields
            BRH->ReportSize+=4;
                   
            //We need 4 bytes per register so finalize the data packet if we are full
            if(DB->current_length>(DB->max_length - 4) && 
               DB->current_length<=(DB->max_length))
               {
                   //Enqueue this buffer and reset our pointer so we getting a new one
                   
                    LOG_INF("Enqueuing modbus report");

                     krusty__enqueue_tx_data(DB);
                   
                    DB = NULL; //
               }             

            } 

        }while(RegIndex >= 0);
               
        //If we ran of of registers to transmit, send out what we have.
        if(DB!=NULL)
        {
                LOG_DBG("Enqueuing last modbus report");
                
                krusty__enqueue_tx_data(DB);
               
                DB = NULL; 
        }

    }

    return true;


}

