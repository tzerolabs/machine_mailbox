#include "stdint.h"

#ifndef _EPOCH_H
#define _EPOCH_H


//void Epoch__Init();

bool Epoch__Update(uint32_t TimeOut_mS);

uint64_t Epoch__Get64();

uint32_t Epoch__Get32();

void Epoch__Set32(uint32_t T);

void Epoch__Set64(uint64_t T);

/*
void Epoch__SetRefreshSeconds(uint32_t Seconds);
*/

char * Epoch__GetLastTimeServer();

bool Epoch__NeedsRefreshed();

#endif