
#include <zephyr.h>
#include <logging/log.h>
#include <shell/shell.h>
#include <stdio.h>
#include <stdlib.h>
#include <nrfx_rtc.h>
#include <modem/lte_lc.h>
#include <net/sntp.h>
#include "Cell.h"
#include "Epoch.h"
#include "System.h"
#include "rt_config.h"

LOG_MODULE_REGISTER(epoch_log);

static int64_t last_kernel_ts = 0;

static int64_t epoch_at_last_kernel_ts = 0;

//static struct k_work_delayable epoch_work;

bool TimeHasBeenSynced = false;

uint32_t LastTimeServer = 0;

static uint32_t tcount = 0;

#define TIME_SERVER_HOST_NAME_MAX_LEN   64

char TimeServerConfig[4][TIME_SERVER_HOST_NAME_MAX_LEN];

RT_CONFIG_ITEM(TimeServer0,"Time Server Option 0",&TimeServerConfig[0][0],RT_CONFIG_DATA_TYPE_STRING,TIME_SERVER_HOST_NAME_MAX_LEN,"","","chrono0.machinemailbox.com");
RT_CONFIG_ITEM(TimeServer1,"Time Server Option 1",&TimeServerConfig[1][0],RT_CONFIG_DATA_TYPE_STRING,TIME_SERVER_HOST_NAME_MAX_LEN,"","","chrono1.machinemailbox.com");
RT_CONFIG_ITEM(TimeServer2,"Time Server Option 2",&TimeServerConfig[2][0],RT_CONFIG_DATA_TYPE_STRING,TIME_SERVER_HOST_NAME_MAX_LEN,"","","chrono2.machinemailbox.com");
RT_CONFIG_ITEM(TimeServer3,"Time Server Option 3",&TimeServerConfig[3][0],RT_CONFIG_DATA_TYPE_STRING,TIME_SERVER_HOST_NAME_MAX_LEN,"","","chrono3.machinemailbox.com");

char * TimeServer = &TimeServerConfig[0][0];

char * LastGoodTimeServer = "";

static int rt_handler(const struct shell *shell, size_t argc,
                         char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

    	LOG_INF("Flagging epoch service to resync time");

        tcount = 0xFFFFFFF;

        return 0;
}

SHELL_CMD_REGISTER(rt, NULL, "resyncs network time", rt_handler);


static uint32_t EpochRefreshSeconds = 60 * 60 * 24;

bool Epoch__NeedsRefreshed()
{
	if((k_uptime_get() - last_kernel_ts) > (EpochRefreshSeconds*1000))
	{
			return true;
	}
	else
	{
		return false;
	}
}


/*
void epoch_work_handler(struct k_work *work)
{
	
	switch (LastTimeServer)
	{
		default:
		case 0: TimeServer = &TimeServerConfig[0][0]; break;
		case 1: TimeServer = &TimeServerConfig[1][0];break;
		case 2: TimeServer = &TimeServerConfig[2][0];break;
		case 3: TimeServer = &TimeServerConfig[3][0];break;					
	}
    
    LastTimeServer++;
	
    if(LastTimeServer >=3)
    {
		LastTimeServer = 0;
	}
	
	LOG_INF("Getting time from %s",log_strdup(TimeServer));

    struct sntp_time CurrentTimeStruct;

	int e = sntp_simple(TimeServer,10000,&CurrentTimeStruct);

	if(!e)
	{
		int32_t time = (uint32_t )CurrentTimeStruct.seconds;
		LOG_INF("Time resolved to %d",time);
        
		Epoch__Set32(time);
	 	TimeHasBeenSynced = true;
        tcount = 0;

        k_work_reschedule(&epoch_work, K_MSEC(1000 * EpochRefreshSeconds));

	}
	else
	{
		LOG_ERR(" error %i getting time",e);
        k_work_reschedule(&epoch_work, K_MSEC(1000 ));
	}
 
}
*/

char * Epoch__GetLastTimeServer()
{
	return LastGoodTimeServer;
}


bool Epoch__Update(uint32_t TimeOut_mS)
{
	switch (LastTimeServer)
	{
		default:
		case 0: TimeServer = &TimeServerConfig[0][0]; break;
		case 1: TimeServer = &TimeServerConfig[1][0];break;
		case 2: TimeServer = &TimeServerConfig[2][0];break;
		case 3: TimeServer = &TimeServerConfig[3][0];break;					
	}
    
    LastTimeServer++;
	
    if(LastTimeServer >=3)
    {
		LastTimeServer = 0;
	}
	
	LOG_INF("Getting time from %s",log_strdup(TimeServer));

    struct sntp_time CurrentTimeStruct;

	int e = sntp_simple(TimeServer,TimeOut_mS,&CurrentTimeStruct);

	if(!e)
	{
		int32_t time = (uint32_t )CurrentTimeStruct.seconds;
		LOG_INF("Time resolved to %d",time);
        
		Epoch__Set32(time);
	 	TimeHasBeenSynced = true;
		LastGoodTimeServer = TimeServer;
      	return true;
      
	}
	else
	{
		LOG_ERR("Error %i getting time",e);

		return false;
    }
}



//void Epoch__Init()
//{
  //  k_work_init_delayable(&epoch_work,epoch_work_handler);

//	k_work_reschedule(&epoch_work, K_MSEC(1000));
//}

uint64_t Epoch__Get64()
{
   return  (uint64_t)(((k_uptime_get() - last_kernel_ts)/1000) + epoch_at_last_kernel_ts);
}

uint32_t Epoch__Get32()
{
   return  (uint32_t)Epoch__Get64();
}

void Epoch__Set32(uint32_t T)
{
	last_kernel_ts = k_uptime_get();
	epoch_at_last_kernel_ts = (int64_t)T;
    
}

void Epoch__Set64(uint64_t T)
{
	last_kernel_ts = k_uptime_get();
	epoch_at_last_kernel_ts =  (int64_t)T;

}
