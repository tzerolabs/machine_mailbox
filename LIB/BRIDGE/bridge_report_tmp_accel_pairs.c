#include <zephyr.h>
#include "stdbool.h"
#include "stdio.h"
#include <stdlib.h>
#include "System.h"
#include <logging/log.h>
#include "bridge.h"
#include "data_buf.h"
#include "BridgeDataStructure.h"
#include "DeviceProvision.h"
#include "Epoch.h"
#include "jack.h"
#include "Cell.h"
#include "krusty.h"

LOG_MODULE_DECLARE(bridge);

static uint8_t TempAccelPair_ReportCounter = 0;


typedef struct 
{
    uint16_t SensorType;
    uint16_t SecondsBetweenSamples;
    uint16_t Count;
    uint16_t Padding;

} __attribute__ ((packed)) TempAccelSensorHeader;
/*
typedef struct
{
    uint16_t SensorType;
    uint16_t SecondsBetweenSamples;
    uint16_t Count;
    uint16_t Padding;
    int16_t Temp[Count];
    uint16_t AccelGRMS[Count];
}*/



bool BridgeReportTempAccel(uint64_t TimeStamp,int16_t * Temps,uint16_t *Accels, uint16_t Cnt,uint16_t SecondsBetweenSamples)
{

    uint32_t TempAccelDataLength = 8 + 4*Cnt;

    uint32_t TotalMessageSize = sizeof(BridgeReportHeader) +
                                TempAccelDataLength;

    data_buf_t * DB =DataBufferCreate(TotalMessageSize,0);
      
    if(DB!=NULL)
    {

        BridgeReportHeader * BRH;

        BRH = (BridgeReportHeader *)(DB->data);
       
        BRH->Flags = 0;
        BRH->RSSI = (int8_t)Cell_GetRSSI();
        
        BRH->ReportType = BRIDGE_TOAD_REPORT;
        BRH->TimeStamp = TimeStamp;
        BRH->SerialNumber = Provision__GetDeviceSerialNumber();
        BRH->SerialNumberExtention = Provision__GetDeviceSerialNumberExtention();
        BRH->ReportCounter = TempAccelPair_ReportCounter++;
        BRH->RetryCounter = 0;
        BRH->ReportSize =  TempAccelDataLength;
 
        DB->current_length = sizeof(BridgeReportHeader);


        TempAccelSensorHeader *Header =(TempAccelSensorHeader *)(&(DB->data[DB->current_length]));

        Header->SensorType = VAL_BULLET_BILL_TEMP_ACCEL;
        Header->SecondsBetweenSamples = SecondsBetweenSamples;
        Header->Padding = 0;
        Header->Count = Cnt;

        DB->current_length+=sizeof(TempAccelSensorHeader);       

        
        memcpy(&(DB->data[DB->current_length]),Temps,sizeof(int16_t)*Cnt);
        DB->current_length+=sizeof(int16_t)*Cnt;

        memcpy(&(DB->data[DB->current_length]),Accels,sizeof(uint16_t)*Cnt);
        DB->current_length+=sizeof(int16_t)*Cnt;



       
       // memcpy(&PSD->PSD_Data,(void *)PSD_In,sizeof(Sensor__PSD_1024));

//        DB->current_length = sizeof(FixedPSD_Report);

        krusty__enqueue_tx_data(DB);
       
        return true;
    }
    else
    {
        
        LOG_ERR("Could not allocate %d bytes for PSD report",sizeof(FixedPSD_Report));
        return false;
    }
   
}
