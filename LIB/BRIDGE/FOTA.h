#ifndef _FOTA_H
#define _FOTA_H

void FOTA_Init();

void UpdateFirmware(char * Host,char *File,int SecurityTag);

void UpdateFromDefault();


/*
	returns -1 if FOTA had an error.
	returns 0 if FOTA is inactive.
	Retusrn 1-100 to indicate progress.
*/

int32_t FOTA_GetStatus();

void UpdateFromPath(char * Path);

#define FOTA_IS_ACTIVE (FOTA_GetStatus() > 0)



#endif