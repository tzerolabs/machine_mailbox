
#include <zephyr.h>
#include "stdbool.h"
#include "System.h"
#include <logging/log.h>
#include "bridge_toad.h"
#include "DataBuffer.h"
#include "BridgeDataStructure.h"
#include "DataBuffer.h"
#include "Modbus.h"
#include "DeviceProvision.h"
#include "Cell.h"
#include <tinycrypt/hmac.h>
#include <tinycrypt/sha256.h>
#include <tinycrypt/constants.h>
#include "rt_config.h"
#include "Epoch.h"
#include <cJSON.h>
#include <cJSON_os.h>
#include <task_wdt/task_wdt.h>
#include "FIRMWARE_VERSION.h"
#include "stdio.h"
#include "FOTA.h"
#include <shell/shell.h>
#include "MachineMailboxIO.h"
#include "jack.h"
#include "thread_wdt.h"
#include "bridge_report_4ch.h"
#include "bridge_report_json.h"
#include "bridge_report_modbus.h"
#include "bridge_report_psd.h"
#include "bridge_report_toad_jack.h"


volatile BridgeState MyBridgeState = BRIDGE_INIT;

LOG_MODULE_REGISTER(bridge,LOG_LEVEL_DBG);

#define BRIDGE_DEBUG(...)             LOG_INF(__VA_ARGS__)                                                          

#define BRIDGE_DEBUG_NO_MARKER(...)   LOG_INF(__VA_ARGS__)   

void Bridge_StateTransition(BridgeState BS);

uint32_t BridgeTimeOut = 0;

uint32_t BridgeHeartBeatTimeOut = 0;

uint16_t HeartBeatCnt = 0;

uint32_t BridgeReportRateSeconds = 60;
uint32_t PSD_ReportRateSeconds = 600;

volatile uint64_t BridgeLastSyncTime;

volatile uint32_t BridgeSyncCount;

int32_t bridge_thread_wdt_channel = -1;

void ReportCompleteCallBack(void *DataBuffer)
{
    BridgeSyncCount++;
    BridgeLastSyncTime = Epoch__Get64();
}

char * GetBirthRecord()
{
        cJSON *Result = cJSON_CreateObject();

		char RSSI_Temp[32];

		sprintf(RSSI_Temp,"%idbm",Cell_GetLastPostedRSSI());
 
                        
        cJSON_AddStringToObject(Result, "ObjID", "BirthRecord");

        cJSON_AddStringToObject(Result, "FirmwareVersion",FIRMWARE_VERSION_STRING);
          
        char *string = NULL;
        string = cJSON_Print(Result);
   
        cJSON_Delete(Result);


		return string;
}


static int br_cmd_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

		char * result = GetBirthRecord();
        
		shell_print(shell,"\x02%s\x03",result);
		
		cJSON_free(result);

        return 0;
}


SHELL_CMD_REGISTER(br, NULL, "Get bridge birth record", br_cmd_handler);


RT_CONFIG_ITEM(ReportRate,"Report Rate in Seconds",&BridgeReportRateSeconds,RT_CONFIG_DATA_TYPE_UINT32,sizeof(BridgeReportRateSeconds),"2","86400","60");
RT_CONFIG_ITEM(PSD_ReportRate,"PSD Report Rate in Seconds",&PSD_ReportRateSeconds,RT_CONFIG_DATA_TYPE_UINT32,sizeof(BridgeReportRateSeconds),"16","86400","600");



void Bridge_ConnectNotify()
{
    Bridge_SyncNow();


    cJSON *Result = cJSON_CreateObject();
  
    cJSON_AddStringToObject(Result, "ObjID","Connect");
 
    cJSON_AddNumberToObject(Result, "RSSI", Cell_GetLastPostedRSSI());
    
    cJSON_AddNumberToObject(Result, "RSRP", Cell_GetLastPostedRSRP());

    cJSON_AddStringToObject(Result, "FirmwareVersion",FIRMWARE_VERSION_STRING);
 
    cJSON_AddNumberToObject(Result, "CellID",Cell_GetCellID());

    cJSON_AddNumberToObject(Result, "TAC", Cell_GetTracking());
 
     cJSON_AddStringToObject(Result, "MCCMNO",Cell_COPS());

    Bridge__EnqueueData(BridgeMakeJSON(0, Result));
  
    cJSON_Delete(Result);


}


void Bridge_RebootNotify()
{

    cJSON *Result = cJSON_CreateObject();

    cJSON_AddStringToObject(Result, "ObjID","Reboot");
 
    cJSON_AddStringToObject(Result, "Result","OK");

    cJSON_AddStringToObject(Result, "Message","Rebooting in 10 seconds");
 
    Bridge__EnqueueData(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);
}

void Bridge_UpdateCompleteNotify()
{

    cJSON *Result = cJSON_CreateObject();

    cJSON_AddStringToObject(Result, "ObjID","Update");
 
    cJSON_AddStringToObject(Result, "Result","OK");

    cJSON_AddStringToObject(Result, "Message","Download complete.  Rebooting to flash new FW");
 
    Bridge__EnqueueData(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);

}

void Bridge_UpdateErrorNotify(int32_t ErrorCode)
{
    cJSON *Result = cJSON_CreateObject();

    char Error[64];

    sprintf(Error,"Error Code %i",ErrorCode);

    cJSON_AddStringToObject(Result, "ObjID","Update");

    cJSON_AddStringToObject(Result, "Result","FOTA Error");
 
    cJSON_AddStringToObject(Result, "Message",Error);
 
    Bridge__EnqueueData(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);

}


void Bridge_UpdateProgressNotify(int32_t Progress)
{
    cJSON *Result = cJSON_CreateObject();

    char ProgressString[64];

    sprintf(ProgressString,"Progress : %i%%",Progress);

    cJSON_AddStringToObject(Result, "ObjID","Update");

    cJSON_AddStringToObject(Result, "Result","OK");
 
    cJSON_AddStringToObject(Result, "Message",ProgressString);
 
    Bridge__EnqueueData(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);

}


uint32_t Bridge_SyncCount()
{

    return BridgeSyncCount;
}

uint64_t Bridge_LastSync()
{
   
     return BridgeLastSyncTime;
}

void Bridge_SyncNow()
{
    LOG_INF("Bridge Sync Now...")
    BridgeHeartBeatTimeOut+=(BridgeReportRateSeconds*1000);
}

static struct k_work_delayable bridge_work;

void bridge_work_handler(struct k_work *work);

void Bridge_Init()
{
    Bridge_StateTransition(BRIDGE_INIT);

    bridge_thread_wdt_channel = thread_wdt_add_channel();

    k_work_init_delayable(&bridge_work,bridge_work_handler);

	k_work_reschedule(&bridge_work, K_MSEC(500));
 
}

BridgeState Bridge_GetState()
{
    return MyBridgeState;
}

void Bridge_StateTransition(BridgeState BS)
{
    switch (BS)
    {
 

       default:
        case BRIDGE_INIT:
            
            LOG_INF("Starting Bridge.  Waiting for cellular connection");

            BridgeTimeOut = 0;

            MyBridgeState = BS;

        break;

         case BRIDGE_CONNECTED:
                
            LOG_INF("Bridge is Active");

            MyBridgeState = BS;
                
            BridgeHeartBeatTimeOut =  System_Get_mS_Ticker();

          
        break;

    }

}

void BridgeHeartBeat()
{

}



volatile uint32_t BridgeNumSent = 0;

uint32_t RptCnt = 0;


bool BridgeReport()
{
    if(MachineMailBoxIO_Mode == MACHINE_MAILBOX_MODE__MODBUS)
    {
        return BridgeReportModbus();
    }
    else
    {
        return BridgeReport4CH();
    }
}

uint32_t BridgeReportTick_Temp;
uint32_t BridgeReportTick_PSD;
uint32_t BridgeReportSubTick_PSD;


bool TxPSD_Active = false;

uint32_t CurrentPSD_Jack = 0;

void bridge_work_handler(struct k_work *work)
{

        thread_wdt_channel_feed(bridge_thread_wdt_channel);

        switch (MyBridgeState)
        {
            default:
            case BRIDGE_INIT:
            
             BridgeHeartBeatTimeOut = 0xFFF00000;

            // DataBuffer_Enqueue(&BridgeDataBufferQueue, MakeStatusReport(INFO,"Bridge Started"));
        
            if(Cell_IsConnected() == true)
            {
                Bridge_StateTransition(BRIDGE_CONNECTED);
            }
            break;

            case BRIDGE_CONNECTED:

              if(Provision__IsGood() == false)
              {
                   break;
              }

              //No send sensor reports during FOTA
              if(FOTA_GetStatus() > 0)
              {
                  break;
              }

            if(Check_mS_Timeout(&BridgeReportTick_Temp,BridgeReportRateSeconds*1000))
            {
                 BridgeReportToadJack();
            }

        
            if(TxPSD_Active == false)
            {

                if(Check_mS_Timeout(&BridgeReportTick_PSD,PSD_ReportRateSeconds * 1000))
                {
                    TxPSD_Active = true;

                    LOG_INF("Sending PSDs...");

                    BridgeReportSubTick_PSD = System_Get_mS_Ticker();

                    BridgeReportPSD(CurrentPSD_Jack);

                    CurrentPSD_Jack++;
                }
            }
            else
            {
                if(Check_mS_Timeout(&BridgeReportSubTick_PSD,5000))
                {
                    BridgeReportPSD(CurrentPSD_Jack);

                    CurrentPSD_Jack++;

                    if(CurrentPSD_Jack >= 4)
                    {
                        CurrentPSD_Jack = 0;
                        TxPSD_Active = false;
                    }
                }
                
            }

//               if(Check_mS_Timeout(&BridgeHeartBeatTimeOut,BridgeReportRateSeconds*1000)) 
 //              {
   //                 BridgeReport();
     //          }
              break;
        }        
    
    k_work_reschedule(&bridge_work, K_MSEC(500));

}

