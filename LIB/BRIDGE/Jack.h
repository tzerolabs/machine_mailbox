
#include "stdint.h"
#include "data_buf.h"

#ifndef _JACK_H
#define _JACK_H

#define CONFIG_NUM_JACKS        1
#define CONFIG_MAX_JACK_SIZE    64

int32_t Jack__GetCount();

uint16_t Jack__GetBytesAtJack(uint32_t JackNumber);

data_buf_t * Jack__MakeReport();

int32_t Jack__PushInData(uint32_t JackNumber,
                         uint8_t *DataBuffer,
                         uint16_t DataBufferLen
                         );


int32_t Jack__SetState(uint32_t JackNumber, uint8_t JackState);

uint8_t Jack__GetState(uint32_t JackNumber);

#endif