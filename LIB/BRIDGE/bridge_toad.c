
#include <zephyr.h>
#include "stdbool.h"
#include "System.h"
#include <logging/log.h>
#include "thread_wdt.h"
#include <shell/shell.h>

#include "rt_config.h"
#include "Epoch.h"
#include "DeviceProvision.h"
#include "Cell.h"
#include "krusty.h"
#include "bridge_toad.h"
#include "data_buf.h"
#include "BridgeDataStructure.h"

#include <cJSON.h>
#include <cJSON_os.h>

#include "bridge_report_4ch.h"
#include "bridge_report_json.h"
#include "bridge_report_psd.h"
#include "bridge_report_toad_jack.h"

#include "FOTA.h"

#include "toad_io.h"

static int toad_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"\r\n");   
   
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"            ██████████       \r\n");     
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"        ████          ████     \r\n");   
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"      ██      ▒▒▒▒▒▒      ██     \r\n"); 
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"    ██      ▒▒▒▒▒▒▒▒▒▒      ██  \r\n");  
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"    ██      ▒▒▒▒▒▒▒▒▒▒        ██  \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██▒▒      ▒▒▒▒▒▒▒▒▒▒      ▒▒██  \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██▒▒▒▒      ▒▒▒▒▒▒        ▒▒▒▒██\r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██▒▒▒▒                    ▒▒▒▒██\r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██▒▒      ████████        ▒▒▒▒██\r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██    ██████    ██████      ▒▒██\r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██  ██    ██    ██    ██      ██\r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"    ████    ██    ██      ██  ██  \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"      ██                  ██  ██  \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"      ██    ████████      ████   \r\n"); 
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"        ██              ████     \r\n"); 
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"        ████████████████████      \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"      ████▒▒██    ██▒▒██    ██   \r\n"); 
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"    ██  ██▒▒██    ██▒▒▒▒██    ██  \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██  ██▒▒██      ██▒▒▒▒██    ██  \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██  ██▒▒██████████▒▒▒▒▒▒██    ██\r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"  ██  ██▒▒██        ██▒▒▒▒██    ██\r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"    ██████    ██      ██████████  \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"        ██      ██        ██      \r\n");
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"      ██▒▒▒▒▒▒████▒▒▒▒██████   \r\n");   
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"    ██▒▒▒▒▒▒▒▒██▒▒▒▒▒▒▒▒▒▒██   \r\n");   
    shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"    ████████████████████████   \r\n");   
     shell_fprintf(shell,SHELL_VT100_COLOR_YELLOW,"\r\n");
   
   return 0;
};

SHELL_CMD_REGISTER(toad, NULL, "", toad_handler);

typedef enum
{
    BRIDGE_INIT =0, 
    BRIDGE_CONNECTED,

} BridgeState;


volatile BridgeState MyBridgeState = BRIDGE_INIT;

LOG_MODULE_REGISTER(bridge,LOG_LEVEL_DBG);

#define BRIDGE_DEBUG(...)             LOG_INF(__VA_ARGS__)                                                          

#define BRIDGE_DEBUG_NO_MARKER(...)   LOG_INF(__VA_ARGS__)   

void Bridge_StateTransition(BridgeState BS);

uint32_t BridgeTimeOut = 0;


uint32_t BridgeReportRateSeconds = 60;
uint32_t PSD_ReportRateSeconds = 600;
int32_t bridge_thread_wdt_channel = -1;


uint32_t BridgeReportTick_Temp;
uint32_t BridgeReportTick_PSD;
uint32_t BridgeReportSubTick_PSD;
bool TxPSD_Active = false;
uint32_t CurrentPSD_Jack =0;
volatile uint64_t BridgeLastSyncTime;
volatile uint32_t BridgeSyncCount;


RT_CONFIG_ITEM(ReportRate,"Report Rate in Seconds",&BridgeReportRateSeconds,RT_CONFIG_DATA_TYPE_UINT32,sizeof(BridgeReportRateSeconds),"2","86400","60");
RT_CONFIG_ITEM(PSD_ReportRate,"PSD Report Rate in Seconds",&PSD_ReportRateSeconds,RT_CONFIG_DATA_TYPE_UINT32,sizeof(BridgeReportRateSeconds),"16","86400","600");

uint32_t Bridge_SyncCount()
{
    return BridgeSyncCount;
}

uint64_t Bridge_LastSync()
{
     return BridgeLastSyncTime;
}

void ReportCompleteCallBack(void *DataBuffer)
{
    BridgeSyncCount++;
    BridgeLastSyncTime = Epoch__Get64();
}

void Bridge_SyncNow()
{
    LOG_INF("Bridge Sync Now...");
    
}

static struct k_work_delayable bridge_work;

void bridge_work_handler(struct k_work *work);

void toad_bridge_init()
{
    krusty__register_on_connect_notify(Bridge_SyncNow);

    Bridge_StateTransition(BRIDGE_INIT);

    bridge_thread_wdt_channel = thread_wdt_add_channel();

    k_work_init_delayable(&bridge_work,bridge_work_handler);

	k_work_reschedule(&bridge_work, K_MSEC(500));
 
}

void Bridge_StateTransition(BridgeState BS)
{
    switch (BS)
    {
 
       default:
        case BRIDGE_INIT:
            
            LOG_INF("Starting Bridge.  Waiting for cellular connection");

            BridgeTimeOut = 0;

            MyBridgeState = BS;

        break;

         case BRIDGE_CONNECTED:
                
            LOG_INF("Bridge is Active");

            MyBridgeState = BS;
                

        break;

    }

}

void bridge_work_handler(struct k_work *work)
{

        thread_wdt_channel_feed(bridge_thread_wdt_channel);

        switch (MyBridgeState)
        {
            default:
            case BRIDGE_INIT:
      
            if(Cell_IsConnected() == true)
            {
                Bridge_StateTransition(BRIDGE_CONNECTED);
            }
            break;

            case BRIDGE_CONNECTED:

              if(Provision__IsGood() == false)
              {
                   break;
              }

              //No send sensor reports during FOTA
              if(FOTA_GetStatus() > 0)
              {
                  break;
              }

            if(Check_mS_Timeout(&BridgeReportTick_Temp,BridgeReportRateSeconds*1000))
            {
                 BridgeReportToadJack();
            }

        
            if(TxPSD_Active == false)
            {

                if(Check_mS_Timeout(&BridgeReportTick_PSD,PSD_ReportRateSeconds * 1000))
                {
                    TxPSD_Active = true;

                    LOG_INF("Sending PSDs...");

                    BridgeReportSubTick_PSD = System_Get_mS_Ticker();

                    BridgeReportPSD(CurrentPSD_Jack,(Sensor__PSD_1024 *)&MyPSDs[CurrentPSD_Jack],Epoch__Get64());

                    CurrentPSD_Jack++;
                }
            }
            else
            {
                if(Check_mS_Timeout(&BridgeReportSubTick_PSD,5000))
                {
                     BridgeReportPSD(CurrentPSD_Jack,&MyPSDs[CurrentPSD_Jack],Epoch__Get64());

                    CurrentPSD_Jack++;

                    if(CurrentPSD_Jack >= 4)
                    {
                        CurrentPSD_Jack = 0;
                        TxPSD_Active = false;
                    }
                }
                
            }

              break;
        }        
    
    k_work_reschedule(&bridge_work, K_MSEC(500));

}

