#include <zephyr.h>
#include "stdbool.h"
#include "stdio.h"
#include <stdlib.h>
#include "System.h"
#include <logging/log.h>
#include "bridge.h"
#include "data_buf.h"
#include "BridgeDataStructure.h"
#include "DeviceProvision.h"
#include "Epoch.h"
#include "jack.h"
#include "Cell.h"
#include "krusty.h"

LOG_MODULE_DECLARE(bridge);

static uint8_t PSD_ReportCounter = 0;

//Send in a zero based Jack ID (0-3)
bool BridgeReportPSD(uint32_t JackID,Sensor__PSD_1024 * PSD_In,uint64_t TimeStamp)
{
    data_buf_t * DB =DataBufferCreate(sizeof(FixedPSD_Report),0);
    FixedPSD_Report * PSD;

    if(DB!=NULL)
    {
        LOG_ERR("Send PSD Report for Jack #%d",JackID+1);

        if(JackID > 3)
        {
            JackID = 0;
        }

        PSD = (FixedPSD_Report *)(DB->data);
       
        PSD->JRH.Flags = 0;
        PSD->JRH.RSSI = (int8_t)Cell_GetRSSI();
        
        PSD->JRH.ReportType = BRIDGE_TOAD_REPORT;
        PSD->JRH.TimeStamp = TimeStamp;
        PSD->JRH.SerialNumber = Provision__GetDeviceSerialNumber();
        PSD->JRH.SerialNumberExtention = Provision__GetDeviceSerialNumberExtention();
        PSD->JRH.ReportCounter = PSD_ReportCounter++;
        PSD->JRH.RetryCounter = 0;
        PSD->JRH.ReportSize =  sizeof(Sensor__PSD_1024);

        PSD->PSD_Data.JackID = JackID+1;
       
        memcpy(&PSD->PSD_Data,(void *)PSD_In,sizeof(Sensor__PSD_1024));

        DB->current_length = sizeof(FixedPSD_Report);

        krusty__enqueue_tx_data(DB);
       
        return true;
    }
    else
    {
        
        LOG_ERR("Could not allocate %d bytes for PSD report",sizeof(FixedPSD_Report));
        return false;
    }
   
}
