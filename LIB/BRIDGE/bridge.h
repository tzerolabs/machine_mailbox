
#include "data_buf.h"
#include <cJSON.h>
#include <cJSON_os.h>

#ifndef BRIDGE_H
#define BRIDGE_H

void Bridge_Init();
void Bridge_Process();

extern volatile bool Bridge_Check;

typedef enum
{
    BRIDGE_INIT =0, 
    BRIDGE_WAIT_FOR_NETWORK, 
    BRIDGE_FETCH_TIME,
    BRIDGE_WAIT_FOR_TIME,
    BRIDGE_WAIT_FOR_AZURE,
    BRIDGE_CONNECTED,

} BridgeState;


BridgeState Bridge_GetState();

bool Bridge__QueueEmpty();
data_buf_t * Bridge__DequeueData();
data_buf_t * Bridge__PeekData();

extern uint32_t BridgeReportRateSeconds;

data_buf_t * BridgeMakeJSON(uint16_t ReportCounter, cJSON *Obj);

void Bridge_ConnectNotify();

void Bridge__EnqueueData(data_buf_t *DB);

void Bridge_SyncNow();

void Bridge_RebootNotify();

void Bridge_UpdateErrorNotify(int32_t ErrorCode);

void Bridge_UpdateCompleteNotify();

uint32_t Bridge_SyncCount();

uint64_t Bridge_LastSync();

void ReportCompleteCallBack(void *DataBuffer);

void Bridge_UpdateProgressNotify(int32_t Progress);

#endif
