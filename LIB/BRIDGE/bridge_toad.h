
#include "data_buf.h"
#include <cJSON.h>
#include <cJSON_os.h>

#ifndef BRIDGE_H
#define BRIDGE_H

void toad_bridge_init();

extern uint32_t BridgeReportRateSeconds;

data_buf_t * BridgeMakeJSON(uint16_t ReportCounter, cJSON *Obj);

void Bridge__EnqueueData(data_buf_t *DB);

void Bridge_SyncNow();

uint32_t Bridge_SyncCount();

uint64_t Bridge_LastSync();

void ReportCompleteCallBack(void *DataBuffer);


#endif
