#include <zephyr.h>
#include <stdio.h>
#include <stdlib.h>
#include <logging/log.h>
#include <cJSON.h>
#include <cJSON_os.h>
#include "Cell.h"
#include "Epoch.h"
#include "System.h"
#include "data_buf.h"
#include "bridge.h"
#include <shell/shell.h>

#include "krusty_crypto.h"
#include "krusty_cmd.h"
#include "krusty.h"
#include "rt_config.h"
#include "BridgeDataStructure.h"

#include "Beep.h"
#include "fota.h"
#include "System.h"
//#include "rt_config.h"

#include "FIRMWARE_VERSION.h"


int32_t beep_handler(cJSON *Result, int argc, char **argv)
{
    cJSON_AddStringToObject(Result,"Result","OK");

    cJSON_AddStringToObject(Result,"Message","Beeping Now");

   // Mario11();

    return 0;
}

int32_t update_handler(cJSON *Result, int argc, char **argv)
{

    cJSON_AddStringToObject(Result,"Result","OK");
    
    
    if(argc == 1)
    {
        UpdateFromDefault();
    }
    else
    {
        UpdateFromPath(argv[1]);
    }
    
    return 0;

}

int32_t reboot_handler(cJSON *Result, int argc, char **argv)
{

     cJSON_AddStringToObject(Result,"Result","OK");

    System__FlagReboot();
    return 0;

}



int32_t bridge_get_handler(cJSON *Result, int argc, char **argv)
{
    if(argc < 2)
    {
        cJSON_AddStringToObject(Result,"Result","Bad arguments");
    }
    else
    {

        struct rt_config_item * ci = NULL;

       char ValueString[CONFIG_MAX_VALUE_STRING_LENGTH];
       ci = rt_config_get_config_item(argv[1]);

        if(ci == NULL)
        {
               cJSON_AddStringToObject(Result, "Result","Parameter Not Found");
        }
        else
        {
               cJSON_AddStringToObject(Result,"Result","OK");
               rt_config_get_value_string(ci,ValueString,CONFIG_MAX_VALUE_STRING_LENGTH);
               cJSON_AddStringToObject(Result,"Name",ci->ConfigItemName);
               cJSON_AddStringToObject(Result,"Value",ValueString);
        }
    }
   
    return 0;

}



int32_t bridge_set_handler(cJSON *Result, int argc, char **argv)
{
    if(argc != 3)
    {
        cJSON_AddStringToObject(Result,"Result","Bad arguments");
    }
    else
    {

        struct rt_config_item * ci = NULL;

        ci = rt_config_get_config_item(argv[1]);

        if(ci == NULL)
        {
               cJSON_AddStringToObject(Result, "Result","Parameter Not Found");
        }
        else
        {
               rt_config_load_with_value(ci,argv[2]);

               bridge_get_handler(Result,argc,argv);
        }
    }
   
    return 0;

}


int32_t bridge_save_handler(cJSON *Result, int argc, char **argv)
{
    rt_config_export();
    cJSON_AddStringToObject(Result,"Result","OK");
    return 0;

}

KRUSTY_CMD(beep,beep_handler);
KRUSTY_CMD(update,update_handler);
KRUSTY_CMD(reboot,reboot_handler);
KRUSTY_CMD(get,bridge_get_handler);
KRUSTY_CMD(set,bridge_set_handler);
KRUSTY_CMD(save,bridge_save_handler);


void Bridge_RebootNotify()
{

    cJSON *Result = cJSON_CreateObject();

    cJSON_AddStringToObject(Result, "ObjID","Reboot");
 
    cJSON_AddStringToObject(Result, "Result","OK");

    cJSON_AddStringToObject(Result, "Message","Rebooting in 10 seconds");
 
    krusty__enqueue_tx_data(BridgeMakeJSON(0, Result));
    
    cJSON_Delete(Result);
}

char * GetBirthRecord()
{
        cJSON *Result = cJSON_CreateObject();

		char RSSI_Temp[32];

		sprintf(RSSI_Temp,"%idbm",Cell_GetLastPostedRSSI());
 
                        
        cJSON_AddStringToObject(Result, "ObjID", "BirthRecord");

        cJSON_AddStringToObject(Result, "FirmwareVersion",FIRMWARE_VERSION_STRING);
          
        char *string = NULL;
        string = cJSON_Print(Result);
   
        cJSON_Delete(Result);


		return string;
}


static int br_cmd_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

		char * result = GetBirthRecord();
        
		shell_print(shell,"\x02%s\x03",result);
		
		cJSON_free(result);

        return 0;
}


SHELL_CMD_REGISTER(br, NULL, "Get bridge birth record", br_cmd_handler);