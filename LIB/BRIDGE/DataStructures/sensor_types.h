#ifndef _SENSOR_TYPES
#define _SENSOR_TYPES
typedef enum
{
    VAL_UNKOWN    = 0xFFFF,
    VAL_HYDRO     = 0x8000,
    VAL_TEMP_RTD  = 0x8100,
    VAL_TOAD_PSD_1024  = 0x9000,
    VAL_TOAD_JACKS  = 0x9010,
    VAL_BULLET_BILL_TEMP_ACCEL  = 0x9020
} SensorDataID;

typedef struct
{
    uint16_t SensorType;    //VAL_TOAD_PSD_1024 for single vib PSD
    uint16_t JackID;
    uint32_t SampleRateHz;
    uint16_t BinSpaceHz;
    uint16_t StartBinHz;
    int8_t dB_G_Bins[1024]; 
}    __attribute__ ((packed)) Sensor__PSD_1024;

typedef struct
{
    uint16_t SensorType;     //VAL_TOAD_JACKS 
    int16_t AccelTemp[4];   //Temperature in C scaled by 100 
    uint16_t AccelGRMS[4];  //Acceleration GRMS scaled by 1000
    uint16_t FourTwenty[4];  //mA Current Scaled by 1000
    
}    __attribute__ ((packed)) ToadJacks;

#endif