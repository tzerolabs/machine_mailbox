#include "data_buf.h"
#include "sensor_types.h"

#ifndef BRIDGE_DATA_STRUCTURES_H
#define BRIDGE_DATA_STRUCTURES_H

/*

TZero Bridge Data structures

Data from the bridge to "the cloud" is pushed in the form of bridge reports. 
Bridge reports are variable length binary structures using little-endian datapacking.

    Generic Bridge Report structure

    
    All bridge reports have a fixed 32-byte Header followed by data whos elength is specified in the header.

        +--------+
        | Bridge |
        | Report |
        | Header |
        +--------+
        | Report |
        |  Data  |
        +--------+


    The header contains meta data about the bridge, a timestamp,  a report specific field
    and a length of the data the follows.

    typedef struct 
    {
        uint32_t ReportType;
        uint32_t SerialNumber;
        uint32_t SerialNumberExtention;
        uint64_t TimeStamp;         
        uint16_t ReportSpecificField;
        uint16_t ReportLength;
    } __attribute__ ((packed)) BridgeReportHeader;

Each Bridge report begins with 32-byte report type field.   The ReportType indicates the
structure of the data following the header

The SerialNumber and SerialNumberExtention are used to uniquely identify a bridge.

The SerialNumberExtention is used as a product code and has a special interpretation.

The upper two byte are to be intepreted as 2 ASCII characters 
and the lower 2 bytes are a LE 16-bit integer whose range is 0000 to 9999.  

For example a SerialNumberExtention of 

0x53420064

would be interpreted as  SB0100

The SerialNumber is a 32-bit number. 

The TimeStamp is seconds since the unix epoch

ReportSpecificField usage is defined but the report type.  It has metadata spefific to the report type

The ReportLength specifies the length data to follow.  Reports are a theorectical max of 65k but will be generally 
limited to 1 MTU and kept typically kept small for the cellular system.

*/


/*
    Report Type Enumerations
*/
typedef enum 
{
    BRIDGE_JACK_REPORT   = 0x6B63,
    BRIDGE_STATUS_REPORT = 0x7461,
    BRIDGE_JSON_REPORT = 0x6E6F,
    BRIDGE_TOAD_REPORT = 0x70AD,
    BRIDGE_MODBUS_REPORT = 0x626B,
  
        
} BridgeReportObjType;


/*
    Bridge Jack Report

    This report is used to report generic sensor data via the Bridge-Jack model.   

    In this model,  sensors are attached to a "jack".  The bridge will query sensor data and associate a generic
    blob of sensor data to this jack.  The bridge will periodically transmit the data associated with each jack via a jack report.
    The bridge does not decode the actual sensor data it reads into the jack. Jack data is compiled and sent as a report to the cloud backend.

    The jack reports are decoded in the backend for further processing.  Associate between Bridges/Jacks/Assets are done in the backend.   
    The Bridge-Jack model was developed such that we could send hardware without knowing an exact configuration.   The bridge just
    gets jack data to a cache in the cloud where it can be assigned a function in the backend.

    The ReportSpecificField in the Header will be a bit counter that increments every time a report is sent.

*/


typedef struct 
{       
        uint8_t  Flags;
        int8_t   RSSI;
        uint16_t ReportType;
        uint32_t SerialNumber;
        uint32_t SerialNumberExtention;   
        uint64_t TimeStamp;  //Number of seconds since unix epoch
        uint8_t  RetryCounter;
        uint8_t  ReportCounter; //This will increment everytime a report is sent
        uint16_t ReportSize; //Number of bytes in all of the jack reports following the header  
} __attribute__ ((packed)) BridgeReportHeader;


/*
    Limit to 8-bit unsigned
*/

#define     JACK_STATE_NO_SENSOR          (0x00)
#define     JACK_STATE_OK                 (0x01)
#define     JACK_STATE_FAULT_OVERCURRENT  (0x80)
#define     JACK_STATE_FAULT_CRC_FAIL     (0x81)
#define     JACK_STATE_FAULT_BASE64_ERROR (0x82)
#define     JACK_STATE_FAULT_LENGTH_ERROR (0x83)
#define     JACK_STATE_FAULT_NO_BINARY    (0x84)


#define   JACK_REPORT_TYPE_FIXED   'F'
#define   JACK_REPORT_TYPE_MESH    'M'
#define   JACK_REPORT_TYPE_MODBUS  'T'



/*
    Limit to 16-bit unsigned
*/
typedef enum 
{
    INFO = 0x00,
    WARNING = 0x01,
    ERROR = 0x02
} BridgeStatusCodes;


typedef struct 
{
    uint32_t ReportType;
    uint32_t SerialNumber;
    uint32_t SerialNumberExtention;
    uint64_t TimeStamp;
    uint8_t RetryCounter;
    uint8_t BridgeStatusCode;
    uint16_t StatusMessageLength;
} __attribute__ ((packed)) StatusReportHeader;


typedef struct
{
    uint8_t JackReportType; 
    uint8_t JackState; //0x00 means no sensor present, 0x01 == Jack OK, 0x80 == Jack fault - Over current. 
    int8_t JackRSSI;
    uint8_t  RSV; //Reserved for future use
    uint32_t JackID;
    uint16_t Length; //Number Bytes in the Datafield
} __attribute__ ((packed)) SingleJackReportHeader;

typedef struct
{
    BridgeReportHeader  JRH;
    Sensor__PSD_1024 PSD_Data;
} __attribute__ ((packed))FixedPSD_Report ;

typedef struct
{
    BridgeReportHeader  JRH;
    ToadJacks ToadData;
} __attribute__ ((packed))FixedToadJack_Report ;

/*
    We are going to make a fixed size struct to handle 10 byte hydro reports and a 4 channel;
    group.
*/


typedef struct
{
    uint8_t JackReportType; 
    uint8_t JackState;
    int8_t JackRSSI;
    uint8_t  RSV; 
    uint32_t JackID;
    uint16_t Length; 
    uint8_t Data[10]; //Hydroscope Data is always 10 bytes (8 data ples 2 byte type)
} __attribute__ ((packed)) FixedHydroSingleJack ;

typedef struct
{
    BridgeReportHeader  JRH;
    FixedHydroSingleJack SJRH[4];
} __attribute__ ((packed))Fixed4ChannelHydroJackReport ;







#endif
