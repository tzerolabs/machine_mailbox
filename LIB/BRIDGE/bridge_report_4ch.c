
#include <zephyr.h>
#include "stdbool.h"
#include "System.h"
#include <logging/log.h>
#include "bridge.h"
#include "DataBuffer.h"
#include "BridgeDataStructure.h"
#include "DataBuffer.h"
#include "Modbus.h"
#include "DeviceProvision.h"
#include "Cell.h"
#include <tinycrypt/hmac.h>
#include <tinycrypt/sha256.h>
#include <tinycrypt/constants.h>
#include "rt_config.h"
#include "Epoch.h"
#include <cJSON.h>
#include <cJSON_os.h>
#include <task_wdt/task_wdt.h>
#include "FIRMWARE_VERSION.h"
#include "stdio.h"
#include "FOTA.h"
#include <shell/shell.h>
#include "MachineMailboxIO.h"
#include "jack.h"
#include "thread_wdt.h"

LOG_MODULE_DECLARE(bridge);

bool BridgeReport4CH()
{
    data_buf_t * DB = Jack__MakeReport();

    if(DB!=NULL)
    {
        LOG_DBG("Enqueuing Jack Report");
                
        Bridge__EnqueueData(DB);
         return true;
    }
    else
    {
        LOG_ERR("Could not make jack report!");
        return false;
    }
   
}
