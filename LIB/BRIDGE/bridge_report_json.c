
#include <zephyr.h>
#include "stdio.h"
#include "stdbool.h"
#include "System.h"
#include <logging/log.h>
#include "bridge.h"
#include "BridgeDataStructure.h"
#include "data_buf.h"
#include "DeviceProvision.h"
#include "Epoch.h"
#include <cJSON.h>
#include <cJSON_os.h>
#include "Cell.h"

LOG_MODULE_DECLARE(bridge);

data_buf_t * BridgeMakeJSON(uint16_t ReportCounter, cJSON *Obj)
{
    data_buf_t *DB;

    if(Obj == NULL)
    {
        LOG_ERR("JSON object is null,  cannot make bridge report");
        return NULL;
    }

    char * JSON_Print = cJSON_PrintUnformatted(Obj);
   
    uint32_t JSON_Len = strlen(JSON_Print);

    
    DB = DataBufferCreate(JSON_Len + sizeof(BridgeReportHeader),NULL);
    
    if(DB == NULL)
    {
        LOG_ERR("DataBuffer is Null,  cannot make bridge report");
        goto j_cleanup;
    }

    BridgeReportHeader * JRH = (BridgeReportHeader *)DB->data;

    JRH->ReportType = BRIDGE_JSON_REPORT;
    
    JRH->Flags = 0;
    JRH->RSSI = (int8_t)Cell_GetRSSI();
   
    JRH->SerialNumber = Provision__GetDeviceSerialNumber();
    JRH->SerialNumberExtention = Provision__GetDeviceSerialNumberExtention();
    JRH->ReportCounter = ReportCounter;
    JRH->TimeStamp = Epoch__Get64();
    JRH->ReportSize = JSON_Len;
    
    DB->current_length += sizeof(BridgeReportHeader);

    memcpy(&DB->data[sizeof(BridgeReportHeader)] , JSON_Print, JSON_Len);
       
  
    DB->current_length += JSON_Len;


j_cleanup:

    cJSON_free(JSON_Print);

    return DB;


}