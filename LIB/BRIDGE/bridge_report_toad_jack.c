
#include <zephyr.h>
#include "stdbool.h"
#include "stdio.h"
#include <stdlib.h>
#include "System.h"
#include <logging/log.h>
#include "bridge.h"
#include "data_buf.h"
#include "BridgeDataStructure.h"
#include "DeviceProvision.h"
#include "Epoch.h"
#include "jack.h"
#include "Cell.h"
#include "toad_io.h"
#include "krusty.h"

LOG_MODULE_DECLARE(bridge);

static uint8_t Toad_ReportCounter = 0;

FixedToadJack_Report * ToadReport;

bool BridgeReportToadJack()
{
    data_buf_t * DB =DataBufferCreate(sizeof(FixedToadJack_Report),0);
    
    if(DB!=NULL)
    {

        LOG_INF("Sending Toad report");

        ToadReport = (FixedToadJack_Report *)(DB->data);


        ToadReport->JRH.Flags = 0;
        ToadReport->JRH.RSSI = (int8_t)Cell_GetRSSI();
        
        ToadReport->JRH.ReportType = BRIDGE_TOAD_REPORT;
        ToadReport->JRH.TimeStamp = Epoch__Get64();
        ToadReport->JRH.SerialNumber = Provision__GetDeviceSerialNumber();
        ToadReport->JRH.SerialNumberExtention = Provision__GetDeviceSerialNumberExtention();
        ToadReport->JRH.ReportCounter = Toad_ReportCounter++;
        ToadReport->JRH.RetryCounter = 0;
        ToadReport->JRH.ReportSize =  sizeof(ToadJacks);

        ToadReport->ToadData.SensorType = VAL_TOAD_JACKS;
  
        ToadReport->ToadData.AccelTemp[0] = Toad_GetTempChannel(0);
        ToadReport->ToadData.AccelTemp[1] = Toad_GetTempChannel(1);
        ToadReport->ToadData.AccelTemp[2] = Toad_GetTempChannel(2);
        ToadReport->ToadData.AccelTemp[3] = Toad_GetTempChannel(3);
 
        ToadReport->ToadData.AccelGRMS[0] = Toad_GetGRMS_Channel(0);
        ToadReport->ToadData.AccelGRMS[1] = Toad_GetGRMS_Channel(1);
        ToadReport->ToadData.AccelGRMS[2] = Toad_GetGRMS_Channel(2);
        ToadReport->ToadData.AccelGRMS[3] = Toad_GetGRMS_Channel(3);

        ToadReport->ToadData.FourTwenty[0] = Toad_GetFourToTwentyChannel(0);
        ToadReport->ToadData.FourTwenty[1] = Toad_GetFourToTwentyChannel(1);
        ToadReport->ToadData.FourTwenty[2] = Toad_GetFourToTwentyChannel(2);
        ToadReport->ToadData.FourTwenty[3] = Toad_GetFourToTwentyChannel(3);

        DB->current_length = sizeof(FixedToadJack_Report);

        krusty__enqueue_tx_data(DB);

        return true;
    }
    else
    {
        
        LOG_ERR("Could not allocate %d bytes for Toad report",sizeof(FixedPSD_Report));
        return false;
    }
   
}
