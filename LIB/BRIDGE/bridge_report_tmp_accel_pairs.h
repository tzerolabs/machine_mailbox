
#include "stdint.h"

#ifndef _BRIDGE_REPORT_TEMP_ACCEL_H
#define _BRIDGE_REPORT_TEMP_ACCEL_H


bool BridgeReportTempAccel(uint64_t TimeStamp,
                           int16_t * Temps,
                           uint16_t *Accels,
                           uint16_t Cnt,
                           uint16_t SecondsBetweenSamples);

#endif