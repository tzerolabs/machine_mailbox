
#include <zephyr.h>
#include <logging/log.h>
#include "stdint.h"
#include "Jack.h"
#include "BridgeDataStructure.h"
#include <init.h>
#include "DataBuffer.h"
#include "DeviceProvision.h"
#include "epoch.h"
#include "Cell.h"

LOG_MODULE_REGISTER(jack, LOG_LEVEL_INF);

struct k_mutex jack_mutex;

typedef struct 
{
    uint8_t JackBuffer[CONFIG_MAX_JACK_SIZE];
    uint16_t JackDataLength;
    uint8_t JackState;
   
} Jack;

#define NUM_JACKS DT_PROP(DT_NODELABEL(machine_mailbox), num_jacks)


Jack MyJacks[NUM_JACKS];

static int Jack__Init()
{
    for(int i=0; i < NUM_JACKS;i++)
    {
        k_mutex_init(&jack_mutex);

        memset(&MyJacks[i],
               0,
               sizeof(Jack)
               );
    }

    LOG_INF("%d jacks initialized",(int)NUM_JACKS);

    return 0;
}

SYS_INIT(Jack__Init, APPLICATION, 10);


int32_t Jack__GetCount()
{

    return NUM_JACKS;

}

uint16_t Jack__GetBytesAtJack(uint32_t JackNumber)
{
    if(JackNumber < NUM_JACKS)
    {
       return MyJacks[JackNumber].JackDataLength;
    }

    return 0;
}

int32_t Jack__GetData(uint32_t JackNumber,
                      uint8_t *DataBuffer,
                      uint16_t DataBufferLen,
                      uint16_t *BytesCopied
                      )
{
    if(JackNumber < NUM_JACKS)
    {
        if(DataBufferLen < MyJacks[JackNumber].JackDataLength)
        {
            return -ENOMEM;
        }    

        if(DataBuffer == NULL)
        {
            return -EINVAL;
        }

        if (k_mutex_lock(&jack_mutex, K_MSEC(500)) != 0)
        {
            LOG_WRN("Could not take mutex on jack while trying to getting data");
            return -EBUSY;
        }

        memcpy(DataBuffer,
               MyJacks[JackNumber].JackBuffer,
               MyJacks[JackNumber].JackDataLength
              );

        if(BytesCopied)
        {
            *BytesCopied = MyJacks[JackNumber].JackDataLength;
        }

        k_mutex_unlock(&jack_mutex);

        return MyJacks[JackNumber].JackDataLength;
    }

    return -EINVAL;

}


data_buf_t * Jack__MakeReport()
{
    static uint16_t RptCntr = 0;

    data_buf_t *DB = NULL;

        if (k_mutex_lock(&jack_mutex, K_MSEC(1000)) != 0)
        {
            LOG_WRN("Could not take mutex on jack while trying to make report");
            return DB;
        }

        uint32_t TotalReportLength = sizeof(BridgeReportHeader) +
                                     (NUM_JACKS * sizeof(SingleJackReportHeader));

        
        for(int i = 0; i<NUM_JACKS; i++)
        {
            TotalReportLength += MyJacks[i].JackDataLength;
        }                          

        DB = DataBufferCreate(TotalReportLength,NULL);

        

        if(DB == NULL)
        {
            LOG_WRN("Could not allocate %d bytes for jack report",TotalReportLength);
            goto cleanup;
        }

        memset(DB->Data,0,DB->max_length);

        BridgeReportHeader * BJRH = (BridgeReportHeader *)(DB->Data);

        BJRH->ReportType = BRIDGE_JACK_REPORT;
        BJRH->Flags = Epoch__Get64();
        BJRH->RSSI = (int8_t)Cell_GetRSSI();
        BJRH->TimeStamp = Epoch__Get64();
        BJRH->SerialNumber = Provision__GetDeviceSerialNumber();
        BJRH->SerialNumberExtention = Provision__GetDeviceSerialNumberExtention();
        BJRH->ReportCounter = RptCntr++;
        BJRH->RetryCounter = 0;
        BJRH->ReportSize = TotalReportLength - sizeof(BridgeReportHeader);

        DB->current_length = sizeof(BridgeReportHeader);
        
        SingleJackReportHeader * SJRH;
   
        for(int i=0;i<NUM_JACKS;i++)
        {
            SJRH = (SingleJackReportHeader *)(&DB->Data[DB->current_length]);
    
            SJRH->JackReportType = JACK_REPORT_TYPE_FIXED;
            SJRH->JackID = i+1;
            SJRH->JackRSSI = Cell_GetLastPostedRSSI();
            SJRH->JackState = MyJacks->JackState;
            SJRH->RSV = 0;
            SJRH->Length = MyJacks[i].JackDataLength;

            DB->current_length += sizeof(SingleJackReportHeader);

            memcpy(&DB->Data[DB->current_length],
                   &MyJacks[i].JackBuffer[0],
                   MyJacks[i].JackDataLength);
              
            DB->current_length += MyJacks[i].JackDataLength;
        }

        cleanup:


        k_mutex_unlock(&jack_mutex);


    return DB;


}





int32_t Jack__PushInData(uint32_t JackNumber,
                         uint8_t *DataBuffer,
                         uint16_t DataBufferLen
                         )
{

   if(JackNumber < NUM_JACKS)
    {
        if(DataBufferLen > CONFIG_MAX_JACK_SIZE)
        {
            return -ENOMEM;
        }    

        if (k_mutex_lock(&jack_mutex, K_MSEC(500)) != 0)
        {
            LOG_WRN("Could not take mutex on jack while trying to push data");
            return -EBUSY;
        }

        memcpy(MyJacks[JackNumber].JackBuffer,
               DataBuffer,
               DataBufferLen
              );

        MyJacks[JackNumber].JackDataLength = DataBufferLen;

        MyJacks[JackNumber].JackState = JACK_STATE_OK;

        k_mutex_unlock(&jack_mutex);

        return DataBufferLen;

    }

    return -EINVAL;

}


int32_t Jack__SetState(uint32_t JackNumber, uint8_t JackState)
{
   if(JackNumber < NUM_JACKS)
   {

        if (k_mutex_lock(&jack_mutex, K_MSEC(500)) != 0)
        {
            LOG_WRN("Could not take mutex on jack while trying to set state");
            return -EBUSY;
        }

        MyJacks[JackNumber].JackState = JackState;
    
        if(JackState!=JACK_STATE_OK)
        {
             memset(MyJacks[JackNumber].JackBuffer,0,10);
        }

        MyJacks[JackNumber].JackDataLength = 10;

        k_mutex_unlock(&jack_mutex);

        return JackState;
   }

    return -EINVAL;
}

uint8_t Jack__GetState(uint32_t JackNumber)
{
   if(JackNumber < NUM_JACKS)
   {
       return MyJacks[JackNumber].JackState;
   }

   return JACK_STATE_NO_SENSOR;

}
