#ifndef _BRIDGE_REPORT_JSON_H
#define _BRIDGE_REPORT_JSON_H

data_buf_t * BridgeMakeJSON(uint16_t ReportCounter, cJSON *Obj);

#endif