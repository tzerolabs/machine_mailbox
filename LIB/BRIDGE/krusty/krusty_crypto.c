#include <stdio.h>
#include <time.h>
#include <string.h>
#include <tinycrypt/constants.h>
#include "krusty_hkdf.h"
#include <tinycrypt/aes.h>
#include <tinycrypt/ccm_mode.h>
#include "krusty_crypto.h"

#include <sys/base64.h>

// STRUCTS/PROTOTYPES (move to header?)

// IMPLEMENTATION

#define SALT_DATA_BASE64_W_CRLF "Vcx4HRWcXn8vYBvz8Dqz10VDKlGumbmHlbjFGbEQ4mI=""\r\n"
#define UP_INFO "up^^-KeY"
#define DOWN_INFO "D0wn-k3y"

int bc_generate_keys(
    struct bc_bridge_keys_struct *bridge_keys,
    const uint8_t *input_key, const int input_key_length
) {

    uint8_t aes_key[16];

    //add a new line for the base64 decode
    
    char salt[128];

    memset(salt,0,sizeof(salt));
    
    int32_t OutputLength = 0;

    base64_decode(salt, sizeof(salt), &OutputLength,SALT_DATA_BASE64_W_CRLF,strlen(SALT_DATA_BASE64_W_CRLF));
   
    int ret = tc_hkdf_extract(input_key, input_key_length, salt, OutputLength, bridge_keys->intermediate_key);
   
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    // expand UP key
    const char up_info[] = UP_INFO;
    ret = tc_hkdf_expand(bridge_keys->intermediate_key, (const void *)up_info, strlen(up_info), 16, aes_key);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }
    ret = tc_aes128_set_encrypt_key(&bridge_keys->up_key, aes_key);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    // expand DOWN key
    const char down_info[] = DOWN_INFO;
    ret = tc_hkdf_expand(bridge_keys->intermediate_key, (const void *)down_info, strlen(down_info), 16, aes_key);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }
    ret = tc_aes128_set_encrypt_key(&bridge_keys->down_key, aes_key);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    return TC_CRYPTO_SUCCESS;
}

int bc_encrypt_data(
    struct bc_bridge_keys_struct *bridge_keys,
    uint8_t *out, unsigned int olen,
    const uint8_t *associated_data, unsigned int alen, // for our purposes, the entire plaintext header
    const uint8_t *payload, unsigned int plen
) {
    uint8_t nonce[13];

    if (olen != plen + MAC_LENGTH) {
        return TC_CRYPTO_FAIL;
    }

    int ret = tc_hkdf_expand(bridge_keys->intermediate_key, associated_data, alen, 13, nonce);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

#ifdef KEY_TEST
    printf("nonce: ");
    for (unsigned int i = 0; i <  13; ++i) {
        printf("%02x", nonce[i]);
    }
    printf("\n");
#endif

    struct tc_ccm_mode_struct ccm_config;
    ret = tc_ccm_config(&ccm_config, &bridge_keys->up_key, nonce, 13, MAC_LENGTH);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    ret = tc_ccm_generation_encryption(out, olen, associated_data, alen, payload, plen, &ccm_config);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    return TC_CRYPTO_SUCCESS;
}

int bc_decrypt_data(
    struct bc_bridge_keys_struct *bridge_keys,
    uint8_t *out, unsigned int olen,
    const uint8_t *associated_data, unsigned int alen, 
    const uint8_t *payload, unsigned int plen
) {
    uint8_t nonce[13];

    if (olen != plen - MAC_LENGTH) {
        return TC_CRYPTO_FAIL;
    }

    int ret = tc_hkdf_expand(bridge_keys->intermediate_key, associated_data, alen, 13, nonce);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    struct tc_ccm_mode_struct ccm_config;
    ret = tc_ccm_config(&ccm_config, &bridge_keys->down_key, nonce, 13, MAC_LENGTH);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    ret = tc_ccm_decryption_verification(out, olen, associated_data, alen, payload, plen, &ccm_config);
    if (ret != TC_CRYPTO_SUCCESS) {
        return ret;
    }

    return TC_CRYPTO_SUCCESS;}

// TESTING

#ifdef KEY_TEST

int main() {
    // source key material and test payload
    const uint8_t ikm[32] = {
        0xb0, 0x34, 0x4c, 0x61, 0xd8, 0xdb, 0x38, 0x53, 0x5c, 0xa8, 0xaf, 0xce,
        0xaf, 0x0b, 0xf1, 0x2b, 0x88, 0x1d, 0xc2, 0x00, 0xc9, 0x83, 0x3d, 0xa7,
        0x26, 0xe9, 0x37, 0x6c, 0x2e, 0x32, 0xcf, 0xf7
    };
    #define DATA_LENGTH 32

    const time_t seconds = time(NULL);
    int ret;
      
    printf("Seconds since January 1, 1970 = %ld\n", seconds);
    printf("digest size: %d\n", TC_SHA256_DIGEST_SIZE);
    printf("time size: %ld\n", sizeof(time_t));

    struct bc_bridge_keys_struct bk;
    ret = bc_generate_keys(&bk, ikm, DATA_LENGTH);
    printf("generate keys success = %d\n", ret == TC_CRYPTO_SUCCESS);

    uint8_t encrypt_out[DATA_LENGTH + MAC_LENGTH];
    ret = bc_encrypt_data(&bk, encrypt_out, DATA_LENGTH + MAC_LENGTH, (const uint8_t *)&seconds, sizeof(time_t), ikm, DATA_LENGTH);
    printf("ccm generation encryption success = %d\n", ret == TC_CRYPTO_SUCCESS);
    for (unsigned int i = 0; i <  DATA_LENGTH + MAC_LENGTH; ++i) {
        printf("%02x", encrypt_out[i]);
    }
    printf("\n");

    uint8_t decrypt_out[DATA_LENGTH];
    //out[0] = 0;
    ret = bc_decrypt_data(&bk, decrypt_out, DATA_LENGTH, (const void *)&seconds, sizeof(time_t), encrypt_out, DATA_LENGTH + MAC_LENGTH);
    printf("ccm decryption verification success = %d\n", ret == TC_CRYPTO_SUCCESS);
    for (unsigned int i = 0; i <  DATA_LENGTH; ++i) {
        printf("%02x", decrypt_out[i]);
    }
    printf("\n");
    printf("memcmp input and output: %d\n", memcmp(ikm, decrypt_out, DATA_LENGTH));
}

#endif