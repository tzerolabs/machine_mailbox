#include "data_buf.h"

#ifndef _KRUSTY_H
#define _KRUSTY_H

typedef void (*krusty_on_connect_notify_t)() ; 

void krusty__register_on_connect_notify(krusty_on_connect_notify_t kocn);

void krusty__init();

bool krusty__is_transmitting();

uint32_t krusty__messages_transmitted();

bool krusty__is_connected();

bool krusty__is_in_retry();

bool krusty__tx_queue_empty();

void krusty__enqueue_tx_data(data_buf_t *DB);

void krusty__connect_notify();

void krusty__park();

void krusty__go();

void krusty_drain_tx_queue();

#endif
