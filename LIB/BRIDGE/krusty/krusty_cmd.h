#ifndef _BRIDGE_CMD
#define _BRIDGE_CMD

#include <zephyr.h>
#include "stdint.h"
#include "cJSON.h"

void krusty__cmd_feed(void *Header, uint32_t HeaderLength,uint8_t *Data,  uint32_t Len);

typedef int32_t (*krusty_cmd_handler)(cJSON *Result, int argc, char **argv);

struct krusty_cmd_item
{
	char * command;
    krusty_cmd_handler callback;

};

#define Z_BRIDGE_CMD_INITIALIZER(command_name,command_callback) \
	{ \
	.command = Z_STRINGIFY(command_name), \
	.callback = command_callback,\
    }


#define KRUSTY_CMD(command_name,command_callback) \
	STRUCT_SECTION_ITERABLE(krusty_cmd_item, command_name) = \
		Z_BRIDGE_CMD_INITIALIZER(command_name,command_callback)



#endif