#ifndef _BRIDGE_CRYPTO_H
#define _BRIDGE_CRYPTO_H

#include <tinycrypt/constants.h>
#include "krusty_hkdf.h"
#include <tinycrypt/aes.h>
#include <tinycrypt/ccm_mode.h>

// mac length can be 4, 6, 8, 10, 12, 16 (>8 recommended)
#define MAC_LENGTH 16


struct bc_bridge_keys_struct {
    struct tc_aes_key_sched_struct up_key;
    struct tc_aes_key_sched_struct down_key;
    uint8_t intermediate_key[TC_SHA256_DIGEST_SIZE];
};

// initialize bridge keys struct from input key
int bc_generate_keys(
    struct bc_bridge_keys_struct *bridge_keys,
    const uint8_t *input_key, const int input_key_length
); 

// encrypts a message with a unique nonce/iv based on header data
int bc_encrypt_data(
    struct bc_bridge_keys_struct *bridge_keys,
    uint8_t *out, unsigned int olen,
    const uint8_t *associated_data, unsigned int alen, // for our purposes, the entire plaintext header
    const uint8_t *payload, unsigned int plen
);

// decrypts and verifies a message
int bc_decrypt_data(
    struct bc_bridge_keys_struct *bridge_keys,
    uint8_t *out, unsigned int olen,
    const uint8_t *associated_data, unsigned int alen, // for our purposes, the entire plaintext header
    const uint8_t *payload, unsigned int plen
);


#endif