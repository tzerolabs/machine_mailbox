#include <zephyr.h>
#include <stdio.h>
#include <stdlib.h>
#include <logging/log.h>
#include <fcntl.h>
#include <net/socket.h>
#include <shell/shell.h>

#include <cJSON.h>
#include <cJSON_os.h>

#include "BridgeDataStructure.h"
#include "DeviceProvision.h"
#include "krusty.h"
#include "krusty_crypto.h"
#include "krusty_cmd.h"

#include "Cell.h"
#include "epoch.h"
#include "System.h"
#include "data_buf.h"
#include "bridge.h"
#include "rt_config.h"

//#include "thread_wdt.h"

#include "FIRMWARE_VERSION.h"

LOG_MODULE_REGISTER(krusty,LOG_LEVEL_INF);

#define CONFIG_KRUSTY_THREAD_STACK_SIZE 4096
#define CONFIG_KRUSTY_MIN_MSG_SIZE    1
#define CONFIG_KRUSTY_ENCRYPT
#define CONFIG_KRUSTY_RETRY
#define CONFIG_KRUSTY_NUM_TX_ATTEMPTS 4
#ifdef CONFIG_KRUSTY_ENCRYPT


    #define UDP_PORT 2103
#else
    #define UDP_PORT 2102
#endif

K_THREAD_STACK_DEFINE(krusty_thread_stack_area, CONFIG_KRUSTY_THREAD_STACK_SIZE);

struct k_thread krusty_thread_data;

char krusty_host_string[96];

uint8_t krusty_decrypt_out[1500];

RT_CONFIG_ITEM(IngestHost,"Remote ingest Host",krusty_host_string,RT_CONFIG_DATA_TYPE_STRING,sizeof(krusty_host_string),"","","connect.machinemailbox.com");

typedef enum 
{
    KRUSTY_INIT = 0,
    KRUSTY_WAIT_CELL,
    KRUSTY_WAIT_FOR_TIME,
    KRUSTY_CONNECT,
    KRUSTY_PROCESS,
    KRUSTY_PARKED
} krusty_cloud_state_t;

static struct sockaddr_storage krusty_sockadd;

static krusty_cloud_state_t  krusty_cloud_state = KRUSTY_INIT;

static uint32_t krusty_tx_ack_timeout;

static uint32_t krusty_tx_attempts = 0;

static struct tc_hmac_state_struct  krusty_hmac_state;

static int krusty_client_fd = -1;

static uint32_t krusty_stats_messages_tx = 0;

static uint32_t krusty_stats_messages_retry = 0;

static uint32_t krusty_stats_messages_failed = 0;

static uint32_t krusty_stats_messages_rx = 0;

static uint32_t krusty_stats_messages_acked = 0;

static uint8_t krusty_rx_buf[1500];

static bool krusty_flag_reconnect = false;

static uint8_t krusty_tx_buf[1500];

static data_buf_t krusty_databuffer_tx;

static bool krusty_tx_ack;

static bool krusty_tx_in_progress;

static bool krusty_flag_attempt_tx;

static data_buf_t *krusty_next_to_send;

static void krusty__thread_work(void *a, void *b, void *c);

static struct k_fifo krusty_tx_fifo;

static bool krusty_request_park;

static bool krusty_request_go;

void krusty__park()
{
    if(krusty_request_park == true)
    {
            LOG_WRN("krusty request park flag alread set, ignoring");
    }
    else
    {
        if(krusty_cloud_state == KRUSTY_PARKED)
        {
             LOG_WRN("krusty is in parked state.  Ignoring request");
        }
        else
        {
           krusty_request_park = true;
        }
    }
}

void krusty__go()
{
    if(krusty_cloud_state == KRUSTY_PARKED)
    {
        krusty_request_park = false;
        krusty_request_go = true;
    }
    else
    {
        LOG_WRN("krusty is not it parked state.  Ignoring go request");
    }
}

void krusty__enqueue_tx_data(data_buf_t *DB)
{
    if(DB!=NULL)
    {
    k_fifo_put(&krusty_tx_fifo,DB);
    }
}

data_buf_t * krusty__dequeue_tx_data()
{
    return  k_fifo_get(&krusty_tx_fifo,K_NO_WAIT);
}

data_buf_t * krusty__peek_data()
{
    return  k_fifo_peek_head(&krusty_tx_fifo);
}

bool krusty__tx_queue_empty()
{
    return k_fifo_is_empty(&krusty_tx_fifo);
}

krusty_on_connect_notify_t krusty_on_connect_notify_handler;

void krusty__register_on_connect_notify(krusty_on_connect_notify_t kocn)
{
    krusty_on_connect_notify_handler = kocn;
}

void krusty__connect_notify()
{
    
    if(krusty_on_connect_notify_handler != NULL)
    {
        krusty_on_connect_notify_handler();
    }

}

void krusty__change_state(krusty_cloud_state_t NewState)
{
    switch(NewState)
    {
        default:
        case KRUSTY_INIT:
            krusty_cloud_state = NewState;
           LOG_INF("Initializing krusty");
        break;

        case KRUSTY_CONNECT:

            krusty_cloud_state = NewState;
            krusty_flag_reconnect = false;
  
            LOG_INF("Setting up socket....");
        break;

        case KRUSTY_WAIT_CELL:
            LOG_INF("Waiting for cellular connection ");
            krusty_cloud_state = NewState;
        break;

        case KRUSTY_WAIT_FOR_TIME:
             LOG_INF("Waiting until we have network time ");
             krusty_cloud_state = NewState;
        break;

        case KRUSTY_PROCESS:
      
            LOG_INF("Moving to main krusty process");
            krusty_cloud_state = NewState;
        
        break;

        case KRUSTY_PARKED:
      
            LOG_INF("Moving to krusty parked");
            krusty_cloud_state = NewState;
        break;

    }
}

void krusty__init()
{
    krusty_databuffer_tx.current_length = 0;
    krusty_databuffer_tx.max_length = sizeof(krusty_tx_buf);
    krusty_databuffer_tx.data = krusty_tx_buf;

    k_fifo_init(&krusty_tx_fifo);

    krusty__change_state(KRUSTY_INIT);

    k_thread_create(&krusty_thread_data, krusty_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(krusty_thread_stack_area),
                    krusty__thread_work,
                    NULL, NULL, NULL,
                    K_HIGHEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);

}

bool krusty__refresh_ip(struct sockaddr_in *socket_address)
{
    int err;
    bool IP_Found = false;

    struct addrinfo *result;
    struct addrinfo *addr;
    struct addrinfo hints =     {
                                    .ai_family = AF_INET,
                                    .ai_socktype = SOCK_STREAM
                                };
           
 
    LOG_INF("Getting IP for %s", log_strdup(krusty_host_string));

    err = getaddrinfo(krusty_host_string, NULL, &hints, &result);

    if (err)
    {
       LOG_ERR("getaddrinfo() failed, err %d\n", errno);
    }
    else
    {
        addr = result;
        
        while (addr != NULL)
        {
            /* IPv4 Address. */
            if (addr->ai_addrlen == sizeof(struct sockaddr_in))
            {
                                  
                char ipv4_addr[NET_IPV4_ADDR_LEN];
               
                socket_address->sin_addr.s_addr = ((struct sockaddr_in *)addr->ai_addr)->sin_addr.s_addr;
                
                socket_address->sin_family = AF_INET;
                
                socket_address->sin_port = htons(UDP_PORT);
                       
                inet_ntop(AF_INET,
                        &socket_address->sin_addr.s_addr,
                        ipv4_addr,
                        sizeof(ipv4_addr)
                        );
               
                LOG_INF("IPv4 Address found %s", log_strdup(ipv4_addr));
               
                IP_Found = true;
            } 
            else
            {
                LOG_ERR("ai_addrlen = %u should be %u or %u",
                    (unsigned int)addr->ai_addrlen,
                    (unsigned int)sizeof(struct sockaddr_in),
                    (unsigned int)sizeof(struct sockaddr_in6));
            }

            addr = addr->ai_next;
        }
        /* Free the address. */
        freeaddrinfo(result);
    }

    return IP_Found;

}

void  do_hmac(TCHmacState_t krusty_hmac_state,  const uint8_t *data,
		          size_t datalen, uint8_t * Digest)
{
        (void)tc_hmac_init(krusty_hmac_state);
        (void)tc_hmac_update(krusty_hmac_state, data, datalen);
        (void)tc_hmac_final(Digest, TC_SHA256_DIGEST_SIZE, krusty_hmac_state);

        return;
}

void process_encrypted_msg(uint8_t *AssociatedData, uint32_t AssociateDataLength, uint8_t *Data,uint32_t Len);

bool decrypt_incoming(uint8_t *Data, uint32_t Len, uint32_t KeyNum)
{
    struct bc_bridge_keys_struct bk;
    int ret;

    uint32_t KeyLen = 0;

    uint8_t *Key;

    Key = Provision__GetDeviceKeyBinary(&KeyLen,KeyNum);

    if(Key == NULL)
    {
        LOG_ERR("Could not get DeviceKey");
        return false;
    }   

    ret = bc_generate_keys(&bk, Key, KeyLen);


        #define IN_ASSOC_DATA_LENGTH  (sizeof(BridgeReportHeader))
        #define IN_ASSOC_DATA_POINTER  (&Data[0])
       
        #define IN_PAYLOAD_LENGTH     (Len - IN_ASSOC_DATA_LENGTH)
        #define IN_PAYLOAD_POINTER    (&Data[IN_ASSOC_DATA_LENGTH])

        #define DECRYPT_LEN     IN_PAYLOAD_LENGTH - MAC_LENGTH

        ret = bc_decrypt_data(&bk,
                            krusty_decrypt_out, DECRYPT_LEN,
                            IN_ASSOC_DATA_POINTER, IN_ASSOC_DATA_LENGTH,
                            IN_PAYLOAD_POINTER, IN_PAYLOAD_LENGTH
                            );

        if(ret == TC_CRYPTO_SUCCESS)
        {
            LOG_DBG("ccm decryption verification success = %d", ret == TC_CRYPTO_SUCCESS);
            process_encrypted_msg(IN_ASSOC_DATA_POINTER, IN_ASSOC_DATA_LENGTH, krusty_decrypt_out,DECRYPT_LEN);
        }
        else
        {
           LOG_DBG("ccm decryption verification success = %d", ret == TC_CRYPTO_SUCCESS);
        }
     
    return ret == TC_CRYPTO_SUCCESS;
}

bool Validate_incoming(uint8_t *Data, uint32_t Len, uint32_t KeyNum)
{
    if(Len < (TC_SHA256_DIGEST_SIZE + CONFIG_KRUSTY_MIN_MSG_SIZE))
        {
            LOG_ERR("Message too small");
            return false;
        }

        (void)memset(&krusty_hmac_state, 0x00, sizeof(krusty_hmac_state));
        uint32_t KeyLen;
        (void)tc_hmac_set_key(&krusty_hmac_state, Provision__GetDeviceKeyBinary(&KeyLen,KeyNum), KeyLen);
        
        uint8_t Digest[TC_SHA256_DIGEST_SIZE];

        do_hmac(&krusty_hmac_state,&Data[TC_SHA256_DIGEST_SIZE],Len-TC_SHA256_DIGEST_SIZE,&Digest[0]);

        if(memcmp(Digest,Data,TC_SHA256_DIGEST_SIZE) == 0)
        {
            return true;
        }
        else
        {
            LOG_ERR("Incoming message not authenticated");
        }

        return false;
}

void process_encrypted_msg(uint8_t *AssociatedData, uint32_t AssociateDataLength, uint8_t *Data,uint32_t Len)
{  
    if(AssociateDataLength == sizeof(BridgeReportHeader) && Len == 0)
    {
            if(krusty_tx_in_progress)
            {
                AssociatedData[0] = AssociatedData[0] ^ 1<<7;

                if(memcmp(krusty_tx_buf,AssociatedData,sizeof(BridgeReportHeader)) == 0)
                {
                    LOG_INF("Last UDP report Ack'd");
                    krusty_tx_ack = true;
                }
                else
                {
                    LOG_ERR("Header did not match when checking acknowledgement");
                }
            }
   }
   else
   {
     BridgeReportHeader * BJRH = (BridgeReportHeader *)AssociatedData; 
     
     if(BJRH->ReportType == BRIDGE_JSON_REPORT)
     {
            krusty__cmd_feed(AssociatedData, AssociateDataLength,Data,Len);
     }

   }
}

void process_unencrypted_msg(uint8_t *Data,uint32_t Len)
{
        if(Len == sizeof(BridgeReportHeader))
        {
            if(krusty_tx_in_progress)
            {
                
                Data[0] = Data[0] ^ 1<<7;

                if(memcmp(Data,&krusty_tx_buf[TC_SHA256_DIGEST_SIZE],sizeof(BridgeReportHeader)) == 0)
                {
                    LOG_INF("Last UDP report Ack'd");
                    krusty_tx_ack = true;
                }
                else
                {
                    LOG_ERR("Header did not match when checking acknowledgement");
                }
            }
        }
}

bool krusty_tx(data_buf_t * DB)
{
     BridgeReportHeader * B = (BridgeReportHeader *)(DB->data);

     LOG_INF("Retry Count Out %d",B->RetryCounter);
    
     int err = send(krusty_client_fd, 
                    DB->data,
                    DB->current_length, 
                    0);

    if (err < 0) 
    {
        LOG_ERR("Failed to transmit UDP packet, %d\n", errno);
       
        krusty_flag_reconnect = true;
        krusty_tx_in_progress = false;
       
        return false;
    }
    
    LOG_INF("Transmitted packet");
    
    krusty_stats_messages_tx++;
 
    #ifdef CONFIG_KRUSTY_RETRY
        krusty_tx_ack_timeout = System_Get_mS_Ticker();
        krusty_tx_in_progress = true;
        krusty_tx_ack = false;

    #else
        krusty_tx_in_progress = false;
        krusty_tx_ack = false;
    #endif        

    return true;
    
}

static uint8_t report_cnt = 0;

void krusty_generate_tx_packet(data_buf_t *In,  data_buf_t *Out, uint8_t RetryCounter)
{
    #ifdef CONFIG_KRUSTY_ENCRYPT

    struct bc_bridge_keys_struct bk;

    uint32_t KeyLen = 0;

    uint8_t *Key;

    Key = Provision__GetDeviceKeyBinary(&KeyLen,Provision__GetActiveDeviceKeyNum());

    LOG_INF("Tx with Key %d",Provision__GetActiveDeviceKeyNum());
    if(Key == NULL)
    {
        LOG_ERR("Could not get DeviceKey");
        return;
    }   

    int ret = bc_generate_keys(&bk, Key, KeyLen);
    
    LOG_DBG("generate keys success = %d", ret == TC_CRYPTO_SUCCESS);


    #define ASSOC_DATA_LENGTH  sizeof(BridgeReportHeader)
    #define ASSOC_DATA_POINTER  &In->data[0]

    #define PAYLOAD_LENGTH    (In->current_length - sizeof(BridgeReportHeader))
    #define PAYLOAD_POINTER    &In->data[sizeof(BridgeReportHeader)]
    
    //Patch the header with the RetryCounter
    BridgeReportHeader *B = (BridgeReportHeader *)ASSOC_DATA_POINTER;
    B->RetryCounter = RetryCounter;
    B->ReportCounter = ++report_cnt;

    ret = bc_encrypt_data(&bk,
                            &Out->data[sizeof(BridgeReportHeader)], 
                            PAYLOAD_LENGTH + MAC_LENGTH,
                            In->data,
                            sizeof(BridgeReportHeader),
                            PAYLOAD_POINTER,
                            PAYLOAD_LENGTH);
   
    memcpy(&(Out->data[0]),ASSOC_DATA_POINTER,ASSOC_DATA_LENGTH);

    Out->current_length = ASSOC_DATA_LENGTH + PAYLOAD_LENGTH + MAC_LENGTH;

/*
    LOG_DBG("ccm generation encryption success = %d", ret == TC_CRYPTO_SUCCESS);

    LOG_DBG("Associated Data Length = %d", ASSOC_DATA_LENGTH);
    LOG_DBG("Payload Length = %d", PAYLOAD_LENGTH);
    LOG_DBG("MAC Length = %d", MAC_LENGTH);
    LOG_DBG("Total Length = %d",  Out->current_length);
    LOG_HEXDUMP_DBG(Out->Data, Out->current_length,HexDumpBuff);
*/
    #else

        if((Out->max_length - krusty_next_to_send->current_length) < TC_SHA256_DIGEST_SIZE)
        {
            LOG_ERR("Not enough space for HMAC. Ignoring.");
        }
        else
        {

            //Patch the header with the RetryCounter
            BridgeReportHeader *B = (BridgeReportHeader *)(&In->Data[0]);
            B->RetryCounter = RetryCounter;


            Out->current_length = In->current_length;

            memcpy(&Out->Data[TC_SHA256_DIGEST_SIZE],
                In->Data,
                In->current_length);

            (void)memset(&krusty_hmac_state, 0x00, sizeof());
            (void)tc_hmac_set_key(&krusty_hmac_state, AzureKeyStorage, strlen(AzureKeyStorage));

            do_hmac(&krusty_hmac_state,
                    &Out->Data[TC_SHA256_DIGEST_SIZE],
                    Out->current_length,
                    &Out->Data[0]);

            Out->current_length+=TC_SHA256_DIGEST_SIZE;
        }
    
    #endif
}

//This will terminate the current transmit in progress but leave the data in the queue
void krusty_terminate_tx_in_progress()
{
    if(krusty_tx_in_progress)
    {
         LOG_INF("Terminating Tx in progress");
         krusty_tx_in_progress = false;
         krusty_tx_ack = false;
         krusty_next_to_send = NULL;
    }
} 

void krusty_drain_tx_queue()
{
     while(!krusty__tx_queue_empty())
     {
        DataBufferDestroy(krusty__dequeue_tx_data());
     }
}


void krusty_cleanup_active_tx_message()
{
   DataBufferDestroy(krusty__dequeue_tx_data());
}

void krusty_tx_check()
{
    
    #ifdef CONFIG_KRUSTY_RETRY
    
    if(krusty_tx_ack == true)
    {
        LOG_INF("Last packet acknowledged");

        krusty_tx_in_progress = false;

        krusty_tx_ack = false;

        krusty_stats_messages_acked++;

        //Actually dequeue the data now that we are done
        krusty_cleanup_active_tx_message();
    }
 
    //Tx In progress, waiting for Ack
    if(krusty_tx_in_progress)
    {
        if(Check_mS_Timeout(&krusty_tx_ack_timeout,5000))
        {

            krusty_tx_attempts++;
           
            if(krusty_tx_attempts<CONFIG_KRUSTY_NUM_TX_ATTEMPTS)
            {
              LOG_ERR("Ack Not recieved, retransmitting");
           
              krusty_stats_messages_retry++; //Keep Track of total retries

              if(krusty_tx_attempts & 0x01)
              {
                  krusty_flag_reconnect = true;
                  LOG_WRN("Reconnecting before attempt %d",krusty_tx_attempts);
              }

              krusty_generate_tx_packet(krusty_next_to_send,
                               &krusty_databuffer_tx,
                               krusty_tx_attempts);
           
              krusty_flag_attempt_tx = true;

            } 
            else
            {
                LOG_ERR("Failed to transmit after %d attempts, giving up on this packet @ %d ",CONFIG_KRUSTY_NUM_TX_ATTEMPTS,Epoch__Get32());
                krusty_tx_in_progress = false;
                krusty_stats_messages_failed++;
                
                //Actually dequeue the data now that we are done
                krusty_cleanup_active_tx_message();
            }
            
        }
    }

    #endif

    if(krusty_tx_in_progress == false)
    {
        if(krusty__tx_queue_empty())
        {
            return;
        }

        //We are only going to get a reference from the head of the queue.  We will fully clean up 
        //one there is an ack or failure.
        krusty_next_to_send =  krusty__peek_data();

        krusty_tx_attempts = 0;

        if(krusty_next_to_send == 0)
        {
            LOG_ERR("Message was null");
            return;
        }
        else if(krusty_next_to_send->current_length == 0)
        {
            LOG_ERR("Empty Data Buffer");
        }

       
        krusty_generate_tx_packet(krusty_next_to_send,
                         &krusty_databuffer_tx,
                         krusty_tx_attempts);
 
        krusty_flag_attempt_tx = true;
        
        //Dispose of the data buffer only if we aren;t going to retry.
        #ifndef CONFIG_KRUSTY_RETRY
            //Actually dequeue the data now that we are done
            krusty_cleanup_active_tx_message();
        #endif
     }
  

}

static void krusty__thread_work(void *a, void *b, void *c)
{
    while(1)
    {
    
        if(Provision__IsGood() == false)
        {
            LOG_ERR("Device not provisioned. krusty will not proceed...");

            while(Provision__IsGood() == false)
            {
                k_sleep(K_MSEC(2000));
            }

            LOG_ERR("Device is now provisioned. krusty will proceed...");
           
            k_sleep(K_MSEC(2000));
        }

        if(krusty_request_park == true)
        {
            krusty_request_park = false;    
                 
            krusty_terminate_tx_in_progress();

            LOG_INF("Parking krusty");

            krusty__change_state(KRUSTY_PARKED);
        }

        else if(krusty_request_go == true)
        {
            krusty_request_go = false;

            krusty_request_park = false;

            krusty__change_state(KRUSTY_WAIT_CELL);

            LOG_WRN("Leaving parked state");;
        }
                

        switch(krusty_cloud_state)
        {
            case KRUSTY_INIT:

                krusty__change_state(KRUSTY_WAIT_CELL);

            break;

            case KRUSTY_WAIT_CELL:

                if(Cell_IsConnected())
                {
                  krusty__change_state(KRUSTY_WAIT_FOR_TIME);
                }

            break;

            case KRUSTY_WAIT_FOR_TIME:

                if(Epoch__Get32())
                {
                    krusty__change_state(KRUSTY_CONNECT);
                }

            break;
            
            case KRUSTY_CONNECT:


                if(krusty__refresh_ip((struct sockaddr_in *)&krusty_sockadd) == false)
                {
                    krusty__change_state(KRUSTY_WAIT_CELL);
                    break;
                }

                if(krusty_client_fd>=0)
                {
                    LOG_DBG("Existing socket is open.   Closing before reopening");
                    if(close(krusty_client_fd) !=0)
                    {
                        LOG_ERR("Error %i trying to close existing socket.",krusty_client_fd);
                    }
                }   

                krusty_client_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
          
                if (krusty_client_fd < 0)
                {
                    krusty__change_state(KRUSTY_WAIT_CELL);
                    LOG_ERR("Failed to create UDP socket: %d", errno);
                    break;
                }



                if (connect(krusty_client_fd, (struct sockaddr *)&krusty_sockadd, sizeof(krusty_sockadd)))
                {
                    LOG_ERR("Cannot connect to UDP remote : %d", errno);
                    close(krusty_client_fd);
                    
                    k_sleep(K_MSEC(5000));
                    krusty_client_fd = -1;

                    krusty__change_state(KRUSTY_WAIT_CELL);

                    break;
                }

                LOG_INF("Socket connect success on socket %d",krusty_client_fd);
                
                krusty__connect_notify();

                krusty__change_state(KRUSTY_PROCESS);
            
            break;

            case KRUSTY_PARKED:

                k_sleep(K_MSEC(1000));
                
            break;

            case KRUSTY_PROCESS:


                if(Cell_IsConnected() == false)
                {
                    LOG_INF("Cellular connection no longer active... waiting for a connection");
                    krusty__change_state(KRUSTY_WAIT_CELL);
                }
                else
                {

                krusty_tx_check();

                    if(krusty_flag_reconnect)
                    {
                        krusty_flag_reconnect = false;
                        krusty__change_state(KRUSTY_WAIT_CELL);
                    }

                    if(krusty_flag_attempt_tx)
                    {
                        krusty_flag_attempt_tx = false;
                        krusty_tx(&krusty_databuffer_tx);
                    }

                    int rcvd=0; 

                    uint32_t ActiveKeyNum = Provision__GetActiveDeviceKeyNum();
                
                    while((rcvd = recv(krusty_client_fd, (uint8_t *)&krusty_rx_buf, sizeof(krusty_rx_buf), MSG_DONTWAIT) ) >0)
                    {
                        LOG_DBG("Received %d byte packet",rcvd);
    
                        #ifdef CONFIG_KRUSTY_ENCRYPT

                            if(decrypt_incoming(krusty_rx_buf,rcvd,ActiveKeyNum))
                            {
                                krusty_stats_messages_rx++;
                                LOG_DBG("Incoming packet decrypted and processed");
                            }
                            else
                            {
                                LOG_ERR("Decrypt on DeviceKey #%d failed,  trying alternate.",ActiveKeyNum);
                                

                                if(decrypt_incoming(krusty_rx_buf,rcvd,(ActiveKeyNum^0x1)))
                                {
                                    krusty_stats_messages_rx++;
                                    LOG_DBG("Incoming packet decrypted and processed DeviceKey #%d",(ActiveKeyNum^0x1));
                                }
                                else
                                {
                                    LOG_DBG("Could not decrypting packet with either key");
                                }
                            }

                        #else

                            if(Validate_incoming(krusty_rx_buf,rcvd))
                            {
                                krusty_stats_messages_rx++;
                                LOG_INF("Incoming packet validated");
                                ProcessMessage(&krusty_rx_buf[TC_SHA256_DIGEST_SIZE],rcvd-TC_SHA256_DIGEST_SIZE);
                            }
                            
                        #endif
                    }
               
                }


        
            break;
        }
        
        if(krusty__tx_queue_empty())
            k_sleep(K_MSEC(1000));
        else
            k_sleep(K_MSEC(50));
        
    }

}

bool krusty__is_connected()
{
    return (krusty_cloud_state == KRUSTY_PROCESS);
}

bool krusty__is_transmitting()
{
    return krusty_tx_in_progress;
}

bool krusty__is_in_retry()
{
    return krusty__is_transmitting() & (krusty_tx_attempts>0);
}

uint32_t krusty__messages_transmitted()
{
    return krusty_stats_messages_tx;
}






