#include <zephyr.h>
#include <stdio.h>
#include <stdlib.h>
#include <logging/log.h>
#include <cJSON.h>
#include <cJSON_os.h>
#include "Cell.h"
#include "epoch.h"
#include "System.h"
#include "data_buf.h"
#include "bridge.h"
#include <shell/shell.h>

#include "krusty_crypto.h"
#include "krusty_cmd.h"
#include "krusty.h"
#include "rt_config.h"
#include "BridgeDataStructure.h"

#include "Beep.h"
#include "fota.h"
#include "System.h"
#include "rt_config.h"

LOG_MODULE_REGISTER(krusty_cmd,LOG_LEVEL_DBG);

#define CONFIG_CMD_MAX_ARGS 16

int32_t ParseLine(const char *cmd, uint32_t len, char *argv[CONFIG_CMD_MAX_ARGS]);

int32_t shell_test(int32_t argc, char **argv)
{

	for(int i=0;i<argc;i++)
	{
		LOG_INF("%s --> string length : %i\r\n",log_strdup(argv[i]),strlen(argv[i]));
	}

	return 0;
}

void krusty__cmd_feed(void *Header, uint32_t HeaderLength,uint8_t *Data,  uint32_t Len)
{
    cJSON *JSON_Cmd = cJSON_Parse(Data);


    if(JSON_Cmd == NULL)
    {
        LOG_ERR("Could not parse JSON");
        return;
    }



    cJSON *ObjID = NULL;
    cJSON *Command = NULL;
    bool RxObjID = false;
    bool RxCommand = false;

    ObjID = cJSON_GetObjectItemCaseSensitive(JSON_Cmd, "ObjID");
    if (cJSON_IsString(ObjID) && (ObjID->valuestring != NULL) )
    {
        LOG_INF("Recieved ObjID : %s ", log_strdup(ObjID->valuestring));
      
        if(strcmp(ObjID->valuestring,"Shell")==0)
        {
            RxObjID = true;
        }
    }
    else
    {
        LOG_ERR("Did not recieve ObjID");
    }

    Command = cJSON_GetObjectItemCaseSensitive(JSON_Cmd,"Command");

    if (cJSON_IsString(Command) && (Command->valuestring != NULL))
    {
        LOG_INF("Recieved Command : %s ", log_strdup(Command->valuestring));
        RxCommand = true;
    }
    else
    {
        LOG_ERR("Did not recieve ObjID");
    }
    
    if(RxCommand && RxCommand)
    {
        char *argv[CONFIG_CMD_MAX_ARGS];
        int32_t argc = ParseLine(Command->valuestring,strlen(Command->valuestring)+1,argv);
        bool CmdFound = false;

        STRUCT_SECTION_FOREACH(krusty_cmd_item,bc)
        {
            if(strcmp(bc->command,argv[0]) == 0)
            {
                if(bc->callback!=NULL)
                {
                    CmdFound = true;
                    bc->callback(JSON_Cmd, argc,argv);
                    break;    
                }
            }
           
           
        }
        if(CmdFound == false)
        {
                cJSON_AddStringToObject(JSON_Cmd,"Result","Command not found");
        }
    }
    else
    {
         cJSON_AddStringToObject(JSON_Cmd,"Result","Object Not Processed");
    }

    /*
    char *string = NULL;
    string = cJSON_Print(JSON_Cmd);
    LOG_INF("%s ",log_strdup(string));
    cJSON_free(string);
*/
    BridgeReportHeader * JRH = (BridgeReportHeader *)Header;

    data_buf_t * DB = BridgeMakeJSON(JRH->ReportCounter, JSON_Cmd);

    if(DB)
    {
        LOG_INF("Sending Response to command");

        krusty__enqueue_tx_data(DB);
    }
    cJSON_Delete(JSON_Cmd);
}


char ParamBuffer[256];

 int32_t ParseLine(const char *cmd, uint32_t len, char *argv[CONFIG_CMD_MAX_ARGS])
{
    uint32_t argc;
    char *p;
    uint32_t position;

    if(len>(sizeof(ParamBuffer)-1))
        return 0;

    /* Init params */
    memset(ParamBuffer, '\0', len + 1);
    strncpy(ParamBuffer, cmd, len);

    p = ParamBuffer;
    position = 0;
    argc = 0;

    while (position < len)
    {
        /* Skip all blanks */
        while (((char)(*p) == ' ') && (position < len))
        {
            *p = '\0';
            p++;
            position++;
        }
        /* Process begin of a string */
        if (*p == '"')
        {
            p++;
            position++;
            argv[argc] = p;
            argc++;
            /* Skip this string */
            while ((*p != '"') && (position < len))
            {
                p++;
                position++;
            }
            /* Skip '"' */
            *p = '\0';
            p++;
            position++;
        }
        else /* Normal char */
        {
            argv[argc] = p;
            argc++;
            if(argc == CONFIG_CMD_MAX_ARGS)
            {
                break;
            }
            while (((char)*p != ' ') && ((char)*p != '\t') && (position < len))
            {
            	p++;
                position++;
            }
        }
    }
    return argc;
}