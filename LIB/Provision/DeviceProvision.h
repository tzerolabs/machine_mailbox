#ifndef PROVISION_H
#define PROVISION_H


bool Provision__SetDeviceName(char *String);

uint32_t Provision__GetDeviceSerialNumber();

uint32_t Provision__GetDeviceSerialNumberExtention();

bool Provision__IsGood();

bool MakeBinarySerial(char *String, uint32_t *SerialNumber,uint32_t *SerialNumberExtention);

bool Provision__Init();

uint8_t * Provision__GetDeviceKeyBinary(uint32_t *Len,uint32_t KeyNum);

uint32_t Provision__GetActiveDeviceKeyNum();

#endif