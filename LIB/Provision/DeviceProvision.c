#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "stdio.h"
#include "ctype.h"
#include "DeviceProvision.h"
#include <logging/log.h>


#include "rt_config.h"
#include <sys/base64.h>


#if CONFIG_MODEM_KEY_MGMT
    #include <modem/modem_key_mgmt.h>
#endif

#define TLS_SEC_TAG 100


static uint32_t DeviceSerialNumber = 0;
static uint32_t DeviceSerialNumberExtention = 0;

static bool DeviceID_OK;

static bool DeviceKeyOK;

LOG_MODULE_REGISTER(provision,4);

static char DeviceName_String[16];
static char DeviceKeyPrimary_String[64];
static char DeviceKeySecondary_String[64];
static uint32_t ActiveDeviceKey=0;
uint8_t DeviceKeyPrimaryBinary[32];
uint8_t DeviceKeySecondaryBinary[32];


/*
Gets the Device key.   Num argument should be primary = 0, secondary = 1
KeyNum is masked with 0x1 so even values will always return the PrimaryKey, odd values will return the secondary key.
*/
uint8_t * Provision__GetDeviceKeyBinary(uint32_t *Len,uint32_t KeyNum)
{
    if(Len!=NULL)
    {
        *Len = sizeof(DeviceKeyPrimaryBinary);

        if(KeyNum & 0x01)
        {
            return &DeviceKeySecondaryBinary[0];
        }
        else
        {
            return &DeviceKeyPrimaryBinary[0];
        }
    }

    return NULL;
}

uint32_t Provision__GetActiveDeviceKeyNum()
{
    return ActiveDeviceKey;
}



RT_CONFIG_ITEM(Serial, "Serial",DeviceName_String,RT_CONFIG_DATA_TYPE_STRING,sizeof(DeviceName_String),"","","????");
RT_CONFIG_ITEM(PrimaryKey, "PrimaryKey",DeviceKeyPrimary_String,RT_CONFIG_DATA_TYPE_STRING,sizeof(DeviceKeyPrimary_String),"","","????");
RT_CONFIG_ITEM(SecondaryKey, "SecondaryKey",DeviceKeySecondary_String,RT_CONFIG_DATA_TYPE_STRING,sizeof(DeviceKeySecondary_String),"","","????");
RT_CONFIG_ITEM(ActiveKey, "ActiveKey",&ActiveDeviceKey,RT_CONFIG_DATA_TYPE_UINT32,sizeof(ActiveDeviceKey),"0","1","0");



#if defined(CONFIG_NET_SOCKETS_SOCKOPT_TLS)
#include <net/tls_credentials.h>

#endif


    static const char ConnectCert[] = {
    //#include "cert"
        //#include "brandin.lol"
        //#include "BaltimoreCyberTrustRoot"
        //#include "interR3"
    #include "certs/Connect"
};

int cert_provision(void)
{
   
   
    #if CONFIG_MODEM_KEY_MGMT

	BUILD_ASSERT(sizeof(ConnectCert) < KB(4), "Certificate too large");

	int err;


        if(modem_key_mgmt_cmp(TLS_SEC_TAG,MODEM_KEY_MGMT_CRED_TYPE_CA_CHAIN,ConnectCert,sizeof(ConnectCert)-1) == 0)
        {
            LOG_INF("connect cert already exists, no need to load.");
        }
        else
        {
            err = modem_key_mgmt_delete(TLS_SEC_TAG,MODEM_KEY_MGMT_CRED_TYPE_CA_CHAIN);
            if (err)
            {
                LOG_ERR("Failed to delete existing connect certificate, err %d\n",err);
            }
           
            
                err = modem_key_mgmt_write(TLS_SEC_TAG,
                            MODEM_KEY_MGMT_CRED_TYPE_CA_CHAIN, ConnectCert,
                            sizeof(ConnectCert) - 1);
                if (err) 
                {
                    LOG_ERR("Failed to provision certificate, err %d\n", err);
                    return err;
                }
                else
                {
                    LOG_INF("Connect certificate loaded");
                }
            
        
        }

    #endif

	return 0;


}




bool Provision__SetDeviceName(char *String)
{
    
    if(MakeBinarySerial(String,&DeviceSerialNumber,&DeviceSerialNumberExtention))
    {
        return true;
    }
    else
    {
        DeviceSerialNumber = 0;
        DeviceSerialNumberExtention = 0;

    }

    return false;
}

bool MakeBinarySerial(char *String, uint32_t *SerialNumber,uint32_t *SerialNumberExtention)
{
    if(
           (String == NULL)                ||
           (SerialNumber == NULL)          ||
           (SerialNumberExtention == NULL) || 
            (strlen(String) < 11)          ||
           (!isalpha((int)String[0]))           ||
           (!isalpha((int)String[1]))           ||
           (!isdigit((int)String[2]))           || 
           (!isdigit((int)String[3]))           ||
           (!isdigit((int)String[4]))           ||
           (!isdigit((int)String[5]))           ||
           (String[6] != ':')              
       )
    {
        return false;
    }
    
    for(int i=7;i<strlen(String);i++)
    {
        if(!isdigit((int)String[i]))
            return false;
    }
    
    uint32_t SNE = 0;
    
    SNE = ((uint32_t)(String[0]))<<24;
    SNE += ((uint32_t)(String[1]))<<16;
    
    uint16_t Val = ((String[2] - 0x30) * 1000) + 
                   ((String[3] - 0x30) * 100 ) +
                   ((String[4] - 0x30) * 10 ) +
                   ((String[5] - 0x30));
    
    SNE+= Val;

    *SerialNumberExtention = SNE;
    
    sscanf(&String[7],"%u", SerialNumber);
    
    return true;
}

uint32_t Provision__GetDeviceSerialNumber()
{
    return DeviceSerialNumber;
}

uint32_t Provision__GetDeviceSerialNumberExtention()
{
    return DeviceSerialNumberExtention;
}



bool MakeBinaryKey(char * Base64_Key, uint8_t *Buf, uint8_t Len)
{
    int OutputLength = 0;
    int err;
    
    //add a new line for the base64 decode
    char dtmp[strlen(Base64_Key) + 3];

    strcpy(dtmp,Base64_Key);
    dtmp[strlen(Base64_Key)] = '\r';
    dtmp[strlen(Base64_Key)+1] = '\n';
    dtmp[strlen(Base64_Key)+2] = 0;
    
    if((err = base64_decode(Buf, Len, &OutputLength, dtmp,strlen(dtmp))) == 0)
    {
        if(OutputLength == 32)
        {
             return true;
        }
        else
        {
             LOG_ERR("Device Key has an incorrect length of %d",OutputLength);
        }
    }    
    else
    {
            LOG_ERR("Device Key Base64 decode error %i",err);
    }

    return false;
   
}

bool Provision__Init()
{
    cert_provision();

    Provision__SetDeviceName(&DeviceName_String[2]);
 
    DeviceID_OK = false;
    DeviceKeyOK = false;

    if(DeviceSerialNumber == 0 || DeviceSerialNumberExtention == 0)
    {
        LOG_ERR("Invalid device ID of %s",log_strdup(DeviceName_String));      
    }
    else
    {
        DeviceID_OK = true;
        LOG_INF("Device ID of %s looks good",log_strdup(DeviceName_String));
    }

   
   
   if(MakeBinaryKey(DeviceKeyPrimary_String,DeviceKeyPrimaryBinary,sizeof(DeviceKeyPrimaryBinary)) &&
      MakeBinaryKey(DeviceKeySecondary_String,DeviceKeySecondaryBinary,sizeof(DeviceKeySecondaryBinary))
     )
     {
         LOG_INF("Both Device Keys look OK");
         DeviceKeyOK = true;
     }



    return Provision__IsGood();

}

bool Provision__IsGood()
{
    if(DeviceID_OK && DeviceKeyOK)
    {
        return true;
    }
    return false;
}