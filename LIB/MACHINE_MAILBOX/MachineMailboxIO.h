
#include <zephyr.h>
#include <drivers/pwm.h>
#include <drivers/uart.h>
#include <drivers/gpio.h>

#ifndef _MACHINE_MAILBOX_IO_H
#define _MACHINE_MAILBOX_IO_H


#define NUM_SENSOR_JACKS DT_PROP(DT_NODELABEL(machine_mailbox), num_jacks)

#define MACHINE_MAILBOX_NODE DT_NODELABEL(machine_mailbox)

#if DT_NODE_HAS_STATUS(MACHINE_MAILBOX_NODE, okay)

#define CH0_POWER_EN_LABEL	DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch0_pwr_en_gpios)
#define CH0_POWER_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch0_pwr_en_gpios)
#define CH0_POWER_EN_FLAGS	DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch0_pwr_en_gpios)

#define CH0_TX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch0_tx_en_gpios)
#define CH0_TX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch0_tx_en_gpios)
#define CH0_TX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch0_tx_en_gpios)

#define CH0_RX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch0_rx_en_gpios)
#define CH0_RX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch0_rx_en_gpios)
#define CH0_RX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch0_rx_en_gpios)


#define CH1_POWER_EN_LABEL	DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch1_pwr_en_gpios)
#define CH1_POWER_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch1_pwr_en_gpios)
#define CH1_POWER_EN_FLAGS	DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch1_pwr_en_gpios)

#define CH1_TX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch1_tx_en_gpios)
#define CH1_TX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch1_tx_en_gpios)
#define CH1_TX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch1_tx_en_gpios)

#define CH1_RX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch1_rx_en_gpios)
#define CH1_RX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch1_rx_en_gpios)
#define CH1_RX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch1_rx_en_gpios)

#define CH2_POWER_EN_LABEL	DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch2_pwr_en_gpios)
#define CH2_POWER_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch2_pwr_en_gpios)
#define CH2_POWER_EN_FLAGS	DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch2_pwr_en_gpios)

#define CH2_TX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch2_tx_en_gpios)
#define CH2_TX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch2_tx_en_gpios)
#define CH2_TX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch2_tx_en_gpios)

#define CH2_RX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch2_rx_en_gpios)
#define CH2_RX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch2_rx_en_gpios)
#define CH2_RX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch2_rx_en_gpios)

#define CH3_POWER_EN_LABEL	DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch3_pwr_en_gpios)
#define CH3_POWER_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch3_pwr_en_gpios)
#define CH3_POWER_EN_FLAGS	DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch3_pwr_en_gpios)

#define CH3_TX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch3_tx_en_gpios)
#define CH3_TX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch3_tx_en_gpios)
#define CH3_TX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch3_tx_en_gpios)

#define CH3_RX_EN_LABEL	 DT_GPIO_LABEL(MACHINE_MAILBOX_NODE, ch3_rx_en_gpios)
#define CH3_RX_EN_PIN    DT_GPIO_PIN(MACHINE_MAILBOX_NODE, ch3_rx_en_gpios)
#define CH3_RX_EN_FLAGS	 DT_GPIO_FLAGS(MACHINE_MAILBOX_NODE, ch3_rx_en_gpios)

#define MM_UART_LABEL    DT_PHANDLE(MACHINE_MAILBOX_NODE, mm_uart)

#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: machine_mailbox node is not defined"

#endif

extern const struct device *CH0_PowerEnableGPIO;
extern const struct device *CH0_TxEnGPIO;
extern const struct device *CH0_RxEnGPIO;

extern const struct device *CH1_PowerEnableGPIO;
extern const struct device *CH1_TxEnGPIO;
extern const struct device *CH1_RxEnGPIO;

extern const struct device *CH2_PowerEnableGPIO;
extern const struct device *CH2_TxEnGPIO;
extern const struct device *CH2_RxEnGPIO;

extern const struct device *CH3_PowerEnableGPIO;
extern const struct device *CH3_TxEnGPIO;
extern const struct device *CH3_RxEnGPIO;
extern const struct device *MM_UART;


#define TX_EN0_INACTIVE        gpio_pin_set(CH0_TxEnGPIO, CH0_TX_EN_PIN, 0);
#define TX_EN0_ACTIVE          gpio_pin_set(CH0_TxEnGPIO, CH0_TX_EN_PIN, 1);

#define RX_EN0_INACTIVE         gpio_pin_set(CH0_RxEnGPIO, CH0_RX_EN_PIN, 1);
#define RX_EN0_ACTIVE           gpio_pin_set(CH0_RxEnGPIO, CH0_RX_EN_PIN, 0);

#define CH0_PWR_EN_INACTIVE    gpio_pin_set(CH0_PowerEnableGPIO, CH0_POWER_EN_PIN, 0);
#define CH0_PWR_EN_ACTIVE      gpio_pin_set(CH0_PowerEnableGPIO, CH0_POWER_EN_PIN, 1);


#define TX_EN1_INACTIVE        gpio_pin_set(CH1_TxEnGPIO, CH1_TX_EN_PIN, 0);
#define TX_EN1_ACTIVE          gpio_pin_set(CH1_TxEnGPIO, CH1_TX_EN_PIN, 1);

#define RX_EN1_INACTIVE         gpio_pin_set(CH1_RxEnGPIO, CH1_RX_EN_PIN, 1);
#define RX_EN1_ACTIVE           gpio_pin_set(CH1_RxEnGPIO, CH1_RX_EN_PIN, 0);

#define CH1_PWR_EN_INACTIVE    gpio_pin_set(CH1_PowerEnableGPIO, CH1_POWER_EN_PIN, 0);
#define CH1_PWR_EN_ACTIVE      gpio_pin_set(CH1_PowerEnableGPIO, CH1_POWER_EN_PIN, 1);


#define TX_EN2_INACTIVE        gpio_pin_set(CH2_TxEnGPIO, CH2_TX_EN_PIN, 0);
#define TX_EN2_ACTIVE          gpio_pin_set(CH2_TxEnGPIO, CH2_TX_EN_PIN, 1);

#define RX_EN2_INACTIVE         gpio_pin_set(CH2_RxEnGPIO, CH2_RX_EN_PIN, 1);
#define RX_EN2_ACTIVE           gpio_pin_set(CH2_RxEnGPIO, CH2_RX_EN_PIN, 0);

#define CH2_PWR_EN_INACTIVE    gpio_pin_set(CH2_PowerEnableGPIO, CH2_POWER_EN_PIN, 0);
#define CH2_PWR_EN_ACTIVE      gpio_pin_set(CH2_PowerEnableGPIO, CH2_POWER_EN_PIN, 1);


#define TX_EN3_INACTIVE        gpio_pin_set(CH3_TxEnGPIO, CH3_TX_EN_PIN, 0);
#define TX_EN3_ACTIVE          gpio_pin_set(CH3_TxEnGPIO, CH3_TX_EN_PIN, 1);

#define RX_EN3_INACTIVE         gpio_pin_set(CH3_RxEnGPIO, CH3_RX_EN_PIN, 1);
#define RX_EN3_ACTIVE           gpio_pin_set(CH3_RxEnGPIO, CH3_RX_EN_PIN, 0);

#define CH3_PWR_EN_INACTIVE    gpio_pin_set(CH3_PowerEnableGPIO, CH3_POWER_EN_PIN, 0);
#define CH3_PWR_EN_ACTIVE      gpio_pin_set(CH3_PowerEnableGPIO, CH3_POWER_EN_PIN, 1);

#define MACHINE_MAILBOX_MODE__4CHANNEL    0
#define MACHINE_MAILBOX_MODE__MODBUS      1


extern uint32_t MachineMailBoxIO_Mode;


#endif