
#include "stdint.h"
#include "stdbool.h"
#include "data_buf.h"
#include "MachineMailboxIO.h"

#ifndef RS485_4CH_H
#define RS485_4CH_H

void FourChannel__Init();

#define MAX_SENSOR_DATA_LENGTH 10

typedef struct
{
        uint8_t JackState;
    data_buf_t *JackData;
} Jack_t;

extern char *BirthRecords[NUM_SENSOR_JACKS];

extern bool BirthRecordAcquired[NUM_SENSOR_JACKS];

typedef enum
{
   FOUR_CHANNEL_BOOT = 0,

   FOUR_CHANNEL_IDLE,
   FOUR_CHANNEL_WAIT   ,

   FOUR_CHANNEL_SELECT  ,
   FOUR_CHANNEL_SEND_SENSOR_ESCAPE,
   FOUR_CHANNEL_SEND_REQUEST,
   FOUR_CHANNEL_WAIT_RESPONSE,
   FOUR_CHANNEL_POWER_CYCLE,
   FOUR_CHANNEL_ESCAPED,

} FourChannelState;

FourChannelState FourChannel__GetCurrentState();

uint32_t FourChannel__GetCurrentChannel();

#endif
