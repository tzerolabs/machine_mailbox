
#include <zephyr.h>
#include "System.h"
#include "beep.h"
#include "stdint.h"
#include <drivers/uart.h>
#include <logging/log.h>
#include <shell/shell.h>
#include "FOUR_CH.h"
#include "BRIDGE/DataStructures/BridgeDataStructure.h"
#include <sys/base64.h>
#include "MachineMailboxIO.h"
#include <cJSON.h>
#include <sys/base64.h>
#include <sys/crc.h>
#include <stdio.h>
#include "DataBuffer.h"
#include "Jack.h"

LOG_MODULE_REGISTER(four_ch,LOG_LEVEL_INF);

#define MAX_JSON_OBJ    2048

#define USE_TX_POLL 0

#define SNS0_OFF             CH0_PWR_EN_INACTIVE
#define SNS0_ON              CH0_PWR_EN_ACTIVE

#define SNS1_OFF             CH1_PWR_EN_INACTIVE
#define SNS1_ON              CH1_PWR_EN_ACTIVE

#define SNS2_OFF             CH2_PWR_EN_INACTIVE
#define SNS2_ON              CH2_PWR_EN_ACTIVE

#define SNS3_OFF             CH3_PWR_EN_INACTIVE
#define SNS3_ON              CH3_PWR_EN_ACTIVE

 #define NUM_SENSOR_ESCAPE_CHARACTERS 4

typedef enum
{
        WAITING_FOR_STX = 0,
        WAITING_FOR_ETX = 1

} JSON_SerialDecodeState;

volatile uint32_t ReadBufferIdx = 0;

volatile uint8_t  ReadBuffer[1024];

volatile uint8_t RS485_UART_InByte = 0;

JSON_SerialDecodeState MyJSON_SerialDecodeState = WAITING_FOR_STX;

volatile uint32_t JSON_MsgIdx = 0;

volatile uint32_t JSON_Ready = 0;

volatile uint8_t IncomingJSON_Buffer[MAX_JSON_OBJ + 1];

#define MAX_BR_SIZE 1024

char BirthRecordsBuffer[MAX_BR_SIZE*NUM_SENSOR_JACKS];

char *BirthRecords[NUM_SENSOR_JACKS];

bool BirthRecordAcquired[NUM_SENSOR_JACKS];

uint8_t SensorEscapeSent[NUM_SENSOR_JACKS];

uint32_t SensorErrorCounter[NUM_SENSOR_JACKS];

bool FourChannelRequestPowerCycle[NUM_SENSOR_JACKS];

FourChannelState CurrentFourChannelState = FOUR_CHANNEL_WAIT;

bool RequestBR = false;

uint32_t CheckHydroscopeTimeOut = 0;

uint32_t FourChProcessTimer = 0;

uint32_t CurrentChannel = 0;

void ResetJSON_Parser();

void ParseJSON(uint8_t NextByteIn);

void FourChannel__RequestPowerCycle(uint8_t Channel);

void FourChannel__Select(uint8_t Channel);

void FourChannel__Process();

struct k_work_delayable four_ch_work;

volatile uint8_t * FourCH_TxOut;

volatile uint8_t FourCH_TxOutLen;

uint32_t ErrorCount = 0;

char TmpString[256];

uint32_t TO;

static void four_ch_uart_cb_handler(const struct device *dev, void *app_data);

void RS485_IRQ_Handler();

void FourChannel__PowerOn(uint8_t Channel);

void FourChannel__Init()
{

    CH0_PowerEnableGPIO = device_get_binding(CH0_POWER_EN_LABEL);
    CH1_PowerEnableGPIO = device_get_binding(CH1_POWER_EN_LABEL);
    CH2_PowerEnableGPIO = device_get_binding(CH2_POWER_EN_LABEL);
    CH3_PowerEnableGPIO = device_get_binding(CH3_POWER_EN_LABEL);
   
    if(CH0_PowerEnableGPIO == NULL)
    {
        LOG_ERR("Error getting CH0_PWR_EN Binding");
    }

    gpio_pin_configure(CH0_PowerEnableGPIO, CH0_POWER_EN_PIN, GPIO_OUTPUT_ACTIVE | CH0_POWER_EN_FLAGS);
    gpio_pin_configure(CH1_PowerEnableGPIO, CH1_POWER_EN_PIN, GPIO_OUTPUT_ACTIVE | CH1_POWER_EN_FLAGS);
    gpio_pin_configure(CH2_PowerEnableGPIO, CH2_POWER_EN_PIN, GPIO_OUTPUT_ACTIVE | CH2_POWER_EN_FLAGS);
    gpio_pin_configure(CH3_PowerEnableGPIO, CH3_POWER_EN_PIN, GPIO_OUTPUT_ACTIVE | CH3_POWER_EN_FLAGS);
  
  
    CH0_TxEnGPIO = device_get_binding(CH0_TX_EN_LABEL);
    CH1_TxEnGPIO = device_get_binding(CH1_TX_EN_LABEL);
    CH2_TxEnGPIO = device_get_binding(CH2_TX_EN_LABEL);
    CH3_TxEnGPIO = device_get_binding(CH3_TX_EN_LABEL);
  
    gpio_pin_configure(CH0_TxEnGPIO, CH0_TX_EN_PIN, GPIO_OUTPUT_INACTIVE);
    gpio_pin_configure(CH1_TxEnGPIO, CH1_TX_EN_PIN, GPIO_OUTPUT_INACTIVE);
    gpio_pin_configure(CH2_TxEnGPIO, CH2_TX_EN_PIN, GPIO_OUTPUT_INACTIVE);
    gpio_pin_configure(CH3_TxEnGPIO, CH3_TX_EN_PIN, GPIO_OUTPUT_INACTIVE);
    
    CH0_RxEnGPIO = device_get_binding(CH0_RX_EN_LABEL);
    CH1_RxEnGPIO = device_get_binding(CH1_RX_EN_LABEL);
    CH2_RxEnGPIO = device_get_binding(CH2_RX_EN_LABEL);
    CH3_RxEnGPIO = device_get_binding(CH3_RX_EN_LABEL);
   
    gpio_pin_configure(CH0_RxEnGPIO, CH0_RX_EN_PIN, GPIO_OUTPUT_INACTIVE);
    gpio_pin_configure(CH1_RxEnGPIO, CH1_RX_EN_PIN, GPIO_OUTPUT_INACTIVE);
    gpio_pin_configure(CH2_RxEnGPIO, CH2_RX_EN_PIN, GPIO_OUTPUT_INACTIVE);
    gpio_pin_configure(CH3_RxEnGPIO, CH3_RX_EN_PIN, GPIO_OUTPUT_INACTIVE);

    FourChannel__PowerOn(0);
    FourChannel__PowerOn(1);
    FourChannel__PowerOn(2);
    FourChannel__PowerOn(3);

    MM_UART = device_get_binding(DT_LABEL(MM_UART_LABEL));

    struct uart_config uart_cfg;
 
    uart_cfg.baudrate = 19200;

    uart_cfg.flow_ctrl = UART_CFG_FLOW_CTRL_NONE;

    uart_cfg.data_bits = UART_CFG_DATA_BITS_8;

	uart_cfg.parity = UART_CFG_PARITY_NONE;
    
	uart_cfg.stop_bits = UART_CFG_STOP_BITS_1;

    if (uart_configure(MM_UART, &uart_cfg) != 0) 
    {
		LOG_ERR("Failed to configure UART");
	}

    uart_irq_callback_user_data_set(MM_UART, four_ch_uart_cb_handler, NULL);

    uart_irq_rx_enable(MM_UART);

    CurrentChannel = 0;

    for(int i=0;i<NUM_SENSOR_JACKS;i++)
    {
        BirthRecords[i] = &BirthRecordsBuffer[i*MAX_BR_SIZE];
        BirthRecordAcquired[i] = false;
        SensorEscapeSent[i] = 0;
         SensorErrorCounter[i] = 0;
    }


    CurrentFourChannelState = FOUR_CHANNEL_BOOT;

    CheckHydroscopeTimeOut = System_Get_mS_Ticker();

    FourChProcessTimer = System_Get_mS_Ticker();

    k_work_init_delayable(&four_ch_work,FourChannel__Process);

    k_work_reschedule(&four_ch_work,K_MSEC(100));
}


FourChannelState FourChannel__GetCurrentState()
{
    return CurrentFourChannelState; 
}

uint32_t FourChannel__GetCurrentChannel()
{
    return CurrentChannel; 
}

void FourChannel__RequestPowerCycle(uint8_t Channel)
{
    if(Channel<NUM_SENSOR_JACKS)
    {
        FourChannelRequestPowerCycle[Channel] = true;
    }

}

void FourChannel__RequestPowerCycleAll()
{
    for(int i=0;i<NUM_SENSOR_JACKS;i++)
    {
        FourChannelRequestPowerCycle[i] = true;
    }
}

void FourChannel__Select(uint8_t Channel)
{
    
   RX_EN0_INACTIVE; TX_EN0_INACTIVE;
   RX_EN1_INACTIVE; TX_EN1_INACTIVE;
   RX_EN2_INACTIVE; TX_EN2_INACTIVE;
   RX_EN3_INACTIVE; TX_EN3_INACTIVE;
   

     switch(Channel)
        {
                case 0: RX_EN0_ACTIVE; TX_EN0_ACTIVE; break;
                case 1: RX_EN1_ACTIVE; TX_EN1_ACTIVE; break;
                case 2: RX_EN2_ACTIVE; TX_EN2_ACTIVE; break;
                case 3: RX_EN3_ACTIVE; TX_EN3_ACTIVE;break;
                default: break;
        }
}

void FourChannel__PowerOff(uint8_t Channel)
{
        LOG_WRN("Channel %d power off",Channel);

        
        switch(Channel)
        {
                case 0: SNS0_OFF; break;
                case 1: SNS1_OFF; break;
                case 2: SNS2_OFF; break;
                case 3: SNS3_OFF;break;
                default: break;
        }
}

void FourChannel__PowerOn(uint8_t Channel)
{
     LOG_WRN("Channel %d power on",Channel);
    

    
        switch(Channel)
        {
                case 0: SNS0_ON; break;
                case 1: SNS1_ON; break;
                case 2: SNS2_ON; break;
                case 3: SNS3_ON; break;
                default: break;
        }
}

void ResetJSON_Parser()
{
    MyJSON_SerialDecodeState = WAITING_FOR_STX;
    JSON_MsgIdx = 0;
    JSON_Ready = false;
    ReadBufferIdx = 0;
}

void ParseJSON(uint8_t NextByteIn)
{
       switch (MyJSON_SerialDecodeState)
        {
            default:
            case WAITING_FOR_STX:

                if (NextByteIn == 0x02)
                {
                    MyJSON_SerialDecodeState = WAITING_FOR_ETX;
                    JSON_MsgIdx = 0;
                }

                break;

            case WAITING_FOR_ETX:

                if (NextByteIn == 0x03)
                {
                    MyJSON_SerialDecodeState = WAITING_FOR_STX;

                    IncomingJSON_Buffer[JSON_MsgIdx] = 0x00;
                   
                    JSON_Ready = JSON_MsgIdx;
                
                    JSON_MsgIdx = 0;
                }
                else
                {
                    if (JSON_MsgIdx == MAX_JSON_OBJ)
                    {
                        MyJSON_SerialDecodeState = WAITING_FOR_STX;
                        JSON_MsgIdx = 0;
                    }
                    else
                    {
                        if(NextByteIn>=0x20)
                        {
                            IncomingJSON_Buffer[JSON_MsgIdx] =  NextByteIn;
                            JSON_MsgIdx++;
                        }
                    }
                }
                break;
        }
}

    static void four_ch_uart_cb_handler(const struct device *dev, void *app_data)
    {
        uint8_t IncomingChar;
        uint32_t Count = 0 ;
        int n;

        while (uart_irq_update(dev) && uart_irq_is_pending(dev))
        {
            if (uart_irq_rx_ready(dev)) 
            {
                 if((Count =uart_fifo_read(dev, &IncomingChar, 1)))
                {
                    ReadBuffer[ReadBufferIdx++] = IncomingChar;

                    if(ReadBufferIdx>=sizeof(ReadBuffer))
                        ReadBufferIdx = 0;

                    ParseJSON(IncomingChar);
                }
            }

            if (uart_irq_tx_ready(dev))
            {
                if (FourCH_TxOutLen> 0)
                {
                    
                    n = uart_fifo_fill(dev, 
                                      (const uint8_t *)FourCH_TxOut,
                                       FourCH_TxOutLen);
                  
                    FourCH_TxOutLen -= n;
                    FourCH_TxOut += n;
                }

                if(FourCH_TxOutLen == 0 && uart_irq_tx_complete(MM_UART))
                {
                    uart_irq_tx_disable(MM_UART);
                }
            }

            
        }
    }



void FourCH_Tx_String(char *Tx)
{
 strcpy(TmpString,Tx);
     

    #if USE_TX_POLL == 1
     for(int i=0;i<strlen(Tx);i++)
        {
            uart_poll_out(MM_UART,Tx[i]);
        }
    #else

    FourCH_TxOutLen = strlen(TmpString);
    FourCH_TxOut = &TmpString[0];
    uart_irq_tx_enable(MM_UART);
    
    
    #endif
    
}


uint16_t CRC16_CCIT_Step(uint16_t CRC_In, uint8_t Data)
{
        uint16_t CRC_Temp;

        CRC_Temp = (uint16_t)((uint8_t)(CRC_In >> 8) | (CRC_In << 8));
        CRC_Temp ^= Data;
        CRC_Temp ^= (uint8_t)(CRC_Temp & 0xff) >> 4;
        CRC_Temp ^= CRC_Temp << 12;
        CRC_Temp ^= (CRC_Temp & 0xff) << 5;
        return CRC_Temp;

}

uint16_t CRC16_CCIT(uint8_t *Data, uint32_t Length)
{
        uint16_t CRC = 0xFFFF;

        for(uint32_t i=0;i<Length;i++)
        {
                CRC = CRC16_CCIT_Step(CRC, Data[i]);
        }

        return CRC;
}

void HandleJSON(char * JSON, uint32_t Len, uint32_t Channel)
{

    cJSON *JSON_Data = cJSON_Parse(JSON);

    if(JSON_Data == NULL)
    {
        LOG_ERR("Could not parse JSON");
        return;
    }
 
    cJSON *ObjID = NULL;
    cJSON *Binary = NULL;

    ObjID = cJSON_GetObjectItemCaseSensitive(JSON_Data, "ObjID");
    
    if ( cJSON_IsString(ObjID) &&
        (ObjID->valuestring != NULL) &&
        (strcmp(ObjID->valuestring,"Read") == 0)
        )
    {
        
        Binary = cJSON_GetObjectItemCaseSensitive(JSON_Data, "Binary");

        if (cJSON_IsString(Binary) &&
            (Binary->valuestring != NULL)
            )
        {
            LOG_DBG("Binary : %s",log_strdup(Binary->valuestring));

            uint8_t DataOut[CONFIG_MAX_JACK_SIZE];

            //add a new line for the base64 decode
            char dtmp[strlen(Binary->valuestring) + 3];

            strcpy(dtmp,Binary->valuestring);
            dtmp[strlen(Binary->valuestring)] = '\r';
            dtmp[strlen(Binary->valuestring)+1] = '\n';
            dtmp[strlen(Binary->valuestring)+2] = 0;

            int OutputLength = 0;
            int err;

            if( (err = base64_decode(DataOut, sizeof(DataOut), &OutputLength,dtmp,strlen(dtmp))) == 0)
            {
                LOG_DBG("%d bytes in binary section",OutputLength);

                // uint32_t CRC16 = CRC16_CCIT (DataOut, OutputLength-2);

                uint32_t CRC16 = crc16_itu_t(0xFFFF, DataOut, OutputLength-2);

                uint32_t  CRC16_Check = *((uint16_t *)(&DataOut[OutputLength-2]));

                

                 if(CRC16 == CRC16_Check)
                 {
                     LOG_DBG("Binary CRC Passed!");

                     Jack__PushInData(Channel,
                                      DataOut,
                                      OutputLength-2
                                     );
                 }
                 else
                 {
                     LOG_DBG("CRC Mismatch %i , %i",CRC16, CRC16_Check);
                     Jack__SetState(Channel,JACK_STATE_FAULT_CRC_FAIL);
                 }
            }
            else
            {
                 LOG_ERR("Base64 decode error %i",err);
                 Jack__SetState(Channel,JACK_STATE_FAULT_BASE64_ERROR);
            }
	
        }

    }
    else
    {
        LOG_ERR("Did not recieve ObjID of Read");
    }

cJSON_Delete(JSON_Data);

}

void FourChannel__Process(struct k_work * Work)
{
        switch(CurrentFourChannelState)
        {
            case FOUR_CHANNEL_ESCAPED:

            break;

            default:
            case FOUR_CHANNEL_BOOT:


            if(Check_mS_Timeout(&CheckHydroscopeTimeOut,1000))
            {
                  CurrentFourChannelState = FOUR_CHANNEL_IDLE;
            }

            break;


            case FOUR_CHANNEL_IDLE:

            
            if(Check_mS_Timeout(&CheckHydroscopeTimeOut,0))
            {
                 CurrentFourChannelState = FOUR_CHANNEL_SELECT;

            }


            break;
   
            case FOUR_CHANNEL_SELECT:

                FourChannel__Select(CurrentChannel);

                if(FourChannelRequestPowerCycle[CurrentChannel] == true)
                {

                    LOG_DBG("Power Cycle requested on channel %i",CurrentChannel);

                    FourChannelRequestPowerCycle[CurrentChannel] =false;

                    FourChannel__PowerOff(CurrentChannel);

                    CurrentFourChannelState = FOUR_CHANNEL_POWER_CYCLE;

                }
                else if (SensorEscapeSent[CurrentChannel] < NUM_SENSOR_ESCAPE_CHARACTERS)
                {
                    SensorEscapeSent[CurrentChannel] = 0;

                    LOG_DBG("Sending escape characters on channel  %i",CurrentChannel);


                    CurrentFourChannelState = FOUR_CHANNEL_SEND_SENSOR_ESCAPE;

                }
                else
                {
                    CurrentFourChannelState = FOUR_CHANNEL_SEND_REQUEST;
                }

            break;

            case FOUR_CHANNEL_SEND_SENSOR_ESCAPE:

                
                if(Check_mS_Timeout(&CheckHydroscopeTimeOut,300))
                {
                    if(SensorEscapeSent[CurrentChannel] < NUM_SENSOR_ESCAPE_CHARACTERS)
                    {
                        FourCH_Tx_String("\e");
                        SensorEscapeSent[CurrentChannel]++;
                    }
                    else
                    {
                    
                        CurrentFourChannelState = FOUR_CHANNEL_SEND_REQUEST;
                    }
                }


            break;

            case FOUR_CHANNEL_SEND_REQUEST:
           
                ResetJSON_Parser();
          
               if(RequestBR == true)
               {
                    FourCH_Tx_String("br\r\n"); 
                    LOG_DBG("Requesting birth record %d",CurrentChannel);
                }               
                else
                {
                    FourCH_Tx_String("read \r\n"); 
                    LOG_DBG("Requesting data on channel %d",CurrentChannel);
                }

                CurrentFourChannelState = FOUR_CHANNEL_WAIT_RESPONSE;

                CheckHydroscopeTimeOut = System_Get_mS_Ticker();

             
            break;

            case FOUR_CHANNEL_WAIT_RESPONSE:

            if(JSON_Ready)
            {
                ResetJSON_Parser();

                  if(RequestBR == true)
                  {
                        LOG_DBG("Recieved Birth Record on Channel %i",CurrentChannel);
                        RequestBR = false;
                       BirthRecordAcquired[CurrentChannel] = true;
                        strncpy(BirthRecords[CurrentChannel],(char *)IncomingJSON_Buffer,MAX_BR_SIZE);
                        CurrentFourChannelState = FOUR_CHANNEL_WAIT;
                  }
                  else
                  {
                        SensorErrorCounter[CurrentChannel] = 0;

                        HandleJSON((char *)IncomingJSON_Buffer, strlen((char *)IncomingJSON_Buffer),CurrentChannel);
                     
                        if(BirthRecordAcquired[CurrentChannel] == false)
                        {
                            RequestBR = true;
                            CurrentFourChannelState = FOUR_CHANNEL_SEND_REQUEST;
                            
                        }
                       else
                        {
                           CurrentFourChannelState = FOUR_CHANNEL_WAIT;
                        }

                  }
                  
            }
            else if(Check_mS_Timeout(&CheckHydroscopeTimeOut,250))
            {
                ResetJSON_Parser();

                LOG_DBG("Timeout waiting for data on channel %d",CurrentChannel);
        
                SensorEscapeSent[CurrentChannel] = 0;

                if(RequestBR == true)
                {
                    RequestBR = false;

                    BirthRecordAcquired[CurrentChannel] = true;

                    //If no birth record is returned then just make it empty.
                    snprintf(BirthRecords[CurrentChannel],MAX_BR_SIZE,"{}");

                    CurrentFourChannelState = FOUR_CHANNEL_WAIT;    

                }
                else
                {
                     Jack__SetState(CurrentChannel,JACK_STATE_NO_SENSOR);
                    
                    BirthRecordAcquired[CurrentChannel] = false;

                    SensorErrorCounter[CurrentChannel]++;

                    if(SensorErrorCounter[CurrentChannel] > 4)
                    {
                        SensorErrorCounter[CurrentChannel] = 0;

                        FourChannel__PowerOff(CurrentChannel);

                        CurrentFourChannelState = FOUR_CHANNEL_POWER_CYCLE;

                    }
                    else
                    {
                        CurrentFourChannelState = FOUR_CHANNEL_WAIT;
                    }

                }

                         
            }    
            break;
           

            case FOUR_CHANNEL_POWER_CYCLE:

                if(Check_mS_Timeout(&CheckHydroscopeTimeOut,2000))
                {
                        FourChannel__PowerOn(CurrentChannel);

                        CurrentFourChannelState = FOUR_CHANNEL_WAIT;
                }

            break;

            case FOUR_CHANNEL_WAIT:

                if(Check_mS_Timeout(&CheckHydroscopeTimeOut,1000))
                {
                      CurrentChannel++;

                     if(CurrentChannel>=NUM_SENSOR_JACKS)
                     {
                          CurrentChannel = 0;
                          CurrentFourChannelState = FOUR_CHANNEL_IDLE;
                     }
                     else
                     {
                         CurrentFourChannelState = FOUR_CHANNEL_SELECT;
                     }
                }

            break;
    
         }

    k_work_reschedule(&four_ch_work,K_MSEC(100));

}


