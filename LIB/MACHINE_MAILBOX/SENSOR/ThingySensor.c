
#include <zephyr.h>
#include "System.h"
#include "beep.h"
#include "stdint.h"
#include <drivers/uart.h>
#include <logging/log.h>
#include <shell/shell.h>
#include "FOUR_CH.h"
#include "BRIDGE/DataStructures/BridgeDataStructure.h"
#include <sys/base64.h>
#include "MachineMailboxIO.h"
#include <sys/base64.h>
#include <sys/crc.h>
#include <stdio.h>
#include "ext_sensors.h"
#include "Jack.h"

LOG_MODULE_REGISTER(thingy_sensor,LOG_LEVEL_INF);


static void ext_sensor_handler(const struct ext_sensor_evt *const evt)
{
	switch (evt->type) {
	case EXT_SENSOR_EVT_ACCELEROMETER_TRIGGER:
		//movement_data_send(evt);
		break;
	case EXT_SENSOR_EVT_ACCELEROMETER_ERROR:
		LOG_ERR("EXT_SENSOR_EVT_ACCELEROMETER_ERROR");
		break;
	case EXT_SENSOR_EVT_TEMPERATURE_ERROR:
		LOG_ERR("EXT_SENSOR_EVT_TEMPERATURE_ERROR");
		break;
	case EXT_SENSOR_EVT_HUMIDITY_ERROR:
		LOG_ERR("EXT_SENSOR_EVT_HUMIDITY_ERROR");
		break;
	default:
		break;
	}
}

struct k_work_delayable thingy_sensor_work;

void ThingySensor__Process(struct k_work * Work);

#define VAL_TEMP_RTD    0x8100

#define CONFIG_TEMP_BUFFER_SIZE 30

static int16_t TempBuffer[CONFIG_TEMP_BUFFER_SIZE];
static uint32_t TempBufferIndex = 0;
static bool TempBufferFilled = false;

void ThingySensor__Init()
{
    int err;

	err = ext_sensors_init(ext_sensor_handler);
	if (err) {
		LOG_ERR("ext_sensors_init, error: %d", err);
	}

    k_work_init_delayable(&thingy_sensor_work,ThingySensor__Process);

    k_work_reschedule(&thingy_sensor_work,K_MSEC(1000));
}



void ThingySensor__Process(struct k_work * Work)
{
    double Temp;
    
    uint8_t JackData[10];
    
    ext_sensors_temperature_get(&Temp);

    int16_t TempInt = (int16_t)(Temp*100);

    TempBuffer[TempBufferIndex] = TempInt;

    TempBufferIndex++;
    if(TempBufferIndex>=CONFIG_TEMP_BUFFER_SIZE)
    {
        TempBufferIndex = 0;
        TempBufferFilled = true;
            
    }  
    
    int32_t Sum = 0;

    
    if(TempBufferFilled)
    {
        for(int i=0;i<CONFIG_TEMP_BUFFER_SIZE;i++)
        {
            Sum+=TempBuffer[i];
        }

        TempInt = Sum/CONFIG_TEMP_BUFFER_SIZE;
    }
    else
    {
        for(int i=0;i<TempBufferIndex;i++)
        {
            Sum+=TempBuffer[i];
        }

        TempInt = Sum/TempBufferIndex;
    }
    
    


    JackData[0] = (VAL_TEMP_RTD & 0xFF);
	JackData[1] = ((VAL_TEMP_RTD>>8) & 0xFF);

	JackData[2] = (uint8_t)((TempInt) & 0xFF);
	JackData[3] = (uint8_t)((TempInt>>8) & 0xFF);

	JackData[4] = (uint8_t)((TempInt) & 0xFF);
	JackData[5] = (uint8_t)((TempInt >> 8) & 0xFF);

	JackData[6] = 0;
	JackData[7] = 0;
	JackData[8] = 0;

	JackData[9] = 0;

    LOG_DBG("Temp %d", TempInt);

    Jack__PushInData(0,
                     JackData,
                     sizeof(JackData)
                     );
   
    k_work_reschedule(&thingy_sensor_work,K_MSEC(1000));
}


