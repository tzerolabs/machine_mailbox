
#include <zephyr.h>
#include "rt_config.h"
#include "MachineMailboxIO.h"


#define MACHINE_MAILBOX_IO__4CHANNEL    0
#define MACHINE_MAILBOX_IO__MODBUS      1


uint32_t MachineMailBoxIO_Mode;

const struct device *CH0_PowerEnableGPIO;
const struct device *CH0_TxEnGPIO;
const struct device *CH0_RxEnGPIO;

const struct device *CH1_PowerEnableGPIO;
const struct device *CH1_TxEnGPIO;
const struct device *CH1_RxEnGPIO;

const struct device *CH2_PowerEnableGPIO;
const struct device *CH2_TxEnGPIO;
const struct device *CH2_RxEnGPIO;

const struct device *CH3_PowerEnableGPIO;
const struct device *CH3_TxEnGPIO;
const struct device *CH3_RxEnGPIO;

const struct device *MM_UART;
