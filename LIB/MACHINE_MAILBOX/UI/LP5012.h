#include "stdint.h"

#ifndef _LP5012_H
#define _LP5012_H


#define LP5012_I2C_ADDRESS (0x14)


typedef struct
{
    uint8_t Brightness;
    uint8_t Red;
    uint8_t Green;
   
    uint8_t Blue;

} LP5012_LED;


#define NUM_LP5012_LEDS    4 

extern LP5012_LED MyLP5012_LED[NUM_LP5012_LEDS];


void LP5012_Init();
void LP5012_SetLED_Brightness(uint8_t LED_Number, uint8_t Brightness);
void LP5012_SetLED_Color(uint8_t LED_Number, LP5012_LED *LED);
void LP5012_SetLED(uint8_t LED_Number, LP5012_LED *LED);

extern const uint8_t GlowTable[256];
#endif
