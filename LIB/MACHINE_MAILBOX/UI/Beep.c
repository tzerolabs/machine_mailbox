#include <zephyr.h>
#include "beep.h"
#include "stdint.h"
#include <drivers/pwm.h>
#include <shell/shell.h>
#include <init.h>

#define BEEP_QUEUE_SIZE 24

#define MACHINE_MAILBOX_NODE DT_NODELABEL(machine_mailbox)

#define MM_BUZZ_PWM       DT_PHANDLE(MACHINE_MAILBOX_NODE, mm_buzz_pwm)
#define MM_BUZZ_PWM_PIN   DT_PROP(MACHINE_MAILBOX_NODE, mm_buzz_pwm_pin)

void NextBeep(uint32_t Freq,uint32_t mSec);

typedef struct
{

  uint16_t Freq;
  uint16_t Time;

} BeepQueueElement;

BeepQueueElement BeepQueue[BEEP_QUEUE_SIZE];

uint8_t BeepQueueWrite = 0;

uint8_t BeepQueueRead = 0;

uint32_t LastPeriod = 1000;

uint16_t  pwm_seq[4]= {8000,8000,8000,8000};

volatile uint32_t BeepTicker = 0;

extern void my_timer_handler(struct k_timer *dummy);

K_TIMER_DEFINE(my_timer, my_timer_handler, NULL);

#define SYSTICK_MSEC_PER_TICK 10

void my_timer_handler(struct k_timer *dummy)
{
      //IF we are idle and the is something in theQueue, start the next beep

      if(BeepTicker > SYSTICK_MSEC_PER_TICK)
      {
        BeepTicker -= SYSTICK_MSEC_PER_TICK;
      }
      else
      {
        
        BeepTicker = 0; //Just in case the mSec is not a multiple of the tick

        if(BeepQueueWrite!=BeepQueueRead)
        { 
          //Start the next beep
           NextBeep(BeepQueue[BeepQueueRead].Freq,BeepQueue[BeepQueueRead].Time);
        
            BeepQueueRead++;
            
            if(BeepQueueRead == BEEP_QUEUE_SIZE)
                BeepQueueRead = 0;
        }
        else
        {
            Beep__Off();
        }

      }

}

#define BEEP_PORT            NRF_P0
#define BEEP_PORT_NUMBER     0
#define BEEP_PIN_NUMBER      0

const struct device *pwm_dev;

static int Beep__Init()
{
    pwm_dev = device_get_binding(DT_LABEL(MM_BUZZ_PWM));
 
    LastPeriod = 10000;

    k_timer_start(&my_timer, K_MSEC(SYSTICK_MSEC_PER_TICK), K_MSEC(SYSTICK_MSEC_PER_TICK));

    return 0;
}
 
void NextBeep(uint32_t Freq,uint32_t mSec)
{
  

   //if(RuntimeConfig.EnableBeep)
   {
     if(Freq>0)
     {

         LastPeriod = (1000000/Freq);
        pwm_pin_set_usec(pwm_dev, MM_BUZZ_PWM_PIN, LastPeriod, LastPeriod/2,0);

    }
    else
    {
        pwm_pin_set_usec(pwm_dev, MM_BUZZ_PWM_PIN, LastPeriod, 0,0);
    }
  }

    //Add one tick time as we use zero as the idle condition
  BeepTicker = mSec + SYSTICK_MSEC_PER_TICK;

}

void Beep(uint32_t Freq,uint32_t mSec)
{

    BeepQueue[BeepQueueWrite].Freq = Freq;
    BeepQueue[BeepQueueWrite].Time = mSec;
 

    BeepQueueWrite++;

    if(BeepQueueWrite == BEEP_QUEUE_SIZE)
       BeepQueueWrite = 0;

}

void Beep__Off()
{
 
  BeepTicker = 0;

  pwm_pin_set_usec(pwm_dev, MM_BUZZ_PWM_PIN, LastPeriod, 0,0);

}


#define QUARTER 220
#define EIGHTH   (QUARTER/2)

void Q(uint32_t Freq)
{
     Beep(Freq,QUARTER*3/4);
     Beep(0,QUARTER/4);
}

void E(uint32_t Freq)
{
     Beep(Freq,EIGHTH*3/4);
     Beep(0,EIGHTH/4);
}

void Mario11()
{
     E(1318);
     E(1318);
     E(0);
     E(1318);
     E(0);
     E(1046);
     Q(1318);
     Q(1567);
     Q(0);
     Q(783);
}


SYS_INIT(Beep__Init, APPLICATION, 2);


static int beep_handler(const struct shell *shell, 
                      size_t argc,
                      char **argv)
{
        ARG_UNUSED(argc);
        ARG_UNUSED(argv);

        Mario11();

        return 0;
}


SHELL_CMD_REGISTER(beep, NULL, "Mario 1-1", beep_handler);