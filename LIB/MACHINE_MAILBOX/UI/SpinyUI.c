#include "stdint.h"
#include "System.h"
#include "LP5012.h"
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <device.h>
#include "Cell.h"
#include "BRIDGE/DataStructures/BridgeDataStructure.h"
#include "System.h"
#include "Cloud_UDP.h"
#include "Epoch.h"
#include "DeviceProvision.h"
#include "Modbus.h"
#include "FOTA.h"
#include <task_wdt/task_wdt.h>
#include "MachineMailboxIO.h"
#include "FOUR_CH.h"
#include "Jack.h"

K_THREAD_STACK_DEFINE(my_ui_thread_stack_area, 256);

struct k_thread my_ui_thread_data;

#define NUM_SPINY_LED   3

LP5012_LED SpinyLED[NUM_SPINY_LED];

#define CELL_LED      1

#define SENSOR_LED   0

#define CLOUD_LED       2

uint8_t GlowIndex[4];

uint32_t FourChUI_Timer = 0;

uint32_t BridgeBlinkTimer = 0; 

uint32_t CellBlinkTimer = 0;

uint32_t SensorBlinkTimer = 0;

uint32_t ChannelBlinkTimer = 0;

uint32_t SensorLED_Toggle;

int ui_thread_wdt_id = 0;

void HandleCellLED();

void HandleCloudLED();

void HandleSensorLEDs();

void ui_work_handler(void *a, void *b, void *c);

void SpinyUI_Init()
{
    LP5012_Init();

    ChannelBlinkTimer = System_Get_mS_Ticker();
    CellBlinkTimer = System_Get_mS_Ticker();
    BridgeBlinkTimer = System_Get_mS_Ticker();
    FourChUI_Timer = System_Get_mS_Ticker();

    ui_thread_wdt_id = task_wdt_add(5000U, NULL, NULL);

    k_thread_create(&my_ui_thread_data, my_ui_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(my_ui_thread_stack_area),
                    ui_work_handler,
                    NULL, NULL, NULL,
                    K_HIGHEST_APPLICATION_THREAD_PRIO, 0, K_NO_WAIT);
}

void ui_work_handler(void *a, void *b, void *c)
{
    while(1)
    {

    task_wdt_feed(ui_thread_wdt_id);

    int FOTA_Status = 0;

    if((FOTA_Status = FOTA_GetStatus()))
    {
            if(Check_mS_Timeout(&CellBlinkTimer,100 + (1000 - (FOTA_Status * 10))))
            {
                for(int i=0;i<NUM_SPINY_LED;i++)
                {
                    if(FOTA_Status>=100)
                    {
                         SpinyLED[i].Brightness = 0xFF;
                    }
                    else
                    {
                        if(SpinyLED[i].Brightness!=0)
                            SpinyLED[i].Brightness = 0;
                        else
                            SpinyLED[i].Brightness = 0xFF;
                    }

                    SpinyLED[i].Blue = 0xFF;
                    SpinyLED[i].Green = 0xFF;
                    SpinyLED[i].Red = 0xFF;
                }
            }
    }
    else
    {

            HandleCellLED();
            HandleCloudLED();
            HandleSensorLEDs();
    }
            for(int i=0;i<NUM_SPINY_LED;i++)
            {
                LP5012_SetLED(i,&SpinyLED[i]);
            }

            GlowIndex[0]+= 8;
            GlowIndex[1] = GlowIndex[0] -64;
            GlowIndex[2] = GlowIndex[1] -64;
            GlowIndex[3] = GlowIndex[2] -64;

            k_sleep(K_MSEC(50));
    }

}

void HandleCellLED()
{
    Cell_ConnectionState_t CellConnectionState;

    if(Cell_GetConnectionState(&CellConnectionState))
    {
        SpinyLED[CELL_LED].Brightness = 0xFF;
        SpinyLED[CELL_LED].Blue = 0;
        SpinyLED[CELL_LED].Green = 0xFF;
        SpinyLED[CELL_LED].Red = 0;
    }
    else
    {
          {
                if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_NOT_REGISTERED)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,150))
                    {
                        if(SpinyLED[CELL_LED].Brightness!=0)
                            SpinyLED[CELL_LED].Brightness = 0;
                        else
                          SpinyLED[CELL_LED].Brightness = 0xFF;
                    }

                    SpinyLED[CELL_LED].Blue = 0;
                    SpinyLED[CELL_LED].Green = 0x80;
                    SpinyLED[CELL_LED].Red = 0xFF;
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_HOME || CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_ROAMING)
                {

                        if(CellConnectionState.ID == -1)
                        {
                            if(Check_mS_Timeout(&CellBlinkTimer,150))
                            {
                                if(SpinyLED[CELL_LED].Brightness!=0)
                                    SpinyLED[CELL_LED].Brightness = 0;
                                else
                                SpinyLED[CELL_LED].Brightness = 0xFF;
                            }

                            SpinyLED[CELL_LED].Blue = 0;
                            SpinyLED[CELL_LED].Green = 0x80;
                            SpinyLED[CELL_LED].Red = 0xFF;
                        }
                        else
                        {
                            SpinyLED[CELL_LED].Brightness = 0xFF;
                                SpinyLED[CELL_LED].Blue = 0;
                                SpinyLED[CELL_LED].Green = 0xFF;
                                SpinyLED[CELL_LED].Red = 0;
                        }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_SEARCHING)
                {
                   if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                         SpinyLED[CELL_LED].Brightness = 0xFF;

                         if(SpinyLED[CELL_LED].Green == 0xFF)
                         {
                            SpinyLED[CELL_LED].Blue = 0;
                            SpinyLED[CELL_LED].Green = 0x80;
                            SpinyLED[CELL_LED].Red = 0xFF;
                         }
                         else
                         {
                            SpinyLED[CELL_LED].Blue = 0;
                            SpinyLED[CELL_LED].Green = 0xFF;
                            SpinyLED[CELL_LED].Red = 0;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTRATION_DENIED)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,100))
                    {
                        if(SpinyLED[CELL_LED].Brightness!=0)
                            SpinyLED[CELL_LED].Brightness = 0;
                        else
                        SpinyLED[CELL_LED].Brightness = 0xFF;
                    }

                    SpinyLED[CELL_LED].Blue = 0;
                    SpinyLED[CELL_LED].Green = 0;
                    SpinyLED[CELL_LED].Red = 0xFF;
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_UNKNOWN)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                         SpinyLED[CELL_LED].Brightness = 0xFF;

                         if(SpinyLED[CELL_LED].Blue != 0x80)
                         {
                            SpinyLED[CELL_LED].Blue = 0x80;
                            SpinyLED[CELL_LED].Green = 0;
                            SpinyLED[CELL_LED].Red = 0xFF;
                         }
                         else
                         {
                            SpinyLED[CELL_LED].Blue = 0;
                            SpinyLED[CELL_LED].Green = 0;
                            SpinyLED[CELL_LED].Red = 0xFF;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_REGISTERED_EMERGENCY)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                        SpinyLED[CELL_LED].Brightness = 0xFF;

                         if(SpinyLED[CELL_LED].Green != 0x80)
                         {
                            SpinyLED[CELL_LED].Blue = 0;
                            SpinyLED[CELL_LED].Green = 0x80;
                            SpinyLED[CELL_LED].Red = 0xFF;
                         }
                         else
                         {
                            SpinyLED[CELL_LED].Blue = 0;
                            SpinyLED[CELL_LED].Green = 0;
                            SpinyLED[CELL_LED].Red = 0xFF;
                         }
                    }
                }
                else if(CellConnectionState.NetworkStatus == LTE_LC_NW_REG_UICC_FAIL)
                {
                    if(Check_mS_Timeout(&CellBlinkTimer,500))
                    {
                        SpinyLED[CELL_LED].Brightness = 0xFF;

                        if(SpinyLED[CELL_LED].Green !=0)
                        {
                            SpinyLED[CELL_LED].Blue = 0x00;
                            SpinyLED[CELL_LED].Green = 0x00;
                            SpinyLED[CELL_LED].Red = 0xFF;
                        }
                        else
                        {
                            SpinyLED[CELL_LED].Blue = 0xFF;
                            SpinyLED[CELL_LED].Green = 0xFF;
                             SpinyLED[CELL_LED].Red = 0xFF;
                        }
                    }
                }
                else
                {
                    SpinyLED[CELL_LED].Blue = 0;
                    SpinyLED[CELL_LED].Green = 0;
                    SpinyLED[CELL_LED].Red = 0xFF;
                    SpinyLED[CELL_LED].Brightness = 0xFF;
                }
        }
    }

}

uint32_t  LED_TX_Flag = 0;
uint32_t  LED_TX_Count = 0;
void HandleCloudLED()
{
    if(!Provision__IsGood())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,500))
             {
                if(SpinyLED[CLOUD_LED].Brightness<255)
                    SpinyLED[CLOUD_LED].Brightness = 255;
                else
                    SpinyLED[CLOUD_LED].Brightness = 0;
                }

                SpinyLED[CLOUD_LED].Green = 0;
                SpinyLED[CLOUD_LED].Red = 255;
                SpinyLED[CLOUD_LED].Blue = 0;
    }
    else if(Cell_IsConnected() == 0)
    {
        SpinyLED[CLOUD_LED].Brightness = 0xFF;
        SpinyLED[CLOUD_LED].Blue = 0;
        SpinyLED[CLOUD_LED].Green = 0;
        SpinyLED[CLOUD_LED].Red = 0;
    }
    else if(Epoch__Get32() == 0)
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,50))
             {
                if(SpinyLED[CLOUD_LED].Brightness<255)
                    SpinyLED[CLOUD_LED].Brightness = 255;
                else
                    SpinyLED[CLOUD_LED].Brightness = 0;
                }

                SpinyLED[CLOUD_LED].Green = 0;
                SpinyLED[CLOUD_LED].Red = 255;
                SpinyLED[CLOUD_LED].Blue = 255;
    }
    else if(LED_TX_Flag != Cloud_MessagesTransmitted())
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,30))
             {
                 if(LED_TX_Count < 8)
                 {
                    LED_TX_Count++;
                 }
                 else
                 {
                     LED_TX_Flag++;
                     LED_TX_Count = 0;
                 }

                if(SpinyLED[CLOUD_LED].Brightness<255)
                    SpinyLED[CLOUD_LED].Brightness = 255;
                else
                    SpinyLED[CLOUD_LED].Brightness = 0;
              }

              SpinyLED[CLOUD_LED].Green = 0;
              SpinyLED[CLOUD_LED].Red = 0;
              SpinyLED[CLOUD_LED].Blue = 255;
    }
    else if(Cloud_IsConnected())
    {
         SpinyLED[CLOUD_LED].Brightness = 255;
         SpinyLED[CLOUD_LED].Green = 0;
         SpinyLED[CLOUD_LED].Red = 0;
         SpinyLED[CLOUD_LED].Blue = 255;
    }
    else
    {
            if( Check_mS_Timeout(&BridgeBlinkTimer,150))
             {
                if(SpinyLED[CLOUD_LED].Brightness<255)
                    SpinyLED[CLOUD_LED].Brightness = 255;
                else
                    SpinyLED[CLOUD_LED].Brightness = 0;
            }

                SpinyLED[CLOUD_LED].Green = 127;
                SpinyLED[CLOUD_LED].Red = 255;
                SpinyLED[CLOUD_LED].Blue = 0;
    }
}

static uint32_t LastMailBoxActivity = 0;


void HandleModbusLEDs()
{

}

void HandleSensorLEDs()
{
    if(MachineMailBoxIO_Mode == MACHINE_MAILBOX_MODE__MODBUS)
    {
        if(Modbus_IO__GetRxActivity() != LastMailBoxActivity)
        {
                if( Check_mS_Timeout(&SensorBlinkTimer,50))
                {
                        LastMailBoxActivity = Modbus_IO__GetRxActivity();
                }
                
                SpinyLED[SENSOR_LED].Brightness = 255;
                SpinyLED[SENSOR_LED].Green = 0;
                SpinyLED[SENSOR_LED].Red = 0;
                SpinyLED[SENSOR_LED].Blue = 255;
        }
        else
        {
                SpinyLED[SENSOR_LED].Brightness = 255;
                SpinyLED[SENSOR_LED].Green = 255;
                SpinyLED[SENSOR_LED].Red = 0;
                SpinyLED[SENSOR_LED].Blue = 0;
        }
    }
    else
    {
        
            switch(Jack__GetState(0))
            {
                default:
                case     JACK_STATE_NO_SENSOR          :
        
                    SpinyLED[SENSOR_LED].Brightness = 255;
                    SpinyLED[SENSOR_LED].Red = 255;
                    SpinyLED[SENSOR_LED].Green =127;
                    SpinyLED[SENSOR_LED].Blue = 0;
    
                break;
    
                case     JACK_STATE_OK                 :
    
                    SpinyLED[SENSOR_LED].Brightness = 255;
                    SpinyLED[SENSOR_LED].Red = 0;
                    SpinyLED[SENSOR_LED].Green = 255;
                    SpinyLED[SENSOR_LED].Blue = 0;
    
                break;
    
                case     JACK_STATE_FAULT_OVERCURRENT  :
                case     JACK_STATE_FAULT_CRC_FAIL     :
                case     JACK_STATE_FAULT_BASE64_ERROR :
                case     JACK_STATE_FAULT_LENGTH_ERROR :
                case     JACK_STATE_FAULT_NO_BINARY    :
    
                    SpinyLED[SENSOR_LED].Brightness = 255;
                    SpinyLED[SENSOR_LED].Red = 255;
                    SpinyLED[SENSOR_LED].Green = 0;
                    SpinyLED[SENSOR_LED].Blue = 0;
    
                break;
            }

                    switch(FourChannel__GetCurrentState())
                    {
                        default:
                        
                        case FOUR_CHANNEL_BOOT:

                            SpinyLED[SENSOR_LED].Brightness = 255;
                            SpinyLED[SENSOR_LED].Red = 255;
                            SpinyLED[SENSOR_LED].Green = 255;
                            SpinyLED[SENSOR_LED].Blue = 255;
                        
                        break;


                        case FOUR_CHANNEL_IDLE:
                        case FOUR_CHANNEL_SELECT:
                        case FOUR_CHANNEL_WAIT:


                        break;


                        case FOUR_CHANNEL_SEND_REQUEST:
                        case FOUR_CHANNEL_WAIT_RESPONSE:

                            SpinyLED[SENSOR_LED].Brightness = 255;
                            SpinyLED[SENSOR_LED].Red = 0;
                            SpinyLED[SENSOR_LED].Green = 0;
                            SpinyLED[SENSOR_LED].Blue = 255;

                        break;
           

                        case FOUR_CHANNEL_POWER_CYCLE:


                             if( Check_mS_Timeout(&ChannelBlinkTimer,100))
                             {

                                SpinyLED[SENSOR_LED].Blue = 255;
                                SpinyLED[SENSOR_LED].Red = 255;
                                SpinyLED[SENSOR_LED].Green = 255;
         
                               
                                  if( SpinyLED[SENSOR_LED].Brightness != 0)
                                  {
                                      SpinyLED[SENSOR_LED].Brightness = 0;
                                  }
                                  else
                                   {
                                        SpinyLED[SENSOR_LED].Brightness = 255;
                                   }
                             }
                             
                           
                            SpinyLED[SENSOR_LED].Blue = 255;
                            SpinyLED[SENSOR_LED].Red = 255;
                            SpinyLED[SENSOR_LED].Green = 255;
            
                       
                        break;

                        case FOUR_CHANNEL_SEND_SENSOR_ESCAPE:


                             if( Check_mS_Timeout(&ChannelBlinkTimer,100))
                             {

                                SpinyLED[SENSOR_LED].Blue = 255;
                                SpinyLED[SENSOR_LED].Red = 255;
                                SpinyLED[SENSOR_LED].Green = 0;
                           
                                 if( SpinyLED[SENSOR_LED].Brightness != 0)
                                  {
                                      SpinyLED[SENSOR_LED].Brightness = 0;
                                  }
                                  else
                                   {
                                        SpinyLED[SENSOR_LED].Brightness = 255;
                                   }
                             }
                             
                           
                            SpinyLED[SENSOR_LED].Blue = 255;
                            SpinyLED[SENSOR_LED].Red = 255;
                            SpinyLED[SENSOR_LED].Green = 0;
            
                       
                        break;

    
                     }

    }


}

