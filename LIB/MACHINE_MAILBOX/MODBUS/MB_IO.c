
#include <zephyr.h>
#include "System.h"
#include "lightmodbus/lightmodbus.h"
#include "MachineMailboxIO.h"
#include <logging/log.h>
#include "Modbus.h"
#include "rt_config.h"

LOG_MODULE_REGISTER(modbus,LOG_LEVEL_INF);



//uint32_t RS485_UART_TxActive = 0 ;

//uint8_t RS485_UART_InByte = 0;

uint32_t RxActivity = 0 ;

uint32_t TxActivity = 0;

volatile uint8_t * TxResponse;

volatile uint8_t TxResponseLen;

uint32_t rtu_timeout = 0;

struct k_timer rtu_timer;

struct k_work modbus_work;

volatile bool ModbusPacketActive = false;

volatile uint32_t ModbusDataIndex = 0;

#define MAX_MODBUS_PACKET_SIZE 		256

volatile uint8_t ModbusPacketIn[MAX_MODBUS_PACKET_SIZE];

volatile ModbusSlave sstatus;

uint16_t ModbusRegisterCallback( ModbusRegisterQuery query, ModbusDataType datatype, uint16_t index, uint16_t value, void *ctx );

void RS485_IRQ_Handler();

void MB_Timer_IRQ_Handler();

void Modbus__Feed(uint8_t Data);

uint32_t Modbus_IO__GetRxActivity()
{
    return RxActivity;
}


static void uart_cb_handler(const struct device *dev, void *app_data)
{
    uint8_t IncomingChar;
    int n;

	while (uart_irq_update(dev) && uart_irq_is_pending(dev))
    {

		if (uart_irq_rx_ready(dev)) 
        {
            		/* Restart timer on a new character */
		    k_timer_start(&rtu_timer,
			              K_USEC(rtu_timeout), K_NO_WAIT);

			uart_fifo_read(dev, &IncomingChar, 1);
            Modbus__Feed(IncomingChar);
		}

		if (uart_irq_tx_ready(dev))
        {
			if (TxResponseLen> 0)
            {
                
                n = uart_fifo_fill(dev, 
                               (const uint8_t *)TxResponse,
                               TxResponseLen);
                TxResponseLen -= n;
                TxResponse += n;
            }
            else 
            {
                TxResponse = &sstatus.response.frame[0];

            }
		}
        if(TxResponseLen == 0 && uart_irq_tx_complete(MM_UART))
        {
                RX_EN0_ACTIVE;
                TX_EN0_INACTIVE;
                uart_irq_tx_disable(MM_UART);
        }
	}
}

void modbus_work_handler(struct k_work *work)
{
       if(ModbusPacketActive == true)
        {
            sstatus.request.frame = (const uint8_t *)ModbusPacketIn;
            sstatus.request.length = ModbusDataIndex;

            //reset the packet
            ModbusPacketActive = false;
            ModbusDataIndex = 0; 

            modbusParseRequest( (ModbusSlave *)&sstatus );

            if(sstatus.response.length)
            {

                    RxActivity++;

                    TxResponse = &sstatus.response.frame[0];
                    TxResponseLen = sstatus.response.length;
                    RX_EN0_INACTIVE;
                    TX_EN0_ACTIVE;

                    uart_irq_tx_enable(MM_UART);
            }
        }
}

static void rtu_tmr_handler(struct k_timer *t_id)
{
	k_work_submit(&modbus_work);
}

void Modbus__IO_Init()
{
    struct uart_config uart_cfg;

    CH0_PowerEnableGPIO = device_get_binding(CH0_POWER_EN_LABEL);
   
    if(CH0_PowerEnableGPIO == NULL)
    {
        LOG_ERR("Error getting CH0_PWR_EN Binding");
    }
    gpio_pin_configure(CH0_PowerEnableGPIO, CH0_POWER_EN_PIN, GPIO_OUTPUT_INACTIVE);
  
    CH0_PWR_EN_ACTIVE;

    CH0_TxEnGPIO = device_get_binding(CH0_TX_EN_LABEL);
    if(CH0_TxEnGPIO == NULL)
    {
        LOG_ERR("Error getting CH0_TX_EN Binding");
    }
    gpio_pin_configure(CH0_TxEnGPIO, CH0_TX_EN_PIN, GPIO_OUTPUT_INACTIVE);

    CH0_RxEnGPIO = device_get_binding(CH0_RX_EN_LABEL);
    if(CH0_RxEnGPIO == NULL)
    {
        LOG_ERR("Error getting RX_EN Binding");
    }
    gpio_pin_configure(CH0_RxEnGPIO, CH0_RX_EN_PIN, GPIO_OUTPUT_INACTIVE);
  

    RX_EN0_INACTIVE;
    TX_EN0_INACTIVE;

    k_work_init(&modbus_work,modbus_work_handler);

	k_timer_init(&rtu_timer, rtu_tmr_handler, NULL);


    const uint32_t if_delay_max = 3500000;
	const uint32_t numof_bits = 11;

	if ((ModbusBaud_Storage * 100) <= 38400) {
		rtu_timeout = (numof_bits * if_delay_max) /
				            (ModbusBaud_Storage * 100);
	} else {
		rtu_timeout = (numof_bits * if_delay_max) / 38400;
	}

    //RS485_UART_TxActive = 0 ;
  
    MM_UART = device_get_binding(DT_LABEL(MM_UART_LABEL));

    uart_cfg.baudrate = ModbusBaud_Storage * 100;

    uart_cfg.flow_ctrl = UART_CFG_FLOW_CTRL_NONE;

    uart_cfg.data_bits = UART_CFG_DATA_BITS_8;

	uart_cfg.parity = UART_CFG_PARITY_NONE;

	uart_cfg.stop_bits = UART_CFG_STOP_BITS_1;

    if (uart_configure(MM_UART, &uart_cfg) != 0) 
    {
		LOG_ERR("Failed to configure UART");
	}


    uart_irq_callback_user_data_set(MM_UART, uart_cb_handler, NULL);

    uart_irq_rx_enable(MM_UART);

	//Init slave (input registers and discrete inputs work just the same)
	sstatus.address = ModbusDeviceID_Storage;
	//sstatus.registers = regs;
	sstatus.registerCount = 9999;

	sstatus.registerCallback = ModbusRegisterCallback;

	modbusSlaveInit((ModbusSlave *)&sstatus);

    RX_EN0_ACTIVE;
    
    TX_EN0_INACTIVE;

}

uint16_t ModbusRegisterCallback( ModbusRegisterQuery query, ModbusDataType datatype, uint16_t index, uint16_t value, void *ctx )
{
	(void)(ctx);
	
	//All can be read
	if ( query == MODBUS_REGQ_R_CHECK )
    {
        return 1;
    } 

	//Read
	if ( (query == MODBUS_REGQ_R) && ( datatype == MODBUS_HOLDING_REGISTER || datatype == MODBUS_INPUT_REGISTER))
	{
        return Modbus__ReadRegister(index);
  	}

	 // writeacc determines if holding register can be written
	if ( query == MODBUS_REGQ_W_CHECK )
	{
        return Modbus__IsRegisterWriteAllowed(index);
	} 

    //Write
    if ( (query == MODBUS_REGQ_W) && (datatype == MODBUS_HOLDING_REGISTER || datatype == MODBUS_INPUT_REGISTER))
    {
    	Modbus__WriteRegister(index,value);
    }

	return 0;
}

void Modbus__Feed(uint8_t Data)
{
   		if(ModbusPacketActive == false)
		{
			ModbusPacketActive = true;
			ModbusDataIndex = 0;
        }

		ModbusPacketIn[ModbusDataIndex] = Data;

		if(ModbusDataIndex<MAX_MODBUS_PACKET_SIZE)
        {
			ModbusDataIndex++;
        }
}