#include "ModbusMap.h"

#ifndef MODBUS_H
#define MODBUS_H


void Modbus__IO_Init();

uint32_t Modbus_IO__GetRxActivity();

uint16_t Modbus__IsRegisterWriteAllowed(uint16_t Index);

void  Modbus__WriteRegister(uint16_t Index,uint16_t Value);

uint16_t Modbus__ReadRegister(uint16_t Index);

//returns -1 if when dirty index cannot found.
//IndexIn is the start point for the search.
int32_t  Modbus__GetNextDirty(uint16_t IndexIn);

uint32_t Modbus__GetNumberDirtyRegisters();

void  Modbus__MarkRegisterDirty(uint16_t Index);

void  Modbus__MarkRegisterRangeDirty(uint16_t Index, uint16_t Length);

void  Modbus__MarkRegisterClean(uint16_t Index);

void  Modbus__MarkRegisterRangeClean(uint16_t Index, uint16_t Length);

int32_t  Modbus__GetNextDirtyRange(uint16_t *Start, 
                                   uint16_t *Length
                                   );


extern uint8_t ModbusDeviceID_Storage;
extern uint16_t ModbusBaud_Storage;
extern uint8_t ModbusTestWrite_Storage;


#endif
