
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "ModbusMap.h"
#include "Modbus.h"
#include <sys/reboot.h>
#include <cJSON.h>
#include <shell/shell.h>
#include "FIRMWARE_VERSION.h"
#include <cJSON.h>
#include "System.h"
#include "Cell.h"
#include "rt_config.h"
#include "epoch.h"
#include "DeviceProvision.h"
#include "krusty.h"
#include "bridge_modbus.h"


uint8_t ModbusDeviceID_Storage = 200;
uint16_t ModbusBaud_Storage = 192;
uint8_t ModbusTestWrite_Storage = 0;

RT_CONFIG_ITEM(ModbusDeviceID,"ModbusDeviceID",&ModbusDeviceID_Storage,RT_CONFIG_DATA_TYPE_UINT8,sizeof(ModbusDeviceID_Storage),"1","247","200");
RT_CONFIG_ITEM(ModbusBaud,"Baudrate / 100",&ModbusBaud_Storage,RT_CONFIG_DATA_TYPE_UINT16,sizeof(ModbusBaud_Storage),"96","1152","192");
RT_CONFIG_ITEM(ModbusTestWrite,"Enable test write",&ModbusTestWrite_Storage,RT_CONFIG_DATA_TYPE_UINT8,sizeof(ModbusTestWrite_Storage),"0","1","0");

volatile uint8_t RegisterDirty[MB_NUM_APP_REGISTERS];
volatile uint16_t Registers[MB_NUM_APP_REGISTERS];
volatile uint16_t ConfigKey = 0;

typedef enum 
{

    CloudState__WaitingForCell = 1000,
    CloudState__FetchingTime,
    CloudState__Connecting = 1003,
    CloudState__Connected = 1004,
    CloudState__Error = 1005,
    CloudState__NotProvisioned = 1006,
    CloudState__Transmitting = 1007
     
} MB_CloudState_t;

typedef enum 
{

    CellularState__Init = 1000,
    CellularState__Disconnected,
    CellularState__Registering,
    CellularState__Connecting,
    CellularState__Connected,
    CellularState__Error,
    CellularState__Denied,
    CellularState__PowerDown,
    CellularState__Airplane,
    CellularState__ConnectedEmergency,
    CellularState__Unknown,
    CellularState__UICC_Fail,
        
    
} MB_CellularState_t;


MB_CloudState_t    MB_CloudState = CloudState__WaitingForCell;
MB_CellularState_t MB_CellularState = CellularState__Init;



uint16_t GetMB_CellularState()
{
    Cell_ConnectionState_t CellConnectionState;

    if(Cell_GetConnectionState(&CellConnectionState))
    {

		switch(CellConnectionState.NetworkStatus)
		{
			default:								 return CellularState__Unknown;break;
			case LTE_LC_NW_REG_NOT_REGISTERED:		 return CellularState__Registering ;break;
			case LTE_LC_NW_REG_REGISTERED_HOME:		 return CellularState__Connected;break;
			case LTE_LC_NW_REG_SEARCHING:			 return CellularState__Connecting;break;
			case LTE_LC_NW_REG_REGISTRATION_DENIED:	 return CellularState__Denied;break;
			case LTE_LC_NW_REG_UNKNOWN:			     return CellularState__Unknown;break;
			case LTE_LC_NW_REG_REGISTERED_ROAMING:   return CellularState__Connected;break;
			case LTE_LC_NW_REG_REGISTERED_EMERGENCY: return CellularState__ConnectedEmergency;break;
			case LTE_LC_NW_REG_UICC_FAIL:			 return CellularState__UICC_Fail;break;
		}

    }
    else
    {
        return CellularState__Disconnected;
    }
}


void  Modbus__MarkRegisterDirty(uint16_t Index)
{
     if(Index<MB_NUM_APP_REGISTERS)
     {
        RegisterDirty[Index] = true;
     }
}

void  Modbus__MarkRegisterRangeDirty(uint16_t Index, uint16_t Length)
{
     for(int i=Index;i<Index+Length;i++)
     {
         Modbus__MarkRegisterDirty(i);
     }
}

void  Modbus__MarkRegisterClean(uint16_t Index)
{
    if(Index<MB_NUM_APP_REGISTERS)
    {
        RegisterDirty[Index] = false;
    }
}

void  Modbus__MarkRegisterRangeClean(uint16_t Index, uint16_t Length)
{
     for(int i=Index;i<Index+Length;i++)
     {
         Modbus__MarkRegisterClean((uint16_t)i);
     }
}

bool  Modbus__RegisterIsDirty(uint16_t Index)
{
    if(Index<MB_NUM_APP_REGISTERS)
    {
         return (bool)RegisterDirty[Index];
    }
   
   return false;
}

//returns negative if no dirty values found
int32_t  Modbus__GetNextDirty(uint16_t IndexIn)
{
    while(IndexIn < MB_NUM_APP_REGISTERS)
    {
        if(RegisterDirty[IndexIn])
            return IndexIn;

        IndexIn++;   
    }

return -1;
    
}

//Returns -1 when nothing is left to search
//Feed in pointers to the Start address and the resolved length
int32_t  Modbus__GetNextDirtyRange(uint16_t *Start, 
                                   uint16_t *Length
                                   )
{


    int32_t S = -1;
    
    int32_t TempLength =0;

    S = Modbus__GetNextDirty(*Start);

    if(S >= 0)
    {
        TempLength = 1;

        while(Modbus__RegisterIsDirty(S + TempLength))
        {
            TempLength++;
        }

        *Start = (uint16_t)S;
        *Length = (uint16_t)TempLength;

    }
    
    return S;
   
}



uint32_t Modbus__GetNumberDirtyRegisters()
{
    uint32_t NumDirty = 0;

    for(uint32_t i=0;i<MB_NUM_APP_REGISTERS;i++)
    {
        if(RegisterDirty[i])
        {
            NumDirty++;
        }
    }

    return NumDirty;
}

uint16_t  Modbus__IsRegisterWriteAllowed(uint16_t Index)
{
    if((Index>=APPLICATION_DATA_BASE) && (Index<(APPLICATION_DATA_BASE+MB_NUM_APP_REGISTERS)))
    {
         return 1;
	}
    else
    {
        switch(Index)
        {
            default:
                return 0;
            break; 
       
            case MB_REG__CONFIG__KEY:
            case MB_REG__CONFIG__SLAVE_ADDRESS:
            case MB_REG__CONFIG__BAUD:
            case MB_REG__CONFIG__SYNC_WINDOW:
            case MB_REG__STAT_SYNC_NOW:
                    return 1;
             break;

        }
    }

    return 0;
}

void  Modbus__WriteRegister(uint16_t Index,uint16_t Value)
{
    if((Index>=APPLICATION_DATA_BASE) && (Index<(APPLICATION_DATA_BASE+MB_NUM_APP_REGISTERS)))
    {
        uint16_t RegIndex = Index - APPLICATION_DATA_BASE;
        Registers[RegIndex] = Value;
        RegisterDirty[RegIndex] = true;
    }
    else
    {
        switch(Index)
        {
            case MB_REG__CONFIG__KEY:

                ConfigKey = Value;

                if (ConfigKey == 0xDEAD)
                {
                     sys_reboot(SYS_REBOOT_COLD);
                }

            break;

            case MB_REG__CONFIG__SLAVE_ADDRESS:

                if(ConfigKey == 0xBEEF)
                {
                    ModbusDeviceID_Storage = Value;
                   
                    rt_config_export();
                }

            break;


            case MB_REG__CONFIG__BAUD:

                if(ConfigKey == 0xBEEF)
                {
                    if(    Value == 96
                        || Value == 192
                        || Value == 384
                        || Value == 576
                        || Value == 1152)
                    {
                       ModbusBaud_Storage = Value;
    
                        rt_config_export();
                   
                    }
                }

            break;

            case MB_REG__CONFIG__SYNC_WINDOW:
                
                if(ConfigKey == 0xBEEF)
                {
                    BridgeReportRateSeconds = Value;
                    rt_config_export();
                }

            break;


             case MB_REG__STAT_SYNC_NOW:

                if(Value == 0xABCD)
                {
                    Bridge_SyncNow();
                }
                
             break;

        }
    }

}

uint16_t Modbus__ReadRegister(uint16_t Index)
{
    if((Index>=APPLICATION_DATA_BASE) && (Index<(APPLICATION_DATA_BASE+MB_NUM_APP_REGISTERS)))
    {
         return Registers[Index-APPLICATION_DATA_BASE];
    }
    else
    {
        switch(Index)
        {
                default:
                    return 0;
                break;

                case MB_REG__CONFIG__SENSOR_ID:
                    return 0xF000;
                break;  
                case MB_REG__CONFIG__FIRMWARE_VERSION:
                    return FIRMWARE_VERSION_CODE;
                break;  
                case MB_REG__CONFIG__KEY:
                    return ConfigKey;
                break;  
                case MB_REG__CONFIG__SLAVE_ADDRESS:
                    return ModbusDeviceID_Storage;
                break;  
                case MB_REG__CONFIG__BAUD:
                    return ModbusBaud_Storage;
                break;  
                case MB_REG__CONFIG__SYNC_WINDOW:
                    return BridgeReportRateSeconds;
                break;  

                case MB_REG__STAT_CELLULAR_STATE:
                    return  GetMB_CellularState();
                break;  
            
                case MB_REG__STAT_CELLULAR_RSSI:
                    return ((int16_t)Cell_GetLastPostedRSSI());
                break;  


                case MB_REG__STAT_CLOUD_STATE:  

                    if(!Provision__IsGood())
                    {
                        MB_CloudState =  CloudState__NotProvisioned;
                    }
                    else if(!Cell_IsConnected())
                    {
                        MB_CloudState = CloudState__WaitingForCell;
                    }
                    else if(!Epoch__Get32())
                    {
                        MB_CloudState = CloudState__FetchingTime;
                    }
                    else if(krusty__is_transmitting())
                    {
                        MB_CloudState =CloudState__Transmitting;
                    }
                    else
                    {
                        MB_CloudState = CloudState__Connected;
                    }

                    return MB_CloudState;   
                break;  

                case MB_REG__STAT_TIME_3:  return ((Epoch__Get64()>>48)&0xFFFF); break;
                case MB_REG__STAT_TIME_2:  return ((Epoch__Get64()>>32)&0xFFFF); break;
                case MB_REG__STAT_TIME_1:  return ((Epoch__Get64()>>16)&0xFFFF); break;
                case MB_REG__STAT_TIME_0:  return ((Epoch__Get64()>>0)&0xFFFF); break;

                case MB_REG__STAT_LAST_SYNC_3:  return ((Bridge_LastSync()>>48)&0xFFFF); break;
                case MB_REG__STAT_LAST_SYNC_2:  return ((Bridge_LastSync()>>32)&0xFFFF); break;
                case MB_REG__STAT_LAST_SYNC_1:  return ((Bridge_LastSync()>>16)&0xFFFF); break;
                case MB_REG__STAT_LAST_SYNC_0:  return ((Bridge_LastSync()>>0)&0xFFFF); break;
                case MB_REG__STAT_SYNC_COUNT_1: return ((Bridge_SyncCount()>>16)&0xFFFF); break;
                case MB_REG__STAT_SYNC_COUNT_0: return ((Bridge_SyncCount()>>0)&0xFFFF); break;

        }   
    } 

return 0;  
}

void OutputMB_Result(cJSON *Result, char * String, int32_t Address, int32_t Value, char *Operation)
{
        cJSON_AddStringToObject(Result,"ObjID","Shell");
        cJSON_AddStringToObject(Result,"Result",String);
        cJSON_AddStringToObject(Result,"Operation",Operation);
        cJSON_AddNumberToObject(Result,"Address",Address);
        cJSON_AddNumberToObject(Result,"Value",Value);
}

int32_t GetValue(char * Input, int32_t *Output)
{
     
    char * Format;
    
    if(Input[0] == '0' && ((Input[1]|1<<6) == 'x'))
    {
        Format = "%x";
    }
    else
    {
        Format = "%d";
    }
    
    return sscanf(Input,Format,Output);
}

int32_t __mbregr(cJSON *Result,int32_t argc, char **argv)
{
        char * Op = "Read";
     
        if(argc == 2)
        {

          int32_t Address =0;
          int32_t Value = 0;

          if(GetValue(argv[1], &Address) == 0)
          {
               OutputMB_Result(Result, "Bad Address",-1,-1,argv[1]);
          }
          else
          {
               if(Address >= 0  
                  && Address < 65536)
                  {
                     
                    Value = Modbus__ReadRegister(Address);
                             
                    OutputMB_Result(Result, "OK",Address,Value,Op);
                     
                  }
                  else
                  {
                    OutputMB_Result(Result, "Arguments Out Of Range",Address,-1,Op);
                  }
          }
        }
        else
        {
               OutputMB_Result(Result, "Bad Number of Arguments",-1,-1,Op);
        }

       
        return 0;

}

int mbregr__shell_handler(const struct shell *shell,int32_t argc, char **argv)
{
   cJSON *Result = cJSON_CreateObject();
   
   __mbregr(Result, argc, argv);
   
   char *string =  string = cJSON_Print(Result);

   shell_print(shell,"\x02%s\x03",string);

   cJSON_free(string);
   
   cJSON_Delete(Result);
  
   return 0;
}

int mbregr__krusty_handler(cJSON *Result,int32_t argc, char **argv)
{
   __mbregr(Result, argc, argv);

   return 0;
}

SHELL_CMD_REGISTER(mbregr, NULL, "reads a modbus register", mbregr__shell_handler);

KRUSTY_CMD(mbregr,mbregr__krusty_handler);


int32_t __mbregw(cJSON *Result,int32_t argc, char **argv)
{
    
     char * Op = "Write";

        if(argc == 3)
        {

          int32_t Address;
          int32_t Value;

          if(GetValue(argv[1], &Address) == 0)
          {
               OutputMB_Result(Result, "Bad Address",-1,-1,argv[1]);
          }
          else if(GetValue(argv[2], &Value) == 0)
          {
               OutputMB_Result(Result, "Bad Value",-1,-1,argv[1]);
          }
          else
          {
              

               if(Address >= 0  
                  && Address < 65536
                  && Value < 65536 
                  && Value >= 0 )
                  {
                      
                 
                         if(Modbus__IsRegisterWriteAllowed(Address))
                         {
                              Modbus__WriteRegister(Address,Value);

                              Value = Modbus__ReadRegister(Address);
                              
                              OutputMB_Result(Result, "OK",Address,Value,Op);
                         }
                         else
                         {
                              OutputMB_Result(Result, "Write Not Allowed",Address,Value,Op);
                         }

                  }
                  else
                  {
                    OutputMB_Result(Result, "Arguments Out Of Range",Address,Value,Op);
                  }
          }
        }
        else
        {
               OutputMB_Result(Result, "Bad Number of Arguments",-1,-1,Op);
        }

        return 0;
}

int mbregw__shell_handler(const struct shell *shell,int32_t argc, char **argv)
{
   cJSON *Result = cJSON_CreateObject();
   
   __mbregw(Result, argc, argv);
   
   char *string =  string = cJSON_Print(Result);

   shell_print(shell,"\x02%s\x03",string);

   cJSON_free(string);
   
   cJSON_Delete(Result);
     
   return 0;
}

int mbregw__krusty_handler(cJSON *Result,int32_t argc, char **argv)
{
   __mbregw(Result, argc, argv);
 
    return 0;
}

SHELL_CMD_REGISTER(mbregw, NULL, "writes a modbus register", mbregw__shell_handler);

KRUSTY_CMD(mbregw,mbregw__krusty_handler);
